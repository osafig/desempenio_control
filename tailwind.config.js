/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
     "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: ["./resources/views/*.blade.php",],
    colors: {
      'custom-red': 'rgb(122, 10, 44)',
      'custom-red-hover': 'rgb(100, 8, 36)' // Nombre personalizado para el color
    },
  },
  plugins: [
      require('daisyui'),
      require('flowbite/plugin'),
  ],
  darkMode: 'class',
}

