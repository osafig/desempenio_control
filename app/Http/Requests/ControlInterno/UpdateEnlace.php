<?php

namespace App\Http\Requests\ControlInterno;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEnlace extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            "nombre"                => "required",
            "primer_apellido"       => "required",
            // "email"                 => "email",
            "puesto"                => "required",
            "correo_institucional"  => "required|email"
        ];
        return $data;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'nombre.required'               => 'El Nombre es requerido.',
            'primer_apellido.required'      => 'El Primer apellido es requerido.',
            // 'email.email'                   => 'El Correo electrónico no es válido.',
            // 'email.unique'                  => 'El Correo electrónico ya se encuentra registrado',
            'puesto.required'               => 'El Cargo que desempeña es requerido.',
            'correo_institucional.required' => 'El Correo institucional es requerido.',
            'correo_institucional.email'    => 'El Correo institucional no es válido.'
        ];
        return $messages;
    }
}
