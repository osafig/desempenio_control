<?php


namespace App\Http\Repositories\Declaracion;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DeclaracionRepository
{
    protected $db_declaracion;
    protected $db_main;

    public function __construct()
    {
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main = DB::connection('main');
    }

    /**
     * Obtiene el ejercicio fiscal a declarar
     */
    public function getEjercicioDeclaracion()
    {
        $ejercicioActual = $this->db_main
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', true)
            ->first();
        $ejercicioAnterior = (int)$ejercicioActual->EjercicioID;
        return $this->db_main
            ->table('osaf_ejercicios_cat')
            ->where('EjercicioID', '=', $ejercicioAnterior)
            ->first();
    }

    /**
     * Obtener el tipo de declaración a presentar
     */
    public function getTipoDeclaracion($declaracionId)
    {
        $declaracion = $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales')
            ->where('DeclaracionID', '=', $declaracionId)
            ->where('Eliminado', '=', 0)
            ->first();
        if (is_null($declaracion)) {
            return false;
        } else {
            $tipoDeclaracion = $this->db_declaracion
                ->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID', '=', $declaracion->TipoDeclaracionID)
                ->first();
            if (is_null($tipoDeclaracion)) {
                return false;
            } else {
                return $tipoDeclaracion;
            }
        }
    }

    /**
     * Obtener los módulos a declarar
     */
    public function getSeccionesADeclarar($declaracion_id)
    {
        return $this->db_declaracion->select('call sp_estadoDeclaracion(?)', [$declaracion_id]);
    }

    /**
     * Obtener a los usuarios a declarar
     */
    public function getUsuariosDeclaracion($ejercicioID)
    {
        return $this->db_declaracion->select('call sp_declarantesPatrimoniales(?)', [$ejercicioID]);
    }

    /**
     * Obtener a los usuarios a declarar conflicto de intereses
     */
    public function getUsuariosConflictoIntereses($ejercicioID)
    {
        return $this->db_declaracion->select('call sp_declarantesConflictoIntereses(?)', [$ejercicioID]);
    }

    public function getTiposDeclaracion()
    {
        return $this->db_declaracion->table('oic_tipo_declaracion_cat')->get();
    }

    /**
     * Obtener declaración patrimonial por usuario
     */
    public function getDeclaracionByUser($user, $ejercicio, $tipoDeclaracion)
    {
        return $this->db_declaracion->table('oic_declaraciones_patrimoniales')
            ->where('UsuarioID', '=', $user)
            ->where('EjercicioID', '=', $ejercicio)
            ->where('Eliminado', '=', 0)
            ->where('TipoDeclaracionID', '=', $tipoDeclaracion)
            ->first();
    }

    /**
     * Obtener datos declaracion
     */
    public function getDeclaracionData($declaracionID)
    {
        return $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales')
            ->select(
                'DeclaracionID',
                'DeclaracionFecha',
                'ObservacionesAclaraciones',
                'RutaArchivoSat',
                'Finalizado',
                'DeclaracionLeida',
                'Hash',
                'DireccionID',
                'RutaAcuse',
                'RutaDeclaracionCompleta',
                'CopiaAnterior',
                'TipoDeclaracionID',
                'EjercicioID')
            ->where('DeclaracionID', '=', $declaracionID)
            ->where('Eliminado', '=', 0)
            ->first();
    }

    /**
     * Obtener datos de declaraciones terminadas
     */
    public function getDeclaracionDataByUserYearType($usuario_id, $ejercicio_id, $tipo_id)
    {
        return $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales')
            ->select(
                'DeclaracionID',
                'DeclaracionFecha',
                'ObservacionesAclaraciones',
                'RutaArchivoSat',
                'Finalizado',
                'DeclaracionLeida',
                'Hash',
                'DireccionID',
                'RutaAcuse',
                'RutaDeclaracionCompleta',
                'CopiaAnterior',
                'TipoDeclaracionID',
                'EjercicioID')
            ->where('UsuarioID', '=', $usuario_id)
            ->where('EjercicioID', '=', $ejercicio_id)
            ->where('TipoDeclaracionID', '=', $tipo_id)
            ->where('Eliminado', '=', 0)
            ->where('Finalizado', '=', 1)
            ->first();
    }

    /**
     * Copiar datos de la declaración
     */
    public function copiarDatosDeclaracion($declaracion_referencia_id, $declaracion_id, $user_id)
    {
        $formato = $this->getFormato(); // Retorna si el formato es completo
        $declaracion_referencia = $this->getDeclaracionData($declaracion_referencia_id);
        $personales = $this->getDatosPersonalesMain($user_id);
        //simplificado
        if($formato == false) {
            // Datos Generales
            $this->registrarCopiaDatosPersonales($personales, $declaracion_id);
            // Dirección
            if (!is_null($declaracion_referencia->DireccionID)) {
                $direccion = $this->getDireccion($declaracion_referencia->DireccionID);
                $this->registrarCopiaDireccion($direccion, $declaracion_id);
            }
            // Datos curriculares
            $curriculares = $this->getDatosCurriculares($declaracion_referencia_id);
            if (!is_null($curriculares)) {
                $this->registrarCopiaDatosCurriculares($curriculares, $declaracion_id);
            }
            // Datos empleo/cargo
            $cargo = $this->getDatosCargo($declaracion_referencia_id);
            if (!is_null($cargo))
            {
                $this->registrarCopiaDatosCargo($cargo, $declaracion_id);
            }
            // completo
        } else {
            // Datos Generales
            $this->registrarCopiaDatosPersonales($personales, $declaracion_id);
            // Dirección
            if (!is_null($declaracion_referencia->DireccionID)) {
                $direccion = $this->getDireccion($declaracion_referencia->DireccionID);
                $this->registrarCopiaDireccion($direccion, $declaracion_id);
            }
            // Datos curriculares
            $curriculares = $this->getDatosCurriculares($declaracion_referencia_id);
            if (!is_null($curriculares)) {
                $this->registrarCopiaDatosCurriculares($curriculares, $declaracion_id);
            }
            // Datos empleo/cargo
            $cargo = $this->getDatosCargo($declaracion_referencia_id);
            if (!is_null($cargo))
            {
                $this->registrarCopiaDatosCargo($cargo, $declaracion_id);
            }
            // Datos bienes inmuebles
            /*$inmuebles = $this->getBienesInmueble($declaracion_referencia_id);
            if(count($inmuebles) > 0) {
                $this->registrarCopiaBienesInmuebles($declaracion_id, $inmuebles);
            }*/
            // Datos vehiculos
            /*$vehiculos = $this->getVehiculos($declaracion_referencia_id);
            if(count($vehiculos) > 0) {
                $this->registrarCopiaVehiculos($declaracion_id, $vehiculos);
            }*/
            // Datos bienes muebles
            /*$muebles = $this->getBienesMueble($declaracion_referencia_id);
            if (count($muebles) > 0) {
                $this->registrarCopiaBienesMuebles($declaracion_id, $muebles);
            }*/
            // Datos inversiones
            /*$inversiones = $this->getInversiones($user_id, $declaracion_referencia_id);
            if(count($inversiones) > 0) {
                $this->registrarCopiaInversiones($declaracion_id, $inversiones);
            }*/
            // Datos adeudos
            /*$adeudos = $this->getAdeudos($declaracion_referencia_id);
            if (count($adeudos)) {
                $this->registrarCopiaAdeudos($declaracion_id, $adeudos);
            }*/
            // Prestamos
            /*$prestamos = $this->getPrestamos($declaracion_referencia_id);
            if (count($prestamos) > 0) {
                $this->registrarCopiaPrestamos($declaracion_id, $prestamos);
            }*/
        }
    }

    /**
     * obtener el formato que se contestara en la declaración
     */
    public function getFormato()
    {
        $formato = $this->db_main->table('osaf_usuarios as ou')
            ->join('osaf_puestos_cat as opc', 'ou.PuestoID', '=', 'opc.PuestoID')
            ->where('ou.UsuarioID', '=', request()->user()->UsuarioID)
            ->select('opc.*')
            ->first();
        return $formato->Completo;
    }

    /**
     * Obtener datos del cargo
     */
    public function getDatosCargo($declaracion_id)
    {
        return $this->db_declaracion->table('oic_cargos')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->where('TipoCargo', '=', 1)
            ->first();
    }

    /**
     * Obtener datos curriculares por id de declaracion
     */
    public function getDatosCurriculares($declaracion_id)
    {
        return $this->db_declaracion->table('oic_nivel_estudios_detalle')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->first();
    }

    /**
     * Obtener dirección por id de dirección
     */
    public function getDireccion($direccion_id)
    {
        return $this->db_declaracion->table('oic_direcciones')
            ->where('DireccionID', '=', $direccion_id)
            ->first();
    }

    /**
     * registrar nuevos datos
     */
    public function registrarCopiaDatosPersonales($data, $declaracion_id)
    {
        $datos_personales = $this->db_declaracion->table('oic_datos_personales')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($datos_personales)) {
            $this->db_declaracion->select('call sp_registrarDatosPersonales(?,?,?,?,?,?,?,?,?,?,?)',[
                $data->UsuarioID,
                $data->Homoclave,
                $data->EmailPersonal,
                $data->TelefonoParticular,
                $data->TelefonoCelular,
                $data->EstadoCivilID,
                $data->RegimenMatrimonial,
                $data->PaisNacimiento,
                $data->Nacionalidad,
                $data->Observaciones,
                $declaracion_id
            ]);
        }
    }

    /**
     * registrar direccion
     */
    public function registrarCopiaDireccion($data, $declaracion_id)
    {
        // Actualizar la direccionID en la declaracion
        $direccion_declaracion = $this->db_declaracion->table('oic_declaraciones_patrimoniales')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (is_object($direccion_declaracion)) {
            $direccion = $this->db_declaracion->select('call sp_nuevaDireccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                $data->EsExtranjero,
                $data->Calle,
                $data->NumeroExterior,
                $data->NumeroInterior,
                $data->Colonia,
                $data->MunicipioID,
                $data->EstadoID,
                $data->CodigoPostal,
                $data->Ciudad,
                $data->EstadoExtranjero,
                $data->Pais,
                $data->Aclaraciones,
                null,
                request()->user()->UsuarioID,
            ]);
            // Actualizar la direccionID en la declaracion
            $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', $declaracion_id)
                ->update([
                    'DireccionID' => $direccion[0]->direccion_id,
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);
            // Actualizar la sección como terminada
            $this->db_declaracion->table('oic_declaracion_estatus_det')
                ->where('DeclaracionID', '=', $declaracion_id)
                ->where('SeccionID', '=', 2)
                ->update([
                    'EstadoActual' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
        }
    }

    /**
     * registrar datos curriculares
     */
    public function registrarCopiaDatosCurriculares($data, $declaracion_id)
    {
        $curriculares = $this->db_declaracion->table('oic_nivel_estudios_detalle')->where('DeclaracionID', '=', $declaracion_id)->first();
        if(!is_object($curriculares)) {
            $this->db_declaracion->select('call sp_registrarDatosCurriculares(?,?,?,?,?,?,?,?,?,?)',[
                $data->NivelEstudioID,
                $data->Institucion,
                $data->Carrera,
                $data->Status,
                $data->DocumentoObtenido,
                $data->FechaDocumento,
                $data->EsExtranjero,
                $data->Observaciones,
                $declaracion_id,
                request()->user()->UsuarioID,
            ]);
        }
    }

    /**
     * registrar datos del cargo
     */
    public function registrarCopiaDatosCargo($data, $declaracion_id)
    {
        $cargo = $this->db_declaracion->table('oic_cargos')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($cargo)) {
            $data = [
                $declaracion_id,
                request()->user()->UsuarioID,
                $data->OrdenGobiernoID,
                $data->AmbitoPublicoID,
                $data->Dependencia,
                $data->AdscripcionID,
                $data->PuestoID,
                $data->Honorarios,
                $data->NivelCargo,
                $data->FuncionPrincipal,
                $data->FechaIngreso,
                $data->Telefono,
                $data->DireccionID,
                $data->Observaciones,
                $data->TipoCargo,
                $data->AreaEspecifica,
                $data->PuestoEspecifico,
                $data->Oculto
            ];
            $this->db_declaracion->select('call sp_registrarCargo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $data);
        }
    }

    /**
     * Registrar copia de bienes inmuebles
     */
    public function registrarCopiaBienesInmuebles($declaracion_id, $inmuebles)
    {
        $inmuebles_declaracion = $this->db_declaracion->table('oic_bienes_inmubles')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($inmuebles_declaracion)) {
            foreach($inmuebles as $inmueble) {
                $this->db_declaracion->select('call sp_registrarBienInmueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                    $declaracion_id,
                    $inmueble->UsuarioID,
                    $inmueble->TipoBienID,
                    $inmueble->OtroTipoBien,
                    $inmueble->Titular,
                    $inmueble->PorcentajePropiedad,
                    $inmueble->SuperficieTerreno,
                    $inmueble->SuperficieConstruida,
                    $inmueble->TipoPersonaTercero,
                    $inmueble->NombreTercero,
                    $inmueble->RFCTercero,
                    $inmueble->FormaOperacionID,
                    $inmueble->FormaPago,
                    $inmueble->TipoPersonaTransmisor,
                    $inmueble->NombreTransmisor,
                    $inmueble->RFCTransmisor,
                    $inmueble->RelacionTransmisor,
                    $inmueble->Valor,
                    $inmueble->TipoMoneda,
                    $inmueble->FechaOperacion,
                    $inmueble->ValorConformeA,
                    $inmueble->FolioReal,
                    $inmueble->DireccionID,
                    $inmueble->MotivoBaja,
                    $inmueble->Observaciones,
                    null
                ]);
            }
        }
    }

    /**
     * Registrar vehiculos
     */
    public function registrarCopiaVehiculos($declaracion_id, $vehiculos)
    {
        $muebles = $this->db_declaracion->table('oic_bienes_muebles')->where('DeclaracionID', '=', $declaracion_id)->first();
        if(!is_object($muebles)) {
            $this->registrarCopiaMuebles($declaracion_id, $vehiculos);
        }
    }

    /**
     * registrar buenes muebles
     */
    public function registrarCopiaBienesMuebles($declaracion_id, $muebles)
    {
        $muebles_declaracion = $this->db_declaracion->table('oic_bienes_muebles')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($muebles_declaracion)) {
            $this->registrarCopiaMuebles($declaracion_id, $muebles);
        }
    }

    /**
     * Registrar inversiones
     */
    public function registrarCopiaInversiones($declaracion_id, $inversiones)
    {
        $inversiones_declaracion = $this->db_declaracion->table('oic_recursos_financieros')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($inversiones_declaracion)) {
            foreach ($inversiones as $inversion) {
                $this->db_declaracion->select('CALL sp_registrarInversion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                    $declaracion_id,
                    $inversion->TipoInversionID,
                    $inversion->ActivoID,
                    $inversion->Titular,
                    $inversion->TipoPersonaTercero,
                    $inversion->NombreTercero,
                    $inversion->RFCTercero,
                    $inversion->NumeroContrato,
                    $inversion->EsExtranjero,
                    $inversion->Ubicacion,
                    $inversion->Institucion,
                    $inversion->RFCIntitucion,
                    $inversion->Saldo,
                    $inversion->Moneda,
                    $inversion->Aclaraciones,
                    null,
                    $inversion->UsuarioID,
                ]);
            }
        }
    }

    /**
     * Registrar bienes muebles
     */
    public function registrarCopiaMuebles($declaracion_id, $data)
    {
        if (count($data) > 0) {
            foreach($data as $d) {
                $this->db_declaracion->select('call sp_registrarBienMueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                    $declaracion_id,
                    $d->TipoBienID,
                    $d->Titular,
                    $d->TipoPersonaTransmisor,
                    $d->NombreRazonSocial,
                    $d->RFCTRansmisor,
                    $d->RelacionTransmisor,
                    $d->NombreTercero,
                    $d->TipoPersonaTercero,
                    $d->RFCTercero,
                    $d->FormaAdquisicionID,
                    $d->FormaPago,
                    $d->Valor,
                    $d->TipoMoneda,
                    $d->FechaOperacion,
                    $d->Caracteristicas,
                    $d->VehiculoMarca,
                    $d->VehiculoModelo,
                    $d->VehiculoNumeroSerie,
                    $d->EnExtranjero,
                    $d->VehiculoAnio,
                    $d->MotivoBaja,
                    $d->DescripcionBien,
                    $d->Ubicacion,
                    $d->OtroTipoBien,
                    $d->BienMuebleID,
                    $d->Observaciones,
                    $d->UsuarioID,
                ]);
            }
        }
    }

    /**
     * Registrar adeudos
     */
    public function registrarCopiaAdeudos($declaracion_id, $adeudos)
    {
        $adeudos_declaracion = $this->db_declaracion->table('oic_adeudos')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($adeudos_declaracion)) {
            foreach($adeudos as $adeudo) {
                $this->db_declaracion->select('CALL sp_registrarAdeudo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                    $adeudo->UsuarioID,
                    $declaracion_id,
                    $adeudo->Titular,
                    $adeudo->TipoAdeudoID,
                    $adeudo->NoCuenta,
                    $adeudo->FechaOtorgamiento,
                    $adeudo->MontoOriginal,
                    $adeudo->Moneda,
                    $adeudo->SaldoInsoluto,
                    $adeudo->NombreTercero,
                    $adeudo->TipoPersonaTercero,
                    $adeudo->RFCTercero,
                    $adeudo->Ubicacion,
                    $adeudo->NombreRazonSocial,
                    $adeudo->TipoPersonaOtorgante,
                    $adeudo->EsExtranjero,
                    $adeudo->Aclaraciones,
                    null,
                    $adeudo->RFCOtorgante,
                    $adeudo->OtroTipoAdeudo
                ]);
            }
        }
    }

    /**
     * Registrar prestamo
     */
    public function registrarCopiaPrestamos($declaracion_id, $prestamos)
    {
        $prestamos_declaracion = $this->db_declaracion->table('oic_prestamo_comodato')->where('DeclaracionID', '=', $declaracion_id)->first();
        if (!is_object($prestamos_declaracion)) {
            foreach($prestamos as $prestamo) {
                $this->db_declaracion->select('CALL sp_registrarPrestamo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                    $prestamo->UsuarioID,
                    $declaracion_id,
                    $prestamo->TipoDeBien,
                    $prestamo->BienMuebleID,
                    $prestamo->BienInmuebleID,
                    $prestamo->DireccionID,
                    $prestamo->Marca,
                    $prestamo->Modelo,
                    $prestamo->Anio,
                    $prestamo->NoSerie,
                    $prestamo->RegistroEnExtranjero,
                    $prestamo->Pais,
                    $prestamo->EntidadFederativa,
                    $prestamo->Titular,
                    $prestamo->TipoPersonaTitular,
                    $prestamo->RFCTitular,
                    $prestamo->Observaciones,
                    null,
                    $prestamo->OtroBien,
                    $prestamo->Vinculo
                ]);
            }
        }
    }

    /**
     * Obtener datos personales
     */
    public function getDatosPersonalesMain($user){
        $datos = $this->db_declaracion->select('call sp_obtenerDatosPersonales(?)', [$user]);
        return is_null($datos) ? false : $datos[0];
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatalogoEstadoCivil(){
        $catalogo = $this->db_declaracion
            ->table('oic_estado_civil_cat')
            ->select('EstadoCivilID','EstadoCivil')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Guardar datos personales
     */
    public function registrarDatosPersonales($info){
        $result = $this->db_declaracion->select('call sp_registrarDatosPersonales(?,?,?,?,?,?,?,?,?,?,?)',[
            $info["UsuarioID"],
            $info["Homoclave"],
            $info["EmailPersonal"],
            $info["TelefonoParticular"],
            $info["TelefonoCelular"],
            $info["EstadoCivilID"],
            $info["RegimenMatrimonial"],
            $info["PaisNacimiento"],
            $info["Nacionalidad"],
            $info["Observaciones"],
            $info["declaracion_id"]
        ]);
        $this->db_declaracion->select('call sp_actualizarDatosPersonales(?,?,?,?,?,?,?)', [
            $info["Nombres"],
            $info["PrimerApellido"],
            $info["SegundoApellido"],
            $info["Email"],
            $info["RFC"].$info['Homoclave'],
            $info["CURP"],
            $info["UsuarioID"]
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Registrar la direccion proporcionada
     */
    public function registrarDireccion($request, $usuario)
    {
        if ($request["pais"] == null || $request['residencia'] == 0) {
            $pais = 'México';
            $ciudad = '';
            $municipio_id = $request["municipio_id"];
            $estado_id = $request["estado_id"];
            $estado_extranjero = null;
            $colonia = $request["colonia"];
        } else {
            $pais = $request["pais"];
            $ciudad = $request["ciudad"];
            $municipio_id = null;
            $estado_id = null;
            $estado_extranjero = $request["estado_extranjero"];
            $colonia = null;
        }
        $result = $this->db_declaracion->select('call sp_nuevaDireccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request["residencia"],
            $request["calle"],
            $request["numero_exterior"],
            $request["numero_interior"],
            $colonia,
            $municipio_id,
            $estado_id,
            $request["codigo_postal"],
            $ciudad,
            $estado_extranjero,
            $pais,
            $request["observaciones"],
            $request["direccion_id"],
            $usuario,
        ]);
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }
    /*
     * Cargar catalogo de niveles de estudio
     */
    public function getCatalogoNivelesEstudio(){
        $catalogo = $this->db_declaracion
            ->table('oic_nivel_estudios_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        }else{
            return $catalogo;
        }
    }

    /**
     * Guardar datos curriculares
     */
    public function registrarDatosCurriculares($info,$user){
        $result = $this->db_declaracion->select('call sp_registrarDatosCurriculares(?,?,?,?,?,?,?,?,?,?)',[
            $info["nivel_estudio_id"],
            $info["institucion"],
            $info["carrera"],
            $info["status"],
            $info["documento"],
            $info["fecha_documento"],
            $info["es_extranjero"],
            $info["observaciones"],
            $info["declaracion_id"],
            $user
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Obtener estados
     */
    public function getEstados()
    {
        return $this->db_declaracion->table('oic_estados_cat')->get();
    }

    /**
     * Obtener municipios
     */
    public function getMunicipios($estado_id)
    {
        return $this->db_declaracion->table('oic_municipios_cat')
            ->where('EstadoID', '=', $estado_id)
            ->get();
    }

    public function getAreasAdscripcion()
    {
        return $this->db_declaracion->table('oic_adscripciones_cat')
        ->where('Visible','=',1)
        ->get();
    }

    public function getNivelesCargo()
    {
        return $this->db_declaracion->table('oic_nivel_empleo_cat')->get();
    }

    /**
     * Obtener ordenes de gobierno
     */
    public function getOrdenesGobierno() {
        return $this->db_declaracion->table('oic_orden_gobierno')->get();
    }

    /**
     * Obtener ambitos publicos
     */
    public function getAmbitosPublicos() {
        return $this->db_declaracion->table('oic_ambito_cat')
        ->where('Activo', '=', 1)
        ->get();
    }

    /**
     * Obtener direccion del osafig
     */
    public function getDireccionOsafig()
    {
        $direcciones_oficiales = $this->db_declaracion->table('oic_direcciones')->where('DireccionOficial', '=', true)->get();
        $direcciones_id = Arr::pluck($direcciones_oficiales, 'DireccionID');
        $direcciones = $this->db_declaracion->table('oic_direcciones as od')
            ->join('oic_estados_cat as oec', 'od.EstadoID', '=', 'oec.EstadoID')
            ->join('oic_municipios_cat as omc', 'od.MunicipioID', '=', 'omc.MunicipioID')
            ->whereIn('od.DireccionID', $direcciones_id)
            ->select('od.*', 'oec.Nombre as Estado', 'omc.Nombre as Municipio')
            ->get();
        return $direcciones;
    }

    /**
     * Guardar datos del cargo
     */
    public function registrarDatosCargo($request, $usuario)
    {
        $otro_empleo = ($request['otro_empleo'] == 0) ? 1 : 2;
        $telefono = (is_null($request['telefono'])) ? '' : $request['telefono'];
        if($otro_empleo == 1) {
            $data = [
                $request['declaracion_id'],
                $usuario,
                $request['orden'],
                $request['ambito'],
                $request['ente'],
                $request['area'],
                $request['puesto_id'],
                $request['honorarios'],
                $request['nivel'],
                $request['funciones'],
                $request['fecha'],
                $telefono,
                $request['direccion_id'],
                $request['aclaraciones'],
                $otro_empleo,
                $request['area_especifica'],
                $request['puesto_especifico'],
                $request['oculto']
            ];
        } else {
            Log::info("Otro cargo");
            $data = [
                $request['declaracion_id'],
                $usuario,
                $request['orden'],
                $request['ambito'],
                $request['ente'],
                null,
                null,
                $request['honorarios'],
                $request['nivel'],
                $request['funciones'],
                $request['fecha'],
                $telefono,
                $request['direccion_id'],
                $request['aclaraciones'],
                $otro_empleo,
                $request['area_especifica'],
                $request['puesto_especifico'],
                $request['oculto']
            ];
        }
        try {
            $result = $this->db_declaracion->select('call sp_registrarCargo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $data);
        } catch(\Exception $e) {
            Log::error($e->getMessage());
            $result = false;
        }
        return $result;
    }

    /**
     * Obtener catalogo de orden de gobierno
     */
    public function getCatalogoOrdenGobierno(){
        $catalogo = $this->db_declaracion
            ->table('oic_orden_gobierno')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de ambito publico
     */
    public function getCatalogoAmbitoPublico(){
        $catalogo = $this->db_declaracion
            ->table('oic_ambito_cat')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de sectores laborales
     */
    public function getCatalogoSectorLaboral(){
        $catalogo = $this->db_declaracion
            ->table('oic_sector_productivo_cat')
            ->select('SectorID','Sector')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Guardar experiencia laboral
     */
    public function registrarExpLaboral($info, $usuario){
        $sector = $this->db_declaracion
            ->table('oic_sector_productivo_cat')
            ->where('Sector', '=', $info['sector_id'])
            ->first();
        $sector_id = (is_null($sector)) ? 0 : $sector->SectorID;
        $result = $this->db_declaracion->select('call sp_registrarExperienciaLaboral(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
            $usuario,
            $info['ambito_sector'],
            $info['orden_gobierno_id'],
            $info['ambito_id'],
            $info['institucion'],
            $info['rfc_institucion'],
            $info['area'],
            $info['puesto'],
            $info['funcion_principal'],
            $sector_id,
            $info['fecha_ingreso'],
            $info['fecha_egreso'],
            $info['en_extranjero'],
            $info['observacion'],
            $info['experiencia_laboral_id'],
            $info['otro_sector']
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar experiencias laborales
     */
    public function getExperienciasLaborales($usuario){
        $result = $this->db_declaracion
            ->table('oic_experiencia_laboral_detalle')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->limit(5)
            ->orderBy('FechaTermino','desc')
            ->get();
        return (is_null($result)) ? false : $result;
    }

    /**
     * Marcar seccion como terminada
     */
    public function marcarSeccionTerminada($declaracion_id, $seccion)
    {
        return $this->db_declaracion
            ->table('oic_declaracion_estatus_det')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('SeccionID', '=', $seccion)
            ->update([
                'EstadoActual' => true,
                'updated_at'   => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * Marcar seccion como terminada
     */
    public function marcarSeccionComo($declaracion_id, $seccion, $opcion)
    {
        return $this->db_declaracion
            ->table('oic_declaracion_estatus_det')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('SeccionID', '=', $seccion)
            ->update([
                'TerminadoComo' => $opcion,
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * Cargar catalogo de instrumentos financieros
     */
    public function getCatalogoInstrumentos(){
        return $this->db_declaracion->table('oic_tipo_instrumento_cat')->get();
    }

    /**
     * Obtener ingresos de la declaracion
     */
    public function getIngresosDeclaracion($declaracion)
    {
        return $this->db_declaracion->table('oic_ingresos')
            ->where('DeclaracionID', '=', $declaracion)
            ->first();
    }

    /**
     * Obtener ingresos de la declaracion servidor pasado
     */
    public function getIngresosDeclaracionServidorPasado($declaracion)
    {
        return $this->db_declaracion->table('oic_servidor_pasado')
            ->where('DeclaracionID', '=', $declaracion)
            ->where('Oculto', '=', false)
            ->first();
    }

    /**
     * Obtener ingresos de la declaracion servidor pasado ocultos
     */
    public function getIngresosDeclaracionServidorPasadoOcultos($declaracion)
    {
        return $this->db_declaracion->table('oic_servidor_pasado')
            ->where('DeclaracionID', '=', $declaracion)
            ->where('Oculto', '=', true)
            ->get();
    }

    /**
     * Obtener ingresos por actividades industriales
     */
    public function getIngresosActividadesIndustriales($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_actividad_indutrial');
    }

    /**
     * Obtener ingresos por actividades financieras
     */
    public function getIngresosActividadesFinancieras($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_actividad_financiera');
    }

    /**
     * Obtener ingresos por servicios profesionales
     */
    public function getIngresosServiciosProfesionales($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_servicios');
    }

    /**
     * Obtener ingresos por otros ingresos
     */
    public function getIngresosOtros($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_extra');
    }

    public function getBienesEnajenados($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_por_enajenacion');
    }

    /**
     * Consulta el ingreso por tipo
     */
    public function getIngresosTipo($ingreso, $tipo)
    {
        return $this->db_declaracion
            ->table($tipo)
            ->where('IngresoID', '=', $ingreso)
            ->get();
    }

    /**
     * Obtener catalogo de bienes inmuebles
     */
    public function getCatalogoBienesInmueble(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Inmueble')
            ->orWhere('Categoria','=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de titulares
     */
    public function getCatalogoTipoTitulares(){
        $catalogo = $this->db_declaracion
            ->table('oic_titulares_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de vinculos personales
     */
    public function getCatalogoVinculoPersonal(){
        $catalogo = $this->db_declaracion
            ->table('oic_vinculo_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

     /**
     * Obtener catalogo de tipo de inversiones
     */
    public function getCatalogoTipoInversion(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_inversion_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de adquisicion
     */
    public function getCatalogoTipoAdquisicion(){
        $catalogo = $this->db_declaracion
            ->table('oic_forma_operacion_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatalogoVehiculos(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Vehículo')
            ->orWhere('Categoria', '=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Cargar bienes inmuebles
     */
    public function getBienesInmueble($declaracion_id)
    {
        $result = $this->db_declaracion
            ->table('oic_bienes_inmubles')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Cargar vehiculos
     */
    public function getVehiculos($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_bienes_muebles as obm')
            ->select('obm.BienMuebleID', 'obm.DeclaracionID', 'obm.TipoBienID', 'obm.Titular',
                'obm.TipoPersonaTransmisor', 'obm.NombreRazonSocial', 'obm.RFCTRansmisor',
                'obm.RelacionTransmisor', 'obm.NombreTercero', 'obm.TipoPersonaTercero',
                'obm.RFCTercero', 'obm.FormaAdquisicionID', 'obm.FormaPago',
                'obm.Valor', 'obm.TipoMoneda', 'obm.FechaOperacion',
                'obm.Caracteristicas', 'obm.VehiculoMarca', 'obm.VehiculoModelo', 'obm.VehiculoNumeroSerie',
                'obm.VehiculoAnio', 'obm.MotivoBaja', 'obm.DescripcionBien',
                'obm.Ubicacion', 'obm.OtroTipoBien', 'obm.UsuarioID',
                'otbc.Categoria', 'obm.Observaciones', 'obm.EnExtranjero')
            ->join('oic_tipo_bien_cat as otbc','obm.TipoBienID', '=','otbc.TipoBienID')
            ->where('otbc.Categoria', '=', 'Vehiculo')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Obtener catalogo de tipos de bienes mueble
     */
    public function getCatalogoBienesMueble(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Mueble')
            ->orWhere('Categoria', '=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

     /**
     * Obtener catalogo de activos
     */
    public function getCatalogoActivos($tipo){
        $catalogo = $this->db_declaracion
            ->table('oic_activos_financieros')
            ->where('TipoInversionID', '=', $tipo)
             ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de adeudos
     */
    public function getCatalogoTipoAdeudo(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_adeudo_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Cargar bienes mueble
     */
    public function getBienesMueble($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_bienes_muebles as obm')
            ->select('obm.BienMuebleID', 'obm.DeclaracionID', 'obm.TipoBienID', 'obm.Titular',
                'obm.TipoPersonaTransmisor', 'obm.NombreRazonSocial', 'obm.RFCTRansmisor',
                'obm.RelacionTransmisor', 'obm.NombreTercero', 'obm.TipoPersonaTercero',
                'obm.RFCTercero', 'obm.FormaAdquisicionID', 'obm.FormaPago',
                'obm.Valor', 'obm.TipoMoneda', 'obm.FechaOperacion',
                'obm.Caracteristicas', 'obm.VehiculoMarca', 'obm.VehiculoModelo',
                'obm.VehiculoNumeroSerie','obm.VehiculoAnio', 'otbc.Categoria',
                'obm.Ubicacion', 'obm.OtroTipoBien', 'obm.UsuarioID',
                'obm.MotivoBaja', 'obm.Observaciones', 'obm.DescripcionBien', 'obm.EnExtranjero')
            ->join('oic_tipo_bien_cat as otbc','obm.TipoBienID', '=','otbc.TipoBienID')
            ->where('otbc.Categoria', '=', 'Mueble')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

     /**
     * Cargar inversiones
     */
    public function getInversiones($usuario, $declaracion){
        $result = $this->db_declaracion
            ->table('oic_recursos_financieros')
            ->where('UsuarioID', '=', $usuario)
            ->where('DeclaracionID', '=', $declaracion)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar catalogo tipos participante
     */
    public function getCatalogoTipoParticipante(){
        $result = $this->db_declaracion
            ->table('oic_tipo_participante')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }
    /**
     * cargar catalogo de tipo institucion
     */
    public function getCatalogoTipoInstitucion(){
        $result = $this->db_declaracion
            ->table('oic_tipo_institucion')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar inversiones
     */
    public function getAdeudos($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_adeudos')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar clientes
     */
    public function getClientes($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_clientes_principales')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar participacion institucion
     */
    public function getParticipacionInstitucion($declaracion_id)
    {
        $result = $this->db_declaracion
            ->table('oic_participacion_instituciones')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * cargar catalogo de beneficiarios
     */
    public function getCatalogoBeneficiarios(){
        $result = $this->db_declaracion
            ->table('oic_beneficiarios_cat')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar participacion empresas
     */
    public function getParticipacionEmpresas($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_participacion_empresas')
            ->select('ParticipacionID', 'NombreEmpresa', 'RFCEmpresa', 'PorcentajeParticipacion', 'TipoParticipacion',
                'RecibeRemuneracion', 'MontoMensual', 'Ubicacion', 'EsExtranjero', 'Participante',
                'SectorID', 'Observaciones', 'DeclaracionID', 'created_at', 'updated_at', 'usuarioID', 'Oculto', 'OtroSector')
                ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    public function getRepresentaciones($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_representacion')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar apoyos y beneficios
     */
    public function getApoyosBeneficios($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_apoyos_beneficios')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar clientes
     */
    public function getBeneficiosPrivados($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_beneficios_privados')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar prestamos
     */
    public function getPrestamos($declaracion_id)
    {
        $result = $this->db_declaracion
            ->table('oic_prestamo_comodato')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Cargar inversiones
     */
    public function getFideicomisos($declaracion_id){
        $result = $this->db_declaracion
            ->table('oic_fideicomisos')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar las declaraciones de este ejercicio
     */
    public function getDeclaracionesEjercicio($request){
        $ejercicio = $this->db_main->table('osaf_ejercicios_cat')->where('Year', '=', $request['ejercicio'])->first();
        return $this->db_declaracion
            ->table('v_estadoDeclaraciones2 as A')
            ->leftJoin('oic_declaracion_sat as B', function($join) {
                $join->on('A.DeclaracionID', '=', 'B.DeclaracionID')
                     ->where('B.Eliminado', '=', false);
            })
            ->where('A.EjercicioID','=', $ejercicio->EjercicioID)
            ->when($request['nombre_filtro'], function ($query, $nombre) {
                return $query->where('Declarante', 'LIKE', '%'.$nombre.'%');
            })
            ->when($request['tipo_declaracion'], function ($query, $tipo) {
                return $query->where('TipoDeclaracion', '=', $tipo);
            })
            ->when($request['formato'], function($query, $formato) {
                return $query->where('tipo', '=', $formato);
            })
            ->when($request['area_ads'], function($query, $area) {
                return $query->where('AreaAdscripcion', 'LIKE', '%'.$area.'%');
            })
            ->when($request['filtro_finalizadas'], function($query, $finalizada) {
                if($finalizada == 'terminada') {
                    return $query->whereNotNull('RutaAcuse');
                } else {
                    return $query->whereNull('RutaAcuse');
                }
            })
            ->when($request['filtro_tipo_trabajador'], function($query, $tipo_trabajador) {
                return $query->where('TipoTrabajadorID', '=', $tipo_trabajador);
            })
            ->when($request['filtro_sat'], function($query) {
                return $query->whereNotNull('RutaArchivoSat');
            })
            ->select('A.*', 'B.Ruta as RutaArchivoSat')
            ->paginate(50);
    }

    public function getDeclaracionesEjercicioCvs($request){
        $ejercicio = $this->db_main->table('osaf_ejercicios_cat')->where('Year', '=', $request['ejercicio'])->first();
        return $this->db_declaracion
            ->table('v_estadoDeclaraciones2 as A')
            ->leftJoin('oic_declaracion_sat as B', function($join) {
                $join->on('A.DeclaracionID', '=', 'B.DeclaracionID')
                    ->where('B.Eliminado', '=', false);
            })
            ->where('A.EjercicioID','=', $ejercicio->EjercicioID)
            ->when($request['nombre_filtro'], function ($query, $nombre) {
                return $query->where('Declarante', 'LIKE', '%'.$nombre.'%');
            })
            ->when($request['tipo_declaracion'], function ($query, $tipo) {
                return $query->where('TipoDeclaracion', '=', $tipo);
            })
            ->when($request['formato'], function($query, $formato) {
                return $query->where('tipo', '=', $formato);
            })
            ->when($request['area_ads'], function($query, $area) {
                return $query->where('AreaAdscripcion', 'LIKE', '%'.$area.'%');
            })
            ->when($request['filtro_finalizadas'], function($query, $finalizada) {
                if($finalizada == 'terminada') {
                    return $query->whereNotNull('RutaAcuse');
                } else {
                    return $query->whereNull('RutaAcuse');
                }
            })
            ->when($request['filtro_tipo_trabajador'], function($query, $tipo_trabajador) {
                return $query->where('TipoTrabajadorID', '=', $tipo_trabajador);
            })
            ->when($request['filtro_sat'], function($query) {
                return $query->whereNotNull('RutaArchivoSat');
            })
            ->select('A.*', 'B.Ruta as RutaArchivoSat')
            ->get();
    }

    /**
     * Cargar las declaraciones de este ejercicio
     */
    public function getDeclaracionesTerminadas($ejercicio){
        $result = $this->db_declaracion
            ->table('v_estadoDeclaraciones as ved')
            ->select('ved.Declarante','ved.TipoDeclaracion', 'ved.AreaAdscripcion','odp.DeclaracionesGeneradas','ved.DeclaracionID')
            ->join('oic_declaraciones_patrimoniales as odp','ved.DeclaracionID','=','odp.DeclaracionID')
            ->where('ved.EjercicioID','=', $ejercicio)
            ->where('ved.Finalizado','=', 1)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Cargar detalles de una declaracion en especifico
     */
    public function getEstadisticaDeclaracion($declaracion){
        $result = $this->db_declaracion
            ->select('call sp_detalleDeclaracion (?)',[
                $declaracion,
            ]);
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Obtener el nombre del ente
     */
    public function getEnte()
    {
        return $this->db_declaracion->table('oic_configuraciones')->where('ConfigID', '=', 1)->first();
    }

    /**
     * Obtener formatos publicos
     */
    public function getFormatosPublicos()
    {
        $formatos = $this->db_declaracion->table('oic_formato_declaracion')
            ->where('Oculto', '=', 0)
            ->get();
        foreach($formatos as $formato) {
            $formato->EnUso = (int) $formato->EnUso;
        }
        return $formatos;
    }

    /**
     * Obtener la lista de los documentos generados de una declaracion
     */
    public function listaDocumentosGenerados($declaracion_id){
        $documentos = $this->db_declaracion
            ->select('call sp_obtenerDocumentos(?)',[
                $declaracion_id
            ]);
        return $documentos;
    }

    /**
     * Obtener las rutas de las declaraciones de cierto formato y declaraciones seleccionadas
     */
    public function getRutasDeclaracion($declaraciones,$formato){
        $rutas = $this->db_declaracion
            ->table('oic_declaraciones_publicas')
            ->select('RutaDeclaracion')
            ->whereIn('DeclaracionID', $declaraciones)
            ->where('FormatoID', '=',$formato)
            ->get();

        \Log::info($formato);

        return $rutas;
    }

    /**
     * Borrar una declaracion generada con un formato personalizado
     */

     public function borarFormatoGenerado($declaracion_id,$formato){
        $this->db_declaracion
            ->table('oic_declaraciones_publicas')
            ->where('DeclaracionID','=', $declaracion_id)
            ->where('FormatoID','=', $formato)
            ->delete();
     }

     /**
     * Borrar Declaraciones generadas con un formato personalizado
     */
    public function borarDeclaracionesGeneradas($declaraciones_id,$formato){
        $this->db_declaracion
            ->table('oic_declaraciones_publicas')
            ->whereIn('DeclaracionID', $declaraciones_id)
            ->where('FormatoID','=', $formato)
            ->delete();
     }

    /**
     * Marcar declaracion como copiada
     */
    public function marcarDeclaracionCopiada($declaracion_id)
    {
        $this->db_declaracion->table('oic_declaraciones_patrimoniales')->where('DeclaracionID', '=', $declaracion_id)->update([
            'CopiaAnterior' => 1,
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
    }

}
