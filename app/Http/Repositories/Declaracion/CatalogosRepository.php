<?php

namespace App\Http\Repositories\Declaracion;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CatalogosRepository
{
    protected $db_declaracion;
    protected $db_main;

    public function __construct()
    {
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main        = DB::connection('main');
    }
}