<?php


namespace App\Http\Repositories;


use Illuminate\Support\Facades\DB;

class EjerciciosRepository
{
    /**
     * Retornar el ejercicio actual
     */
    public function getEjercicioActual()
    {
        return DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', 1)
            ->first();
    }

    /**
     * Retornar el ejercicio que se esta auditando actualmente
     */
    public function getEjercicioAuditado()
    {
        $ejercicio_actual = DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', 1)
            ->first();
        return DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Year', '=', $ejercicio_actual->Year - 1)
            ->first();
    }

    /**
     * Retornar el ejercicio por año especificado
     * @param $ejercicio
     */
    public function getEjercicioByYear($ejercicio)
    {
        return DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Year', '=', $ejercicio)
            ->first();
    }
}
