<?php


namespace App\Http\Repositories;


use Illuminate\Support\Facades\{DB, Log};

class EtapasRepository
{
    /**
     * Retornar las etapas de los enlaces
     * @return \Illuminate\Support\Collection
     */
    public function getEtapasEnlace()
    {
        return DB::table('control_etapa_cat')
            ->where('RolEncargado', '=', 'Enlace')
            ->orderBy('NoEtapa', 'asc')
            ->get();
    }

    /**
     * Obtener las etapas por auditoria
     * @param $auditoria
     * @return \Illuminate\Support\Collection
     */
    public function getEtapasAuditoria($auditoria)
    {
        return DB::table('control_auditoria_etapa_det as caed')
            ->join('control_etapa_cat as cec', 'caed.EtapaID', '=', 'cec.EtapaID')
            ->where('AuditoriaID', '=', $auditoria)
            ->select('caed.*', 'cec.DescripcionEtapa', 'cec.DescripcionCorta')
            ->orderBy('Secuencial', 'asc')
            ->get();
    }

    public function getEtapaActualAuditoria($auditoria)
    {
        $etapas_auditoria = $this->getEtapasAuditoria($auditoria);
        //Determinar si ninguna etapa ha sido finalizada
        $count_etapas = count($etapas_auditoria);
        $i = 0;
        $finalizado = 0;
        while( ($i < $count_etapas) && ($finalizado == 0)) {
            $finalizado = $etapas_auditoria[$i]->Finalizado;
            if($finalizado == 0) {
                $i++;
            } else {
                $finalizado = 1;
            }
        }
        if($i == $count_etapas) {
            $primera_etapa = $etapas_auditoria[0];
            return DB::table('control_auditoria_etapa_det as caed')
                ->join('control_etapa_cat as cec', 'caed.EtapaID', '=', 'cec.EtapaID')
                ->where('caed.AuditoriaID', '=', $auditoria)
                ->where('cec.NoEtapa', $primera_etapa->Secuencial)
                ->select('cec.*', 'caed.AuditoriaEtapaID')
                ->first();
        } else {
            $j = 0;
            $finalizado = 1;
            while( ($j < $count_etapas) && ($finalizado == 1)) {
                $finalizado = $etapas_auditoria[$j]->Finalizado;
                if($finalizado == 1) {
                    $j++;
                } else {
                    $finalizado = 0;
                }
            }
            if($j == $count_etapas) {
                $j = $count_etapas - 1;
            }
            return DB::table('control_auditoria_etapa_det as caed')
                ->join('control_etapa_cat as cec', 'caed.EtapaID', '=', 'cec.EtapaID')
                ->where('caed.AuditoriaID', '=', $auditoria)
                ->where('cec.NoEtapa', $etapas_auditoria[$j]->Secuencial)
                ->select('cec.*', 'caed.AuditoriaEtapaID')
                ->first();
        }
    }

    /**
     * Determinar si la etapa actual es de un enlace
     */
    public function isEtapaEnlace($auditoria)
    {
        $etapa_actual = $this->getEtapaActualAuditoria($auditoria);
        if($etapa_actual->RolEncargado == 'Enlace') {
            return true;
        }
        return false;
    }

    public function getEtapas()
    {
        return DB::table('control_etapa_cat')->orderBy('EtapaID', 'asc')->get();
    }

}
