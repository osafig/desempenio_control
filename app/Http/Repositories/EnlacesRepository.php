<?php


namespace App\Http\Repositories;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EnlacesRepository
{
    /**
     * Obtener a todos los enlaces
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function getEnlaces($request)
    {
        $enlaces = null;
        if (in_array("all", $request->ejercicios)) {
            $enlaces = DB::connection('main')
                ->table('osaf_v_enlaces')
                ->where(function($query) use($request) {
                    if(is_null($request->search) == false) {
                        $query->where('Entidad', 'like', '%'.$request->search.'%');
                        $query->orWhere('NombreCompleto', 'like', '%'.$request->search.'%');
                    }
                })
                ->orderBy('Entidad', 'asc')
                ->get();
        } else {
            if($request->tipoAuditoria == 'control_interno') {
                $funcionarios_id = DB::table('control_asignacion_personal_auditoria as capa')
                    ->join('v_control_auditorias as vca', 'capa.AuditoriaID', '=', 'vca.AuditoriaID')
                    ->where('capa.Enlace', '=', true)
                    ->whereNull('capa.deleted_at')
                    ->whereIn('vca.Ejercicio', $request->ejercicios)
                    ->select('capa.UsuarioID')
                    ->get();
            } else {
                $funcionarios_id = DB::connection('sad')
                    ->table('sad_asignacion_personal_auditoria as capa')
                    ->join('v_control_auditorias as vca', 'capa.AuditoriaID', '=', 'vca.AuditoriaID')
                    ->where('capa.Enlace', '=', true)
                    ->whereNull('capa.deleted_at')
                    ->whereIn('vca.Ejercicio', $request->ejercicios)
                    ->select('capa.UsuarioID')
                    ->get();
            }
            $funcionariosID = Arr::pluck($funcionarios_id, 'UsuarioID');
            $enlaces = DB::connection('main')
                ->table('osaf_v_enlaces')
                ->whereIn('FuncionarioID', $funcionariosID)
                ->where(function($query) use($request) {
                    if(is_null($request->search) == false) {
                        $query->where('Entidad', 'like', '%'.$request->search.'%');
                        $query->orWhere('NombreCompleto', 'like', '%'.$request->search.'%');
                    }
                })
                ->orderBy('Entidad', 'asc')
                ->get();
        }
        return $enlaces;
    }

    /**
     * Obtener la información del enlace
     * @param $request
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getEnlace($request)
    {
        return DB::connection('main')
            ->table('osaf_funcionarios_cat as ofc')
            ->join('osaf_entidades_funcionarios_det as oefd', 'ofc.FuncionarioID', '=', 'oefd.FuncionarioID')
            ->join('osaf_entidades_fiscalizables as oef', 'oefd.EntidadID', '=', 'oef.EntidadID')
            ->where('ofc.FuncionarioID', '=', $request->enlaceId)
            ->select('ofc.FuncionarioID', 'ofc.Nombres', 'ofc.PrimerApellido', 'ofc.SegundoApellido', 'ofc.Profesion', 'ofc.Email as Correo',
                              'oefd.Puesto', 'oefd.FechaIngreso', 'ofc.Telefono', 'oefd.Email as CorreoInstitucional', 'oef.Nombre as Entidad')
            ->first();
    }
}
