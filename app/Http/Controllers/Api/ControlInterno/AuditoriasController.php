<?php

namespace App\Http\Controllers\Api\ControlInterno;

use App\Mail\EnviarAccesosEnlaces;
use App\Http\Repositories\{EjerciciosRepository, EtapasRepository};
use App\Http\Repositories\ControlInterno\AuditoriasRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\{DB, Log, Mail, Storage};
use Illuminate\Support\Arr;
use Barryvdh\DomPDF\Facade as PDF;
use PhpOffice\PhpWord\PhpWord as WORD;
use PhpOffice\PhpWord\IOFactory as WORDIOFACTORY;
use PhpOffice\PhpWord\Style\Language as WORDLANGUAGE;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Http\Request;


class AuditoriasController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios
    protected $ar;  //Repositorio de datos de Auditorias
    protected $etr; //Repositorio de etapas de Auditorias
    protected $ah;  //Helper de Auditorias

    public function __construct(EjerciciosRepository $er, AuditoriasRepository $ar, EtapasRepository $etr)
    {
        $this->er = $er;
        $this->ar = $ar;
        $this->etr = $etr;
    }

    /**
     * Obtener auditorias
     */

     public function avanzarAuditoria(Request $request)
    {

        // Validar que se haya recibido el ID de la auditoría
        $validatedData = $request->validate([
            'idauditoria' => 'required|integer'
        ]);
        //----------datos estaticos-------//

        // Obtener el ID de la auditoría desde la solicitud
        $auditoriaID = $validatedData['idauditoria'];
        $RespuestaNoContestada = 'Sin justificaciones, ni aclaraciones emitidas por el Ente Fiscalizado, una vez revisadas todas las preguntas hasta que se revisen todas';

        // Conectarse a la base de datos 'sad' y obtener el CuestionarioID
        $cuestionarioID = DB::table('control_aplicacion_cuestionario')
            ->where('AuditoriaID', $auditoriaID)
            ->value('CuestionarioID');

        // Obtener AplicacionID basado en el AuditoriaID
        $aplicacionID = DB::table('control_aplicacion_cuestionario')
        ->where('AuditoriaID', $auditoriaID)
        ->value('AplicacionID');

        // Obtener AuditoriaEtapaID basado en el AuditoriaID
        $auditoriaEtapaID = DB::table('control_auditoria_etapa_det')
        ->where('AuditoriaID', $auditoriaID)
        ->value('EtapaID');


        // Consultar la tabla sad_auditoria_etapa_det y hacer JOIN con sad_etapa_cat
        $etapas = DB::table('control_auditoria_etapa_det')
        ->join('control_etapa_cat', 'control_auditoria_etapa_det.EtapaID', '=', 'control_etapa_cat.EtapaID')
        ->where('control_auditoria_etapa_det.AuditoriaID', $auditoriaID)
        ->select('control_auditoria_etapa_det.EtapaID', 'control_etapa_cat.DescripcionEtapa')
        ->first();

        $etapaDescripcion = $etapas ? $etapas->DescripcionEtapa : '';
        //----------------datos estaticos---------------------------------------------//

        if ($cuestionarioID) {
            // Hacer un JOIN con las tablas sad_preguntas_cat y sad_respuestas
            Log::info(json_encode($cuestionarioID));
            // Hacer el JOIN y filtrar por CuestionarioID y AplicacionID
            $preguntas = DB::table('control_preguntas_cat')
            ->leftJoin('control_respuestas', function($join) use ($aplicacionID) {
                $join->on('control_preguntas_cat.PreguntaID', '=', 'control_respuestas.PreguntaID')
                    ->where('control_respuestas.AplicacionID', '=', $aplicacionID);
            })
            ->join('control_tema_cuestionario_cat', function($join) use ($cuestionarioID) {
                $join->on('control_preguntas_cat.TemaID', '=', 'control_tema_cuestionario_cat.TemaID')
                     ->where('control_tema_cuestionario_cat.CuestionarioID', '=', $cuestionarioID);
            })
            ->select(
                'control_preguntas_cat.PreguntaID',
                'control_preguntas_cat.Pregunta',
                'control_respuestas.Respuesta'
            )
            ->get();
        
            //verificar si esta con el enlace
            $condicionCumplida = DB::table('control_auditoria_etapa_det')
            ->where('AuditoriaID', $auditoriaID)
            ->where('EtapaID', 1)
            ->where('Finalizado', 0)
            ->exists();

            if ($condicionCumplida) {
                Log::info("se encontro que la auditoria esta con el enlace");
                    // Iterar sobre las preguntas
                foreach ($preguntas as $pregunta) {
                    // Verificar si ya existe una respuesta para la PreguntaID correspondiente
                    if (($pregunta->Respuesta)===NULL) {
                           
                        // Si no existe una respuesta, insertar una nueva
                        
                        $insert = DB::table('control_respuestas')
                            ->insertGetId([
                                'PreguntaID' => $pregunta->PreguntaID,
                                'AuditoriaEtapaID' =>  $auditoriaEtapaID, // Ajusta según corresponda
                                'AplicacionID' =>  $aplicacionID,         // Ajusta según corresponda
                                'Respuesta' => $RespuestaNoContestada,
                                'No_Anexos' => 0, // O el valor que corresponda
                                'Valoracion' => null,
                                'Etapa' => $etapaDescripcion, // Ajusta según corresponda
                                'created_at' => now(),
                                'updated_at' => now(),
                                'RespuestaTXT' => null,
                            ]);
                     
                    }
                }

                // Avanzar auditoria
                DB::table('control_auditoria_etapa_det')
                    ->where('AuditoriaID', $auditoriaID)
                    ->where('EtapaID', 1)
                    ->where('Finalizado', 0)
                    ->update([
                        'Finalizado' => 1
                    ]);

                    return response()->json([
                        'message' => 'Auditoría avanzada con éxito.',
                        'cuestionarioID' => $cuestionarioID,
                    ], 200);
      
            }else{
                Log::info("Esta Auditoria no esta con el enlace");
            }
             
        } else {
            return response()->json(['message' => 'Cuestionario no encontrado.'], 404);
        }
    }

    public function getAuditorias()
    {
        $ejercicio_fiscal = $this->er->getEjercicioAuditado()->Year;
        $auditorias = DB::table('v_monitoreo_auditoria')
            ->where('Anio', '=', 2023/*$ejercicio_fiscal*/)
            ->where(function($query) {
                if(!is_null(request()->search)) {
                    $query->where('Nombre', 'like', '%' . request()->search . '%');
                }
                if(count(request()->etapas) > 0) {
                    $query->whereIn('etapa_actual', request()->etapas);
                }
            })
            ->orderBy('Nombre', 'asc')
            ->get();
            
        return response()->json([
            'status' => 'Ok',
            'auditorias' => $auditorias
        ], 200);
    }

    /**
     * Crear nuevas auditorias
     */
    public function createAuditorias()
    {
        try {
            $ejercicio_fiscal = $this->er->getEjercicioAuditado();
            $user = request()->user();
            // Validar que no se dupliquen auditorias
            $auditorias_creadas = $this->ar->getAuditoriasCreadas($ejercicio_fiscal);
            $entesID = Arr::pluck($auditorias_creadas, 'EnteID');
            //Obtener los entesID disponibles para generar una auditoria
            $entes = array_diff(request()->entes, $entesID);
            // Armar un arreglo con el ejercicio fiscal a auditar,
            // usuario que da de alta la auditoria y ente a generar su auditoria
            for ($i = 0; $i < count($entes); $i++) {
                DB::select('call sp_crearAuditoria1(?,?,?,?,?,?)', [
                    $entes[$i],
                    $ejercicio_fiscal->EjercicioID,
                    $user->UsuarioID,
                    '',
                    0,
                    ''
                ]);
            }
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener el detalle de la auditoria
     */
    public function getAuditoriaDetalle()
    {
        try {
            $ejercicio = $this->er->getEjercicioByYear(request()->ejercicio);
            $auditoria = DB::table('control_auditorias')
                ->where('AuditoriaID', '=', request()->auditoriaID)
                ->where('EjercicioID', '=', $ejercicio->EjercicioID)
                ->first();
            if (is_object($auditoria)) {
                $data_auditoria = DB::select('call sp_obtenerDatosAuditoria(?)', [request()->auditoriaID]);
                return response()->json([
                    'status' => 'Ok',
                    'auditoria' => $data_auditoria[0]
                ], 200);
            }
            throw new \Exception("AUDITORÍA NO ENCONTRADA");
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las fechas de contestar cuestionario y solventar recomendaciones de la auditoria
     */
    public function getAuditoriaFechas()
    {
        try {
            // Obtener las etapas del enlace
            $etapas_enlace = $this->etr->getEtapasEnlace();
            $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
            // Obtener los registros del detalle de auditoria etapas
            $det_auditoria_etapas = DB::table('control_auditoria_etapa_det')
                ->whereIn('EtapaID', $etapas_id)
                ->where('AuditoriaID', '=', request()->auditoriaID)
                ->select('*')
                ->get();
            // Consultar fechas de inicio y fin de etapas
            $fechas = [];
            for ($i = 0; $i < count($det_auditoria_etapas); $i++) {
                $fecha = DB::table('control_fechas_etapa_det')
                    ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                    ->select('*')
                    ->first();
                if (is_null($fecha)) {
                    array_push($fechas, [
                        'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                        'fecha_inicial' => null,
                        'fecha_final' => null
                    ]);
                } else {
                    array_push($fechas, [
                        'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                        'fecha_inicial' => $fecha->FechaInicio,
                        'fecha_final' => $fecha->FechaFin
                    ]);
                }
            }
            return response()->json([
                'status' => 'Ok',
                'fechas' => $fechas
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar fechas de auditoria
     */
    public function updateAuditoriaFechas()
    {
        try {
            DB::beginTransaction();
            // Obtener las etapas del enlace
            $etapas_enlace = DB::table('control_etapa_cat')
                ->where('RolEncargado', '=', 'Enlace')
                ->orderBy('NoEtapa', 'asc')
                ->get();
            $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
            // Obtener los registros del detalle de auditoria etapas
            $det_auditoria_etapas = DB::table('control_auditoria_etapa_det as caed')
                ->join('control_etapa_cat as cec', 'caed.EtapaID', '=', 'cec.EtapaID')
                ->whereIn('caed.EtapaID', $etapas_id)
                ->where('caed.AuditoriaID', '=', request()->auditoriaID)
                ->select('caed.*', 'cec.RolEncargado', 'cec.DescripcionEtapa')
                ->get();
            for ($i = 0; $i < count($det_auditoria_etapas); $i++) {
                if ($det_auditoria_etapas[$i]->DescripcionEtapa == 'Enlace contestando') {
                    $fecha = DB::table('control_fechas_etapa_det')
                        ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                        ->first();
                    if (is_null($fecha)) {
                        DB::table('control_fechas_etapa_det')
                            ->insert([
                                'AuditoriaEtapaID' => $det_auditoria_etapas[$i]->AuditoriaEtapaID,
                                'FechaInicio' => request()->fechas['cuestionarioInicio'],
                                'FechaFin' => request()->fechas['cuestionarioFinal'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    } else {
                        DB::table('control_fechas_etapa_det')
                            ->where('AuditoriaEtapaID', '=', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                            ->update([
                                'FechaInicio' => request()->fechas['cuestionarioInicio'],
                                'FechaFin' => request()->fechas['cuestionarioFinal'],
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    }
                } else {
                    $fecha = DB::table('control_fechas_etapa_det')
                        ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                        ->first();
                    if (is_null($fecha)) {
                        DB::table('control_fechas_etapa_det')
                            ->insert([
                                'AuditoriaEtapaID' => $det_auditoria_etapas[$i]->AuditoriaEtapaID,
                                'FechaInicio' => request()->fechas['solventacionInicio'],
                                'FechaFin' => request()->fechas['solventacionFinal'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    } else {
                        DB::table('control_fechas_etapa_det')
                            ->where('AuditoriaEtapaID', '=', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                            ->update([
                                'FechaInicio' => request()->fechas['solventacionInicio'],
                                'FechaFin' => request()->fechas['solventacionFinal'],
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    }
                }
            }
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener el nombre del jefe de auditoria
     */
    public function getJefeAuditoria()
    {
        try {
            $jefe = $this->ar->getJefeAuditoria();
            return response()->json([
                'status' => 'Ok',
                'jefeAuditoria' => $jefe
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener nmbre de todos los auditores de desempeño
     */
    public function getAuditores()
    {
        try {
            $auditores = $this->ar->getAuditores();
            return response()->json([
                'status' => 'Ok',
                'auditores' => $auditores
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar auditor asignado a la auditoria
     */
    public function updateAuditor()
    {
        try {
            DB::beginTransaction();
            // Validar si la auditoria cuenta con un usuario auditor
            $auditoria_id = request()->auditoria;
            $asignacion_auditor = DB::table('control_asignacion_personal_auditoria')
                ->where('AuditoriaID', '=', $auditoria_id)
                ->where('Enlace', '=', 0)
                ->where('ResponsableAuditoria', '=', 0)
                ->whereNull('deleted_at')
                ->first();
            //Si no existe un usuario, asignamos al usuario a la auditoria
            $auditor = DB::connection('main')
                ->table('osaf_v_auditores_desempenio')
                ->where('NombreCompleto', '=', request()->auditor)
                ->first();
            if (is_null($asignacion_auditor)) {
                //Insertar auditor a la auditoria
                DB::table('control_asignacion_personal_auditoria')
                    ->insert([
                        'AuditoriaID' => $auditoria_id,
                        'UsuarioID' => $auditor->UsuarioID,
                        'Enlace' => 0,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
            } else {
                //Si existe un usuario auditor asignado a la auditoria lo marcamos como eliminado
                DB::table('control_asignacion_personal_auditoria')
                    ->where('AsignacionID', '=', $asignacion_auditor->AsignacionID)
                    ->update([
                        'deleted_at' => date('Y-m-d H:i:s')
                    ]);
                DB::table('control_asignacion_personal_auditoria')
                    ->insert([
                        'AuditoriaID' => $auditoria_id,
                        'UsuarioID' => $auditor->UsuarioID,
                        'Enlace' => 0,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
            }
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener al auditor asignado
     */
    public function getAuditorAsignado()
    {
        try {
            $auditor_asignado = $this->ar->getAuditorAsignado(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'auditor_asignado' => $auditor_asignado
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Editar datos generales
     */
    public function editarGenerales()
    {
        try {
            DB::beginTransaction();
            DB::table('control_auditorias')
                ->where('AuditoriaID', '=', request()->auditoria)
                ->update([
                    'NoAuditoria' => mb_strtoupper(request()->noAuditoria, 'UTF-8'),
                    'Titular' => mb_strtoupper(request()->titularAuditoria, 'UTF-8'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener al enlace asignado a la auditoria
     */
    public function getEnlaceAsignado()
    {
        try {
            $enlace_asignado = $this->ar->getEnlaceAsignado(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'enlace_asignado' => $enlace_asignado
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener los enlaces asignados al ente
     */
    public function getEnlacesEnte()
    {
        try {
            $enlaces_ente = $this->ar->getEnlacesEnte(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'enlaces' => $enlaces_ente
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar el enlace a la auditoria
     */
    public function updateEnlace()
    {
        try {
            DB::beginTransaction();
            $enlace_asignado = $this->ar->getEnlaceAsignado(request()->auditoria);
            if ($enlace_asignado == "") {
                // Obtener al enlace
                $enlace = $this->ar->getEnlace(null, request()->enlace, request()->auditoria);
                // Asignar un nuevo enlace a la auditoria
                DB::table('control_asignacion_personal_auditoria')
                    ->insertGetId ([
                        'UsuarioID' => $enlace->FuncionarioID,
                        'AuditoriaID' => request()->auditoria,
                        'Enlace' => 1,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                // Actualizar tokens de acceso a la plataforma
                $this->ar->getDataAccessosPdf(request());
            } else {
                // Actualizar el enlace a la auditoria solamente si es diferente del enlace actual
                if ($enlace_asignado->NombreCompleto != request()->enlace) {
                    //Obtener el enlace actual
                    $enlace_actual = DB::table('control_asignacion_personal_auditoria')
                        ->where('AuditoriaID', '=', request()->auditoria)
                        ->where('Enlace', '=', 1)
                        ->where('ResponsableAuditoria', '=', 0)
                        ->where('UsuarioID', '=', $enlace_asignado->FuncionarioID)
                        ->whereNull('deleted_at')
                        ->first();
                    //Marcar el enlace actual como borrado
                    DB::table('control_asignacion_personal_auditoria')
                        ->where('AsignacionID', '=', $enlace_actual->AsignacionID)
                        ->update([
                            'deleted_at' => date('Y-m-d H:i:s')
                        ]);
                    //Asignar al nuevo enlace
                    // Obtener al enlace
                    $enlace = $this->ar->getEnlace(null, request()->enlace, request()->auditoria);
                    // Asignar un nuevo enlace a la auditoria
                    DB::table('control_asignacion_personal_auditoria')
                        ->insert([
                            'UsuarioID' => $enlace->FuncionarioID,
                            'AuditoriaID' => request()->auditoria,
                            'Enlace' => 1,
                            'ResponsableAuditoria' => 0,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    // Actualizar tokens de acceso a la plataforma
                    $this->ar->getDataAccessosPdf(request());
                }
            }
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Descargar accesos del enlace
     */
    public function decargarAccesos()
    {
        try {
            //Consultar los datos que contendra el pdf
            $data = $this->ar->getDataAccessosPdf(request());
            $pdf = PDF::loadView('pdf.acceso_enlace', compact('data'));
            return $pdf->stream();
        } catch (Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Envíar accesos al correo del enlace
     */
    public function enviarAccesos()
    {
        try {
            //Consultar los datos que contendra el pdf
            $data = $this->ar->getDataAccessosPdf(request());
            if($data->Email == '') {
                $emails = [$data->EmailInstitucional];
            } else {
                $emails = [$data->EmailInstitucional, $data->Email];
            }
            Mail::to($emails)->send(new EnviarAccesosEnlaces($data));
            $message = "CORREO DE ENTREGA DE ACCESOS ENVIADO |
                        ENTIDAD: {$data->Entidad} |
                        NOMBRE DE ENLACE: {$data->NombreCompleto} |
                        CORREO INSTITUCIONAL: {$data->EmailInstitucional} |
                        CORREO PERSONAL: {$data->Email}";
            Log::info($message);
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las prorrogas de la auditoria
     */
    public function getProrrogas()
    {
        try {
            $prorrogas = $this->ar->getProrrogas(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'prorrogas' => $prorrogas
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener la etapa actual de la auditoria
     */
    public function getEtapaAuditoria()
    {
        try {
            $etapaActual = $this->etr->getEtapaActualAuditoria(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'etapa_actual' => $etapaActual
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener etapas de la auditoria
     */
    public function getEtapasAuditoria()
    {
        try {
            $etapas = $this->etr->getEtapasAuditoria(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'etapas' => $etapas
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Agregar nuevas prorrogas
     */
    public function addProrrogas()
    {
        try {
            DB::beginTransaction();
            $user = request()->user();
            $etapaActual = $this->etr->getEtapaActualAuditoria(request()->auditoria);
            DB::table('control_prorrogas_cat')
                ->insert([
                    'AuditoriaEtapaID' => $etapaActual->AuditoriaEtapaID,
                    'UsuarioID' => $user->UsuarioID,
                    'FechaCierre' => request()->fechaVigencia,
                    'Motivo' => request()->motivo,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * actualizar una prorroga
     */
    public function updateProrrogas()
    {
        try {
            DB::beginTransaction();
            DB::table('control_prorrogas_cat')
                ->where('ProrrogaID', '=', request()->prorrogaId)
                ->update([
                    'FechaCierre' => request()->fechaVigencia,
                    'Motivo' => request()->motivo,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Eliminar prorrogas
     */
    public function deleteProrrogas()
    {
        try {
            DB::beginTransaction();
            DB::table('control_prorrogas_cat')
                ->where('ProrrogaID', '=', request()->prorrogaId)
                ->delete();
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener auditoria del ejercicio actual por funcionario
     */
    public function getAuditoriaByFuncionarioId()
    {
        //Ejercicio fiscal
        $ejercicio = $this->er->getEjercicioAuditado()->Year;
        $auditoria = DB::table('v_control_auditorias')
            ->where('Ejercicio', '=', $ejercicio)
            ->where('Enlace', '=', request()->funcionarioID)
            ->first();
        $hash = DB::table('control_auditorias')
            ->where('AuditoriaID', '=', $auditoria->AuditoriaID)
            ->select('Hash')
            ->first();
        $auditoria->Hash = $hash->Hash;
        return response()->json([
            'status' => 'Ok',
            'auditoria' => $auditoria
        ], 200);
    }

    /**
     * Obtener datos generales del cuestionario de auditoria
     */
    public function getGeneralDataCuestionario()
    {
        $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria);
        $fecha = DB::table('control_fechas_etapa_det')
            ->where('AuditoriaEtapaID', $etapa->AuditoriaEtapaID)
            ->select('*')
            ->first();
        $fecha_inicio = null;
        $fecha_fin = null;
        if (!is_null($fecha)) {
            $fecha_inicio = $fecha->FechaInicio;
            $fecha_fin = $fecha->FechaFin;
        }
        return response()->json([
            'status' => 'Ok',
            'etapaActual' => $etapa->DescripcionEtapa,
            'fechaInicio' => $fecha_inicio,
            'fechaFin' => $fecha_fin
        ], 200);
    }

    /**
     * Obtener los temas del cuestionario por auditoria
     */
    public function getTemasCuestionario()
    {
        $temas = DB::select('call sp_estatus_cuestionario(?)', [request()->auditoria]);
        return response()->json([
            'status' => 'Ok',
            'temasCuestionario' => $temas
        ], 200);
    }

    /**
     * Obtener preguntas del cuestionario por temas
     */
    public function getPreguntasTema()
    {
        $preguntas = DB::select('call sp_obtenerRecomendacionesPorTema(?, ?)', [request()->formTopic['auditoriaId'], request()->formTopic['temaId']]);
        return response()->json([
            'status' => 'Ok',
            'preguntas' => $preguntas
        ], 200);
    }

    /**
     * Obtener opciones multiples de la pregunta
     */
    public function getOpcionesPregunta()
    {
        $opciones = DB::table('control_opciones_pregunta')
            ->where('PreguntaID', '=', request()->preguntaID)
            ->whereNull('deleted_at')
            ->get();
        //RespuestaID
        $opciones_guardadas_db = DB::table('control_opciones_seleccionadas_det')
            ->where('RespuestaID', '=', request()->RespuestaID)
            ->where('Seleccionado', '=', 1)
            ->get();
        $opciones_guardadas_id = Arr::pluck($opciones_guardadas_db, 'OpcionID');
        $opciones_guardadas = DB::table('control_opciones_pregunta')
            ->whereIn('OpcionID', $opciones_guardadas_id)
            ->get();
        //RespuestaSolventacionID
        $opciones_guardadas_solventacion_db = DB::table('control_opciones_seleccionadas_det')
            ->where('RespuestaID', '=', request()->RespuestaID2)
            ->where('Seleccionado', '=', 1)
            ->get();
        $opciones_guardadas_solventacion_id = Arr::pluck($opciones_guardadas_solventacion_db, 'OpcionID');
        $opciones_guardadas_solventacion = DB::table('control_opciones_pregunta')
            ->whereIn('OpcionID', $opciones_guardadas_solventacion_id)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'opciones_pregunta' => $opciones,
            'opciones_guardadas' => $opciones_guardadas,
            'opciones_guardadas_solventacion' => $opciones_guardadas_solventacion
        ], 200);
    }

    /**
     * Guardar las respuestas
     */
    public function saveRespuesta()
    {
        try {
            //Determinar si la etapa de auditoria esta con el enlace
            $is_etapa_enlace = $this->etr->isEtapaEnlace(request()->auditoriaId);
            if ($is_etapa_enlace == false) {
                throw new \Exception("La etapa actual de auditoria no se encuentra con el enlace");
            }
            //Si la etapa de enlace es 1 obtener la fecha de vigencia
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            $fechas_vigencia_cuestionario = $this->ar->getAuditoriaFechas(request()->auditoriaId);
            $fechas = [];
            //Obteher fechas de la etapa vigente
            foreach ($fechas_vigencia_cuestionario as $fecha) {
                if (count($fechas) == 0) {
                    if ($fecha['etapa'] == $etapa_actual->NoEtapa) {
                        $fechas = $fecha;
                    }
                }
            }
            $fecha_limite = \Carbon\Carbon::parse($fechas['fecha_final'])->startOfDay();
            $fecha_limite = $fecha_limite->copy()->endOfDay();
            $carbon = new \Carbon\Carbon();
            $now = $carbon->now();
            if ($now->gt($fecha_limite)) {
                //TODO: Validar si la auditoria cuenta con prorrogas activas
                throw new \Exception("La fecha para contestar el cuestionario ha expirado");
            }
            $data_pregunta = json_decode(request('pregunta'));
            //DATOS PREGUNTA
            $pregunta_id = $data_pregunta->PreguntaID;
            $tipo_pregunta = $data_pregunta->TipoPreguntaID;
            // DATOS RESPUESTA
            $condicional = request()->respuesta;
            $respuesta = (request()->subrespuesta == null || request()->subrespuesta == 'null') ? '' : request()->subrespuesta;
            if ($condicional == 1) {
                $fecha_mecanismo = null;
            } else {
                $fecha_mecanismo = (request()->fechaTermino == 'null') ? null : request()->fechaTermino;
            }
            if($etapa_actual->DescripcionEtapa == 'Enlace contestando') {
                $respuesta_id = $data_pregunta->RespuestaID;
            } else {
                $respuesta_id = $data_pregunta->solvRespuestaID;
            }
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            $auditoria_etapa_id = $etapa_actual->AuditoriaEtapaID;
            $etapa = $etapa_actual->DescripcionEtapa;
            $aplicacion_cuestionario = DB::table('control_aplicacion_cuestionario')
                ->where('AuditoriaID', '=', request()->auditoriaId)
                ->first();
            $aplicacion_id = $aplicacion_cuestionario->AplicacionID;
            if ($respuesta_id == null) {
                if (request()->hasFile('fileToUpload')) {
                    $no_anexos = count(request()->fileToUpload);
                } else {
                    $no_anexos = 0;
                }
            } else {
                $anexos_respuesta = DB::table('control_anexo_det')
                    ->where('RespuestaID', '=', $respuesta_id)
                    ->get();
                if (request()->hasFile('fileToUpload')) {
                    $no_anexos = count(request()->fileToUpload) + count($anexos_respuesta);
                } else {
                    $no_anexos = count($anexos_respuesta);
                    if ($condicional == 0 && count($anexos_respuesta) > 0) {
                        foreach ($anexos_respuesta as $anexo) {
                            try {
                                Storage::disk('public')->delete($anexo->Ruta);
                            } catch (\Exception $e) {
                                Log::info("ERROR | NO SE PUDO BORRAR EL DOCUMENTO FISICO | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
                            }
                        }
                        DB::table('control_anexo_det')
                            ->where('RespuestaID', '=', $respuesta_id)
                            ->delete();
                        $no_anexos = 0;
                    }
                }
            }
            // REGISTRAR LA RESPUESTA
            $resp = DB::select('call sp_registrarRespuesta(?,?,?,?,?,?,?,?,?)', [
                $pregunta_id,
                $auditoria_etapa_id,
                $aplicacion_id,
                $condicional,
                $respuesta,
                $no_anexos,
                $fecha_mecanismo,
                $respuesta_id,
                $etapa
            ]);
            if ($respuesta_id !== null) {
                $respuesta_id = $respuesta_id;
            } else {
                $respuesta_id = $resp[0]->respuesta_insertada;
            }
            if (request()->hasFile('fileToUpload')) {
                try {
                    DB::beginTransaction();
                    $files = [];
                    $ejercicio = $this->er->getEjercicioAuditado();
                    $auditoria_id = request()->auditoriaId;
                    foreach (request()->fileToUpload as $file) {
                        $nombre_archivo = $file->getClientOriginalName();
                        $descripcion = '';
                        foreach(json_decode(request()->documentosSeleccionadosDes) as $file_sel) {
                            if($file_sel->name == $nombre_archivo) {
                                $descripcion = $file_sel->description;
                            }
                        }
                        $dir = "/control_interno/{$ejercicio->Year}/{$auditoria_id}/anexos";
                        $path = $file->store($dir, 'public');
                        $data = [
                            'RespuestaID'   => $respuesta_id,
                            'NombreArchivo' => $nombre_archivo,
                            'Ruta'          => $path,
                            'Hash'          => $this->getHash($path),
                            'Descripcion'   => $descripcion,
                            'EnlaceID'      => request()->funcionarioID,
                            'created_at'    => date('Y-m-d H:i:s')
                        ];
                        array_push($files, $data);
                    }
                    DB::table('control_anexo_det')->insert($files);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error("ERROR | NO SE PUDO GUARDAR LOS ANEXOS A LA RESPUESTA | {$e->getMessage()}");
                }
            }
            if ($tipo_pregunta == 3) {
                if (request()->opcionesSeleccionadas !== null) {
                    try {
                        DB::beginTransaction();
                        $opciones_seleccionadas = explode(",", request()->opcionesSeleccionadas);
                        $opciones_guardadas = DB::table('control_opciones_seleccionadas_det')
                            ->where('RespuestaID', '=', $respuesta_id)
                            ->get();
                        if (count($opciones_guardadas) > 0) {
                            foreach ($opciones_guardadas as $opcion) {
                                DB::table('control_opciones_seleccionadas_det')
                                    ->where('OpcionSeleccionadaID', '=', $opcion->OpcionSeleccionadaID)
                                    ->where('Seleccionado', '=', 1)
                                    ->update([
                                        'Seleccionado' => 0,
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                            foreach ($opciones_guardadas as $opcion3) {
                                $seleccionado = 0;
                                foreach ($opciones_seleccionadas as $opcion2) {
                                    $opcion3Name = DB::table('control_opciones_pregunta')->where('OpcionID', '=', $opcion3->OpcionID)->first();
                                    if ($seleccionado == 0) {
                                        if ($opcion3Name->Opcion == $opcion2) {
                                            $seleccionado = 1;
                                        }
                                    }
                                }
                                DB::table('control_opciones_seleccionadas_det')
                                    ->where('OpcionSeleccionadaID', '=', $opcion3->OpcionSeleccionadaID)
                                    ->update([
                                        'Seleccionado' => $seleccionado,
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                            DB::commit();
                        } else {
                            $opciones_pregunta = DB::table('control_opciones_pregunta')->get();
                            $data = [];
                            foreach ($opciones_pregunta as $opcion) {
                                $seleccionado = 0;
                                foreach ($opciones_seleccionadas as $opcion2) {
                                    if ($seleccionado == 0) {
                                        if ($opcion->Opcion == $opcion2) {
                                            $seleccionado = 1;
                                        }
                                    }
                                }
                                $temp = [
                                    'OpcionID' => $opcion->OpcionID,
                                    'RespuestaID' => $respuesta_id,
                                    'Seleccionado' => $seleccionado,
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                array_push($data, $temp);
                            }
                            DB::table('control_opciones_seleccionadas_det')->insert($data);
                            DB::commit();
                        }
                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::error("ERROR | NO SE PUDO GUARDAR LAS OPCIONES A LA RESPUESTA | {$e->getMessage()}");
                    }
                } else {
                    try {
                        DB::beginTransaction();
                        $opciones_guardadas = DB::table('control_opciones_seleccionadas_det')
                            ->where('RespuestaID', '=', $respuesta_id)
                            ->get();
                        if (count($opciones_guardadas) > 0) {
                            foreach ($opciones_guardadas as $opcion) {
                                DB::table('control_opciones_seleccionadas_det')
                                    ->where('OpcionSeleccionadaID', '=', $opcion->OpcionSeleccionadaID)
                                    ->where('Seleccionado', '=', 1)
                                    ->update([
                                        'Seleccionado' => 0,
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                        }
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::error("ERROR | NO SE PUDO GUARDAR LAS OPCIONES A LA RESPUESTA | {$e->getMessage()}");
                    }
                }
            }
            return response()->json([
                'status' => 'Ok',
                'respuesta_id' => $respuesta_id
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Genera el hash de cada uno de los archivos que se suben al servidor
     */
    public function getHash($path)
    {
        $algoritmo = 'sha256';
        $content = Storage::disk('public')->get($path);
        $hash = hash($algoritmo, $content);
        return strtoupper($hash);
    }

    /**
     * Retornar los anexos de la respuesta
     */
    public function getAnexos()
    {
        $anexos = DB::table('control_anexo_det')
            ->where('RespuestaID', '=', request()->respuesta_id)
            ->orWhere('RespuestaID', '=', request()->respuesta_id2)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'anexos' => $anexos
        ], 200);
    }

    /**
     * Borrar anexo mandado
     */
    public function deleteAnexos()
    {
        $anexo = DB::table('control_anexo_det')
            ->where('AnexoID', '=', request()->anexo_id)
            ->first();
        $auditoria = (int)explode('/', $anexo->Ruta)[2];
        $is_etapa_enlace = $this->etr->isEtapaEnlace($auditoria);
        if ($is_etapa_enlace == false) {
            Log::info("ERROR | La etapa actual de auditoria no se encuentra con el enlace");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
        //Si la etapa de enlace es 1 obtener la fecha de vigencia
        $etapa_actual = $this->etr->getEtapaActualAuditoria($auditoria);
        $fechas_vigencia_cuestionario = $this->ar->getAuditoriaFechas($auditoria);
        $fechas = [];
        //Obteher fechas de la etapa vigente
        foreach ($fechas_vigencia_cuestionario as $fecha) {
            if (count($fechas) == 0) {
                if ($fecha['etapa'] == $etapa_actual->NoEtapa) {
                    $fechas = $fecha;
                }
            }
        }
        $fecha_limite = \Carbon\Carbon::parse($fechas['fecha_final'])->startOfDay();
        $fecha_limite = $fecha_limite->copy()->endOfDay();
        $carbon = new \Carbon\Carbon();
        $now = $carbon->now();
        if ($now->gt($fecha_limite)) {
            Log::info("ERROR | La etapa fecha para eliminar anexos ha expirado");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
        $anexo = DB::table('control_anexo_det')
            ->where('AnexoID', '=', request()->anexo_id)
            ->first();
        //BORRAR ANEXO DE BASE DE DATOS
        DB::table('control_anexo_det')->where('AnexoID', '=', $anexo->AnexoID)->delete();
        //ACTUALIZAR NUMERO DE ANEXOS A LA RESPUESTA
        $respuesta = DB::table('control_respuestas')
            ->where('RespuestaID', '=', $anexo->RespuestaID)
            ->first();
        $noAnexos = $respuesta->No_Anexos;
        DB::table('control_respuestas')
            ->where('RespuestaID', '=', $anexo->RespuestaID)
            ->update([
                'No_Anexos' => $noAnexos - 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        try {
            Storage::disk('public')->delete($anexo->Ruta);
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | NO SE PUDO BORRAR EL DOCUMENTO FISICO | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Enviar cuestionario
     */
    public function enviarCuestionario()
    {
        try {
            DB::beginTransaction();
            //Obtener temas de cuestionario
            $temas = DB::select('call sp_estatus_cuestionario(?)', [request()->auditoria]);
            $data = [];
            $noAnexosTotal = 0;
            $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria)->DescripcionEtapa;
            if($etapa == 'Solventación de CRP') {
                $totalPreguntas = 0;
                foreach($temas as $tema) {
                    $totalPreguntas = $tema->NoPregunta + $totalPreguntas;
                }
                $puntuacion_maxima = (float) number_format((float) (100/ $totalPreguntas), 2, '.', '');
                for ($i = 0; $i < count($temas); $i++) {
                    //Obtener las respuestas por tema y auditoria
                    $tema = $temas[$i];
                    $data_tema = [];
                    $data_tema['Tema'] = $tema->Tema;
                    $preguntas = DB::select('call sp_obtenerRecomendacionesPorTema(?, ?)', [request()->auditoria, $tema->TemaID]);
                    $preguntasSolventadas = array_values(array_filter($preguntas, function($var) use ($puntuacion_maxima) {
                        return $var->PuntosCRP < $puntuacion_maxima;
                    }));
                    $preguntas_anexos = [];
                    for ($j = 0; $j < count($preguntasSolventadas); $j++) {
                        $pregunta = $preguntasSolventadas[$j];
                        $pregunta_data = [];
                        if ($pregunta->solvNo_Anexos > 0) {
                            $anexos = DB::table('control_anexo_det')
                                ->where('RespuestaID', '=', $pregunta->solvRespuestaID)
                                ->get();
                            $noAnexosTotal = $noAnexosTotal + $pregunta->solvNo_Anexos;
                        } else {
                            $anexos = [];
                        }
                        $pregunta_data['pregunta'] = $pregunta;
                        $pregunta_data['anexos'] = $anexos;
                        array_push($preguntas_anexos, $pregunta_data);
                    }
                    $data_tema['preguntas'] = $preguntas_anexos;
                    array_push($data, $data_tema);
                }
                //Obtener al enlace de la auditoria
                $asignacion_personal_enlace = DB::table('control_asignacion_personal_auditoria')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->where('Enlace', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
                $enlace_auditoria = DB::connection('main')
                    ->table('osaf_v_enlaces')
                    ->where('FuncionarioID', '=', $asignacion_personal_enlace->UsuarioID)
                    ->first();
                //Finalizar cuiestionario, cambiando la etapa
                $auditoriaId = request()->auditoria;
                $this->markEtapaCompleted($auditoriaId);
                // ------------------------------------------------------
                // GENERAR DOCUMENTO DE RESPUESTAS
                // ------------------------------------------------------
                $auditoria = DB::table('v_control_auditorias')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->first();

                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_solventacion', compact('auditoria', 'data', 'enlace_auditoria'));
                $url = "control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/";
                $path = public_path($url);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "solventacion_respuestas_auditoria_control_interno.pdf");
                $path_acuse_respuestas = $url . "solventacion_respuestas_auditoria_control_interno.pdf";
                // ------------------------------------------------------
                // GENERAR ACUSE DE RECIBO
                // ------------------------------------------------------
                $hash = $this->getHash($path_acuse_respuestas);
                //Guardar el hash en la tabla de cuestionario
                DB::table('control_auditorias')
                    ->where('AuditoriaID', '=', $auditoria->AuditoriaID)
                    ->update([
                        'HashSolventacion' => $hash,
                        'updated_at' => date('Y-m-d H:i:s')
                ]);
                //Generar el documento de acuse final
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_final', compact('auditoria', 'hash', 'enlace_auditoria', 'noAnexosTotal'));
                $path = public_path("control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/");
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "acuse_solventacion_auditoria_control_interno.pdf");
                DB::commit();

            } else {
                for ($i = 0; $i < count($temas); $i++) {
                    //Obtener las respuestas por tema y auditoria
                    $tema = $temas[$i];
                    $data_tema = [];
                    $data_tema['Tema'] = $tema->Tema;
                    $preguntas = DB::select('call sp_obtener_preguntas_tema(?, ?)', [request()->auditoria, $tema->TemaID]);
                    $preguntas_anexos = [];
                    for ($j = 0; $j < count($preguntas); $j++) {
                        $pregunta = $preguntas[$j];
                        $pregunta_data = [];
                        if ($pregunta->No_Anexos > 0) {
                            $anexos = DB::table('control_anexo_det')
                                ->where('RespuestaID', '=', $pregunta->RespuestaID)
                                ->get();
                            $noAnexosTotal = $noAnexosTotal + $pregunta->No_Anexos;
                        } else {
                            $anexos = [];
                        }
                        $pregunta_data['pregunta'] = $pregunta;
                        $pregunta_data['anexos'] = $anexos;
                        array_push($preguntas_anexos, $pregunta_data);
                    }
                    $data_tema['preguntas'] = $preguntas_anexos;
                    array_push($data, $data_tema);
                }
                //Obtener al enlace de la auditoria
                $asignacion_personal_enlace = DB::table('control_asignacion_personal_auditoria')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->where('Enlace', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
                $enlace_auditoria = DB::connection('main')
                    ->table('osaf_v_enlaces')
                    ->where('FuncionarioID', '=', $asignacion_personal_enlace->UsuarioID)
                    ->first();
                //Finalizar cuiestionario, cambiando la etapa
                $auditoriaId = request()->auditoria;
                $this->markEtapaCompleted($auditoriaId);
                // ------------------------------------------------------
                // GENERAR DOCUMENTO DE RESPUESTAS
                // ------------------------------------------------------
                $auditoria = DB::table('v_control_auditorias')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->first();

                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria', compact('auditoria', 'data', 'enlace_auditoria'));
                $url = "control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/";
                $path = public_path($url);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "respuestas_auditoria_control_interno.pdf");
                $path_acuse_respuestas = $url . "respuestas_auditoria_control_interno.pdf";
                // ------------------------------------------------------
                // GENERAR ACUSE DE RECIBO
                // ------------------------------------------------------
                $hash = $this->getHash($path_acuse_respuestas);
                //Guardar el hash en la tabla de cuestionario
                DB::table('control_auditorias')->where('AuditoriaID', '=', $auditoria->AuditoriaID)->update([
                    'Hash' => $hash,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                //Generar el documento de acuse final
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_final', compact('auditoria', 'hash', 'enlace_auditoria', 'noAnexosTotal'));
                $path = public_path("control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/");
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "acuse_auditoria_control_interno.pdf");
                DB::commit();
            }
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | NO SE PUDO GENERAR EL ACUSE DE RECIBO | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las respuestas iniciales y solventacion de recomendaciones
     */
    public function getRespuestasSolventaciones()
    {
        $user = request()->user();
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $userId = $user->UsuarioID;
        $ejercicioId = $ejercicio_auditado->EjercicioID;
        //Respuestas iniciales
        $no_respuestas_1 = $this->ar->getCountRespuestas(3, $userId, $ejercicioId);
        $no_respuestas_2 = $this->ar->getCountRespuestas(4, $userId, $ejercicioId);
        $no_respuestas_3 = $this->ar->getCountRespuestas(5, $userId, $ejercicioId);
        $no_respuestas   = $no_respuestas_1 + $no_respuestas_2 + $no_respuestas_3;
        // Solventaciones
        $no_solventaciones1 = $this->ar->getCountRespuestas(8, $userId, $ejercicioId);
        $no_solventaciones2 = $this->ar->getCountRespuestas(9, $userId, $ejercicioId);
        $no_solventaciones = $no_solventaciones1 + $no_solventaciones2;
        return response()->json([
            'status' => 'Ok',
            'noRespuestas' => $no_respuestas,
            'noSolventaciones' => $no_solventaciones
        ], 200);
    }

    /**
     * Obtener respuestas iniciales de auditoria
     */
    public function getRespuestasInicales()
    {
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $respuestas = DB::select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [3, $ejercicio_auditado->EjercicioID]);
        foreach ($respuestas as $respuesta) {
            $respuesta->temas = DB::select('call sp_estatus_cuestionario(?)', [$respuesta->AuditoriaID]);
            $respuesta->auditor = $this->ar->getAuditorAsignado($respuesta->AuditoriaID);
        }
        // Preparar respuestas revisadas
        $revisadas_generar_crp = DB::select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [4, $ejercicio_auditado->EjercicioID]);
        $revisadas_enviar_crp  = DB::select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [5, $ejercicio_auditado->EjercicioID]);
        $revisadas = [];
        foreach ($revisadas_generar_crp as $generar) {
            $generar->etapa = 4;
            array_push($revisadas, $generar);
        }
        foreach ($revisadas_enviar_crp as $enviar) {
            $enviar->etapa = 5;
            array_push($revisadas, $enviar);
        }
        //Respuestas iniciales
        return response()->json([
            'status' => 'Ok',
            'respuestas' => $respuestas,
            'revisadas' => $revisadas
        ], 200);
    }

    /**
     * Obtener solventaciones de auditoría
     */
    public function getRespuestasSolventacion()
    {
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $respuestas = DB::select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [8, $ejercicio_auditado->EjercicioID]);
        foreach ($respuestas as $respuesta) {
            $respuesta->temas = DB::select('call sp_estatus_cuestionario(?)', [$respuesta->AuditoriaID]);
            $respuesta->auditor = $this->ar->getAuditorAsignado($respuesta->AuditoriaID);
        }
        // Preparar respuestas revisadas
        $revisadas_generar_informe = DB::select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [9, $ejercicio_auditado->EjercicioID]);
        $finalizadas = DB::table('control_auditoria_etapa_det as caed')
            ->join('v_control_auditorias as vca', 'caed.AuditoriaID', '=', 'vca.AuditoriaID')
            ->where('caed.EtapaID', '=', 9)
            ->where('caed.Finalizado', '=', 1)
            ->where('vca.Ejercicio', '=', $ejercicio_auditado->Year)
            ->select('vca.*')
            ->get();
        //Respuestas iniciales
        return response()->json([
            'status' => 'Ok',
            'respuestas'  => $respuestas,
            'revisadas'   => $revisadas_generar_informe,
            'finalizadas' => $finalizadas
        ], 200);
    }


    /**
     * Validar que la auditoria exista y este asignada al responsable
     */
    public function validateAuditoria()
    {
        $validate = false;
        $user = request()->user();
        $auditoria = DB::table('control_asignacion_personal_auditoria')
            ->where('AuditoriaID', '=', request()->auditoria)
            ->where('Enlace', '=', 0)
            ->where('ResponsableAuditoria', '=', 1)
            ->where('UsuarioID', '=', $user->UsuarioID)
            ->whereNull('deleted_at')
            ->first();
        if (is_object($auditoria)) {
            $validate = true;
        }
        return response()->json([
            'status' => 'Ok',
            'validate' => $validate
        ], 200);
    }

    /**
     * Obtener el detalle de la auditoria
     */
    public function getGeneralDataAuditoria()
    {
        $auditoria = $this->ar->getAuditoriaDetalle(request()->auditoria);
        $enlace = DB::connection('main')
            ->table('osaf_v_enlaces')
            ->where('FuncionarioID', '=', $auditoria->Enlace)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'auditoria' => $auditoria,
            'enlace' => $enlace
        ]);
    }

    /**
     * Obtener preguntas del cuestionario por temas para su revision
     */
    public function getPreguntasTemaRevision()
    {
        $preguntas = DB::select('call sp_respuestasTemaRevision(?,?,?)', [
            request()->formTopic['temaId'],
            request()->formTopic['auditoriaId'],
            'Responsable'
        ]);

        return response()->json([
            'status' => 'Ok',
            'preguntas' => $preguntas
        ], 200);
    }

    /**
     * Guardar valoración del auditor
     */
    public function saveValoracion()
    {
        try {
            $user = request()->user();
            $etapa_actual       = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            //DATOS PREGUNTA
            if($etapa_actual->EtapaID < 8) {
                $respuesta_id = request()->pregunta['RespuestaID'];
            } else {
                $respuesta_id = request()->pregunta['solvRespuestaID'];
            }
            $usuario_id = $user->UsuarioID;
            $recomendacion = request()->recomendacion ?? '';
            $valoracion = request()->valoracion;
            $puntuacion = (float)request()->puntuacion;
            $recomendacion_id = request()->pregunta['recomResponRecomendacionID'];
            //dd($respuesta_id, $usuario_id, $recomendacion_id);
            // REGISTRAR LA RESPUESTA
            $resp = DB::select('call sp_registrarRecomendacion(?,?,?,?,?,?,?,?)', [
                $respuesta_id,
                $usuario_id,
                'Responsable',
                $recomendacion,
                $valoracion,
                $puntuacion,
                $etapa_actual->AuditoriaEtapaID,
                $recomendacion_id
            ]);
            if ($recomendacion_id !== null) {
                $recomendacion_id = $recomendacion_id;
            } else {
                $recomendacion_id = $resp[0]->recomendacion_id;
            }
            return response()->json([
                'status' => 'Ok',
                'recomendacion_id' => $recomendacion_id
            ], 200);
        } catch (\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener los archivos de la primera vuelta
     */
    public function getFilesCrp()
    {
        $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria);
        $ejercicio = $this->er->getEjercicioAuditado();
        $files = null;
        if($etapa->NoEtapa > 1) {
            $auditoria = DB::table('control_auditorias')
                ->where('AuditoriaID', '=', request()->auditoria)
                ->first();
            if(is_null($auditoria->Hash) == false) {
                //Acuse de respuestas
                $files = true;
            } else {
                $files = null;
            }
        }
        return response()->json([
            'status' => 'Ok',
            'files' => $files,
            'ejercicio' => $ejercicio->Year
        ], 200);
    }

    /**
     * Descargar acuse de respuestas de desempenio
     */
    public function descargarRespuestasCrp()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditoria = request()->auditoria;
        $file= public_path(). "/control_interno/{$ejercicio->Year}/{$auditoria}/acuses/respuestas_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_control_interno.pdf', $headers);
    }

    /**
     * Descargar acuse de respuestas de desempenio
     */
    public function descargarAcuseCrp()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditoria = request()->auditoria;
        $file= public_path(). "/control_interno/{$ejercicio->Year}/{$auditoria}/acuses/acuse_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_control_interno.pdf', $headers);
    }

    /**
     * Obtener datos para la seccion de monitoreo de auditorias de control interno
     */
    public function getDataAuditoriasMonitoreo()
    {
        // Obtener ejercicio fiscal, etapas y auditores
        $ejercicio = $this->er->getEjercicioAuditado();
        $etapas = $this->etr->getEtapas();
        $auditores = $this->ar->getAuditores();
        return response()->json([
            'status' => 'Ok',
            'ejercicio_fiscal' => 2023,//$ejercicio->Year,
            'etapas' => $etapas,
            'auditores' => $auditores
        ], 200);
    }

    /**
     * Obtener las auditorias para la sección de monitoreo
     */
    public function getAuditoriasMonitoreo()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditorias = DB::table('v_monitoreo_auditoria')
            ->where('Anio', '=', $ejercicio->Year)
            ->where(function($query) {
                if(!is_null(request()->search)) {
                    $query->where('Nombre', 'like', '%' . request()->search . '%');
                }
                if(count(request()->etapas) > 0) {
                    $query->whereIn('etapa_actual', request()->etapas);
                }
                if(count(request()->auditores) > 0) {
                    $query->whereIn('Auditor', request()->auditores);
                }
            })
            ->orderBy('Nombre', 'asc')
            ->get();
        foreach ($auditorias as $auditoria) {
            $auditoria->estatus = DB::table('v_respuestas_recomendaciones')
                ->where('AuditoriaID','=', $auditoria->AuditoriaID)
                ->get();
        }
        return response()->json([
            'status' => 'Ok',
            'auditorias' => $auditorias
        ], 200);
    }

    /**
     * Obtener las auditorías del año actual
     */
    public function getAuditoriasAnioActual()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditorias = DB::table('v_monitoreo_auditoria')
            ->where('Anio', '=', $ejercicio->Year)
            ->orderBy('Nombre', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'auditorias' => $auditorias
        ], 200);
    }

    /**
     * Verifica si la auditoria cuenta con expedientes
     */
    public function getExpedientes()
    {
        // Consultar expedientes CRP
        $expedientesCrp = false;
        $expedientesSolventacion = false;
        $auditoria = DB::table('control_auditorias')
            ->where('AuditoriaID', '=', request()->AuditoriaID)
            ->first();
        if(!is_null($auditoria->Hash)) {
            $expedientesCrp = true;
        }
        if(!is_null($auditoria->HashSolventacion)) {
            $expedientesSolventacion = true;
        }
        return response()->json([
            'status' => 'Ok',
            'hasExpedienteCrp' => $expedientesCrp,
            'hasExpedientesSolventacion' => $expedientesSolventacion
        ], 200);
    }

    /**
     * Genercion de la CRP
     */
    public function generateCrp()
    {
        try {
            DB::beginTransaction();
            // Datos de la auditoría
            $auditoria = DB::table('v_control_auditorias')
                ->where('AuditoriaID', '=', request()->AuditoriaID)
                ->first();
            // Datos de preguntas del cuestionario
            $temas = DB::select('call sp_estatus_cuestionario(?)', [request()->AuditoriaID]);
            $data = [];
            $noAnexosTotal = 0;
            for ($i = 0; $i < count($temas); $i++) {
                //Obtener las respuestas por tema y auditoria
                $tema = $temas[$i];
                $data_tema = [];
                $data_tema['Tema'] = $tema->Tema;
                $preguntas = DB::select('call sp_respuestasTemaRevision(?, ?, ?)', [$tema->TemaID, request()->AuditoriaID, 'Responsable']);
                $preguntas_anexos = [];
                for ($j = 0; $j < count($preguntas); $j++) {
                    $pregunta = $preguntas[$j];
                    $pregunta_data = [];
                    if ($pregunta->No_Anexos > 0) {
                        $anexos = DB::table('control_anexo_det')
                            ->where('RespuestaID', '=', $pregunta->RespuestaID)
                            ->get();
                        $noAnexosTotal = $noAnexosTotal + $pregunta->No_Anexos;
                    } else {
                        $anexos = [];
                    }
                    $pregunta_data['pregunta'] = $pregunta;
                    $pregunta_data['anexos'] = $anexos;
                    array_push($preguntas_anexos, $pregunta_data);
                }
                $data_tema['preguntas'] = $preguntas_anexos;
                array_push($data, $data_tema);
            }
            //Importar template
            $path_template = public_path('control_interno/template_crp_2021.docx');
            $templateProcessor = new TemplateProcessor($path_template);
            //Remplazar valores
            $templateProcessor->setValue('nombreEnte', $auditoria->Entidad);
            // ---------------------------------------------------------------------
            //                  AMBIENTE DE CONTROL
            // ---------------------------------------------------------------------
            $templateProcessor->setValue('tema_ambiente',  mb_strtoupper("1. ".$data[0]['Tema'], 'UTF-8'));
            //Preparar preguntas ambiente de control
            $preguntas_ambiente = $data[0]['preguntas'];
            $data_ambiente = $this->setPreguntasCrp('ambiente', $preguntas_ambiente, 1);
            $templateProcessor->cloneBlock('block_pregunta_ambiente', 0, true, false, $data_ambiente);
            // ---------------------------------------------------------------------
            //                  ADMINISTRACION DE RIESGOS
            // ---------------------------------------------------------------------
            $templateProcessor->setValue('tema_administracion',  mb_strtoupper("2. ".$data[1]['Tema'], 'UTF-8'));
            //Preparar preguntas administracion
            $preguntas_administracion = $data[1]['preguntas'];
            $data_administracion = $this->setPreguntasCrp('administracion', $preguntas_administracion, 2);
            $templateProcessor->cloneBlock('block_pregunta_administracion', 0, true, false, $data_administracion);
            // ---------------------------------------------------------------------
            //              ACTIVIDADES DE CONTROL
            // ---------------------------------------------------------------------
            $templateProcessor->setValue('tema_actividades',  mb_strtoupper("3. ".$data[2]['Tema'], 'UTF-8'));
            //Preparar preguntas actividades
            $preguntas_actividades = $data[2]['preguntas'];
            $data_actividades = $this->setPreguntasCrp('actividades', $preguntas_actividades, 3);
            $templateProcessor->cloneBlock('block_pregunta_actividades', 0, true, false, $data_actividades);
            // ---------------------------------------------------------------------
            //              INFORMACIÓN Y COMUNICACIÓN
            // ---------------------------------------------------------------------
            $templateProcessor->setValue('tema_informacion',  mb_strtoupper("4. ".$data[3]['Tema'], 'UTF-8'));
            $preguntas_informacion = $data[3]['preguntas'];
            $data_informacion = $this->setPreguntasCrp('informacion', $preguntas_informacion, 4);
            $templateProcessor->cloneBlock('block_pregunta_informacion', 0, true, false, $data_informacion);
            // ---------------------------------------------------------------------
            // SUPERVICIÓN
            // ---------------------------------------------------------------------
            $templateProcessor->setValue('tema_supervicion',  mb_strtoupper("5. ".$data[4]['Tema'], 'UTF-8'));
            $preguntas_supervicion = $data[4]['preguntas'];
            $data_supervicion = $this->setPreguntasCrp('supervicion', $preguntas_supervicion, 5);
            $templateProcessor->cloneBlock('block_pregunta_supervicion', 0, true, false, $data_supervicion);
            // Generar documento de word
            $path_file = public_path("control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/CedulaDeResultadosPreliminares.docx");
            $templateProcessor->saveAs($path_file);
            //Finalizar cuiestionario, cambiando la etapa
            $auditoriaId = request()->AuditoriaID;
            $this->markEtapaCompleted($auditoriaId);
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Preparar preguntas para la crp
     */
    public function setPreguntasCrp($seccion, $preguntas, $seccionNumber)
    {
        $data = [];
        for($i = 0; $i < count($preguntas); $i++) {
            $pregunta = $preguntas[$i]['pregunta'];
            $tipo_pregunta = $pregunta->TipoPreguntaID;
            $position = $i + 1;
            $noPregunta = $seccionNumber.".". $position;
            if($tipo_pregunta == 1) {
                if(is_null($pregunta->Subpregunta)) {
                    array_push($data, [
                        "pregunta_{$seccion}"             => $this->htmltodocx_clean_text($noPregunta." ".$pregunta->Pregunta),
                        "respuesta_{$seccion}"            => $this->htmltodocx_clean_text(''),
                        "subrespuesta_{$seccion}"         => $this->htmltodocx_clean_text($pregunta->Respuesta),
                        "recomendacion_titulo_{$seccion}" => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Cumplimiento' : 'Recomendación',
                        "recomendacion_{$seccion}"        => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Del análisis de la respuesta y evidencia proporcionada por el ente fiscalizado se determina que cumple con lo solicitado en la pregunta correspondiente.' : $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                    ]);
                } else {
                    $array_temp = [
                        "pregunta_{$seccion}"             => $this->htmltodocx_clean_text($noPregunta." ".$pregunta->Pregunta),
                        "respuesta_{$seccion}"            => $this->htmltodocx_clean_text(($pregunta->Condicional == 1) ? 'Si' : 'No'),
                        "recomendacion_titulo_{$seccion}" => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Cumplimiento' : 'Recomendación',
                        "recomendacion_{$seccion}"        => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Del análisis de la respuesta y evidencia proporcionada por el ente fiscalizado se determinaque cumple con lo solicitado en la pregunta correspondiente.' : $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion)
                    ];
                    if($pregunta->Condicional == 1) {
                        $array_temp["subrespuesta_{$seccion}"]  = $this->htmltodocx_clean_text($pregunta->Respuesta);
                    } else {
                        if(is_null($pregunta->FechaMecanismos) == false) {
                            $fecha = explode('-', $pregunta->FechaMecanismos);
                            $fechaMecanismos = $fecha[2]."/".$fecha[1]."/".$fecha[0];
                        } else {
                            $fechaMecanismos = 'No fue definida';
                        }
                        $array_temp["subrespuesta_{$seccion}"] = $this->htmltodocx_clean_text($pregunta->Respuesta).' - Fecha de atención: '.$fechaMecanismos;
                    }
                    array_push($data, $array_temp);
                }
            } else {
                $array_temp = [
                    "pregunta_{$seccion}"     => $this->htmltodocx_clean_text($noPregunta." ".$pregunta->Pregunta),
                    "respuesta_{$seccion}"    => $this->htmltodocx_clean_text(($pregunta->Condicional == 1) ? 'Si' : 'No'),
                    "recomendacion_titulo_{$seccion}" => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Cumplimiento' : 'Recomendación',
                    "recomendacion_{$seccion}"        => ($pregunta->recomAuditorPuntuacion == 1.96) ? 'Del análisis de la respuesta y evidencia proporcionada por el ente fiscalizado se determina quecumple con lo solicitado en la pregunta correspondiente.' : $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion)
                ];
                if($pregunta->Condicional == 1) {
                    $array_temp["subrespuesta_{$seccion}"] = $this->htmltodocx_clean_text($pregunta->Respuesta);
                } else {
                    if(is_null($pregunta->FechaMecanismos) == false) {
                        $fecha = explode('-', $pregunta->FechaMecanismos);
                        $fechaMecanismos = $fecha[2]."/".$fecha[1]."/".$fecha[0];
                    } else {
                        $fechaMecanismos = 'No fue definida';
                    }
                    $array_temp["subrespuesta_{$seccion}"] = $this->htmltodocx_clean_text($pregunta->Respuesta).' - Fecha de atención: '.$fechaMecanismos;
                }
                array_push($data, $array_temp);
            }
        }
        return $data;
    }

    public function htmltodocx_clean_text($text) {
        // Replace each &nbsp; with a single space:
        $xml = array('&#34;','&#38;','&#38;','&#60;','&#62;','&#160;','&#161;','&#162;','&#163;','&#164;','&#165;','&#166;','&#167;','&#168;','&#169;','&#170;','&#171;','&#172;','&#173;','&#174;','&#175;','&#176;','&#177;','&#178;','&#179;','&#180;','&#181;','&#182;','&#183;','&#184;','&#185;','&#186;','&#187;','&#188;','&#189;','&#190;','&#191;','&#192;','&#193;','&#194;','&#195;','&#196;','&#197;','&#198;','&#199;','&#200;','&#201;','&#202;','&#203;','&#204;','&#205;','&#206;','&#207;','&#208;','&#209;','&#210;','&#211;','&#212;','&#213;','&#214;','&#215;','&#216;','&#217;','&#218;','&#219;','&#220;','&#221;','&#222;','&#223;','&#224;','&#225;','&#226;','&#227;','&#228;','&#229;','&#230;','&#231;','&#232;','&#233;','&#234;','&#235;','&#236;','&#237;','&#238;','&#239;','&#240;','&#241;','&#242;','&#243;','&#244;','&#245;','&#246;','&#247;','&#248;','&#249;','&#250;','&#251;','&#252;','&#253;','&#254;','&#255;');
        $html = array('&quot;','&amp;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;');
        $text = str_replace($html,$xml,$text);
        $text = str_ireplace($html,$xml,$text);
        if (strpos($text, '<') !== FALSE) {
            // We only run strip_tags if it looks like there might be some tags in the text
            // as strip_tags is expensive:
            $text = strip_tags($text);
        }
        // Strip out extra spaces:
        $text = preg_replace('/\s+/u', ' ', $text);
        // Convert entities:
        $text = html_entity_decode($text, ENT_COMPAT, 'UTF-8');
        return $text;
    }

    /**
     * Dercargar CRP
     */
    public function descargarCrp()
    {
        $auditoria = request()->AuditoriaID;
        $ejercicio = $this->er->getEjercicioAuditado();
        $file = public_path("control_interno/{$ejercicio->Year}/{$auditoria}/CedulaDeResultadosPreliminares.docx");
        return \Response::download($file);
    }

    /**
     * Envíar crp como enviada
     */
    public function markSentCrp()
    {
        try {
            DB::beginTransaction();
            $auditoriaId = request()->AuditoriaID;
            $this->markEtapaCompleted($auditoriaId);
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Marcar estapa actual como finalizada
     */
    public function markEtapaCompleted($auditoriaId)
    {
        //Finalizar cuiestionario, cambiando la etapa
        $etapa_actual = $this->etr->getEtapaActualAuditoria($auditoriaId);
        $etapa_id = $etapa_actual->EtapaID;
        DB::table('control_auditoria_etapa_det')
            ->where('EtapaID', '=', $etapa_id)
            ->where('AuditoriaID', '=', $auditoriaId)
            ->update([
                'Finalizado' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * Marcar auditoria como revisada
     */
    public function marcarAuditoriaRevisada()
    {
        try {
            DB::beginTransaction();
            $auditoriaId = request()->auditoria;
            $this->markEtapaCompleted($auditoriaId);
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Generar informe de auditoria
     */
    public function generateInforme()
    {
        try {
            DB::beginTransaction();
            // Datos de la auditoría
            $auditoria = DB::table('v_control_auditorias')
                ->where('AuditoriaID', '=', request()->AuditoriaID)
                ->first();
            $auditoria_control = DB::table('control_auditorias')->where('AuditoriaID', '=', request()->AuditoriaID)->first();
            $auditoria->noExpediente = $auditoria_control->NoExpediente;
            //Temas de la auiditoría
            $temas = DB::select('call sp_estatus_cuestionario(?)', [request()->AuditoriaID]);
            $data = [];
            //Setear datos y preguntas de los temas
            for ($i = 0; $i < count($temas); $i++) {
                //Obtener las respuestas por tema y auditoria
                $tema = $temas[$i];
                $preguntas = DB::select('call sp_respuestasTemaRevision(?, ?, ?)', [$tema->TemaID, request()->AuditoriaID, 'Responsable']);
                $preguntas =  collect($preguntas);
                $preguntas_solventadas = $preguntas->where('recomResponRecomendacionID', '==', null);
                $total = $preguntas_solventadas->sum('crpPuntuacion');
                $preguntas_no_solventadas = $preguntas->where('recomResponRecomendacionID', '!=', null);
                $total = $total + $preguntas_no_solventadas->sum('recomResponPuntos');
                $data[$i]['tema']           = $tema->Tema;
                $data[$i]['preguntas']      = $preguntas;
                $data[$i]['solventadas']    = $preguntas_solventadas;
                $data[$i]['no_solventadas'] = $preguntas_no_solventadas;
                $data[$i]['total'] = $total;
            }
            //Setear datos generales del documento word
            $total = 0;
            for($j = 0; $j < count($data); $j++) {
                $total = (float) $total + $data[$j]['total'];
            }
            // Importar template
            $path_template = public_path('control_interno/template_informe_2021.docx');
            $templateProcessor = new TemplateProcessor($path_template);
            $templateProcessor->setValue('total', $total);
            $templateProcessor->setValue('nombreEnte', $auditoria->Entidad);
            $templateProcessor->setValue('no_expediente', "F7-{$auditoria->noExpediente}");
            //Template2
            $path_template2 = public_path('control_interno/template_informe2_2021.docx');
            $templateProcessor2 = new TemplateProcessor($path_template2);
            $preguntas_ambiente = [];
            $preguntas_administracion_riesgos = [];
            $preguntas_actividades_control = [];
            $preguntas_informacion_comunicacion = [];
            $preguntas_supervicion = [];
            $contador = 0;
            foreach($data as $tema) {
                $nombre_tema = trim($tema['tema']);
                if($nombre_tema == 'AMBIENTE DE CONTROL') {
                    $preguntas = $tema['preguntas'];
                    foreach($preguntas as $pregunta) {
                        $puntuacion = (float) (is_null($pregunta->recomResponRecomendacionID)) ? $pregunta->crpPuntuacion : $pregunta->recomResponPuntos;
                        if($puntuacion < 1.96) {
                            $contador = $contador + 1;
                        }
                        $array_temp = [
                            'no_expediente1' => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'Recomendación '.$contador,
                            'pregunta_tema1' => $pregunta->Pregunta,
                            'estado1'        => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'No atendida',
                            'ponderacion1'   => $puntuacion,
                          ];
                        array_push($preguntas_ambiente, $array_temp);
                    }
                }
                else if($nombre_tema == 'Administración de Riesgos') {
                    $preguntas = $tema['preguntas'];
                    foreach($preguntas as $pregunta) {
                        $puntuacion = (float) (is_null($pregunta->recomResponRecomendacionID)) ? $pregunta->crpPuntuacion : $pregunta->recomResponPuntos;
                        if($puntuacion < 1.96) {
                            $contador = $contador + 1;
                        }
                        $array_temp2 = [
                            'no_expediente2' => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'Recomendación '.$contador,
                            'pregunta_tema2' => $pregunta->Pregunta,
                            'estado2'        => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'No atendida',
                            'ponderacion2' => $puntuacion,
                        ];
                        array_push($preguntas_administracion_riesgos, $array_temp2);
                    }
                }
                else if($nombre_tema == 'Actividades de Control') {
                    $preguntas = $tema['preguntas'];
                    foreach($preguntas as $pregunta) {
                        $puntuacion = (float) (is_null($pregunta->recomResponRecomendacionID)) ? $pregunta->crpPuntuacion : $pregunta->recomResponPuntos;
                        if($puntuacion < 1.96) {
                            $contador = $contador + 1;
                        }
                        $array_temp3 = [
                            'no_expediente3' => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'Recomendación '.$contador,
                            'pregunta_tema3' => $pregunta->Pregunta,
                            'estado3'        => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'No atendida',
                            'ponderacion3' => $puntuacion,
                        ];
                        array_push($preguntas_actividades_control, $array_temp3);
                    }
                }
                else if($nombre_tema == 'Información y Comunicación') {
                    $preguntas = $tema['preguntas'];
                    foreach($preguntas as $pregunta) {
                        $puntuacion = (float) (is_null($pregunta->recomResponRecomendacionID)) ? $pregunta->crpPuntuacion : $pregunta->recomResponPuntos;
                        if($puntuacion < 1.96) {
                            $contador = $contador + 1;
                        }
                        $array_temp4 = [
                            'no_expediente4' => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'Recomendación '.$contador,
                            'pregunta_tema4' => $pregunta->Pregunta,
                            'estado4'        => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'No atendida',
                            'ponderacion4' => $puntuacion,
                        ];
                        array_push($preguntas_informacion_comunicacion, $array_temp4);
                    }
                }
                else if($nombre_tema == 'Supervisión') {
                    $preguntas = $tema['preguntas'];
                    foreach($preguntas as $pregunta) {
                        $puntuacion = (float) (is_null($pregunta->recomResponRecomendacionID)) ? $pregunta->crpPuntuacion : $pregunta->recomResponPuntos;
                        if($puntuacion < 1.96) {
                            $contador = $contador + 1;
                        }
                        $array_temp5 = [
                            'no_expediente5' => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'Recomendación '.$contador,
                            'pregunta_tema5' => $pregunta->Pregunta,
                            'estado5'        => ($puntuacion == 1.96) ? 'Sin Hallazgo' : 'No atendida',
                            'ponderacion5' => $puntuacion,
                        ];
                        array_push($preguntas_supervicion, $array_temp5);
                    }
                }
            }
            $templateProcessor->cloneBlock('block_pregunta_tema1', 0, true, false, $preguntas_ambiente);
            $templateProcessor->cloneBlock('block_pregunta_tema2', 0, true, false, $preguntas_administracion_riesgos);
            $templateProcessor->cloneBlock('block_pregunta_tema3', 0, true, false, $preguntas_actividades_control);
            $templateProcessor->cloneBlock('block_pregunta_tema4', 0, true, false, $preguntas_informacion_comunicacion);
            $templateProcessor->cloneBlock('block_pregunta_tema5', 0, true, false, $preguntas_supervicion);
            //TemplateProcessor2
            $templateProcessor2->cloneBlock('block_pregunta_tema1', 0, true, false, $preguntas_ambiente);
            $templateProcessor2->cloneBlock('block_pregunta_tema2', 0, true, false, $preguntas_administracion_riesgos);
            $templateProcessor2->cloneBlock('block_pregunta_tema3', 0, true, false, $preguntas_actividades_control);
            $templateProcessor2->cloneBlock('block_pregunta_tema4', 0, true, false, $preguntas_informacion_comunicacion);
            $templateProcessor2->cloneBlock('block_pregunta_tema5', 0, true, false, $preguntas_supervicion);
            $ns_preguntas_ambiente = [];
            $ns_preguntas_administracion_riesgos = [];
            $ns_preguntas_actividades_control = [];
            $ns_preguntas_informacion_comunicacion = [];
            $ns_preguntas_supervicion = [];
            $contador2 = 0;
            foreach($data as $tema) {
                $nombre_tema = trim($tema['tema']);
                if($nombre_tema == 'AMBIENTE DE CONTROL') {
                   $ns_preguntas = $tema['no_solventadas']->where('recomResponPuntos', '<', 1.96);
                   $ns_preguntas = $ns_preguntas->all();
                   foreach($ns_preguntas as $pregunta) {
                       $contador2 = $contador2 + 1;
                       $array = [
                           'recomendacion1'         => "Recomendación No. {$contador2}",
                           'no_expediente_block_1'  => "No atendida",
                           'motivacion_block_1'     => $this->htmltodocx_clean_text($pregunta->recomResponValoracion),
                           'recomendacion_block_1'  => $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                       ];
                       array_push($ns_preguntas_ambiente, $array);
                   }
                }
                else if($nombre_tema == 'Administración de Riesgos') {
                    $ns_preguntas = $tema['no_solventadas']->where('recomResponPuntos', '<', 1.96);
                    $ns_preguntas = $ns_preguntas->all();
                    foreach($ns_preguntas as $pregunta) {
                        $contador2 = $contador2 + 1;
                        $array2 = [
                            'recomendacion2'        => "Recomendación No. {$contador2}",
                            'no_expediente_block_2' => "No atendida",
                            'motivacion_block_2'    => $this->htmltodocx_clean_text($pregunta->recomResponValoracion),
                            'recomendacion_block_2' => $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                        ];
                        array_push($ns_preguntas_administracion_riesgos, $array2);
                    }
                }
                else if($nombre_tema == 'Actividades de Control') {
                    $ns_preguntas = $tema['no_solventadas']->where('recomResponPuntos', '<', 1.96);
                    $ns_preguntas = $ns_preguntas->all();
                    foreach($ns_preguntas as $pregunta) {
                        $contador2 = $contador2 + 1;
                        $array3 = [
                            'recomendacion3'        => "Recomendación No. {$contador2}",
                            'no_expediente_block_3' => "No atendida",
                            'motivacion_block_3'    => $this->htmltodocx_clean_text($pregunta->recomResponValoracion),
                            'recomendacion_block_3' => $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                        ];
                        array_push($ns_preguntas_actividades_control, $array3);
                    }
                }
                else if($nombre_tema == 'Información y Comunicación') {
                    $ns_preguntas = $tema['no_solventadas']->where('recomResponPuntos', '<', 1.96);
                    $ns_preguntas = $ns_preguntas->all();
                    foreach($ns_preguntas as $pregunta) {
                        $contador2 = $contador2 + 1;
                        $array4 = [
                            'recomendacion4'        => "Recomendación No. {$contador2}",
                            'no_expediente_block_4' => "No atendida",
                            'motivacion_block_4'    => $this->htmltodocx_clean_text($pregunta->recomResponValoracion),
                            'recomendacion_block_4' => $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                        ];
                        array_push($ns_preguntas_informacion_comunicacion, $array4);
                    }
                }
                else if($nombre_tema == 'Supervisión') {
                    $ns_preguntas = $tema['no_solventadas']->where('recomResponPuntos', '<', 1.96);
                    $ns_preguntas = $ns_preguntas->all();
                    foreach($ns_preguntas as $pregunta) {
                        $contador2 = $contador2 + 1;
                        $array5 = [
                            'recomendacion5'        => "Recomendación No. {$contador2}",
                            'no_expediente_block_5' => "No atendida",
                            'motivacion_block_5'    => $this->htmltodocx_clean_text($pregunta->recomResponValoracion),
                            'recomendacion_block_5' => $this->htmltodocx_clean_text($pregunta->recomResponRecomendacion),
                        ];
                        array_push($ns_preguntas_supervicion, $array5);
                    }
                }
            }
            $templateProcessor->cloneBlock('block_pregunta_1', 0, true, false, $ns_preguntas_ambiente);
            $templateProcessor->cloneBlock('block_pregunta_2', 0, true, false, $ns_preguntas_administracion_riesgos);
            $templateProcessor->cloneBlock('block_pregunta_3', 0, true, false, $ns_preguntas_actividades_control);
            $templateProcessor->cloneBlock('block_pregunta_4', 0, true, false, $ns_preguntas_informacion_comunicacion);
            $templateProcessor->cloneBlock('block_pregunta_5', 0, true, false, $ns_preguntas_supervicion);
            // Generar documento de word
            $path_file = public_path("control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/InformeFinal.docx");
            $templateProcessor->saveAs($path_file);
            //Informe final 2
            $path_file2 = public_path("control_interno/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/InformeFina2.docx");
            $templateProcessor2->saveAs($path_file2);
            //Finalizar cuiestionario, cambiando la etapa
            $this->markEtapaCompleted(request()->AuditoriaID);
            DB::commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Descargar Informe final de auditorias
     */
    public function descargarInforme()
    {
        $auditoria = request()->AuditoriaID;
        $ejercicio = $this->er->getEjercicioAuditado();
        $file = public_path("control_interno/{$ejercicio->Year}/{$auditoria}/InformeFinal.docx");
        return \Response::download($file);
    }

    /**
     * Obtener lista de cuestionarios
     */
    public function getCuestionarios(){
        $cuestionarios = $this->ar->getCuestionarios();
        return response()->json([
            'status' => 'Ok',
            'cuestionarios' => $cuestionarios
        ], 200);
    }

    /**
     * Obtener preguntas del cuestionario
     */
    public function getCuestionario(){
        // $cuestionarios = $this->ar->getCuestionario();
        $cuestionarios = DB::select('call sp_obtenerCuestionario(?)', [
            request()->cuestionario
        ]);
        return response()->json([
            'status' => 'Ok',
            'cuestionario' => $cuestionarios
        ], 200);
    }

    /**
     * Obtener temas del cuestionario
     */
    public function getListaTemasCuestionario(){
        $cuestionario = DB::table('control_cuestionario_cat')
            ->where('CuestionarioID','=', request()->cuestionario)
            ->select()
            ->first();
        $cantidad_preguntas = DB::table('control_preguntas_cat as cpc')
            ->join('control_tema_cuestionario_cat as ctcc', function($join) {
                $join->on('cpc.TemaID', '=', 'ctcc.TemaID')
                    ->whereNull('ctcc.deleted_at');
            })
            ->join('control_cuestionario_cat as ccc', 'ctcc.CuestionarioID', '=', 'ccc.CuestionarioID')
            ->whereNull('cpc.deleted_at')
            ->where('ccc.CuestionarioID', '=', $cuestionario->CuestionarioID)
            ->count();
        $asignaciones = DB::table('control_aplicacion_cuestionario')->where('CuestionarioID', '=', $cuestionario->CuestionarioID)->count();
        $cuestionario->cantidad_preguntas = $cantidad_preguntas;
        $cuestionario->asignaciones = $asignaciones;
        $temas = DB::table('control_tema_cuestionario_cat')
            ->where('CuestionarioID','=', request()->cuestionario)
            ->where('deleted_at','=', null)
            ->select()
            ->get();
        foreach ($temas as $tema) {
            $preguntas = DB::table('control_preguntas_cat')
                            ->where('TemaID', '=', $tema->TemaID)
                            ->whereNull('deleted_at')
                            ->get();
            $tema->preguntas = $preguntas;
        }
        return response()->json([
            'status'       => 'Ok',
            'cuestionario' => $cuestionario,
            'temas'        => $temas
        ], 200);
    }

    /**
     * Obtener preguntas de  un tema
     */
    public function getTemaCuestionario(){
        // Obtener las preguntas de un tema en especifico
        $preguntas = DB::table('control_preguntas_cat')
        ->where('TemaID', '=', request()->tema_id)
        ->select('*')
        ->get();

        $tema = DB::table('control_tema_cuestionario_cat')
        ->where('TemaID', '=', request()->tema_id)
        ->select('*')
        ->get();

        return response()->json([
            'status'=>'Ok',
            'cuestionario' => $preguntas,
            'tema' => $tema
        ],200);
    }

    /**
     * Guardar cuestionario y temas nuevos
     */
    public function saveCuestionarioNuevo(){
        try {
            DB::beginTransaction();
            DB::table('control_cuestionario_cat')
                ->insertGetID([
                        'Version'           => 1,
                        'Nombre'            => request()->cuestionario,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    ]
                );
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch (\Exception $e){
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status'  => 'Error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Guardar un nuevo tema
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTemaNuevo()
    {
        try {
            DB::beginTransaction();
            $last_cuestionario = DB::table('control_tema_cuestionario_cat')
                ->where('CuestionarioID', '=', request()->cuestionario_id)
                ->orderBy('TemaID', 'desc')
                ->take(1)
                ->first('Punto');
            if(is_object($last_cuestionario)) {
                DB::table('control_tema_cuestionario_cat')
                    ->insert([
                        'CuestionarioID' => request('cuestionario_id'),
                        'Punto'          => $last_cuestionario->Punto,
                        'Tema'           => request('tema'),
                        'created_at'     => date('Y-m-d H:i:s')
                    ]);
            } else {
                DB::table('control_tema_cuestionario_cat')
                    ->insert([
                        'CuestionarioID' => request('cuestionario_id'),
                        'Punto'          => 1,
                        'Tema'           => request('tema'),
                        'created_at'     => date('Y-m-d H:i:s')
                    ]);
            }
            DB::commit();
            return response()->json([
               'status' => 'Ok'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'  => 'Error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Guardar cuestionario y temas nuevos
     */
    public function updateCuestionario(){
        try {
            // dd(request()->all());
            DB::beginTransaction();

            // Update del nombre del cuestionario
            DB::table('control_cuestionario_cat')
                ->where('CuestionarioID','=',request()->cuestionario_id)
                ->update([
                        'Version'           => 1,
                        'Nombre'            => request()->cuestionario,
                        'updated_at'        => date('Y-m-d H:i:s')
                    ]
                );
            $array_temas = [];

            // dar de baja temas que estaban anteriormente
            DB::table('control_tema_cuestionario_cat')
                ->where('CuestionarioID','=',request()->cuestionario_id)
                ->update([
                        'updated_at'        => date('Y-m-d H:i:s'),
                        'deleted_at'        => date('Y-m-d H:i:s')
                    ]
                );

            //alta de los temas actualizados
            $counter = 1;
            foreach(request()->temas as $tema){
                array_push($array_temas, [
                    'CuestionarioID' => request()->cuestionario_id,
                    'Punto'=>$counter,
                    'Tema'=>$tema,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')]);
                $counter++;
            }

            DB::table('control_tema_cuestionario_cat')
                ->insert($array_temas);
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch (\Exception $e){
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Baja de cuestionario
     */
    public function bajaCuestionario(){
        try{
            // dd(request()->cuestionario["CuestionarioID"]);
            DB::table('control_cuestionario_cat')
                ->where('CuestionarioID','=',request()->cuestionario["CuestionarioID"])
                ->update([
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Baja de tema de un cuestionario
     */
    public function bajaTema(){
        try{
            DB::beginTransaction();
            DB::table('control_tema_cuestionario_cat')
                ->where('TemaID','=',request()->tema)
                ->update([
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
               'status' => 'Ok'
            ]);
        }catch(\Exception $e){
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Guardar pregunta del cuestionario
     * @return void
     */
    public function guardarPregunta()
    {
        try {
            DB::beginTransaction();
            // Adaptar el procedimiento sp_insertarPregunta
            $ultima_pregunta = DB::table('control_preguntas_cat')
                ->where('TemaID','=', request('pregunta')['tema_id'])
                ->orderBy('NoPregunta', 'desc')
                ->first();
            $noPregunta  = (is_object($ultima_pregunta)) ? $ultima_pregunta->NoPregunta + 1 : 1;
            $subpregunta = (request()->tipo_pregunta == 3) ? null : request('pregunta')['subpregunta'];
            DB::select('call sp_insertarPregunta(?,?,?,?,?,?,?,?,?)', [
                request('pregunta')['tema_id'],
                request()->tipo_pregunta,
                $noPregunta,
                request('pregunta')['pregunta'],
                $subpregunta,
                request('pregunta')['descripcion'],
                request('pregunta')['motivacion'],
                request('pregunta')['cuestionario_id'],
                null
            ]);
            DB::commit();
            return response()->json([
                'status' => 'Ok',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Borrar pregunta del cuestionario
     * @return void
     */
    public function borrarPregunta()
    {
        try{
            DB::beginTransaction();
            //TODO::Borrar la pregunta
            DB::table('control_preguntas_cat')->where('PreguntaID', '=', request('PreguntaID'))->update([
               'deleted_at' => date('Y-m-d H:i:s')
            ]);
            //TODO: Re calcular la ponderación
            $tema = DB::table('control_tema_cuestionario_cat')->where('TemaID', '=', request('TemaID'))->first();
            $cuestionario_id = $tema->CuestionarioID;
            DB::commit();
            return response()->json([
               'status' => 'Ok'
            ]);
        }catch (\Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     *
     */
    public function getResultadosAuditoria(){
        $estatus = DB::table('v_respuestas_recomendaciones')
            ->where('AuditoriaID','=',request('auditoriaID'))
            ->get();
        return response()->json([
            'status'=>'OK',
            'respuestas_recomendaciones' => $estatus
        ],200);
    }

    /*
     * Actualizar la descripción de los anexos
     */
    public function updateDescriptionAnexo()
    {
        try {
            DB::beginTransaction();
            DB::table('control_anexo_det')
                ->where('AnexoID', request()->anexoID)
                ->update([
                    'Descripcion' => request()->descripcion,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
                'status' => 'OK'
            ], 200);
        } catch(\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
               'status' => 'Error'
            ], 500);
        }
    }

    public function updateDescripcionAnexo()
    {
        try {
            $user = request()->user();
            DB::beginTransaction();
            DB::table('control_anexo_det')
                ->where('AnexoID', request()->anexoID)
                ->update([
                    'Descripcion' => request()->descripcion,
                    'UsuarioID'   => $user->UsuarioID,
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return response()->json([
                'status' => 'OK'
            ], 200);
        } catch(\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    public function limpiarControlInterno()
    {
       
        // Consultar los ID de las auditorías
        $auditorias = DB::table('control_auditorias')
            ->where('EjercicioID', '=', 15)
            ->whereIn('EnteID', request()->entidades)
            ->get();
    
        $auditoriasId = $auditorias->pluck('AuditoriaID')->all();
        
    
        $resultados = DB::table('v_resultados_control2024')
            ->whereIn('AuditoriaID', $auditoriasId)
            ->get([
                'crpRecomendacionID', 
                'crpRecomendacionOLD', 
                'crpValoracionOLD', 
                'RespuestaID', 
                'RespuestaOLD',
                'solvRecomendacionID', 
                'solvRecomendacionOLD', 
                'solvValoracionOLD', 
                'solvRespuestaID', 
                'solvRespuestaOLD'
            ]);
        foreach ($resultados as $resultado) {
            // Limpiar y formatear cada campo
            $recomendacion = $this->limpiarTexto($resultado->solvRecomendacionOLD);
            $valoracion = $this->limpiarTexto($resultado->solvValoracionOLD);
            $respuesta = $this->limpiarTexto($resultado->solvRespuestaOLD);
    
            // Actualizar la base de datos con los textos limpios
            DB::table('control_recomendaciones_det')
                ->where('RecomendacionID', $resultado->solvRecomendacionID)
                ->update([
                    'updated_at' => now(),
                    'RecomendacionTXT' => $recomendacion,
                    'ValoracionTXT' => $valoracion
                ]);
    
            DB::table('control_respuestas')
                ->where('RespuestaID', $resultado->solvRespuestaID)
                ->update([
                    'updated_at' => now(),
                    'RespuestaTXT' => $respuesta
                ]);
        }
    
        return response()->json([
            'status' => 'Ok'
        ], 200);
    }
    
    private function limpiarTexto($texto)
    {
        // Reemplazar cada </p> con "hola"
        $textoFormateado = str_replace('</p>', chr(13) . chr(10) . chr(13) . chr(10), $texto);

        // Eliminar todas las etiquetas HTML excepto </p>
        $textoSinEtiquetas = strip_tags($textoFormateado, '<p>');

        // Eliminar cualquier etiqueta <p> restante (si existe)
        $textoSinEtiquetas = str_replace('<p>', '', $textoSinEtiquetas);

        // Decodificar entidades HTML y XML
        $textoDecodificado = html_entity_decode($textoSinEtiquetas, ENT_QUOTES | ENT_HTML5, 'UTF-8');

        // Eliminar espacios adicionales generados por &nbsp;
        $textoLimpio = preg_replace('/\s*[\x{00A0}]+\s*/u', ' ', $textoDecodificado);

        // Eliminar espacios adicionales al principio y al final
        $textoLimpio = trim($textoLimpio);

        // Opcional: Reemplazar múltiples espacios consecutivos por uno solo
        $textoFinal = preg_replace('/\s+/', ' ', $textoLimpio);

        return $textoFinal;
    }

    

}
