<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EntidadesController extends Controller
{
    /**
     * Obtener las entidades disponibles, tomadas como referencia de la auditoría de desempeño
     */
    public function getEntidades()
    {
        $entidades = DB::connection('main')
            ->table('osaf_entidades_fiscalizables')
            ->whereIn('EntidadID', [
                // ORGANOS DE AGUA
                257, // CIAPACOV
                311, // CAPACO
                312, // COMAPAL
                313, // CAPAC
                314, // COMAPAC
                315, // CAPAI
                316, // CAPAMI
                317, // CAPDAM
                318, // COMAPAT
                // MUNICIPIOS
                299, // MPIO ARMERIA
                300, // MPIO COLIMA
                301, // MPIO COIMALA
                302, // MPIO COQUIMATLAN
                303, // MPIO CUAUHTEMOC
                304, // MPIO IXTLAHUACAN
                305, // MPIO MANZANILLO
                306, // MPIO MINATITLAN
                307, // MPIO TECOMAN
                308, // MPIO VILLA DE ALVAREZ
                // PODERES
                287, // PODER LEGISLATIVO
                288, // PODER JUDICIAL
                356, // PODER EJECIUTIVO,
                // AUTONOMOS
                289, // COMISIÓN DE DERECHOS HUMANOS DEL ESTADO
                294, // TAE
                295, // TJAE
                296, // TEE
                353, // FGE
                290, // INFOCOL
                291, // IEE
                298, // UCOL
                // DIF MUNICIPALES
                281, // DIF ESTATAL
                273, // INSUVI
                279, // SERVICIOS DE SALUD
                292, // INSTITUTO TECNOLÓGICO DE COLIMA (PRUEBAS)
                264, // Instituto para el Medio Ambiente y Desarrollo Sustentable
                340, // DIF Armería
                341, // DIF Colima
                342, // DIF Comala
                343, // DIF Coquimatlán
                344, // DIF Cuauhtémoc
                345, // DIF Ixtlahuacán
                346, // DIF Mazanillo
                347, // DIF Minatitlán
                348, // DIF Tecomán
                349, // DIF Villa de Álvarez
                // DECENTRALIZADO
                336, // Instituto Municipal de la Mujer Comalteca
                325, // Instituto Municipal de la Mujer Manzanillense
                321, // Instituto de las Mujeres para el Municipio de Colima
                328, // Instituto de las Mujeres Villalvarenses
                327, // Instituto Tecomense de las Mujeres
                333, // Instituto de Planeación para el Municipio de Colima
                332  // Instituto de Planeación para el Desarrollo Sustentable de Manzanillo
            ])
            ->select('EntidadID', 'Nombre')
            ->orderBy('Nombre', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'entidades' => $entidades
        ], 200);
    }
}
