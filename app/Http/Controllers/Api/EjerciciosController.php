<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Repositories\EjerciciosRepository;
use Illuminate\Support\Facades\DB;

class EjerciciosController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios

    public function __construct(EjerciciosRepository $er)
    {
        $this->er = $er;
    }

    /**
     * Obtener el ejercicio fiscal a auditar
     */
    public function getEjercicioFiscal()
    {
        $ejercicio_fiscal = $this->er->getEjercicioActual()->Year - 1;
        return response()->json([
            'status' => 'Ok',
            'ejercicio_fiscal' => $ejercicio_fiscal
        ], 200);
    }

    /**
     * Obtener los ejercicios fiscales de las auditorias
     */
    public function getEjerciciosFiscales()
    {
        $ejercicios = DB::table('v_control_auditorias')
            ->groupBy('Ejercicio')
            ->orderBy('Ejercicio', 'desc')
            ->select('Ejercicio')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'ejercicios' => $ejercicios
        ], 200);
    }
}
