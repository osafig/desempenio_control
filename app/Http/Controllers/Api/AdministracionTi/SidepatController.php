<?php

namespace App\Http\Controllers\Api\AdministracionTi;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Exception;
use App\User;
use DB;

class SidepatController extends Controller
{
    protected $db_declaracion;
    protected $db_main;

    public function __construct()
    {
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main = DB::connection('main');
    }

    /**
     * Obtener la configuración de sidepat
     */
    public function getConfiguracion()
    {
        $configuracion = $this->db_declaracion->table('oic_configuraciones')->where('ConfigID', '=', 1)->first();
        $ambitos_publicos = $this->getCatAmbitosPublicos()->getData()->ambitos_publicos;
        $ordenes_gobierno = $this->getCatOrdenesGobierno()->getData()->ordenes_gobierno;
        $ejercicios = $this->getEjercicios()->getData()->ejercicios;
        return response()->json([
            'status'           => 'Ok',
            'configuracion'    => $configuracion,
            'ambitos_publicos' => $ambitos_publicos,
            'ordenes_gobierno' => $ordenes_gobierno,
            'ejercicios'       => $ejercicios
        ]);
    }

    /**
     * Obtener el catalogo de ambitos publicos
     */
    public function getCatAmbitosPublicos()
    {
        $ambitos_publicos = $this->db_declaracion->table('oic_ambito_cat')->where('Activo', '=', true)->get();
        return response()->json([
           'status'           => 'Ok',
           'ambitos_publicos' => $ambitos_publicos
        ]);
    }

    /**
     * Obtener el catalogo de ordenes de gobierno
     */
    public function getCatOrdenesGobierno()
    {
        $ordenes_gobierno = $this->db_declaracion->table('oic_orden_gobierno')->where('Activo', '=', true)->get();
        return response()->json([
           'status'           => 'Ok',
           'ordenes_gobierno' => $ordenes_gobierno
        ]);
    }

    /**
     * Actualizar la configuración
     */
    public function saveConfiguracion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_configuraciones')->where('ConfigID', '=', 1)->update([
                'OrdenGobiernoID' => request('orden'),
                'AmbitoID'        => request('ambito'),
                'NombreEnte'      => request('nombre'),
                'FechaLimite'     => request('fecha'),
                'ColorPlataforma' => request('color')
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtner tipos de usuarios
     */
    public function getTipoUsuarios()
    {
        $tipo_usuarios = $this->db_main->table('osaf_tipo_trabajador_cat')->where('Visible', 1)->get();
        return response()->json([
            'status' => 'Ok',
            'tipo_usuarios' => $tipo_usuarios
        ], 200);
    }

    /**
     * Actualizar al tipo de trabajador
     */
    public function actualizarTipoTrabajador()
    {
        $this->db_main->table('osaf_usuarios')->where('UsuarioID', request('UsuarioID'))->update([
            'TipoTrabajadorID' => request('tipoTrabajador'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Obtener lista de ejercicios que presentan declaraciones patrimoniales
     */
    public function getEjercicios()
    {
        $main_db_name = $this->db_main->getDatabaseName();
        // Obtener los ejercicios que cuentan con declaración
        $ejercicios_declaraciones = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$main_db_name}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            ->groupBy('oec.EjercicioID')
            ->select('oec.*')
            ->get();
        // Revisar si hay algun ejercicio despues del último
        $ejercicios = $this->db_main->table('osaf_ejercicios_cat')->where('Year', '>', $ejercicios_declaraciones->last()->Year)->get();
        if(count($ejercicios) > 0) {
            foreach ($ejercicios as $ejercicio) {
                $ejercicio_temporal = (object) [
                    'EjercicioID' => $ejercicio->EjercicioID,
                    'Year'        => $ejercicio->Year,
                    'Actual'      => $ejercicio->Actual
                ];
                $ejercicios_declaraciones->add($ejercicio_temporal);
            }
        }
        return response()->json([
           'status'           => 'Ok',
           'ejercicios' => $ejercicios_declaraciones
        ]);
    }

    /**
     * Cambiar ejercicio activo
     */
    public function cambiarEjercicioActivo()
    {
        $this->db_declaracion->select('call sp_seleccionarEjercicioActivo(?)',[request('ejericio_id')]);
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Cambiar ejercicio activo
     */
    public function aperturaEjericio()
    {
        $this->db_declaracion->select('call sp_aperturarNuevoEjercicio()');
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Validar password
     */
    public function validarPassword()
    {
        try {
            $user = User::where('Email', request('email'))->first();
            if( Hash::check(request('password'), $user->Password) ) {
                return response()->json([
                    'message'   => 'Ok',
                ], 200);
            } else {
                throw new Exception("Error");
            }
        } catch (Exception $e) {
            Log::error("ERROR | {$e->getMessage()}");
            return response()->json([
                'message'   => 'Error',
            ], 401);
        }
    }
}
