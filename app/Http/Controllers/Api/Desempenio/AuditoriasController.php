<?php

namespace App\Http\Controllers\Api\Desempenio;

use App\Mail\EnviarAccesosEnlaces;
use App\Http\Repositories\Desempenio\{AuditoriasRepository, EtapasRepository};
use App\Http\Repositories\EjerciciosRepository;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Http\Request;


class AuditoriasController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios
    protected $ar;  //Repositorio de datos de Auditorias
    protected $etr; //Repositorio de etapas de Auditorias
    protected $ah;  //Helper de Auditorias

    public function __construct(EjerciciosRepository $er, AuditoriasRepository $ar, EtapasRepository $etr)
    {
        $this->er = $er;
        $this->ar = $ar;
        $this->etr = $etr;
    }
    public function avanzarAuditoria(Request $request)
    {

        // Validar que se haya recibido el ID de la auditoría
        $validatedData = $request->validate([
            'idauditoria' => 'required|integer',
        ]);
        //----------datos estaticos-------//

        // Obtener el ID de la auditoría desde la solicitud
        $auditoriaID = $validatedData['idauditoria'];
        $RespuestaNoContestada = 'Sin justificaciones, ni aclaraciones emitidas por el Ente Fiscalizado, una vez revisadas todas las preguntas hasta que se revisen todas';

        // Conectarse a la base de datos 'sad' y obtener el CuestionarioID
        $cuestionarioID = DB::connection('sad')
            ->table('sad_aplicacion_cuestionario')
            ->where('AuditoriaID', $auditoriaID)
            ->value('CuestionarioID');

        // Obtener AplicacionID basado en el AuditoriaID
        $aplicacionID = DB::connection('sad')
        ->table('sad_aplicacion_cuestionario')
        ->where('AuditoriaID', $auditoriaID)
        ->value('AplicacionID');

        // Obtener AuditoriaEtapaID basado en el AuditoriaID
        $auditoriaEtapaID = DB::connection('sad')
        ->table('sad_auditoria_etapa_det')
        ->where('AuditoriaID', $auditoriaID)
        ->value('EtapaID');


        // Consultar la tabla sad_auditoria_etapa_det y hacer JOIN con sad_etapa_cat
        $etapas = DB::connection('sad')
        ->table('sad_auditoria_etapa_det')
        ->join('sad_etapa_cat', 'sad_auditoria_etapa_det.EtapaID', '=', 'sad_etapa_cat.EtapaID')
        ->where('sad_auditoria_etapa_det.AuditoriaID', $auditoriaID)
        ->select('sad_auditoria_etapa_det.EtapaID', 'sad_etapa_cat.DescripcionEtapa')
        ->first();

        $etapaDescripcion = $etapas ? $etapas->DescripcionEtapa : '';
        //----------------datos estaticos---------------------------------------------//

        
        if ($cuestionarioID) {
            // Hacer un JOIN con las tablas sad_preguntas_cat y sad_respuestas
            Log::info(json_encode($cuestionarioID));
            // Hacer el JOIN y filtrar por CuestionarioID y AplicacionID
            $preguntas = DB::connection('sad')
            ->table('sad_preguntas_cat')
            ->leftJoin('sad_respuestas', function($join) use ($aplicacionID) {
                $join->on('sad_preguntas_cat.PreguntaID', '=', 'sad_respuestas.PreguntaID')
                    ->where('sad_respuestas.AplicacionID', '=', $aplicacionID);
            })
            ->where('sad_preguntas_cat.CuestionarioID', $cuestionarioID)
            ->select('sad_preguntas_cat.PreguntaID', 'sad_preguntas_cat.Pregunta', 'sad_respuestas.Respuesta',)
            ->get();

            //verificar si esta con el enlace
            $condicionCumplida = DB::connection('sad')
            ->table('sad_auditoria_etapa_det')
            ->where('AuditoriaID', $auditoriaID)
            ->where('EtapaID', 1)
            ->where('Finalizado', 0)
            ->exists();

            if ($condicionCumplida) {
                Log::info("se encontro que la auditoria esta con el enlace");
                    // Iterar sobre las preguntas
                foreach ($preguntas as $pregunta) {
                    // Verificar si ya existe una respuesta para la PreguntaID correspondiente
                    

                    if (($pregunta->Respuesta)===NULL) {
                        
                    
                        // Si no existe una respuesta, insertar una nueva
                        
                        $insert = DB::connection('sad')
                            ->table('sad_respuestas')
                            ->insertGetId([
                                'PreguntaID' => $pregunta->PreguntaID,
                                'AuditoriaEtapaID' =>  $auditoriaEtapaID, // Ajusta según corresponda
                                'AplicacionID' =>  $aplicacionID,         // Ajusta según corresponda
                                'Respuesta' => $RespuestaNoContestada,
                                'No_Anexos' => 0, // O el valor que corresponda
                                'Valoracion' => null,
                                'Etapa' => $etapaDescripcion, // Ajusta según corresponda
                                'created_at' => now(),
                                'updated_at' => now(),
                                'RespuestaTXT' => null,
                            ]);

                        

                            
                    }
                }

                // Avanzar auditoria
                DB::connection('sad')
                    ->table('sad_auditoria_etapa_det')
                    ->where('AuditoriaID', $auditoriaID)
                    ->where('EtapaID', 1)
                    ->where('Finalizado', 0)
                    ->update([
                        'Finalizado' => 1
                    ]);

                    return response()->json([
                        'message' => 'Auditoría avanzada con éxito.',
                        'cuestionarioID' => $cuestionarioID,
                    ], 200);

            
            }else{
                Log::info("Esta Auditoria no esta con el enlace");
            }
    
                    
           
            
            
            
        } else {
            return response()->json(['message' => 'Cuestionario no encontrado.'], 404);
        }
    }


    /**
     * Obtener las auditorias creadas
     */
    public function getAuditorias()
    {
        $ejercicio_fiscal = $this->er->getEjercicioAuditado()->Year;
        $auditorias = DB::connection('sad')
            ->table('v_monitoreo_auditoria')
            ->where('Anio', '=', 2023/*$ejercicio_fiscal*/)
            ->where(function($query) {
                if(!is_null(request()->search)) {
                    $query->where('Nombre', 'like', '%' . request()->search . '%');
                }
                if(count(request()->etapas) > 0) {
                    $query->whereIn('etapa_actual', request()->etapas);
                }
            })
            ->orderBy('Nombre', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'auditorias' => $auditorias
        ], 200);
    }

    /**
     * Crear nuevas auditorias
     */
    public function createAuditorias()
    {
        try {
            $ejercicio_fiscal = $this->er->getEjercicioAuditado();
            $user = request()->user();
            // Validar que no se dupliquen auditorias
            $auditorias_creadas = $this->ar->getAuditoriasCreadas($ejercicio_fiscal);
            $entesID = Arr::pluck($auditorias_creadas, 'EnteID');
            //Obtener los entesID disponibles para generar una auditoria
            $entes = array_diff(request()->entes, $entesID);
            // Armar un arreglo con el ejercicio fiscal a auditar,
            // usuario que da de alta la auditoria y ente a generar su auditoria
            for($i = 0; $i < count($entes); $i++) {
                DB::connection('sad')
                    ->select('call sp_crearAuditoria(?,?,?,?,?)',[
                        $entes[$i],
                        $ejercicio_fiscal->EjercicioID,
                        $user->UsuarioID,
                        null,
                        ''
                    ]);
            }
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener el detalle de la auditoria
     */
    public function getAuditoriaDetalle()
    {
        try {
            $ejercicio = $this->er->getEjercicioByYear(request()->ejercicio);
            $auditoria = DB::connection('sad')
                ->table('sad_auditorias')
                ->where('AuditoriaID', '=', request()->auditoriaID)
                ->where('EjercicioID', '=', $ejercicio->EjercicioID)
                ->first();
            if(is_object($auditoria)) {
                $data_auditoria = DB::connection('sad')->select('call sp_obtenerDatosAuditoria(?)', [request()->auditoriaID]);
                $ente = $data_auditoria[0]->Nombre;
                $control_auditoria = DB::table('v_control_auditorias')
                    ->where('Entidad', '=', $ente)
                    ->where('Ejercicio', '=', $ejercicio->Year)
                    ->first();
                $auditoria = DB::table('control_auditorias')
                    ->where('AuditoriaID', '=', $control_auditoria->AuditoriaID)
                    ->first();
                $data_auditoria[0]->Titular = $auditoria->Titular;
                $data_auditoria[0]->NoAuditoria = $auditoria->NoAuditoria;
                return response()->json([
                    'status' => 'Ok',
                    'auditoria' => $data_auditoria[0]
                ], 200);
            }
            throw new \Exception("AUDITORÍA NO ENCONTRADA");
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las fechas de contestar cuestionario y solventar recomendaciones de la auditoria
     */
    public function getAuditoriaFechas()
    {
        try {
            // Obtener las etapas del enlace
            $etapas_enlace = $this->etr->getEtapasEnlace();
            $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
            // Obtener los registros del detalle de auditoria etapas
            $det_auditoria_etapas = DB::connection('sad')
                ->table('sad_auditoria_etapa_det')
                ->whereIn('EtapaID', $etapas_id)
                ->where('AuditoriaID', '=', request()->auditoriaID)
                ->select('*')
                ->get();
            // Consultar fechas de inicio y fin de etapas
            $fechas = [];
            for($i = 0; $i < count($det_auditoria_etapas); $i++) {
                $fecha = DB::connection('sad')
                    ->table('sad_fechas_etapa_det')
                    ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                    ->select('*')
                    ->first();
                if(is_null($fecha)) {
                    array_push($fechas, [
                        'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                        'fecha_inicial' => null,
                        'fecha_final' => null
                    ]);
                } else {
                    array_push($fechas, [
                        'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                        'fecha_inicial' => $fecha->FechaInicio,
                        'fecha_final' => $fecha->FechaFin
                    ]);
                }
            }
            return response()->json([
                'status' => 'Ok',
                'fechas' => $fechas
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar fechas de auditoria
     */
    public function updateAuditoriaFechas()
    {
        try {
            DB::connection('sad')->beginTransaction();
            // Obtener las etapas del enlace
            $etapas_enlace = DB::connection('sad')
                ->table('sad_etapa_cat')
                ->where('RolEncargado', '=', 'Enlace')
                ->orderBy('NoEtapa', 'asc')
                ->get();
            $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
            // Obtener los registros del detalle de auditoria etapas
            $det_auditoria_etapas = DB::connection('sad')
                ->table('sad_auditoria_etapa_det as saed')
                ->join('sad_etapa_cat as sec', 'saed.EtapaID', '=', 'sec.EtapaID')
                ->whereIn('saed.EtapaID', $etapas_id)
                ->where('saed.AuditoriaID', '=', request()->auditoriaID)
                ->select('saed.*', 'sec.RolEncargado', 'sec.DescripcionEtapa')
                ->get();
            for($i = 0; $i < count($det_auditoria_etapas); $i++){
                if($det_auditoria_etapas[$i]->DescripcionEtapa == 'Enlace contestando') {
                    $fecha = DB::connection('sad')
                        ->table('sad_fechas_etapa_det')
                        ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                        ->first();
                    if(is_null($fecha)) {
                        DB::connection('sad')
                            ->table('sad_fechas_etapa_det')
                            ->insert([
                                'AuditoriaEtapaID' => $det_auditoria_etapas[$i]->AuditoriaEtapaID,
                                'FechaInicio' => request()->fechas['cuestionarioInicio'],
                                'FechaFin' => request()->fechas['cuestionarioFinal'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    } else {
                        DB::connection('sad')
                            ->table('sad_fechas_etapa_det')
                            ->where('AuditoriaEtapaID', '=', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                            ->update([
                                'FechaInicio' => request()->fechas['cuestionarioInicio'],
                                'FechaFin' => request()->fechas['cuestionarioFinal'],
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    }
                } else {
                    $fecha = DB::connection('sad')
                        ->table('sad_fechas_etapa_det')
                        ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                        ->first();
                    if(is_null($fecha)) {
                        DB::connection('sad')
                            ->table('sad_fechas_etapa_det')
                            ->insert([
                                'AuditoriaEtapaID' => $det_auditoria_etapas[$i]->AuditoriaEtapaID,
                                'FechaInicio' => request()->fechas['solventacionInicio'],
                                'FechaFin' => request()->fechas['solventacionFinal'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    } else {
                        DB::connection('sad')
                            ->table('sad_fechas_etapa_det')
                            ->where('AuditoriaEtapaID', '=', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                            ->update([
                                'FechaInicio' => request()->fechas['solventacionInicio'],
                                'FechaFin' => request()->fechas['solventacionFinal'],
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                    }
                }
            }
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener al enlace asignado a la auditoria
     */
    public function getEnlaceAsignado()
    {
        try {
            $enlace_asignado = $this->ar->getEnlaceAsignado(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'enlace_asignado' => $enlace_asignado
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener los enlaces asignados al ente
     */
    public function getEnlacesEnte()
    {
        try {
            $enlaces_ente = $this->ar->getEnlacesEnte(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'enlaces' => $enlaces_ente
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar el enlace a la auditoria
     */
    public function updateEnlace()
    {
        try {
            DB::connection('sad')->beginTransaction();
            $enlace_asignado = $this->ar->getEnlaceAsignado(request()->auditoria);
            if($enlace_asignado == "") {
                // Obtener al enlace
                $enlace = $this->ar->getEnlace(null, request()->enlace, request()->auditoria);
                // Asignar un nuevo enlace a la auditoria
                DB::connection('sad')
                    ->table('sad_asignacion_personal_auditoria')
                    ->insert([
                        'UsuarioID' => $enlace->FuncionarioID,
                        'AuditoriaID' => request()->auditoria,
                        'Enlace' => 1,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                // Actualizar tokens de acceso a la plataforma
                $this->ar->getDataAccessosPdf(request());
            } else {
                // Actualizar el enlace a la auditoria solamente si es diferente del enlace actual
                if($enlace_asignado->NombreCompleto != request()->enlace) {
                    //Obtener el enlace actual
                    $enlace_actual = DB::connection('sad')->table('sad_asignacion_personal_auditoria')
                        ->where('AuditoriaID', '=', request()->auditoria)
                        ->where('Enlace', '=', 1)
                        ->where('ResponsableAuditoria', '=', 0)
                        ->where('UsuarioID', '=', $enlace_asignado->FuncionarioID)
                        ->whereNull('deleted_at')
                        ->first();
                    //Marcar el enlace actual como borrado
                    DB::connection('sad')
                        ->table('sad_asignacion_personal_auditoria')
                        ->where('AsignacionID', '=', $enlace_actual->AsignacionID)
                        ->update([
                            'deleted_at' => date('Y-m-d H:i:s')
                        ]);
                    //Asignar al nuevo enlace
                    // Obtener al enlace
                    $enlace = $this->ar->getEnlace(null, request()->enlace, request()->auditoria);
                    // Asignar un nuevo enlace a la auditoria
                    DB::connection('sad')
                        ->table('sad_asignacion_personal_auditoria')
                        ->insert([
                            'UsuarioID' => $enlace->FuncionarioID,
                            'AuditoriaID' => request()->auditoria,
                            'Enlace' => 1,
                            'ResponsableAuditoria' => 0,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    // Actualizar tokens de acceso a la plataforma
                    $this->ar->getDataAccessosPdf(request());
                }
            }
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Descargar accesos del enlace
     */
    public function decargarAccesos()
    {
        try {
            //Consultar los datos que contendra el pdf
            $data = $this->ar->getDataAccessosPdf(request());
            $pdf = PDF::loadView('pdf.acceso_enlace', compact('data'));
            return $pdf->stream();
        } catch (Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener el nombre del jefe de auditoria
     */
    public function getJefeAuditoria()
    {
        try {
            $jefe = $this->ar->getJefeAuditoria();
            return response()->json([
                'status' => 'Ok',
                'jefeAuditoria' => $jefe
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener nmbre de todos los auditores de desempeño
     */
    public function getAuditores()
    {
        try {
            $auditores = $this->ar->getAuditores();
            return response()->json([
                'status' => 'Ok',
                'auditores' => $auditores
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar auditor asignado a la auditoria
     */
    public function updateAuditor()
    {
        try {
            DB::connection('sad')->beginTransaction();
            // Validar si la auditoria cuenta con un usuario auditor
            $auditoria_id = request()->auditoria;
            $asignacion_auditor = DB::connection('sad')
                ->table('sad_asignacion_personal_auditoria')
                ->where('AuditoriaID', '=', $auditoria_id)
                ->where('Enlace', '=', 0)
                ->where('ResponsableAuditoria', '=', 0)
                ->whereNull('deleted_at')
                ->first();
            //Si no existe un usuario, asignamos al usuario a la auditoria
            $auditor = DB::connection('main')
                ->table('osaf_v_auditores_desempenio')
                ->where('NombreCompleto','=', request()->auditor)
                ->first();
            if(is_null($asignacion_auditor)) {
                //Insertar auditor a la auditoria
                DB::connection('sad')
                    ->table('sad_asignacion_personal_auditoria')
                    ->insert([
                        'AuditoriaID' => $auditoria_id,
                        'UsuarioID' => $auditor->UsuarioID,
                        'Enlace' => 0,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
            } else {
                //Si existe un usuario auditor asignado a la auditoria lo marcamos como eliminado
                DB::connection('sad')
                    ->table('sad_asignacion_personal_auditoria')
                    ->where('AsignacionID', '=', $asignacion_auditor->AsignacionID)
                    ->update([
                        'deleted_at' => date('Y-m-d H:i:s')
                    ]);
                DB::connection('sad')
                    ->table('sad_asignacion_personal_auditoria')
                    ->insert([
                        'AuditoriaID' => $auditoria_id,
                        'UsuarioID' => $auditor->UsuarioID,
                        'Enlace' => 0,
                        'ResponsableAuditoria' => 0,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
            }
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener al auditor asignado
     */
    public function getAuditorAsignado()
    {
        try {
            $auditor_asignado = $this->ar->getAuditorAsignado(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'auditor_asignado' => $auditor_asignado
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las prorrogas de la auditoria
     */
    public function getProrrogas()
    {
        try {
            $prorrogas = $this->ar->getProrrogas(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'prorrogas' => $prorrogas
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener la etapa actual de la auditoria
     */
    public function getEtapaAuditoria()
    {
        try {
            $etapaActual = $this->etr->getEtapaActualAuditoria(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'etapa_actual' => $etapaActual
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Agregar nuevas prorrogas
     */
    public function addProrrogas()
    {
        try {
            DB::connection('sad')->beginTransaction();
            $user = request()->user();
            $etapaActual = $this->etr->getEtapaActualAuditoria(request()->auditoria);
            DB::connection('sad')
                ->table('sad_prorrogas_cat')
                ->insert([
                    'AuditoriaEtapaID' => $etapaActual->AuditoriaEtapaID,
                    'UsuarioID' => $user->UsuarioID,
                    'FechaCierre' => request()->fechaVigencia,
                    'Motivo' => request()->motivo,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * actualizar una prorroga
     */
    public function updateProrrogas()
    {
        try {
            DB::connection('sad')->beginTransaction();
            DB::connection('sad')
                ->table('sad_prorrogas_cat')
                ->where('ProrrogaID', '=', request()->prorrogaId)
                ->update([
                    'FechaCierre' => request()->fechaVigencia,
                    'Motivo' => request()->motivo,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Eliminar prorrogas
     */
    public function deleteProrrogas()
    {
        try {
            DB::connection('sad')->beginTransaction();
            DB::connection('sad')->table('sad_prorrogas_cat')
                ->where('ProrrogaID', '=', request()->prorrogaId)
                ->delete();
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener etapas de la auditoria
     */
    public function getEtapasAuditoria()
    {
        try {
            $etapas = $this->etr->getEtapasAuditoria(request()->auditoria);
            return response()->json([
                'status' => 'Ok',
                'etapas' => $etapas
            ], 200);
        } catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Consultar cuestionarios disponibles para la auditoria y cuestionario asignado
     */
    public function getDataCuestionario()
    {
        $cuestionarios = DB::connection('sad')
            ->table('sad_cuestionario')
            ->orderBy('Nombre', 'asc')
            ->get();
        $asignado = DB::connection('sad')
            ->table('sad_aplicacion_cuestionario')
            ->where('AuditoriaID', '=', request()->auditoria)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'cuestionarios' => $cuestionarios,
            'asignado' => $asignado
        ], 200);
    }

    /**
     * Actualizar cuestionario
     */
    public function updateCuestionario()
    {
        try {
            DB::connection('sad')->beginTransaction();
            $cuestionario = DB::connection('sad')
                ->table('sad_cuestionario')
                ->where('Nombre', '=', request()->cuestionario)
                ->first();
            DB::connection('sad')
                ->table('sad_aplicacion_cuestionario')
                ->where('AuditoriaID', '=', request()->auditoria)
                ->update([
                   'CuestionarioID' => $cuestionario->CuestionarioID,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener auditoria del ejercicio actual por funcionario
     */
    public function getAuditoriaByFuncionarioId()
    {
        //Ejercicio fiscal
        $ejercicio = $this->er->getEjercicioAuditado()->Year;
        $auditoria = DB::connection('sad')
            ->table('v_control_auditorias')
            ->where('Ejercicio', '=', $ejercicio)
            ->where('Enlace', '=', request()->funcionarioID)
            ->first();
        //Consultar el hash del cuestionario
        $hash = DB::connection('sad')
            ->table('sad_aplicacion_cuestionario')
            ->where('AuditoriaID', '=', $auditoria->AuditoriaID)
            ->select('Hash')
            ->first();
        $auditoria->Hash = $hash->Hash;
        return response()->json([
            'status' => 'Ok',
            'auditoria' => $auditoria
        ], 200);
    }

    /**
     * Obtener datos generales del cuestionario de auditoria
     */
    public function getGeneralDataCuestionario()
    {
        $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria);
        $fecha = DB::connection('sad')
            ->table('sad_fechas_etapa_det')
            ->where('AuditoriaEtapaID', $etapa->AuditoriaEtapaID)
            ->select('*')
            ->first();
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!is_null($fecha)) {
            $fecha_inicio = $fecha->FechaInicio;
            $fecha_fin = $fecha->FechaFin;
        }
        return response()->json([
            'status' => 'Ok',
            'etapaActual' => $etapa->DescripcionEtapa,
            'fechaInicio' => $fecha_inicio,
            'fechaFin' => $fecha_fin
        ], 200);
    }

    /**
     * Obtener preguntas del cuestionario por temas
     */
    public function getPreguntas()
    {
        $preguntas = DB::connection('sad')->select('call sp_obtener_preguntas_auditoria(?)', [request()->auditoria]);
        foreach($preguntas as $pregunta) {
            $pregunta_db = DB::connection('sad')->table('sad_preguntas_cat')->where('PreguntaID', '=', $pregunta->PreguntaID)->first();
            $pregunta->ValoracionDefault = $pregunta_db->ValoracionPredeterminada;
            $pregunta->RecomendacionDefault = $pregunta_db->RecomendacionPredeterminada;
        }
        return response()->json([
            'status' => 'Ok',
            'preguntas' => $preguntas
        ], 200);
    }

    /**
     * Guardar las respuestas
     */
    public function saveRespuesta()
    {
        try {
            //Determinar si la etapa de auditoria esta con el enlace
            $is_etapa_enlace = $this->etr->isEtapaEnlace(request()->auditoriaId);
            if($is_etapa_enlace == false) {
                throw new \Exception("La etapa actual de auditoria no se encuentra con el enlace");
            }
            //Si la etapa de enlace es 1 obtener la fecha de vigencia
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            $fechas_vigencia_cuestionario = $this->ar->getAuditoriaFechas(request()->auditoriaId);
            $fechas = [];
            //Obteher fechas de la etapa vigente
            foreach ($fechas_vigencia_cuestionario as $fecha) {
                if(count($fechas) == 0) {
                    if($fecha['etapa'] == $etapa_actual->NoEtapa) {
                        $fechas = $fecha;
                    }
                }
            }
            $fecha_limite = \Carbon\Carbon::parse($fechas['fecha_final'])->startOfDay();
            $fecha_limite = $fecha_limite->copy()->endOfDay();
            $carbon = new \Carbon\Carbon();
            $now = $carbon->now();
            if($now->gt($fecha_limite)) {
                //TODO: Validar si la auditoria cuenta con prorrogas activas
                throw new \Exception("La fecha para contestar el cuestionario ha expirado");
            }
            $data_pregunta = json_decode(request('pregunta'));
            //DATOS PREGUNTA
            $pregunta_id = $data_pregunta->PreguntaID;
            // DATOS RESPUESTA
            $respuesta = request()->respuesta;
            if($etapa_actual->DescripcionEtapa == 'Enlace contestando') {
                $respuesta_id = $data_pregunta->RespuestaID;
            } else {
                $respuesta_id = $data_pregunta->SolvRespID;
            }
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            $auditoria_etapa_id = $etapa_actual->AuditoriaEtapaID;
            $etapa = $etapa_actual->DescripcionEtapa;
            $aplicacion_cuestionario = DB::connection('sad')
                ->table('sad_aplicacion_cuestionario')
                ->where('AuditoriaID', '=', request()->auditoriaId)
                ->first();
            $aplicacion_id = $aplicacion_cuestionario->AplicacionID;
            $auditoria_id = request()->auditoriaId;
            if($respuesta_id == null) {
                if (request()->hasFile('fileToUpload')) {
                    $no_anexos = count(request()->fileToUpload);
                } else {
                    $no_anexos = 0;
                }
            } else {
                $anexos_respuesta = DB::connection('sad')
                    ->table('sad_anexo_det')
                    ->where('RespuestaID', '=', $respuesta_id)
                    ->get();
                if(request()->hasFile('fileToUpload')) {
                    $no_anexos = count(request()->fileToUpload) + count($anexos_respuesta);
                } else {
                    $no_anexos = count($anexos_respuesta);
                }
            }
            // REGISTRAR LA RESPUESTA
            $resp = DB::connection('sad')
                ->select('call sp_registrarRespuesta(?,?,?,?,?,?,?)',[
                    $pregunta_id,
                    $auditoria_etapa_id,
                    $auditoria_id,
                    $respuesta,
                    $no_anexos,
                    $respuesta_id,
                    $etapa
            ]);
            if($respuesta_id !== null) {
                $respuesta_id = $respuesta_id;
            } else {
                $respuesta_id = $resp[0]->respuesta_insertada;
            }
            if (request()->hasFile('fileToUpload')){
                try {
                    DB::connection('sad')->beginTransaction();
                    $files = [];
                    $ejercicio = $this->er->getEjercicioAuditado();
                    $auditoria_id = request()->auditoriaId;
                    foreach(request()->fileToUpload as $file) {
                        $nombre_archivo = $file->getClientOriginalName();
                        $descripcion = '';
                        foreach(json_decode(request()->documentosSeleccionadosDes) as $file_sel) {
                            if($file_sel->name == $nombre_archivo) {
                                $descripcion = $file_sel->description;
                            }
                        }
                        $dir = "/desempenio/{$ejercicio->Year}/{$auditoria_id}/anexos";
                        $path = $file->store($dir, 'public');
                        $data = [
                            'RespuestaID'   => $respuesta_id,
                            'NombreArchivo' => $file->getClientOriginalName(),
                            'Ruta'          => $path,
                            'Hash'          => $this->getHash($path),
                            'Descripcion'   => $descripcion,
                            'EnlaceID'      => request()->funcionarioID,
                            'created_at'    => date('Y-m-d H:i:s')
                        ];
                        array_push($files, $data);
                    }
                    DB::connection('sad')->table('sad_anexo_det')->insert($files);
                    DB::connection('sad')->commit();
                } catch(\Exception $e) {
                    DB::connection('sad')->rollBack();
                    Log::error("ERROR | NO SE PUDO GUARDAR LOS ANEXOS A LA RESPUESTA | {$e->getMessage()}");
                }
            }
            return response()->json([
                'status' => 'Ok',
                'respuesta_id' => $respuesta_id
            ], 200);
        }catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Genera el hash de cada uno de los archivos que se suben al servidor
     */
    public function getHash($path)
    {
        $algoritmo = 'sha256';
        $content = Storage::disk('public')->get($path);
        $hash = hash($algoritmo, $content);
        return strtoupper($hash);
    }

    /**
     * Retornar los anexos de la respuesta
     */
    public function getAnexos()
    {
        $anexos = DB::connection('sad')
            ->table('sad_anexo_det')
            ->where('RespuestaID', '=', request()->respuesta_id)
            ->orWhere('RespuestaID', '=', request()->respuesta_id2)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'anexos' => $anexos
        ], 200);
    }

    /**
     * Borrar anexo mandado
     */
    public function deleteAnexos()
    {
        $anexo = DB::connection('sad')
            ->table('sad_anexo_det')
            ->where('AnexoID', '=', request()->anexo_id)
            ->first();
        $auditoria = (int) explode('/', $anexo->Ruta)[2];
        $is_etapa_enlace = $this->etr->isEtapaEnlace($auditoria);
        if($is_etapa_enlace == false) {
            Log::info("ERROR | La etapa actual de auditoria no se encuentra con el enlace");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
        //Si la etapa de enlace es 1 obtener la fecha de vigencia
        $etapa_actual = $this->etr->getEtapaActualAuditoria($auditoria);
        $fechas_vigencia_cuestionario = $this->ar->getAuditoriaFechas($auditoria);
        $fechas = [];
        //Obteher fechas de la etapa vigente
        foreach ($fechas_vigencia_cuestionario as $fecha) {
            if(count($fechas) == 0) {
                if($fecha['etapa'] == $etapa_actual->NoEtapa) {
                    $fechas = $fecha;
                }
            }
        }
        $fecha_limite = \Carbon\Carbon::parse($fechas['fecha_final'])->startOfDay();
        $fecha_limite = $fecha_limite->copy()->endOfDay();
        $carbon = new \Carbon\Carbon();
        $now = $carbon->now();
        if($now->gt($fecha_limite)) {
            Log::info("ERROR | La etapa fecha para eliminar anexos ha expirado");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
        $anexo = DB::connection('sad')
            ->table('sad_anexo_det')
            ->where('AnexoID', '=', request()->anexo_id)
            ->first();
        //BORRAR ANEXO DE BASE DE DATOS
        DB::connection('sad')
            ->table('sad_anexo_det')
            ->where('AnexoID', '=', $anexo->AnexoID)
            ->delete();
        //ACTUALIZAR NUMERO DE ANEXOS A LA RESPUESTA
        $respuesta = DB::connection('sad')
            ->table('sad_respuestas')
            ->where('RespuestaID', '=', $anexo->RespuestaID)
            ->first();
        $noAnexos = $respuesta->No_Anexos;
        DB::connection('sad')
            ->table('sad_respuestas')
            ->where('RespuestaID', '=', $anexo->RespuestaID)
            ->update([
                'No_Anexos' => $noAnexos - 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        try {
            Storage::disk('public')->delete($anexo->Ruta);
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch(\Exception $e) {
            Log::info("ERROR | NO SE PUDO BORRAR EL DOCUMENTO FISICO | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Enviar cuestionario
     */
    public function enviarCuestionario()
    {
        try {
            DB::connection('sad')->beginTransaction();
            // Obtener preguntas
            $preguntas = DB::connection('sad')
                ->select('call sp_obtener_preguntas_auditoria(?)', [request()->auditoria]);
            $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria)->DescripcionEtapa;
            if($etapa == 'Solventación de CRP') {
                $data = [];
                $noAnexosTotal = 0;
                $preguntasSolventadas = array_values(array_filter($preguntas, function($var) {
                    return $var->PuntuacionActual < 6.25;
                }));
                for($i = 0; $i < count($preguntasSolventadas); $i++) {
                    $pregunta = $preguntasSolventadas[$i];
                    $pregunta_data = [];
                    if($pregunta->SolvNo_Anexos > 0) {
                        $anexos = DB::connection('sad')
                            ->table('sad_anexo_det')
                            ->where('RespuestaID', '=', $pregunta->SolvRespID)
                            ->get();
                        $noAnexosTotal = $noAnexosTotal + $pregunta->SolvNo_Anexos;
                    } else {
                        $anexos = [];
                    }
                    $pregunta_data['pregunta'] = $pregunta;
                    $pregunta_data['anexos'] = $anexos;
                    array_push($data, $pregunta_data);
                }
                //Obtener al enlace de la auditoria
                $asignacion_personal_enlace = DB::connection('sad')->table('sad_asignacion_personal_auditoria')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->where('Enlace', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
                $enlace_auditoria = DB::connection('main')
                    ->table('osaf_v_enlaces')
                    ->where('FuncionarioID', '=', $asignacion_personal_enlace->UsuarioID)
                    ->first();
                //Finalizar cuiestionario, cambiando la etapa
                $auditoriaId = request()->auditoria;
                $this->markEtapaCompleted($auditoriaId);
                // ------------------------------------------------------
                // GENERAR DOCUMENTO DE RESPUESTAS
                // ------------------------------------------------------
                $auditoria = DB::connection('sad')
                    ->table('v_control_auditorias')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->first();
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_solventacion_auditoria_desempenio', compact('auditoria', 'data', 'enlace_auditoria'));
                $url = "desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/";
                $path = public_path($url);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "solventacion_respuestas_auditoria_desempenio.pdf");
                $path_acuse_respuestas = $url . "solventacion_respuestas_auditoria_desempenio.pdf";
                // ------------------------------------------------------
                // GENERAR ACUSE DE RECIBO
                // ------------------------------------------------------
                $hash = $this->getHash($path_acuse_respuestas);
                //Guardar el hash en la tabla de cuestionario
                DB::connection('sad')
                    ->table('sad_aplicacion_cuestionario')
                    ->where('AuditoriaID', '=', $auditoria->AuditoriaID)
                    ->update([
                        'HashSolventacion' => $hash,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                //Generar el documento de acuse final
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_final_desempenio', compact('auditoria', 'hash', 'enlace_auditoria', 'noAnexosTotal'));
                $path = public_path("desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/");
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "acuse_solventacion_auditoria_desempenio.pdf");
                DB::connection('sad')->commit();
                return response()->json([
                    'status' => 'Ok'
                ], 200);
            } else {
                $data = [];
                $noAnexosTotal = 0;
                for($i = 0; $i < count($preguntas); $i++) {
                    $pregunta = $preguntas[$i];
                    $pregunta_data = [];
                    if($pregunta->No_Anexos > 0) {
                        $anexos = DB::connection('sad')
                            ->table('sad_anexo_det')
                            ->where('RespuestaID', '=', $pregunta->RespuestaID)
                            ->get();
                        $noAnexosTotal = $noAnexosTotal + $pregunta->No_Anexos;
                    } else {
                        $anexos = [];
                    }
                    $pregunta_data['pregunta'] = $pregunta;
                    $pregunta_data['anexos'] = $anexos;
                    array_push($data, $pregunta_data);
                }
                //Obtener al enlace de la auditoria
                $asignacion_personal_enlace = DB::connection('sad')->table('sad_asignacion_personal_auditoria')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->where('Enlace', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
                $enlace_auditoria = DB::connection('main')
                    ->table('osaf_v_enlaces')
                    ->where('FuncionarioID', '=', $asignacion_personal_enlace->UsuarioID)
                    ->first();
                //Finalizar cuiestionario, cambiando la etapa
                $auditoriaId = request()->auditoria;
                $this->markEtapaCompleted($auditoriaId);
                // ------------------------------------------------------
                // GENERAR DOCUMENTO DE RESPUESTAS
                // ------------------------------------------------------
                $auditoria = DB::connection('sad')
                    ->table('v_control_auditorias')
                    ->where('AuditoriaID', '=', request()->auditoria)
                    ->first();
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_desempenio', compact('auditoria', 'data', 'enlace_auditoria'));
                
                $url = "desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/";
                $path = public_path($url);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "respuestas_auditoria_desempenio.pdf");
                $path_acuse_respuestas = $url . "respuestas_auditoria_desempenio.pdf";
                // ------------------------------------------------------
                // GENERAR ACUSE DE RECIBO
                // ------------------------------------------------------
                $hash = $this->getHash($path_acuse_respuestas);
                //Guardar el hash en la tabla de cuestionario
                DB::connection('sad')
                    ->table('sad_aplicacion_cuestionario')
                    ->where('AuditoriaID', '=', $auditoria->AuditoriaID)
                    ->update([
                        'Hash' => $hash,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                //Generar el documento de acuse final
                ini_set('memory_limit', '-1');
                $pdf = \PDF::loadView('pdf.acuse_recibo_auditoria_final_desempenio', compact('auditoria', 'hash', 'enlace_auditoria', 'noAnexosTotal'));
                $path = public_path("desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/acuses/");
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $pdf->save($path . "acuse_auditoria_desempenio.pdf");
                DB::connection('sad')->commit();
                return response()->json([
                    'status' => 'Ok'
                ], 200);
            }
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::info("ERROR | NO SE PUDO GENERAR EL ACUSE DE RECIBO | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Envíar accesos al correo del enlace
     */
    public function enviarAccesos()
    {
        try {
            //Consultar los datos que contendra el pdf
            $data = $this->ar->getDataAccessosPdf(request());
            $emails = [$data->EmailInstitucional, $data->Email];
            Mail::to($emails)->send(new EnviarAccesosEnlaces($data));
            $message = "CORREO DE ENTREGA DE ACCESOS ENVIADO |
                        ENTIDAD: {$data->Entidad} |
                        NOMBRE DE ENLACE: {$data->NombreCompleto} |
                        CORREO INSTITUCIONAL: {$data->EmailInstitucional} |
                        CORREO PERSONAL: {$data->Email}";
            Log::info($message);
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener los archivos de la primera vuelta
     */
    public function getFilesCrp()
    {
        $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria);
        $ejercicio = $this->er->getEjercicioAuditado();
        $files = null;
        if($etapa->NoEtapa > 1) {
            $auditoria = DB::connection('sad')
                ->table('sad_auditorias as sa')
                ->leftJoin('sad_aplicacion_cuestionario as sac', 'sa.AuditoriaID', '=', 'sac.AuditoriaID')
                ->where('sa.AuditoriaID', '=', request()->auditoria)
                ->select('sa.*', 'sac.Hash')
                ->first();
            if(is_null($auditoria->Hash) == false) {
                //Acuse de respuestas
                $files = true;
            } else {
                $files = null;
            }
        }
        return response()->json([
            'status' => 'Ok',
            'files' => $files,
            'ejercicio' => $ejercicio->Year
        ], 200);
    }

    /**
     * Descargar acuse de respuestas de desempenio
     */
    public function descargarRespuestasCrp()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditoria = request()->auditoria;
        $file= public_path(). "/desempenio/{$ejercicio->Year}/{$auditoria}/acuses/respuestas_auditoria_desempenio.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_desempenio.pdf', $headers);
    }

    /**
     * Descargar acuse de respuestas de desempenio
     */
    public function descargarAcuseCrp()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditoria = request()->auditoria;
        $file= public_path(). "/desempenio/{$ejercicio->Year}/{$auditoria}/acuses/acuse_auditoria_desempenio.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_desempenio.pdf', $headers);
    }

    /**
     * Obtener datos para la seccion de monitoreo de auditorias de control interno
     */
    public function getDataAuditoriasMonitoreo()
    {
        // Obtener ejercicio fiscal, etapas y auditores
        $ejercicio = $this->er->getEjercicioAuditado();
        $etapas = $this->etr->getEtapas();
        $auditores = $this->ar->getAuditores();
        return response()->json([
            'status' => 'Ok',
            'ejercicio_fiscal' => 2023,//$ejercicio->Year,
            'etapas' => $etapas,
            'auditores' => $auditores
        ], 200);
    }

    /**
     * Obtener las auditorias para la sección de monitoreo
     */
    public function getAuditoriasMonitoreo()
    {
        $ejercicio = $this->er->getEjercicioAuditado();
        $auditorias = DB::connection('sad')
            ->table('v_monitoreo_auditoria')
            ->where('Anio', '=', $ejercicio->Year)
            ->where(function($query) {
                if(!is_null(request()->search)) {
                    $query->where('Nombre', 'like', '%' . request()->search . '%');
                }
                if(count(request()->etapas) > 0) {
                    $query->whereIn('etapa_actual', request()->etapas);
                }
                if(count(request()->auditores) > 0) {
                    $query->whereIn('Auditor', request()->auditores);
                }
            })
            ->orderBy('Nombre', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'auditorias' => $auditorias
        ], 200);
    }

    /**
     * Obtener las respuestas iniciales y solventacion de recomendaciones
     */
    public function getRespuestasSolventaciones()
    {
        $user = request()->user();
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $userId = $user->UsuarioID;
        $ejercicioId = $ejercicio_auditado->EjercicioID;
        //Respuestas iniciales
        $no_respuestas1 = $this->ar->getCountRespuestas(3, $userId, $ejercicioId);
        $no_respuestas2 = $this->ar->getCountRespuestas(4, $userId, $ejercicioId);
        $no_respuestas3 = $this->ar->getCountRespuestas(5, $userId, $ejercicioId);
        $no_respuestas  = $no_respuestas1 + $no_respuestas2 + $no_respuestas3;
        // Solventaciones
        $no_solventaciones1 = $this->ar->getCountRespuestas(8, $userId, $ejercicioId);
        $no_solventaciones2 = $this->ar->getCountRespuestas(9, $userId, $ejercicioId);
        $no_solventaciones3 = DB::connection('sad')->table('sad_auditoria_etapa_det as saed')
            ->join('v_control_auditorias as vca', 'saed.AuditoriaID', '=', 'vca.AuditoriaID')
            ->where('saed.EtapaID', '=', 9)
            ->where('saed.Finalizado', '=', 1)
            ->where('vca.Ejercicio', '=', $ejercicio_auditado->Year)
            ->count();
        $no_solventaciones  = $no_solventaciones1 + $no_solventaciones2 + $no_solventaciones3;
        return response()->json([
            'status'           => 'Ok',
            'noRespuestas'     => $no_respuestas,
            'noSolventaciones' => $no_solventaciones
        ], 200);
    }

    /**
     * Obtener respuestas iniciales del responsable
     */
    public function getRespuestasInicales()
    {
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $respuestas = DB::connection('sad')
            ->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [3, $ejercicio_auditado->EjercicioID]);
        foreach ($respuestas as $respuesta) {
            $respuesta->preguntas = DB::connection('sad')->select('call sp_obtener_preguntas_auditoria(?)', [$respuesta->AuditoriaID]);
            $respuesta->auditor = $this->ar->getAuditorAsignado($respuesta->AuditoriaID);
        }
        // Preparar respuestas revisadas
        $revisadas_generar_crp = DB::connection('sad')->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [4, $ejercicio_auditado->EjercicioID]);
        $revisadas_enviar_crp  = DB::connection('sad')->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [5, $ejercicio_auditado->EjercicioID]);
        $revisadas = [];
        foreach ($revisadas_generar_crp as $generar) {
            $generar->etapa = 4;
            array_push($revisadas, $generar);
        }
        foreach ($revisadas_enviar_crp as $enviar) {
            $enviar->etapa = 5;
            array_push($revisadas, $enviar);
        }
        //Respuestas iniciales
        return response()->json([
            'status'     => 'Ok',
            'respuestas' => $respuestas,
            'revisadas'  => $revisadas
        ], 200);
    }

    /**
     * Obtener solventaciones de auditoria
     */
    public function getRespuestasSolventacionesAuditoria()
    {
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $respuestas = DB::connection('sad')->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [8, $ejercicio_auditado->EjercicioID]);
        foreach ($respuestas as $respuesta) {
            $respuesta->preguntas = DB::connection('sad')->select('call sp_obtener_preguntas_auditoria(?)', [$respuesta->AuditoriaID]);
            $respuesta->auditor = $this->ar->getAuditorAsignado($respuesta->AuditoriaID);
        }
        // Respuestas revisadas
        $revisadas = DB::connection('sad')->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [9, $ejercicio_auditado->EjercicioID]);
        // Finalizadas
        $finalizadas = DB::connection('sad')->table('sad_auditoria_etapa_det as saed')
            ->join('v_control_auditorias as vca', 'saed.AuditoriaID', '=', 'vca.AuditoriaID')
            ->where('saed.EtapaID', '=', 9)
            ->where('saed.Finalizado', '=', 1)
            ->where('vca.Ejercicio', '=', $ejercicio_auditado->Year)
            ->select('saed.*', 'vca.Entidad')
            ->get();
        return response()->json([
            'status'      => 'Ok',
            'respuestas'  => $respuestas,
            'revisadas'   => $revisadas,
            'finalizadas' => $finalizadas
        ], 200);
    }


    /**
     * Validar que la auditoria exista y este asignada al responsable
     */
    public function validateAuditoria()
    {
        $validate = false;
        $user = request()->user();
        $auditoria = DB::connection('sad')
            ->table('sad_asignacion_personal_auditoria')
            ->where('AuditoriaID', '=', request()->auditoria)
            ->where('Enlace', '=', 0)
            ->where('ResponsableAuditoria', '=', 1)
            ->where('UsuarioID', '=', $user->UsuarioID)
            ->whereNull('deleted_at')
            ->first();
        if (is_object($auditoria)) {
            $validate = true;
        }
        return response()->json([
            'status' => 'Ok',
            'validate' => $validate
        ], 200);
    }

    /**
     * Obtener el detalle de la auditoria
     */
    public function getGeneralDataAuditoria()
    {
        $auditoria = $this->ar->getAuditoriaDetalle(request()->auditoria);
        $enlace = DB::connection('main')
            ->table('osaf_v_enlaces')
            ->where('FuncionarioID', '=', $auditoria->Enlace)
            ->first();
        $etapa = $this->etr->getEtapaActualAuditoria(request()->auditoria);
        return response()->json([
            'status' => 'Ok',
            'auditoria' => $auditoria,
            'enlace' => $enlace,
            'etapa' => $etapa
        ]);
    }

    /**
     * Guardar valoración del auditor
     */
    public function saveValoracion()
    {
        try {
            $user = request()->user();
            //DATOS PREGUNTA
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            if($etapa_actual->NoEtapa < 8) {
                $respuesta_id = request()->pregunta['RespuestaID'];
            } else {
                $respuesta_id = request()->pregunta['SolvRespID'];
            }
            $usuario_id = $user->UsuarioID;
            $recomendacion = request()->recomendacion ?? '';
            $valoracion = request()->valoracion;
            $puntuacion = (float) request()->puntuacion;
            $auditoria_etapa_id = $etapa_actual->AuditoriaEtapaID;
            $recomendacion_id = request()->pregunta['RecomendacionIDRespon'];
            // REGISTRAR LA RESPUESTA
            $resp = DB::connection('sad')->select('call sp_registrarRecomendacion(?,?,?,?,?,?,?,?)',[
                $respuesta_id,
                $usuario_id,
                'Responsable',
                $recomendacion,
                $valoracion,
                $puntuacion,
                $auditoria_etapa_id,
                $recomendacion_id
            ]);
            if($recomendacion_id !== null) {
                $recomendacion_id = $recomendacion_id;
            } else {
                $recomendacion_id = $resp[0]->recomendacion_id;
            }
            return response()->json([
                'status' => 'Ok',
                'recomendacion_id' => $recomendacion_id
            ], 200);
        }catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Envíar revisión al responsable de auditoria
     */
    public function enviarRevision()
    {
        try{
            DB::connection('sad')->beginTransaction();
            //Finalizar cuiestionario, cambiando la etapa
            $auditoriaId = request()->auditoria;
            $this->markEtapaCompleted($auditoriaId);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            DB::connection('sad')->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    public function generateCrp()
    {
        try {
            DB::connection('sad')->beginTransaction();
            // Datos de la auditoría
            $auditoria = DB::connection('sad')
                ->table('v_control_auditorias')
                ->where('AuditoriaID', '=', request()->AuditoriaID)
                ->first();
            $preguntas = DB::connection('sad')
                ->select('call sp_obtener_preguntas_auditoria(?)', [request()->AuditoriaID]);
            $data = [];
            $noAnexosTotal = 0;
            for($i = 0; $i < count($preguntas); $i++) {
                $pregunta = $preguntas[$i];
                $pregunta_data = [];
                if($pregunta->No_Anexos > 0) {
                    $anexos = DB::connection('sad')
                        ->table('sad_anexo_det')
                        ->where('RespuestaID', '=', $pregunta->RespuestaID)
                        ->get();
                    $noAnexosTotal = $noAnexosTotal + $pregunta->No_Anexos;
                } else {
                    $anexos = [];
                }
                $pregunta_data['pregunta'] = $pregunta;
                $pregunta_data['anexos'] = $anexos;
                array_push($data, $pregunta_data);
            }
            //Importar template
            $path_template = public_path('desempenio/template_crp_2021.docx');
            $templateProcessor = new TemplateProcessor($path_template);
            $data_preguntas = [];
            dd($data_preguntas);
            //Remplazar valores
            $templateProcessor->setValue('nombreEnte', $auditoria->Entidad);
            for($i = 0; $i < count($data); $i++) {
                $pregunta = $data[$i]['pregunta'];
                $noPregunta = $i + 1;
                $array_temp = [
                    'pregunta'  => $this->htmltodocx_clean_text($noPregunta.". ".$pregunta->Pregunta),
                    'respuesta' => $this->htmltodocx_clean_text($pregunta->Respuesta),
                ];
                $array_temp['recomendacion_cumplimiento_title'] = ($pregunta->PuntosRespon == 6.25) ? 'Cumplimiento' : 'Recomendación';
                $array_temp['recomendacion'] = ($pregunta->PuntosRespon == 6.25) ? 'Del análisis de la respuesta y evidencia proporcionada por el ente fiscalizado se determina que cumple con lo solicitado en la pregunta correspondiente.' : $this->htmltodocx_clean_text($pregunta->RecomendacionRespon);
                array_push($data_preguntas, $array_temp);
            }
            $templateProcessor->cloneBlock('block_pregunta', 0, true, false, $data_preguntas);
            // Generar documento de word
            $path_file = public_path("desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/CedulaDeResultadosPreliminares.docx");
            $templateProcessor->saveAs($path_file);
            //Finalizar cuiestionario, cambiando la etapa
            $auditoriaId = request()->AuditoriaID;
            $this->markEtapaCompleted($auditoriaId);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    public function htmltodocx_clean_text($text) {
        $text = str_replace('&nbsp;', ' ', $text);
        if (strpos($text, '<') !== FALSE) {
            $text = strip_tags($text);
        }
        $text = preg_replace('/\s+/u', ' ', $text);
        $text = html_entity_decode($text, ENT_COMPAT, 'UTF-8');
        return $text;
    }

    /**
     * Dercargar CRP
     */
    public function descargarCrp()
    {
        $auditoria = request()->AuditoriaID;
        $ejercicio = $this->er->getEjercicioAuditado();
        $file = public_path("desempenio/{$ejercicio->Year}/{$auditoria}/CedulaDeResultadosPreliminares.docx");
        return \Response::download($file);
    }

    /**
     * Envíar crp como enviada
     */
    public function markSentCrp()
    {
        try {
            DB::connection('sad')->beginTransaction();
            //Finalizar cuiestionario, cambiando la etapa
            $auditoriaId = request()->AuditoriaID;
            $this->markEtapaCompleted($auditoriaId);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Generar informe de auditoria
     */
    public function generateInforme()
    {
        try {
            DB::connection('sad')->beginTransaction();
            // Datos de la auditoría
            $auditoria = DB::connection('sad')
                ->table('v_control_auditorias')
                ->where('AuditoriaID', '=', request()->AuditoriaID)
                ->first();
            $auditoria_control = DB::table('control_auditorias')->where('AuditoriaID', '=', request()->AuditoriaID)->first();
            $auditoria->noExpediente = $auditoria_control->NoExpediente;
            //Preguntas de la auditoria
            $preguntas = DB::connection('sad')
                ->select('call sp_obtener_preguntas_auditoria(?)', [request()->AuditoriaID]);
            $preguntas =  collect($preguntas);
            $preguntas_solventadas = $preguntas->where('RecomendacionIDRespon', '==', null);
            $total = $preguntas_solventadas->sum('PuntuacionActual');
            $preguntas_no_solventadas = $preguntas->where('RecomendacionIDRespon', '!=', null);
            $total = $total + $preguntas_no_solventadas->sum('PuntosRespon');
            // Importar template
            $path_template = public_path('desempenio/template_informe_2021.docx');
            $templateProcessor = new TemplateProcessor($path_template);
            $templateProcessor->setValue('total', $total);
            $templateProcessor->setValue('nombreEnte', $auditoria->Entidad);
            // Tabla de preguntas
            $data_preguntas = [];
            foreach($preguntas as $pregunta) {
                $puntuacion = (float) (is_null($pregunta->RecomendacionIDRespon)) ? $pregunta->PuntuacionActual : $pregunta->PuntosRespon;
                if($puntuacion == 6.25) {
                    $clasificacion = 'A';
                } else if($puntuacion < 6.25 && $puntuacion > 0) {
                    $clasificacion = 'B';
                } else {
                    $clasificacion = 'C';
                }
                $array_temp = [
                    'no_expediente' => "AD{$pregunta->NoPregunta}-".$auditoria->noExpediente,
                    'pregunta'      => $pregunta->Pregunta,
                    'clasificacion' => $clasificacion,
                    'ponderacion'   => $puntuacion
                ];
                array_push($data_preguntas, $array_temp);
            }
            $templateProcessor->cloneBlock('block_pregunta', 0, true, false, $data_preguntas);
            // Sección de preguntas no solventadas
            $preguntas_seccion_no_solventadas = $preguntas_no_solventadas->where('PuntosRespon', '<', 6.25);
            $preguntas_seccion_no_solventadas = $preguntas_seccion_no_solventadas->all();
            $data_preguntas2 = [];
            foreach($preguntas_seccion_no_solventadas as $no_solventada) {
                $array_temp2 = [
                    'no_expediente2' => "AD{$no_solventada->NoPregunta}-".$auditoria->noExpediente,
                    'pregunta_dos'   => $no_solventada->Pregunta,
                    'recomendacion_2'=> $this->htmltodocx_clean_text($no_solventada->RecomendacionRespon)
                ];
                array_push($data_preguntas2, $array_temp2);
            }
            $templateProcessor->cloneBlock('block_pregunta_dos', 0, true, false, $data_preguntas2);
            // Generar documento de word
            $path_file = public_path("desempenio/{$auditoria->Ejercicio}/{$auditoria->AuditoriaID}/InformeFinal.docx");
            $templateProcessor->saveAs($path_file);
            //Finalizar cuiestionario, cambiando la etapa
            $this->markEtapaCompleted(request()->AuditoriaID);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error("ERROR {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Descargar Informe final de auditorias
     */
    public function descargarInforme() {
        $auditoria = request()->AuditoriaID;
        $ejercicio = $this->er->getEjercicioAuditado();
        $file = public_path("desempenio/{$ejercicio->Year}/{$auditoria}/InformeFinal.docx");
        return \Response::download($file);
    }

    /**
     * Marcar estapa actual como finalizada
     */
    public function markEtapaCompleted($auditoriaId)
    {
        //Finalizar cuiestionario, cambiando la etapa
        $etapa_actual = $this->etr->getEtapaActualAuditoria($auditoriaId);
        $etapa_id = $etapa_actual->EtapaID;
        DB::connection('sad')->table('sad_auditoria_etapa_det')
            ->where('EtapaID', '=', $etapa_id)
            ->where('AuditoriaID', '=', $auditoriaId)
            ->update([
                'Finalizado' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * Marcar la auditoia como finalizada
     */
    public function marcarAuditoriaRevisada()
    {
        try {
            DB::connection('sad')->beginTransaction();
            $this->markEtapaCompleted(request('auditoria'));
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Verifica si la auditoria cuenta con expedientes
     */
    public function getExpedientesSad()
    {
        // Consultar expedientes CRP
        $expedientesCrp = false;
        $expedientesSolventacion = false;
        $auditoria = DB::connection('sad')->table('v_etapas_auditoria')
            ->where('AuditoriaID', '=', request()->AuditoriaID)
            ->get();
        if($auditoria[4]->Finalizado == 1) {
            $expedientesCrp = true;
        }
        if($auditoria[8]->Finalizado == 1) {
            $expedientesSolventacion = true;
        }
        return response()->json([
            'status' => 'Ok',
            'hasExpedientesCrpSad' => $expedientesCrp,
            'hasExpedientesSolventacionSad' => $expedientesSolventacion
        ], 200);
    }

    /**
     * consultar estatus de respuestas y recomendaciones de una adutoria
     */
    public function getResultadosAuditoriaSad(){
        DB::beginTransaction();
        $estatus = DB::connection('sad')->table('v_respuestas_recomendaciones')
            ->where('AuditoriaID','=',request('auditoriaID'))
            ->get();
        return response()->json([
            'status'=>'OK',
            'respuestas_recomendaciones' => $estatus
        ],200);
    }

    /**
     * Actualizar la descripción de los anexos
     */
    public function updateDescriptionAnexo()
    {
        try {
            DB::connection('sad')->beginTransaction();
            DB::connection('sad')->table('sad_anexo_det')
                ->where('AnexoID', request()->anexoID)
                ->update([
                    'Descripcion' => request()->descripcion,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'OK'
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar descripción auditoria de desempeño
     */
    public function updateDescripcionAnexoDesempenio()
    {
        try {
            $user = request()->user();
            DB::connection('sad')->beginTransaction();
            DB::connection('sad')->table('sad_anexo_det')
                ->where('AnexoID', request()->anexoID)
                ->update([
                    'Descripcion' => request()->descripcion,
                    'UsuarioID'   => $user->UsuarioID,
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'OK'
            ], 200);
        } catch(\Exception $e) {
            DB::connection('sad')->rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
    //--------------------------------------------------------Proceso de limpieza desempeño---------------------------------------------------------------//
    public function limpiarDesempenio()
    {
       // Consultar los ID de las auditorías
        $auditorias = DB::connection('sad')
        ->table('sad_auditorias')
        ->where('EjercicioID', '=', 15) // TODO: este valor cambia con cada ejercicio
        ->whereIn('EnteID', request()->entidades)
        ->get();

        //almacenar los id de las auditorias obtenidas
        $auditoriasId = $auditorias->pluck('AuditoriaID')->all();
        //se consultan las respuestas sin limpiar
        $resultados = DB::connection('sad')
        ->table('v_resultados_desempenioe2024')
        ->whereIn('AuditoriaID', $auditoriasId)
        ->get([
            'crpRecomendacionID', 
            'crpRecomendacionOLD', 
            'crpValoracionOLD', 
            'RespuestaID', 
            'RespuestaOLD', 
            'solvRecomendacionID', 
            'solvRecomendacionOLD', 
            'solvValoracionOLD', 
            'solvRespuestaID', 
            'solvRespuestaOLD'
        ]);
        
        //recorrer con un forech respuestas sin limpiar 
        foreach ($resultados as $resultado) {
            // limpiar y formatear los campos
            //$crpRecomendacion = $this->limpiarTexto($resultado->crpRecomendacionOLD);
            $crpRecomendacion = $this->limpiarTexto($resultado->solvRecomendacionOLD);
            //$crpValoracion = $this->limpiarTexto($resultado->crpValoracionOLD);
            $crpValoracion = $this->limpiarTexto($resultado->solvValoracionOLD);
            //$crpRespuesta = $this->limpiarTexto($resultado->RespuestaOLD);
            $crpRespuesta = $this->limpiarTexto($resultado->solvRespuestaOLD);
            
            // Actualizar la base de datos con los textos limpios para Solventación
            DB::connection('sad')->table('sad_recomendaciones_det')
                ->where('RecomendacionID', $resultado->solvRecomendacionID)
                ->update([
                    'updated_at' => now(),
                    'RecomendacionTXT' => $crpRecomendacion,
                    'ValoracionTXT' => $crpValoracion
                ]);
    
            DB::connection('sad')->table('sad_respuestas')
                ->where('RespuestaID', $resultado->solvRespuestaID)
                ->update([
                    'updated_at' => now(),
                    'RespuestaTXT' => $crpRespuesta
                ]);
        }
        //respuesta si todo salio bien
        return response()->json([
            'status' => 'Ok'
        ], 200);
    }

    private function limpiarTexto($texto)
    {
       // Reemplazar cada </p> con "hola"
       $textoFormateado = str_replace('</p>', chr(13) . chr(10) . chr(13) . chr(10), $texto);

       // Eliminar todas las etiquetas HTML excepto </p>
       $textoSinEtiquetas = strip_tags($textoFormateado, '<p>');

       // Eliminar cualquier etiqueta <p> restante (si existe)
       $textoSinEtiquetas = str_replace('<p>', '', $textoSinEtiquetas);

       // Decodificar entidades HTML y XML
       $textoDecodificado = html_entity_decode($textoSinEtiquetas, ENT_QUOTES | ENT_HTML5, 'UTF-8');

       // Eliminar espacios adicionales generados por &nbsp;
       $textoLimpio = preg_replace('/\s*[\x{00A0}]+\s*/u', ' ', $textoDecodificado);

       // Eliminar espacios adicionales al principio y al final
       $textoLimpio = trim($textoLimpio);

       // Opcional: Reemplazar múltiples espacios consecutivos por uno solo
       $textoFinal = preg_replace('/\s+/', ' ', $textoLimpio);

       return $textoFinal;
    }


     //--------------------------------------------------------/Proceso de limpieza desempeño---------------------------------------------------------------//
    


    /**
     * Obtener cuestionarios de auditoria de desempeño
     */
    public function getCuestionarios()
    {
        $cuestionarios = $this->ar->getCuestionarios();
        return response()->json([
            'status' => 'Ok',
            'cuestionarios' => $cuestionarios
        ], 200);
    }

    /**
     * Obtener el detalle del cuestionario y las preguntas
     */
    public function getCuestionario()
    {
        $cuestionario = $this->ar->getCuestionario(request()->cuestionario);
        return response()->json([
            'status' => 'Ok',
            'cuestionario' => $cuestionario
        ], 200);
    }
}
