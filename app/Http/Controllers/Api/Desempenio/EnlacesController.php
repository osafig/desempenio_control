<?php

namespace App\Http\Controllers\Api\Desempenio;

use App\Http\Controllers\Controller;
use App\Http\Repositories\{EjerciciosRepository, EnlacesRepository, EntidadesRepository};
use Illuminate\Support\Facades\DB;

class EnlacesController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios
    protected $enr;  //Repositorio de datos de Enlaces
    protected $entr; //Repositorio de datos de Entidades

    public function __construct(EjerciciosRepository $er, EnlacesRepository $enr, EntidadesRepository $entr)
    {
        $this->er = $er;
        $this->enr = $enr;
        $this->entr = $entr;
    }

    /**
     * Consultar si el enlace tiene una auiditoria en el ejercicio actual
     */
    public function checkHasAuditoria()
    {
        $ejercicio_fiscal = $this->er->getEjercicioAuditado()->Year;
        $auditoria = DB::connection('sad')
            ->table('v_control_auditorias')
            ->where('Ejercicio', '=', $ejercicio_fiscal)
            ->where('Entidad', '=', request()->Entidad)
            ->where('Enlace', '=', request()->FuncionarioID)
            ->first();
        if(is_null($auditoria)) {
            return response()->json([
                'status' => 'OK',
                'audDesempenio' => false
            ], 200);
        } else {
            return response()->json([
                'status' => 'OK',
                'audDesempenio' => true
            ], 200);
        }
    }
}
