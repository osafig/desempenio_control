<?php

namespace App\Http\Controllers\Api\Declaracion;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Declaracion\DeclaracionRepository;
use App\Mail\NotificationDeclare;
use Carbon\Carbon;
use Illuminate\Support\Facades\{Crypt, DB, Hash, Log, Mail, Storage};
use Barryvdh\DomPDF\Facade as PDF;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class DeclaracionController extends Controller{

    protected $dr; //Repositorio de declaracion
    protected $db_declaracion;
    protected $db_main;

    public function __construct(DeclaracionRepository $dr){
        $this->dr = $dr;
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main = DB::connection('main');
    }

    /**
     * Obtener el tipo de declaración patrimonial, ejercicio de la declaración y modulos de la declaracion
     */
    public function getDataDeclaracion()
    {
        $user               = request()->user();
        $data['ejercicio']  = $this->dr->getEjercicioDeclaracion();
        $data['tipo']       = $this->dr->getTipoDeclaracion(request('declaracion_id'));
        $data['inf_gral']   = $this->dr->getDeclaracionData(request('declaracion_id'));
        $data['secciones']  = $this->dr->getSeccionesADeclarar(request('declaracion_id'));
        if($data['tipo']->TipoDeclaracionID != 7) {
            $data['fecha_limite'] = $this->db_declaracion->table('oic_configuraciones')->where('ConfigID', '=', 1)->first('FechaLimite');
        }
        return response()->json([
            'status'      => 'Ok',
            'declaracion' => $data
        ], 200);
    }

    /**
     * Obtener usuarios a declarar
     */
    public function getUsersToDeclare()
    {
        $ejercicio = $this->dr->getEjercicioDeclaracion();
        // obtener a todos los usuarios del ejercicio actual que presentaran una declaración (situación patrimonial)
        $users     = $this->dr->getUsuariosDeclaracion($ejercicio->EjercicioID);
        // obtener a todos los usuarios del ejercicio actual que presentaran una declaración (conflicto de intereses)
        $usuariosConflictos = $this->dr->getUsuariosConflictoIntereses($ejercicio->EjercicioID);
        // obtener a usuarios que presentaron una declaración de situación patrimonial el ejercicio pasado de tipo inicial y modificación
        $usersPast = collect($this->db_declaracion->select('call sp_listaDeclarantesEJercicioAnterior()'));
        $plucked   = $usersPast->pluck('UsuarioID');
        return response()->json([
            'status'    => 'Ok',
            'users'     => $users,
            'usersPast' => $plucked->all(),
            'usersConflicto' => $usuariosConflictos
        ], 200);
    }

    /**
     * Obtener tipos de declaracion
     */
    public function getTiposDeclaracion()
    {
        $tipos_declaracion = $this->dr->getTiposDeclaracion();
        return response()->json([
            'status'            => 'Ok',
            'tiposDeclaracion' => $tipos_declaracion
        ], 200);
    }

    /**
     * Asignar a usuarios a una declaración en especifico
     */
    public function asignarUsuariosDeclaracion()
    {
        $usuarios   = request('usuarios');
        $tipo       = request('tipo');
        $conflictos = request('conflictos');
        $ejercicio  = $this->dr->getEjercicioDeclaracion();
        for($i = 0; $i < count($usuarios); $i++) {
            $this->db_declaracion->select('call sp_crearDeclaracion(?, ?, ?)', [$usuarios[$i], $ejercicio->EjercicioID, $tipo]);
        }
        // Declaraciones de conflicto de interes
        if($tipo == 2 && $conflictos) {
            for($i = 0; $i < count($usuarios); $i++) {
                $this->db_declaracion->select('call sp_crearDeclaracion(?, ?, ?)', [$usuarios[$i], $ejercicio->EjercicioID, 7]);
            }
        }
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Remover a usuarios a una declaración en especifico
     */
    public function removerUsuariosDeclaracion()
    {
        $usuario = request()->user;
        $ejercicio = $this->dr->getEjercicioDeclaracion();
        $declaracion = $this->dr->getDeclaracionByUser($usuario, $ejercicio->EjercicioID, request('tipoDeclaracion'));
        $this->db_declaracion->select('call sp_baja_declaracion(?)', [$declaracion->DeclaracionID]);
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    public function getDatosPersonales()
    {
        $user = request()->user();
        $datos =  $this->dr->getDatosPersonalesMain($user->UsuarioID);
        return response()->json([
            'status'    => 'Ok',
            'datos'     => $datos
        ], 200);
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatalogoEstadoCivil()
    {
        $estado_civil_cat = $this->dr->getCatalogoEstadoCivil();
        return response()->json([
            'status'    => 'Ok',
            'catalogo'     => $estado_civil_cat
        ], 200);
    }

    /**
     * Guardar datos personales
     */
    public function saveInformacionPersonal(){
        try {
            $this->db_declaracion->beginTransaction();
            $resultado = $this->dr->registrarDatosPersonales(request()->info);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDIERON REGISTRAR LOS DATOS PERSONALES");
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener estados
     */
    public function getEstados()
    {
        return response()->json([
            'status'  => 'Ok',
            'estados' =>  $this->dr->getEstados()
        ], 200);
    }

    /**
     * Obtener tipos de declaracion
     */
    public function getNivelesEstudio()
    {
        $niveles = $this->dr->getCatalogoNivelesEstudio();
        return response()->json([
            'status'            => 'Ok',
            'niveles' => $niveles
        ], 200);
    }

    /**
     * Obtener municipios del estado
     */
    public function getMunicipios()
    {
        return response()->json([
            'status'     => 'Ok',
            'municipios' => $this->dr->getMunicipios(request('estado_id'))
        ], 200);
    }

    /**
     * Guardar la direccion a la declaracion
     */
    public function saveDireccion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            // Actualizar direccion a la declaracion patrimonial
            $this->db_declaracion
                ->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'DireccionID' => $resultado[0]->direccion_id
                ]);
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 2);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener direccion
     */
    public function getDireccion()
    {
        $direccion = $this->db_declaracion->table('oic_direcciones')
            ->where('DireccionID', '=', request('direcion_id'))
            ->first();
        return response()->json([
            'status'    => 'Ok',
            'direccion' => $direccion
        ], 200);
    }

    /**
     * Guardar los datos curriculares del declarante
     */
    public function saveDatosCurriculares()
    {
        $user = request()->user()->UsuarioID;
        try {
            $resultado = $this->dr->registrarDatosCurriculares(request()->info, $user);
            if ($resultado === false) {
                throw new Exception("ERROR | NO SE PUDIERON REGISTRAR LOS DATOS PERSONALES");
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatOrdenGobierno()
    {
        $orden_gobierno_cat = $this->dr->getCatalogoOrdenGobierno();
        return response()->json([
            'status'    => 'Ok',
            'catalogo_orden'     => $orden_gobierno_cat
        ], 200);
    }

    /**
     * Obtener catalogo de ambito publico
     */
    public function getCatAmbitoPublico()
    {
        $ambito_publico_cat = $this->dr->getCatalogoAmbitoPublico();
        return response()->json([
            'status'    => 'Ok',
            'catalogo_ambito'     => $ambito_publico_cat
        ], 200);
    }

    /**
     * Obtener catalogo de sector laboral
     */
    public function getCatSectorLaboral()
    {
        $sector_cat = $this->dr->getCatalogoSectorLaboral();
        return response()->json([
            'status' => 'Ok',
            'catalogo_sector' => $sector_cat
        ], 200);
    }

    /**
     * Guardar experiencia laboral
     */
    public function saveExpLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarExpLaboral(request()->info, $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener datos para seccion de cargo
     */
    public function getDataCargo()
    {
        return response()->json([
            'estados'           => $this->dr->getEstados(),
            'areas_adscripcion' => $this->dr->getAreasAdscripcion(),
            'niveles_cargo'     => $this->dr->getNivelesCargo(),
            'ordenes_gobierno'  => $this->dr->getOrdenesGobierno(),
            'ambitos_publicos'  => $this->dr->getAmbitosPublicos(),
            'direccion'         => $this->dr->getDireccionOsafig(),
            'ente'              => $this->dr->getEnte()
        ]);
    }

    /**
     * Cargar experiencias laborales
     */
    public function getExperienciasLaborales()
    {
        $usuario = request()->user()->UsuarioID;
        $experiencias = $this->dr->getExperienciasLaborales($usuario);
        return response()->json([
            'status' => 'Ok',
            'experiencias' => $experiencias
        ]);
    }

    /**
     * Guardar datos del cargo
     */
    public function saveDatosCargo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario   = request()->user()->UsuarioID;
            if(request()->has('otro_cargo') == false) {
                //Marcar la segunda como oculta
                $cargos = $this->db_declaracion->table('oic_cargos')
                    ->where('DeclaracionID', '=', request()['cargo']['declaracion_id'])
                    ->where(    'Oculto', '=', false)
                    ->get();
                if(count($cargos) == 2) {
                    $otroCargoTemp = $cargos->where('TipoCargo', 2)['1'];
                    $otroCargo['declaracion_id']    = $otroCargoTemp->DeclaracionID;
                    $otroCargo['orden']             = $otroCargoTemp->OrdenGobiernoID;
                    $otroCargo['ambito']            = $otroCargoTemp->AmbitoPublicoID;
                    $otroCargo['ente']              = $otroCargoTemp->Dependencia;
                    $otroCargo['area']              = null;
                    $otroCargo['puesto_id']         = null;
                    $otroCargo['honorarios']        = $otroCargoTemp->Honorarios;
                    $otroCargo['nivel']             = $otroCargoTemp->NivelCargo;
                    $otroCargo['funciones']         = $otroCargoTemp->FuncionPrincipal;
                    $otroCargo['fecha']             = $otroCargoTemp->FechaIngreso;
                    $otroCargo['telefono']          = $otroCargoTemp->Telefono;
                    $otroCargo['direccion_id']      = $otroCargoTemp->DireccionID;
                    $otroCargo['aclaraciones']      = $otroCargoTemp->Observaciones;
                    $otroCargo['area_especifica']   = $otroCargoTemp->AreaEspecifica;
                    $otroCargo['puesto_especifico'] = $otroCargoTemp->PuestoEspecifico;
                    $otroCargo['oculto']            = true;
                    $otroCargo['otro_empleo']       = 1;
                    $resultado = $this->dr->registrarDatosCargo($otroCargo, $usuario);
                    if($resultado === false) {
                        throw new Exception("ERROR | NO SE PUDO REGISTRAR EL CARGO");
                    }
                    //Mandamos el contenido de cargo
                    $cargo = request()['cargo'];
                    $cargo['area_especifica'] = null;
                    $cargo['puesto_especifico'] = null;
                    $cargo['oculto'] = false;
                    $resultado = $this->dr->registrarDatosCargo($cargo, $usuario);
                    if($resultado === false) {
                        throw new Exception("ERROR | NO SE PUDO REGISTRAR EL CARGO");
                    }
                } else {
                    //Mandamos el contenido de cargo
                    $cargo = request()['cargo'];
                    $cargo['area_especifica'] = null;
                    $cargo['puesto_especifico'] = null;
                    $cargo['oculto'] = false;
                    $resultado = $this->dr->registrarDatosCargo($cargo, $usuario);
                    if($resultado === false) {
                        throw new Exception("ERROR | NO SE PUDO REGISTRAR EL CARGO");
                    }
                }
            } else {
                $cargo = request()['cargo'];
                // Mandamos el contenido del cargo
                $cargo['area_especifica'] = null;
                $cargo['puesto_especifico'] = null;
                $cargo['oculto'] = false;
                $cargo['otro_empleo'] = 0;
                $cargo['direccion_id'] = request()['cargo']['direccion'];
                $resultado = $this->dr->registrarDatosCargo($cargo, $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR EL CARGO");
                }
                // Validamos la dirección del otro cargo
                $otro_cargo = request()['otro_cargo'];
                $direccion = $this->dr->registrarDireccion($otro_cargo['direccion'], $usuario);
                if($direccion === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
                }
                $id = (array) $direccion[0];
                $direccion_id = $id["direccion_id"];
                // Mandamos el contenido de otro cargo
                $otro_cargo['direccion_id'] = $direccion_id;
                $otro_cargo['area_especifica'] = $otro_cargo['area'];
                $otro_cargo['area'] = null;
                $otro_cargo['puesto_especifico'] = $otro_cargo['empleo'];
                $otro_cargo['puesto_id'] = null;
                $otro_cargo['oculto'] = false;
                $otro_cargo['otro_empleo'] = 1;
                $resultado = $this->dr->registrarDatosCargo($otro_cargo, $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR EL OTRO CARGO");
                }
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar experiencia laboral como oculta
     */
    public function borrarExperienciaLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_experiencia_laboral_detalle')
                ->where('ExperienciaLaboralDetalleID', '=', request()->ExperienciaLaboralDetalleID)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener datos curriculares de una auditoria dada
     */
    public function getDataCurriculares()
    {
        $curriculares =$this->db_declaracion->table('oic_nivel_estudios_detalle')
            ->where('DeclaracionID', '=', request()->declaracionID)
            ->first();
        return response()->json([
            'status'       => 'Ok',
            'curriculares' => $curriculares
        ], 200);
    }

    /**
     * Obtener el cargo registrado en caso de existir
     */
    public function getCargoRegistrado()
    {
        $cargos = $this->db_declaracion->table('oic_cargos')
            ->where('DeclaracionID', '=', request()->declaracion_id)
            ->where('Oculto', '=', false)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'cargos'  => $cargos
        ], 200);
    }

    /**
     * Marcar experiencia laboral como ninguino
     */
    public function marcarNungunoExperienciaLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $experiencias = $this->dr->getExperienciasLaborales($usuario);
            foreach($experiencias as $experiencia) {
                $this->db_declaracion->table('oic_experiencia_laboral_detalle')
                    ->where('ExperienciaLaboralDetalleID', '=', $experiencia->ExperienciaLaboralDetalleID)
                    ->update([
                        'Oculto'     => true,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 5);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo de instrumentos financieros
     */
    public function getInstrumentosFinancieros()
    {
        $instrumentos_cat = $this->dr->getCatalogoInstrumentos();
        return response()->json([
            'status'    => 'Ok',
            'catalogo'     => $instrumentos_cat
        ], 200);
    }

    /**
     * registrar ingresos de la declaracion
     */
    public function saveIngresos()
    {
        $ingresos = request()->all();
        try{
            //Registro de los ingresos principal
            $this->db_declaracion->beginTransaction();
            $usuario   = request()->user()->UsuarioID;
            $declaracion_ingreso = $this->db_declaracion->table('oic_ingresos')
                ->where('DeclaracionID', '=', $ingresos["declaracion_id"])
                ->first();
            if(is_null($declaracion_ingreso)) {
                $ingreso_principal = $this->db_declaracion->table('oic_ingresos')
                    ->insertGetId([
                        'RemuneracionMensualNeta'   => $ingresos["punto_1"],
                        'TotalOtrosIngresos'        => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones'             => $ingresos["observaciones"],
                        'DeclaracionID'             => $ingresos["declaracion_id"],
                        'created_at'                => Carbon::now(),
                        'updated_at'                => Carbon::now(),
                        'UsuarioID'                 => $usuario
                    ]);
            } else {
                $ingreso_principal = $declaracion_ingreso->IngresoID;
                $this->db_declaracion->table('oic_ingresos')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->update([
                        'RemuneracionMensualNeta'   => $ingresos["punto_1"],
                        'TotalOtrosIngresos'        => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones'             => $ingresos["observaciones"],
                        'updated_at'                => Carbon::now(),
                    ]);
            }
            // ----------------------------------
            //      INGRESOS POR ACTIVIDAD
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_actividad = [];
            foreach($ingresos["actividad_industrial"] as $ingreso){
                $data = [
                    'NombreRazonSocial' => $ingreso["nombre"],
                    'TipoNegocio'       => $ingreso["tipo"],
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                    'IngresoPasado'     => 0,
                    'IngresoID'         => $ingreso_principal,
                    'Monto'             => $ingreso["monto"]
                ];
                array_push($ingresos_actividad, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_indutrial')
                ->insert($ingresos_actividad);

            // ----------------------------------
            //  INGRESOS POR ACTIVIDAD FINANCIERA
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_financiera = [];
            $instrumentos = $this->db_declaracion
                ->table('oic_tipo_instrumento_cat')
                ->get();
            $instrumentosId = Arr::pluck($instrumentos, 'IntrumentoID');
            foreach($ingresos["actividad_financiera"] as $ingreso){
                $instrumentoId = (int) $ingreso["nombre"];
                if (in_array($instrumentoId, $instrumentosId)) {
                    $otroInstrumento = "";
                } else {
                    $instrumentoId   = 99;
                    $otroInstrumento = $ingreso["nombre"];
                }
                $data = [
                    'InstrumentoID'     => $instrumentoId,
                    'Monto'             => $ingreso["monto"],
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                    'IngresoPasado'     => 0,
                    'IngresoID'         => $ingreso_principal,
                    'OtroInstrumento'   => $otroInstrumento,
                ];
                array_push($ingresos_financiera, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_financiera')
                ->insert($ingresos_financiera);

            // ----------------------------------
            //  INGRESOS POR SERVICIOS PRESTADOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_servicios')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_servicios')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_servicios = [];
            foreach($ingresos["servicios_prestados"] as $ingreso){
                $data = [
                    'TipoServicioPrestado' => $ingreso["nombre"],
                    'Monto'                => $ingreso["monto"],
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                    'IngresoPasado'        => 0,
                    'IngresoID'            => $ingreso_principal
                ];
                array_push($ingresos_servicios, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_servicios')
                ->insert($ingresos_servicios);

            // ----------------------------------
            //      INGRESOS ENAJENACION
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_enajenacion = [];
            foreach($ingresos["ingresos_enajenacion"] as $ingreso){
                $data = [
                    'Monto'         => $ingreso["monto"],
                    'TipoBien'      => $ingreso["nombre"],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID'     => $ingreso_principal,
                ];
                array_push($ingresos_enajenacion, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_por_enajenacion')
                ->insert($ingresos_enajenacion);

            // ----------------------------------
            //      OTROS INGRESOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_extra')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_extra')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $otros_ingresos = [];
            foreach($ingresos["otros_ingresos"] as $ingreso){
                $data = [
                    'TipoBien'      => $ingreso["nombre"],
                    'Monto'         => $ingreso["monto"],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID'     => $ingreso_principal
                ];
                array_push($otros_ingresos, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_extra')
                ->insert($otros_ingresos);
            // marcar seccion como terminada
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada($ingresos["declaracion_id"], 8);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar seccion experiencia como terminada
     */
    public function marcarExperieinciaTerminada()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 5);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener ingresos de la declaración
     */
    public function getIngresosDeclaracion()
    {
        // Obtener datos de ingreso
        $data['ingresos']                                   = $this->dr->getIngresosDeclaracion(request()->declaracion_id);
        // actividades industriales
        $data['otros_ingresos']['actividades_industriales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesIndustriales($data['ingresos']->IngresoID);
        // actividades financieras
        $data['otros_ingresos']['actividades_financieras']  = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesFinancieras($data['ingresos']->IngresoID);
        // servicios profesionales
        $data['otros_ingresos']['servicios_profesionales']  = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosServiciosProfesionales($data['ingresos']->IngresoID);
        // otros ingresos
        $data['otros_ingresos']['otros_ingresos']           = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosOtros($data['ingresos']->IngresoID);
        // bienes enajenados
        $data['otros_ingresos']['bienes_enajenados']        = (is_null($data['ingresos'])) ? [] : $this->dr->getBienesEnajenados($data['ingresos']->IngresoID);
        return response()->json([
            'status'              => 'Ok',
            'ingresosDeclaracion' => $data
        ], 200);
    }

    /**
     * Obtener catalogo de bien inmueble
     */
    public function getCatBienesInmuebles()
    {
        $bien_inmueble_cat = $this->dr->getCatalogoBienesInmueble();
        return response()->json([
            'status' => 'Ok',
            'catalogo_bien_inmueble' => $bien_inmueble_cat
        ], 200);
    }

    /**
     * Obtener catalogo de tipos de titulares
     */
    public function getCatTipoTitulares()
    {
        $tipo_titular_cat = $this->dr->getCatalogoTipoTitulares();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_titular' => $tipo_titular_cat
        ], 200);
    }

    /**
     * Obtener catalogo de tipo de titulares
     */
    public function getCatalogoTipoTitulares(){
        $catalogo = $this->db_declaracion
            ->table('oic_titulares_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatBienesMueble()
    {
        $bienes_mueble_cat = $this->dr->getCatalogoBienesMueble();
        return response()->json([
            'status' => 'Ok',
            'catalogo_bienes_mueble' => $bienes_mueble_cat
        ], 200);
    }

    /**
     * Obtener catalogo de formas de adquisicion
     */
    public function getCatFormaAdquisicion()
    {
        $forma_adquisicion_cat = $this->dr->getCatalogoTipoAdquisicion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_forma_adquisicion' => $forma_adquisicion_cat
        ], 200);
    }

    /**
     * Obtener catalogo de vinculos personales
     */
    public function getCatVinculoPersonal()
    {
        $vinculo_personal_cat = $this->dr->getCatalogoVinculoPersonal();
        return response()->json([
            'status' => 'Ok',
            'catalogo_vinculo_personal' => $vinculo_personal_cat
        ], 200);
    }

    /**
     * Registro de servidor público pasado
     */
    public function saveServidorPasado()
    {
        $ingresos = request()->all();
        try {
            //Registro de los ingresos principal
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $declaracion_ingreso = $this->db_declaracion
                ->table('oic_servidor_pasado')
                ->where('DeclaracionID', '=', $ingresos["declaracion_id"])
                ->where('Oculto', '=', false)
                ->first();
            if (is_null($declaracion_ingreso)) {
                $ingreso_principal = $this->db_declaracion->table('oic_servidor_pasado')
                    ->insertGetId([
                        'FechaInicio' => $ingresos["fecha_inicio"],
                        'FechaConclusion' => $ingresos["fecha_fin"],
                        'RemuneracionNeta' => $ingresos["punto_1"],
                        'TotalOtrosIngresos' => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones' => $ingresos["observaciones"],
                        'DeclaracionID' => $ingresos["declaracion_id"],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'SeDesempeño' => $ingresos['aplica'],
                        'UsuarioID' => $usuario
                    ]);
            } else {
                $ingreso_principal = $declaracion_ingreso->ServidorPasadoID;
                $this->db_declaracion->table('oic_servidor_pasado')
                    ->where('ServidorPasadoID', '=', $ingreso_principal)
                    ->update([
                        'FechaInicio' => $ingresos["fecha_inicio"],
                        'FechaConclusion' => $ingresos["fecha_fin"],
                        'RemuneracionNeta' => $ingresos["punto_1"],
                        'TotalOtrosIngresos' => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones' => $ingresos["observaciones"],
                        'updated_at' => Carbon::now(),
                    ]);
            }
            // ----------------------------------
            //      INGRESOS POR ACTIVIDAD
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_actividad = [];
            foreach ($ingresos["actividad_industrial"] as $ingreso) {
                $data = [
                    'NombreRazonSocial' => $ingreso["nombre"],
                    'TipoNegocio' => $ingreso["tipo"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 1,
                    'IngresoID' => $ingreso_principal,
                    'Monto' => (int) $ingreso["monto"]
                ];
                array_push($ingresos_actividad, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_indutrial')
                ->insert($ingresos_actividad);

            // ----------------------------------
            //  INGRESOS POR ACTIVIDAD FINANCIERA
            // ----------------------------------
            // Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_financiera = [];
            $instrumentos = $this->db_declaracion
                ->table('oic_tipo_instrumento_cat')
                ->get();
            $instrumentosId = Arr::pluck($instrumentos, 'IntrumentoID');
            foreach ($ingresos["actividad_financiera"] as $ingreso) {
                $instrumentoId = (int)$ingreso["nombre"];
                if (in_array($instrumentoId, $instrumentosId)) {
                    $otroInstrumento = "";
                } else {
                    $instrumentoId = 99;
                    $otroInstrumento = $ingreso["nombre"];
                }
                $data = [
                    'InstrumentoID' => $instrumentoId,
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal,
                    'OtroInstrumento' => $otroInstrumento,
                ];
                array_push($ingresos_financiera, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_financiera')
                ->insert($ingresos_financiera);

            // ----------------------------------
            //  INGRESOS POR SERVICIOS PRESTADOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_servicios')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_servicios')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_servicios = [];
            foreach ($ingresos["servicios_prestados"] as $ingreso) {
                $data = [
                    'TipoServicioPrestado' => $ingreso["nombre"],
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal
                ];
                array_push($ingresos_servicios, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_servicios')
                ->insert($ingresos_servicios);

            // ----------------------------------
            //      INGRESOS ENAJENACION
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_enajenacion = [];
            foreach ($ingresos["bienes_enajenados"] as $ingreso) {
                $data = [
                    'Monto' => $ingreso["monto"],
                    'TipoBien' => $ingreso["nombre"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal,
                ];
                array_push($ingresos_enajenacion, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_por_enajenacion')
                ->insert($ingresos_enajenacion);

            // ----------------------------------
            //      OTROS INGRESOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_extra')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_extra')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $otros_ingresos = [];
            foreach ($ingresos["otros_ingresos"] as $ingreso) {
                $data = [
                    'TipoBien' => $ingreso["nombre"],
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal
                ];
                array_push($otros_ingresos, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_extra')
                ->insert($otros_ingresos);
            // marcar seccion como terminada
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada($ingresos["declaracion_id"], 9);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Guardar bien inmueble
     */
    public function saveBienInmueble()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            $id = (array) $resultado[0];
            $direccion_id = $id["direccion_id"];
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('call sp_registrarBienInmueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                $usuario,
                request()->tipo_bien_id,
                request()->otro_tipo_bien,
                request()->titular,
                request()->porcentaje_propiedad,
                request()->superficie_terreno,
                request()->superficie_construida,
                request()->tipo_persona_tercero,
                request()->nombre_tercero,
                request()->rfc_tercero,
                request()->forma_adquisicion,
                request()->forma_pago,
                request()->tipo_persona_transmisor,
                request()->nombre_transmisor,
                request()->rfc_transmisor,
                request()->vinculo_transmisor,
                request()->valor,
                request()->tipo_moneda,
                request()->fecha_operacion,
                request()->valor_conforme_a,
                request()->folio_real,
                $direccion_id,
                request()->motivo_baja,
                request()->observaciones,
                request()->bien_inmueble
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatVehiculos()
    {
        $vehiculos_cat = $this->dr->getCatalogoVehiculos();
        return response()->json([
            'status' => 'Ok',
            'catalogo_vehiculos' => $vehiculos_cat
        ], 200);
    }

    /**
     * Registrar vehiculo
     */
    public function saveVehiculo(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            if(request()->relacion_transmisor == 'Otro') {
                $relacion_transmisor = request()->otro_vinculo;
            } else {
                $relacion_transmisor = request()->relacion_transmisor;
            }
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('call sp_registrarBienMueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                request()->tipo_bien_id,
                request()->titular,
                request()->tipo_persona_transmisor,
                request()->nombre_razon_social,
                request()->rfc_transmisor,
                $relacion_transmisor,
                request()->nombre_tercero,
                request()->tipo_persona_tercero,
                request()->rfc_tercero,
                request()->forma_adquisiscion_id,
                request()->forma_pago,
                request()->valor,
                request()->tipo_moneda,
                request()->fecha_operacion,
                request()->caracteristicas,
                request()->vehiculo_marca,
                request()->vehiculo_modelo,
                request()->vehiculo_no_serie,
                request()->en_extranjero,
                request()->vehiculo_anio,
                request()->motivo_baja,
                request()->descripcion_bien,
                request()->ubicacion,
                request()->otro_tipo_bien,
                request()->bien_mueble_id,
                request()->observaciones,
                $usuario,
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener bienes inmuebles
     */
    public function getBienesInmuebles()
    {
        $declaracion_id = request()->declaracion_id;
        $bienes_inmuebles = $this->dr->getBienesInmueble($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'bienes_inmuebles' => $bienes_inmuebles
        ]);
    }

    /**
     * Obtener Vehiculos
     */
    public function getVehiculos()
    {
        $declaracion_id = request()->declaracion_id;
        // $usuario = request()->user()->UsuarioID;
        $vehiculos = $this->dr->getVehiculos($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'vehiculos' => $vehiculos
        ]);
    }

    /**
     * Borrar el bien inmueble
     */
    public function borrarBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_inmubles')
                ->where('BienInmuebleID', '=', request()->bien_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Borrar bien mueble
     */
    public function borrarBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('BienMuebleID', '=', request()->bien_mueble_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * marcar servidor pasado como no aplica
     */
    public function saveServidorPasadoNoAplica()
    {
        try {
            $this->db_declaracion->beginTransaction();
            //verificar si existe un registro en oic_servidor_pasado
            $servidor_pasado = $this->db_declaracion
                ->table('oic_servidor_pasado')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->where('Oculto', '=', false)
                ->first();
            if (is_null($servidor_pasado)) {
                $this->db_declaracion->table('oic_servidor_pasado')
                    ->insert([
                        'DeclaracionID' => request()->declaracion_id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'seDesempeño' => false,
                        'Oculto' => true,
                        'UsuarioID' => request()->user()->UsuarioID
                    ]);
            } else {
                $this->db_declaracion
                    ->table('oic_servidor_pasado')
                    ->where('DeclaracionID', '=', request()->declaracion_id)
                    ->where('Oculto', '=', false)
                    ->update([
                        'Oculto' => true,
                        'updated_at' => Carbon::now()
                    ]);
            }
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 9);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion de vehiculos como terminadas
     */
    public function marcarTerminadaVehiculos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 11);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como ninguna
     */
    public function marcarNingunaBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_inmubles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 10);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener ingresos de la declaración servicios pasados
     */
    public function getIngresosDeclaracionServidorPasado()
    {
        // Obtener datos de ingreso
        $data['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado(request()->declaracion_id);
        // Obtener datos de ingreso pasados
        $data['ingresos_ocultos'] = count($this->dr->getIngresosDeclaracionServidorPasadoOcultos(request()->declaracion_id));
        // actividades industriales
        $data['otros_ingresos']['actividades_industriales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesIndustriales($data['ingresos']->ServidorPasadoID);
        // actividades financieras
        $data['otros_ingresos']['actividades_financieras'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesFinancieras($data['ingresos']->ServidorPasadoID);
        // servicios profesionales
        $data['otros_ingresos']['servicios_profesionales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosServiciosProfesionales($data['ingresos']->ServidorPasadoID);
        // otros ingresos
        $data['otros_ingresos']['otros_ingresos'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosOtros($data['ingresos']->ServidorPasadoID);
        // bienes enajenados
        $data['otros_ingresos']['bienes_enajenados'] = (is_null($data['ingresos'])) ? [] : $this->dr->getBienesEnajenados($data['ingresos']->ServidorPasadoID);
        return response()->json([
            'status' => 'Ok',
            'ingresosDeclaracion' => $data
        ], 200);
    }

    /**
     * Marcar sección como terminada
     */
    public function marcarTerminadaBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 10);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion como ninguna
     */
    public function marcarNingunaVehiculos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 11);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener bienes muebles
     */
    public function getBienesMueble()
    {
        $declaracion_id = request()->declaracion_id;
        $bienes_mueble = $this->dr->getBienesMueble($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'bienes_mueble' => $bienes_mueble
        ]);
    }

    /**
     * Marcar seccion de buen mueble como terminada
     */
    public function marcarTerminadaBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 12);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar ninguna bien mueble
     */
    public function marcarNingunaBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 12);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * obtener catalogo de tipo de inversion
     */
    public function getCatInversion()
    {
        $tipo_inversion_cat = $this->dr->getCatalogoTipoInversion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_inversion' => $tipo_inversion_cat
        ], 200);
    }

    /**
     * obtener catalogo de tipo de inversion
     */
    public function getCatActivos()
    {
        $activos_cat = $this->dr->getCatalogoActivos(request()->id_inversion);
        return response()->json([
            'status' => 'Ok',
            'catalogo_activos' => $activos_cat
        ], 200);
    }

    /**
     * Registrar inversion
     */
    public function saveInversion(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro del inmueble
            $this->db_declaracion->select('CALL sp_registrarInversion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                request()->tipo_inversion_id,
                request()->activo,
                request()->tipo_titular,
                request()->tipo_persona_tercero,
                request()->nombre_tercero,
                request()->rfc_tercero,
                request()->numero_contrato,
                request()->es_extranjero,
                request()->ubicacion,
                request()->institucion,
                request()->rfc_institucion,
                request()->saldo,
                request()->moneda,
                request()->aclaraciones,
                request()->recurso_financiero_id,
                $usuario,
            ]);

            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener inversiones
     */
    public function getInversiones($declaracion_id)
    {
        $usuario = request()->user()->UsuarioID;
        $inversiones = $this->dr->getInversiones($usuario, $declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'inversiones' => $inversiones
        ]);
    }

    /**
     * Borrar Inversión
     */
    public function borrarInversion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_recursos_financieros')
                ->where('RecursoFinancieroID', '=', request()->recurso_financiero_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar inversiones como terminada
     */
    public function marcarTerminadaInversiones()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 13);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * marcar ninguna inversion
     */
    public function marcarNingunaInversion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_recursos_financieros')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 13);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Borrar Adeudo
     */
    public function borrarAdeudo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_adeudos')
                ->where('AdeudoID', '=', request()->adeudo_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar adeudos como terminada
     */
    public function marcarTerminadaAdeudos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 14);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * marcar ninguna adeudos
     */
    public function marcarNingunaAdeudos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_adeudos')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 14);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener prestamos
     */
    public function getPrestamos()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $prestamos = $this->dr->getPrestamos($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'prestamos' => $prestamos
        ]);
    }

    /**
     * Guardar prestamo
     */
    public function savePrestamo()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            if(request()->tipo_bien == 1) {
                $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
                }
                $id = (array) $resultado[0];
                $direccion_id = $id["direccion_id"];
            } else {
                $direccion_id = null;
            }
            //registro del inmueble
            $this->db_declaracion->select('CALL sp_registrarPrestamo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->tipo_bien,
                request()->tipo_bien_id_vehiculo,
                request()->tipo_bien_id_inmueble,
                $direccion_id,
                request()->marca,
                request()->modelo,
                request()->anio,
                request()->no_serie,
                request()->en_extranjero,
                request()->pais,
                request()->entidad_federativa,
                request()->titular,
                request()->tipo_persona,
                request()->rfc,
                request()->observaciones,
                request()->prestamo_id,
                request()->otro_bien,
                request()->vinculo
            ]);

            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Borrar prestamo
     */
    public function borrarPrestamo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_prestamo_comodato')
                ->where('PrestamoID', '=', request()->prestamo_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar terminada prestamos
     */
    public function marcarTerminadaPrestamos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 15);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar como ninguna seccion de prestamos
     */
    public function marcarNingunaPrestamos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_prestamo_comodato')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 15);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     *Subir declaracion del sat
     */
    public function subirDeclaracionSat()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $declaracion_id = request()->declaracion_id;
            foreach (request()->fileToUpload as $file) {
                $dir = "/declaracion_patrimonial/{$declaracion_id}";
                $path = $file->store($dir, 'public');
                $this->db_declaracion->select('call sp_subirDeclaracionSAT(?,?,?)',[
                    $usuario,
                    $path,
                    request()->declaracion_id,
                ]);
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch (Exception $e) {
            $this->$this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener declaracion sat
     */
    public function getDeclaracionSat()
    {
        $usuario = request()->user()->UsuarioID;
        $declaracion = $this->db_declaracion->table('oic_declaracion_sat')
            ->where('UsuarioID', '=', $usuario)
            ->where('Eliminado', '=', false)
            ->where('DeclaracionID', '=', request('declaracion_id'))
            ->first();
        return response()->json([
            'status' => 'Ok',
            'declaracion' => $declaracion
        ], 200);
    }

    /**
     * Borrar declaracion sat
     */
    public function borrarDeclaracionSat()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_declaracion_sat')
                ->where('DeclaracionSatID', '=', request()->DeclaracionSatID)
                ->update([
                    'Eliminado' => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch (Exception $e) {
            $this->$this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de sector laboral
     */
    public function getCatTipoParticipante()
    {
        $tipo_participante_cat = $this->dr->getCatalogoTipoParticipante();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_participante' => $tipo_participante_cat
        ], 200);
    }

    /**
     * Guardar participacion en empresa
     */
    public function saveParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registroParticipacionEmpresas(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->nombre_empresa,
                request()->rfc_empresa,
                request()->porcentaje_participacion,
                request()->tipo_participacion,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->ubicacion,
                request()->es_extranjero,
                request()->participante,
                request()->sector_id,
                request()->observaciones,
                request()->participacion_id,
                request()->otro_sector
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener participacion empresas
     */
    public function getParticipacionEmpresas()
    {
        $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $participacion_empresas = $this->dr->getParticipacionEmpresas($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'participacion' => $participacion_empresas
        ]);
    }

    /**
     * borrar participacion en empresa
     */
    public function borrarParticipacionEmpresa(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_empresas')
                ->where('ParticipacionID', '=', request()->participacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada participacion empresa
     */
    public function marcarTerminadaParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 16);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en empresa
     */
    public function marcarNingunaParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_empresas')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 16);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo de tipo institucion
     */
    public function getCatTipoInstitucion()
    {
        $tipo_Institucion_cat = $this->dr->getCatalogoTipoInstitucion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_institucion' => $tipo_Institucion_cat
        ], 200);
    }

    /**
     * Guardar participacion en empresa
     */
    public function saveParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registroParticipacionInstitucion(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->participante,
                request()->tipo_institucion_id,
                request()->nombre_institucion,
                request()->rfc_institucion,
                request()->puesto_rol,
                request()->fecha_inicio,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->observaciones,
                request()->participacion_institucion_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener participacion institucion
     */
    public function getParticipacionInstitucion()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $participacion_institucion = $this->dr->getParticipacionInstitucion($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'participacion' => $participacion_institucion
        ]);
    }

    /**
     * borrar participacion en institucion
     */
    public function borrarParticipacionInstitucion(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_instituciones')
                ->where('ParticipacionInstitucionesID', '=', request()->participacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada participacion institucion
     */
    public function marcarTerminadaParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 17);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en institucion
     */
    public function marcarNingunaParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_instituciones')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 17);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener datos de dependientes - pareja
     */
    public function getDatosPreja()
    {
        $usuario = request()->user()->UsuarioID;
        $datos_pareja = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('UsuarioID', '=', $usuario)
            ->where('Pareja', '=', 1)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'datos_pareja' => $datos_pareja
        ], 200);
    }

    /**
     * Obtener datos de dependientes
     */
    public function getDatosDependientes()
    {
        $usuario = request()->user()->UsuarioID;
        $datos_pareja = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('UsuarioID', '=', $usuario)
            ->where('Pareja', '=', 0)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'datos_pareja' => $datos_pareja
        ], 200);
    }

    /**
     * Guardar dependientes
     */
    public function saveDependientes()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $direccion_id = null;
            if(request()->habita_domicilio == 0) {
                $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
                }
                $id = (array) $resultado[0];
                $direccion_id = $id["direccion_id"];
            }
            $actividad_laboral = null;
            //registro del inmueble
            $this->db_declaracion->select('call sp_registrarDependientes(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->nombre,
                request()->primer_apellido,
                request()->segundo_apellido,
                request()->fecha_nacimiento,
                request()->rfc,
                request()->relacion_id,
                request()->es_extrajero,
                request()->curp,
                request()->es_dependiente,
                request()->habita_domicilio,
                '',
                $direccion_id,
                $actividad_laboral,
                request()->observaciones,
                request()->is_pareja,
                request()->dependiente_id,
                $usuario,
                request()->otra_relacion
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener la actividad laboral de la pareja
     */
    public function getExperienciasDependiente($actividad_laboral_id)
    {
        $experiencias = $this->db_declaracion->table('oic_actividad_laboral_detalle')
            ->where('ActividadLaboralDetalleID', '=', $actividad_laboral_id)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'experiencias' => $experiencias
        ]);
    }

    /**
     * Guardar experiencia laboral de los dependientes
     */
    public function saveExperienciaLaboralDependientes()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $request = request()->info;
            $sector = $this->db_declaracion
                ->table('oic_sector_productivo_cat')
                ->where('Sector', '=', $request['sector_id'])
                ->first();
            $sector_id = (is_null($sector)) ? 0 : $sector->SectorID;
            $actividad_laboral = $this->db_declaracion->select('call sp_registrarActividadLaboral(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $request['experiencia_laboral_id'],
                $request['ambito_sector'],
                $request['orden_gobierno_id'],
                $request['ambito_id'],
                $request['institucion'],
                $request['area'],
                $request['puesto'],
                $request['funcion_principal'],
                $request['salario'],
                $request['fecha_ingreso'],
                $request['rfc_institucion'],
                $sector_id,
                $request['es_proveedor'],
                $request['observacion'],
            ]);
            $actividad_laboral_id = $actividad_laboral[0]->actividad_laboral_id;
            $dependiente_id       = $request['dependiente_id'];
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('DependienteID', '=', $dependiente_id)
                ->update([
                    'ActividadLaboralID' => $actividad_laboral_id,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener datos del dependiente
     */
    public function getDependiente($dependiente_id)
    {
        $dependiente = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('DependienteID', '=', $dependiente_id)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'dependiente' => $dependiente
        ], 200);
    }

    /**
     * Borrar actividad laboral
     */
    public function borrarExperienciaDependiente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('ActividadLaboralID', '=', request()->ActividadLaboralDetalleID)
                ->update([
                    'ActividadLaboralID' => null
                ]);
            $this->db_declaracion->table('oic_actividad_laboral_detalle')
                ->where('ActividadLaboralDetalleID', '=', request()->ActividadLaboralDetalleID)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    public function borrarDependiente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('DependienteID', '=', request()->dependiente_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de la pareja terminada
     */
    public function marcarTerminadaDatosPareja()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 6);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de pareja como ningun
     */
    public function marcarNingunaDatosPareja()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $this->db_declaracion
                ->table('oic_usuarios_dependientes')
                ->where('UsuarioID', '=', $usuario)
                ->where('Pareja', '=', 1)
                ->where('Oculto', '=', 0)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 6);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar datos de la pareja terminada
     */
    public function marcarTerminadaDatosDependientes()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 7);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de pareja como ningun
     */
    public function marcarNingunaDatosDependientes()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $this->db_declaracion
                ->table('oic_usuarios_dependientes')
                ->where('UsuarioID', '=', $usuario)
                ->where('Pareja', '=', 0)
                ->where('Oculto', '=', 0)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 7);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo beneficiarios
     */
    public function getCatBeneficiarios()
    {
        $beneficiarios_cat = $this->dr->getCatalogoBeneficiarios();
        return response()->json([
            'status' => 'Ok',
            'beneficiarios_cat' => $beneficiarios_cat
        ]);
    }

    public function saveApoyoBeneficio()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registroApoyos(?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->beneficiario,
                request()->nombre_programa,
                request()->nombre_institucion,
                request()->ambito_id,
                request()->tipo_apoyo,
                request()->forma_recepcion,
                request()->monto_apoyo,
                request()->especificacion,
                request()->observaciones,
                request()->apoyo_beneficio_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener apoyos y beneficios
     */
    public function getApoyosBeneficios()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $apoyos_beneficios = $this->dr->getApoyosBeneficios($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'apoyos' => $apoyos_beneficios
        ]);
    }

    /**
     * borrar participacion apoyo o beneficio
     */
    public function borrarApoyosBeneficios(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_apoyos_beneficios')
                ->where('ApoyoBeneficioID', '=', request()->apoyo_beneficios)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada la seccion de apoyo o beneficio
     */
    public function marcarTerminadaApoyosBeneficios()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 18);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun apoyo o beneficio
     */
    public function marcarNingunaApoyosBeneficios()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_apoyos_beneficios')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 18);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Guardar representacion
     */
    public function saveRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registrarRepresentacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->representante,
                request()->tipo_representacion,
                request()->fecha_inicio,
                request()->representante_tipo_persona,
                request()->nombre_razon_social,
                request()->rfc_representante,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->sector_id,
                request()->observaciones,
                request()->declaracion_id,
                $usuario,
                request()->representacion_id,
                request()->otro_sector
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener representaciones
     */
    public function getRepresentaciones()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $representaciones = $this->dr->getRepresentaciones($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'representaciones' => $representaciones
        ]);
    }

    /**
     * borrar representacion
     */
    public function borrarRepresentacion(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_representacion')
                ->where('RepresentacionID', '=', request()->representacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada representacion
     */
    public function marcarTerminadaRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 19);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en empresa
     */
    public function marcarNingunaRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_representacion')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 19);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    public function saveCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registroCliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->act_lucrativa_dependiente,
                request()->titular,
                request()->nombre_empresa_servicio,
                request()->rfc_empresa,
                request()->cliente_rfc,
                request()->razon_social_cliente,
                request()->sector_id,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->observaciones,
                request()->cliente_principal_id,
                request()->tipo_persona_cliente,
                request()->otro_sector
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getClientes()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $clientes = $this->dr->getClientes($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'clientes' => $clientes
        ]);
    }

    /**
     * borrar cliente
     */
    public function borrarCliente(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_clientes_principales')
                ->where('ClientePrincipalID', '=', request()->cliente_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada clientes principales
     */
    public function marcarTerminadaCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 20);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun cliente
     */
    public function marcarNingunaCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_clientes_principales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 20);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    public function saveBeneficiosPrivados()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL sp_registroBeneficioPrivado(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                $usuario,
                request()->tipo_beneficio,
                request()->titular,
                request()->otorgante_tipo_persona,
                request()->nombre_razon_social,
                request()->rfc_otorgante,
                request()->recepcion_beneficio,
                request()->beneficio,
                request()->monto_mensual,
                request()->tipo_moneda,
                request()->sector_id,
                request()->observaciones,
                request()->beneficio_privado_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getBeneficios()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $beneficios_privados = $this->dr->getBeneficiosPrivados($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'beneficios_privados' => $beneficios_privados
        ]);
    }

    /**
     * borrar beneficio privado
     */
    public function borrarBeneficioApoyo(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_beneficios_privados')
                ->where('BeneficioPrivadoID', '=', request()->beneficio_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada beneficios privados
     */
    public function marcarTerminadaBeneficioApoyo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 21);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun beneficio privado
     */
    public function marcarNingunaBeneficioApoyo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_beneficios_privados')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 21);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Enviar correos de notificaciones
     */
    public function enviarCorreosNotificacion()
    {
        $users = request()->users;
        foreach ($users as $user) {
            try {
                $user = $this->db_main->table('osaf_usuarios')->where('UsuarioID', '=', $user['UsuarioID'])->first();
                $ejercicio = $this->db_main->table('osaf_ejercicios_cat')->where('Actual', '=', 1)->first();
                $driver_mail = config('sidepat.driver_correos');
                $formato_email_osafig = config('sidepat.mail_formato_osafig');
                if($driver_mail == 'phpmailer') {
                    $enviado = $this->sendMailPhpMailer($user, $ejercicio);
                    if($enviado == false) {
                        throw new Exception("PHP MAILER ERROR");
                    }
                } else {
                    if(is_array($user)) {
                        Mail::to($user['Email'])->send(new NotificationDeclare($user, $ejercicio, $formato_email_osafig));
                    } else {
                        Mail::to($user->Email)->send(new NotificationDeclare($user, $ejercicio, $formato_email_osafig));
                    }
                }
            } catch (Exception $e) {
                Log::error("ERROR: NO SE PUDO MANDAR EL CORREO DE NOTIFICACION AL USUARIO {$user->Email}");
                Log::error("DETALLE DE ERROR: {$e->getMessage()}");
            }
        }
        return response()->json([
            'status' => 'Ok'
        ], 200);
    }

    public function sendMailPhpMailer($user, $ejercicio) {
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true); // notice the \  you have to use root namespace here
        try {
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->isSMTP(); // tell to use smtp
            $mail->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_SERVER;
            //Set the hostname of the mail server
            $mail->Host = '';
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->Port = 0;
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication
            $mail->Username = '';
            //Password to use for SMTP authentication
            $mail->Password = '';
            //Set who the message is to be sent from
            $mail->setFrom('', '');
            //Set an alternative reply-to address
            $mail->addReplyTo('', '');
            //Set who the message is to be sent to
            $mail->addAddress($user->Email, $user->Nombres.' '.$user->PrimerApellido.' '.$user->SegundoApellido);
            //Set the subject line
            $mail->Subject = 'SIDEPAT - DECLARACIÓN PATRIMONIAL';
            $mail->Body = $this->bodyMessageDeclaracion($user, $ejercicio); // Mensaje a enviar
            $mail->IsHTML(true);
            $mail->send();
            return true;
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            Log::error($e->getMessage());
            return false;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * Enviar correos de notificaciones
     */
    public function enviarCorreoNotificacion()
    {
        try {
            $user = request()->user;
            $user = $this->db_main->table('osaf_usuarios')->where('UsuarioID', '=', $user['UsuarioID'])->first();
            $ejercicio = $this->db_main->table('osaf_ejercicios_cat')->where('Actual', '=', 1)->first();
            $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                                                ->where('UsuarioID', $user->UsuarioID)
                                                ->where('EjercicioID', $ejercicio->EjercicioID)
                                                ->where('Eliminado', false)
                                                ->update([
                                                    'NotificacionEnviada' => true,
                                                    'updated_at' => date('Y-m-d H:i:s')
                                                ]);
            $driver_mail = config('sidepat.driver_correos');
            $formato_email_osafig = config('sidepat.mail_formato_osafig');
            if($driver_mail == 'phpmailer') {
                $enviado = $this->sendMailPhpMailer($user, $ejercicio);
                if($enviado == false) {
                    throw new Exception("PHP MAILER ERROR");
                }
            } else {
                if(is_array($user)) {
                    Mail::to($user['Email'])->send(new NotificationDeclare($user, $ejercicio, $formato_email_osafig));
                } else {
                    Mail::to($user->Email)->send(new NotificationDeclare($user, $ejercicio, $formato_email_osafig));
                }
            }
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::info("ERROR: NO SE PUDO MANDAR EL CORREO DE NOTIFICACION AL USUARIO {$user->Email}");
            Log::info("DETALLE DE ERROR: {$e->getMessage()}");
        }
    }

    /**
     * Retorna el cuerpo del mensaje de la notificación de la declaración patrimonial
     */
    public function bodyMessageDeclaracion($user, $ejercicio)
    {
        $email = $user->Email;
        $ejercicio = $ejercicio->Year - 1;
        $password = \Illuminate\Support\Facades\Crypt::decryptString(($user->PasswordEncrypt));
        $enlace = env('APP_URL');
        $message = "<html><head><meta charset=utf-8\" /></head><body>";
        $message .= "<center>
                        <table style=\"color: #000000 !important;\">
                            <tr>
                                <td style=\"text-align: center;\"><strong>SIDEPAT</strong></td>
                            </tr>
                            <tr>
                                <td style=\"text-align: center;\"><strong></strong></td>
                            </tr>
                        </table> <table style=\"color: #000000 !important;\">
                            <tr>
                                <p style=\"text-align: left; font-weight: bold;\">
                                    TRABAJADORES, SERVIDORES PÚBLICOS
                                </p>
                                <p style=\"text-align: left; font-weight: bold;\">
                                    P R E S E N T E S.-
                                </p>
                                <p style=\"text-align: justify\">
                                    Por medio del presente y con fundamento en lo dispuesto por los artículos 2 fracción X, 49 fracción I y 51 de la Ley General del Sistema Nacional Anticorrupción; 9 fracción II, 10 fracción I, 15, 30, 31, 32, 33, 34, 46 y 48 de la Ley General de Responsabilidades Administrativas; Normas Quinta y Vigésimo primera del Anexo Segundo \"ACUERDO por el que se modifican los Anexos Primero y Segundo del Acuerdo por el que el Comité Coordinador del Sistema Nacional Anticorrupción emite el formato de declaraciones: de situación patrimonial y de intereses; y expide las normas e instructivo para su llenado y presentación\".
                                    Se les recuerda la obligación de presentar ante esta Contraloría Interna la Declaración de Situación Patrimonial y de Intereses, de modificación; toda vez que de no hacerlo se harán acreedores a las sanciones señaladas en el artículo 75 fracciones I, II, III, y IV, y artículo 78 fracciones I, II, III, y IV de la Ley General de Responsabilidades Administrativas.
                                </p>
                                <p style=\"text-align: center; margin-top: 10px; margin-bottom: 10px;\">
                                    Enlace: $enlace <br>
                                    Correo: $email <br>
                                    Contraseña: $password
                                </p>
                                <p style=\"text-align: justify\">
                                    Así mismo, se solicita a quienes estén obligados a presentar la declaración de anual de sueldos y salarios presenten constancia del acuse de la presentación de la declaración del Impuesto Sobre la Renta, correspondiente al ejercicio fiscal
                                    de $ejercicio, en caso de encontrarse en los supuestos del artículo 150 de la Ley del Impuesto Sobre la Renta y artículos 31 y 31 Código Fiscal de la Federación, y tener la obligación de presentarla ante el SAT.
                                </p>
                                <p style=\"text-align: justify\">
                                    Sin otro particular, reciban un cordial saludo.
                                </p>
                            </tr>
                        </table>
                    </center>";
        $message .= "</body></html>";
        return $message;
    }

    /**
     * Registrar fideicomiso
     */
    public function saveFideicomiso(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro de participacion
            $this->db_declaracion->select('CALL sp_registroFideicomiso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->participante,
                request()->tipo_fideicomiso,
                request()->tipo_participacion,
                request()->rfc_fideicomiso,
                request()->tipo_persona_fidecomitente,
                request()->razon_social_fideicomitente,
                request()->rfc_fideicomitente,
                request()->tipo_persona_fideicomisario,
                request()->razon_social_fideicomisario,
                request()->rfc_fideicomisario,
                request()->sector_id,
                request()->es_extranjero,
                request()->observaciones,
                request()->fideicomisos_id,
                request()->tipo_persona_fiduciario,
                request()->razon_social_fiduciario,
                request()->rfc_fiduciario,
                request()->otro_sector
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(exception $e){
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'error' => $e,
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getFideicomisos()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $fideicomisos = $this->dr->getFideicomisos($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'fideicomisos' => $fideicomisos
        ]);
    }

    /**
     * Borrar fideicomiso
     */
    public function borrarFideicomiso()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_fideicomisos')
                ->where('FideicomisosID', '=', request()->fideicomiso_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion como terminada
     */
    public function marcarTerminadaFideicomisos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 22);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * marcar seccion de fideicomisos como ninguna
     */
    public function marcarNingunaFideicomisos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_fideicomisos')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 22);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * obtener catalogo de tipo de adeudo
     */
    public function getCatTipoAdeudo()
    {
        $tipo_adeudo_cat = $this->dr->getCatalogoTipoAdeudo();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_adeudo' => $tipo_adeudo_cat
        ], 200);
    }
    /**
     * Obtener inversiones
     */
    public function getAdeudos()
    {
        // $usuario = request()->user()->UsuarioID;
        $declaracion_id = request()->declaracion_id;
        $inversiones = $this->dr->getAdeudos($declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'adeudos' => $inversiones
        ]);
    }
    /**
     * Registrar adeudo
     */
    public function saveAdeudos(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('CALL sp_registrarAdeudo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->titular,
                request()->tipo_adeudo_id,
                request()->no_cuenta,
                request()->fecha_otorgamiento,
                request()->monto_original,
                request()->moneda,
                request()->saldo_insoluto,
                request()->nombre_tercero,
                request()->tipo_persona_tercero,
                request()->rfc_tercero,
                request()->ubicacion,
                request()->nombre_razon_social,
                request()->tipo_persona_declarante,
                request()->es_extranjero,
                request()->aclaraciones,
                request()->adeudo_id,
                request()->rfc_otorgante,
                request()->otro_tipo_adeudo
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Finalizar declaracion patrimonial
     */
    public function finalizarDeclaracion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $declaracion_id = request()->declaracion_id;
            $datos_declaracion = $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID','=', $declaracion_id)
                ->first();

            $ejercicio = $this->db_main->table('osaf_ejercicios_cat')
            ->where('EjercicioID','=',$datos_declaracion->EjercicioID)
            ->first();

            $datos_declaracion->AnioDeclarado = $ejercicio->Year;

            $espec_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID','=',$datos_declaracion->TipoDeclaracionID)
                ->first();

            $datos_declaracion->NombreDeclaracion = $espec_declaracion->Nombre;
            $datos_declaracion->NombreCorto = $espec_declaracion->NombreCorto;

            $usuario = $this->db_main->table('osaf_usuarios')
                ->select('Nombres','PrimerApellido','SegundoApellido','CURP','RFC','Email')
                ->where('UsuarioID','=',$datos_declaracion->UsuarioID)
                ->first();
            // Obtener tipo de declaracion patrimonial
            $tipo_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')->where('TipoDeclaracionID', '=', $datos_declaracion->TipoDeclaracionID)->first();
            // ------------------------------------------------------
            // GENERAR DECLARACION PATRIMONIAL
            // ------------------------------------------------------
            $secciones = $this->db_declaracion->select('call sp_estadoDeclaracion(?)',[
                $declaracion_id
            ]);
            $secciones_pdf = [];
            $indice = [];
            foreach($secciones as $seccion){
                if ($seccion->Aplica == 1){
                    array_push($indice, $seccion->Nombre);
                    $secciones_pdf[$seccion->Nombre] = $seccion;
                }
            }
            // -------------------------------------------Datos generales
            if(array_key_exists('DATOS GENERALES',$secciones_pdf)){
                $datos_generales = $this->db_declaracion->table('oic_datos_personales as odp')
                    ->join('oic_estado_civil_cat as oec','oec.EstadoCivilID','=','odp.EstadoCivilID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('UsuarioMainID','=',$datos_declaracion->UsuarioID)
                    ->first();
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDatos        = $datos_generales;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }else{
                $secciones_pdf["DATOS GENERALES"] = (object)[
                    "SeccionID" => 1,
                    "Nombre" => "DATOS GENERALES",
                    "EstadoActual" => 1,
                    "Aplica" => 1,
                    "Parte"=> 1,
                ];
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }
            // -----------------------------------------Domicilio del declarante
            if(array_key_exists('DOMICILIO DECLARANTE',$secciones_pdf)){
                $domicilio = $this->db_declaracion->table('oic_direcciones as od')
                    ->select('od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','od.Aclaraciones',
                        'od.Aclaraciones','od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DireccionID','=', $datos_declaracion->DireccionID)
                    ->first();
                $secciones_pdf["DOMICILIO DECLARANTE"]->Info = $domicilio;
            }
            // ------------------------------------------Datos curriculares
            if(array_key_exists('DATOS CURRICULARES DEL DECLARANTE',$secciones_pdf)){
                $curriculares = $this->db_declaracion->table('oic_nivel_estudios_detalle as oned')
                    ->join('oic_nivel_estudios_cat as onec','oned.NivelEstudioID','=','onec.NivelEstudioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->first();
                $secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info = $curriculares;
            }
            // -----------------------------------------Datos cargo
            if(array_key_exists('DATOS DEL EMPLEO, CARGO O COMISIÓN',$secciones_pdf)){
                $cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo')
                    ->join('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->join('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('TipoCargo','=',1)
                    ->first();
                $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info = $cargo;
                $otro_cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones', 'oc.AreaEspecifica',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo', 'oc.PuestoEspecifico')
                    ->leftJoin('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->leftJoin('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('Oculto', '=', 0)
                    ->where('TipoCargo','=',2)
                    ->first();
                $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo = (is_null($otro_cargo)) ? [] : $otro_cargo;
            }
            // -----------------------------------------Experiencia laboral
            if(array_key_exists('EXPERIENCIA LABORAL',$secciones_pdf)){
                $experiencias = $this->db_declaracion->table('oic_experiencia_laboral_detalle as exp')
                    ->leftJoin('oic_orden_gobierno as oog','exp.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->leftJoin('oic_ambito_cat as oac','exp.AmbitoID','=','oac.AmbitoID')
                    ->leftJoin('oic_sector_productivo_cat as osp','exp.SectorID','=','osp.SectorID')
                    ->where('exp.UsuarioID','=',$datos_declaracion->UsuarioID)
                    ->where('exp.Oculto','=',0)
                  ->get();
                $secciones_pdf["EXPERIENCIA LABORAL"]->Info = $experiencias;
            }
            // ---------------------------------------Datos de la pareja
            if(array_key_exists('DATOS DE LA PAREJA',$secciones_pdf)){
                $pareja = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 1]);
                $secciones_pdf["DATOS DE LA PAREJA"]->Info = $pareja;
            }
            // --------------------------------------Datos dependientes
            if(array_key_exists('DATOS DEL DEPENDIENTE ECONÓMICO',$secciones_pdf)){
                $dependientes = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 0]);
                $secciones_pdf["DATOS DEL DEPENDIENTE ECONÓMICO"]->Info = $dependientes;
            }
            // ------------------------------------Ingresos
            // Obtener datos de ingreso
            if(array_key_exists('INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS',$secciones_pdf)){
                $ingresos['ingresos'] = $this->dr->getIngresosDeclaracion($declaracion_id);
                // actividades industriales
                $industriales = $this->dr->getIngresosActividadesIndustriales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['actividades_industriales'] = (is_null($ingresos['ingresos'])) ? [] : $industriales;
                // actividades financieras
                $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                    ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                    ->where('oiaf.IngresoID','=',$ingresos['ingresos']->IngresoID)
                    ->get();
                $ingresos['otros_ingresos']['actividades_financieras'] = (is_null($ingresos['ingresos'])) ? [] : $financieras;
                // servicios profesionales
                $profesionales = $this->dr->getIngresosServiciosProfesionales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['servicios_profesionales'] = (is_null($ingresos['ingresos'])) ? [] : $profesionales;
                // otros ingresos
                $otros = $this->dr->getIngresosOtros($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['otros_ingresos'] = (is_null($ingresos['ingresos'])) ? [] : $otros;
                // bienes enajenados
                $enajenados = $this->dr->getBienesEnajenados($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['bienes_enajenados'] = (is_null($ingresos['ingresos'])) ? [] : $enajenados;
                $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info = ($ingresos != []) ? $ingresos : [];
            }
            // ------------------------------------Servidor publico el año pasado
            if (array_search('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true){
                // Obtener datos de ingreso
                $servidor_pasado['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado($declaracion_id);
                if($servidor_pasado['ingresos'] != null){
                    // actividades industriales
                    $industriales = $this->dr->getIngresosActividadesIndustriales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['actividades_industriales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $industriales;
                    // actividades financieras
                    $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                        ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                        ->where('oiaf.IngresoID','=',$servidor_pasado['ingresos']->ServidorPasadoID)
                        ->get();
                    $servidor_pasado['otros_ingresos']['actividades_financieras'] = (is_null($servidor_pasado['ingresos'])) ? [] : $financieras;
                    // servicios profesionales
                    $servicios = $this->dr->getIngresosServiciosProfesionales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['servicios_profesionales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $servicios;
                    // otros ingresos
                    $otros = $this->dr->getIngresosOtros($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['otros_ingresos'] = (is_null($servidor_pasado['ingresos'])) ? [] : $otros;
                    // bienes enajenados
                    $bienes_enajenados = $this->dr->getBienesEnajenados($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['bienes_enajenados'] = (is_null($servidor_pasado['ingresos'])) ? [] : $bienes_enajenados;
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = $servidor_pasado;
                }
                else{
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = 'No aplica';
                }
            }

            // ------------------------------------BIENES INMUEBLES
            // obtener BIENES INMUEBLES
            if (array_search('BIENES INMUEBLES',$indice) == true){
                $bienes_inmuebles = $this->db_declaracion->select('call sp_obtenerBienesInmuebles(?)',[$declaracion_id]);
                $secciones_pdf["BIENES INMUEBLES"]->Info = $bienes_inmuebles;
            }
            // ------------------------------------VEHÍCULOS
            // obtener vehiculos
            if (array_search('VEHÍCULOS',$indice) == true){
                $vehiculos = $this->db_declaracion->select('call sp_obtenerVehiculos(?)',[$declaracion_id]);
                $secciones_pdf["VEHÍCULOS"]->Info = $vehiculos;
            }
            // ------------------------------------BIENES MUEBLES
            // obtener BIENES MUEBLES
            if (array_search('BIENES MUEBLES',$indice) == true){
                $bienes_muebles = $this->db_declaracion->select('call sp_obtenerBienesMueble(?)',[$declaracion_id]);
                $secciones_pdf["BIENES MUEBLES"]->Info = $bienes_muebles;
            }
            // ------------------------------------INVERSIONES
            // obtener INVERSIONES
            if (array_search('INVERSIONES',$indice) == true){
                $inversiones = $this->db_declaracion->select('call sp_obtenerInversiones(?)',[$declaracion_id]);
                $secciones_pdf["INVERSIONES"]->Info = $inversiones;
            }
            // ------------------------------------ADEUDOS/PASIVOS
            // obtener ADEUDOS/PASIVOS
            if (array_search('ADEUDOS/PASIVOS',$indice) == true){
                $adeudos = $this->db_declaracion->select('call sp_obtenerAdeudos(?)',[$declaracion_id]);
                $secciones_pdf["ADEUDOS/PASIVOS"]->Info = $adeudos;
            }
            // ------------------------------------PRESTAMO O COMODATO POR TERCEROS
            // obtener PRESTAMO O COMODATO POR TERCEROS
            if (array_search('PRESTAMO O COMODATO POR TERCEROS',$indice) == true){
                $prestamos = $this->db_declaracion->select('call sp_obtenerPrestamos(?)',[$declaracion_id]);
                $secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info = $prestamos;
            }
            // ------------------------------------PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // obtener PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            if (array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice) !== false){
                $participacion_empresas = $this->db_declaracion->table('oic_participacion_empresas as ope')
                ->join('oic_sector_productivo_cat as ospc', 'ope.SectorID', '=' ,'ospc.SectorID')
                ->where('ope.DeclaracionID','=',$declaracion_id)
                ->where('ope.Oculto','=',0)
                ->get();
                $secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info = $participacion_empresas;
            }
            // ------------------------------------PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            // obtener PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            if (array_search('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN',$indice) == true){
                $participacion_instituciones = $this->db_declaracion->table('oic_participacion_instituciones as opi')
                    ->join('oic_tipo_institucion as oti', 'oti.TipoInstitucionID', '=' ,'opi.TipoInstitucionID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('opi.Oculto','=',0)
                    ->get();
                $secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info = $participacion_instituciones;
            }
            // ------------------------------------APOYOS O BENEFICIOS PÚBLICOS
            // obtener APOYOS O BENEFICIOS PÚBLICOS
            if(array_search('APOYOS O BENEFICIOS PÚBLICOS',$indice) == true){
                $apoyos = $this->db_declaracion->table('oic_apoyos_beneficios as oab')
                    ->join('oic_ambito_cat as oac', 'oab.AmbitoID', '=' ,'oac.AmbitoID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('Oculto','=',0)
                    ->get();
                $secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info = $apoyos;
            }
            // ------------------------------------REPRESENTACIÓN
            // obtener REPRESENTACIÓN
            if(array_search('REPRESENTACIÓN',$indice) == true){
                $representaciones = $this->db_declaracion->table('oic_representacion as rep')
                    ->join('oic_sector_productivo_cat as ospc', 'rep.SectorID', '=' ,'ospc.SectorID')
                    ->where('rep.DeclaracionID','=',$declaracion_id)
                    ->where('rep.Oculto','=',0)
                    ->get();
                $secciones_pdf["REPRESENTACIÓN"]->Info = $representaciones;
            }
            // ------------------------------------CLIENTES PRINCIPALES
            // obtener CLIENTES PRINCIPALES
            if(array_search('CLIENTES PRINCIPALES',$indice) == true){
                $clientes = $this->db_declaracion->table('oic_clientes_principales as ocp')
                    ->join('oic_sector_productivo_cat as ospc', 'ocp.SectorID', '=' ,'ospc.SectorID')
                    ->where('ocp.DeclaracionID','=',$declaracion_id)
                    ->where('ocp.Oculto','=',0)
                    ->get();
                $secciones_pdf["CLIENTES PRINCIPALES"]->Info = $clientes;
            }
            // ------------------------------------BENEFICIOS PRIVADOS
            // obtener BENEFICIOS PRIVADOS
            if(array_search('BENEFICIOS PRIVADOS',$indice) == true){
                $beneficios = $this->db_declaracion->table('oic_beneficios_privados as obp')
                    ->join('oic_sector_productivo_cat as ospc', 'obp.SectorID', '=' ,'ospc.SectorID')
                    ->where('obp.DeclaracionID','=',$declaracion_id)
                    ->where('obp.Oculto','=',0)
                    ->get();
                $secciones_pdf["BENEFICIOS PRIVADOS"]->Info = $beneficios;
            }
            // ------------------------------------FIDEICOMISOS
            // obtener FIDEICOMISOS
            if(array_search('FIDEICOMISOS',$indice) == true){
                $fideicomisos = $this->db_declaracion->table('oic_fideicomisos as of2')
                    ->join('oic_sector_productivo_cat as ospc', 'of2.SectorID', '=' ,'ospc.SectorID')
                    ->where('of2.DeclaracionID','=',$declaracion_id)
                    ->where('of2.Oculto','=',0)
                    ->get();
                $secciones_pdf["FIDEICOMISOS"]->Info = $fideicomisos;
            }
            ini_set('memory_limit', '-1');
            if($datos_declaracion->TipoDeclaracionID != 7){
                $pdf = \PDF::loadView('pdf.declaracion_patrimonial',compact("indice","secciones_pdf"));
            }
            else{
                $pdf = \PDF::loadView('pdf.declaracion_conflicto',compact("indice","secciones_pdf"));
            }
            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'declaracion.pdf');
            $path_acuse_declaracion = $ubicacion."declaracion.pdf";
            ini_set('memory_limit', '-1');
            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'declaracion_publica.pdf');
            $path_acuse_declaracion = $ubicacion."declaracion_publica.pdf";
            // ------------------------------------------------------
            // GENERAR ACUSE DE RECIBO DE LA DECLARACIÓN
            // ------------------------------------------------------
            // Se debe generar el acuse
            $hash = $this->getHash($path_acuse_declaracion);
            $nombre_usuario = $usuario->Nombres.' '.$usuario->PrimerApellido.' '.$usuario->SegundoApellido;
            // Generacion del ause
            ini_set('memory_limit', '-1');
            $ejercicio = \DB::connection('main')->table('osaf_ejercicios_cat')->where('Actual', '=', 1)->first();
            if($datos_declaracion->TipoDeclaracionID == 2) {
                $ejercicio_cat = $this->db_main->table('osaf_ejercicios_cat')
                    ->where('EjercicioID','=',$datos_declaracion->EjercicioID)
                    ->first();
                $ejercicio = '31-12-'.$ejercicio_cat->Year;
            } else {
                $ejercicio = date('Y-m-d H:i:s');
            }
            $declaracion_temp = $this->db_declaracion->table('oic_declaraciones_patrimoniales')->where('DeclaracionID', '=', request()->declaracion_id)->first();
            if(is_null($declaracion_temp->DeclaracionFecha)) {
                $fecha_declaracion_temp = date('Y-m-d H:i:s');
            } else {
                $fecha_declaracion_temp = $declaracion_temp->DeclaracionFecha;
            }
            //Cargar el tipo de declaracion
            $pdf = \PDF::loadView('pdf.acuse_recibo_declaracion',compact("nombre_usuario","hash", "ejercicio", "tipo_declaracion", "fecha_declaracion_temp"));
            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'acuse.pdf');

            $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Finalizado'       => 1,
                    'RutaDeclaracionCompleta' => $ubicacion.'declaracion.pdf',
                    'RutaDeclaracionPublica' => $ubicacion.'declaracion_publica.pdf',
                    'RutaAcuse'         => $ubicacion.'acuse.pdf',
                    'Hash'              => $hash,
                    'DeclaracionFecha' => $fecha_declaracion_temp,
                    'updated_at'       => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Finaliza la declaración patrimonial sin generar acuses
     */
    public function finalizarDeclaracionSinPdf()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $declaracion_id = request()->declaracion_id;
            $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID','=', $declaracion_id)
                ->update([
                    'DeclaracionFecha' => date('Y-m-d H:i:s'),
                    'Finalizado'       => true
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener las declaraciones por ejercicio
     */
    public function getDeclaraciones(){
        $declaraciones = $this->dr->getDeclaracionesEjercicio(request()->all());
        $estatus = $this->db_declaracion->table('v_resumenEstatus2')->where('Year', '=', request('ejercicio'))->get();
        $tipo = $this->db_declaracion->table('v_resumenTipo2')->where('Year', '=', request('ejercicio'))->get();
        return response()->json([
            'status' => 'Ok',
            'declaraciones' => $declaraciones,
            'estatus'       => $estatus,
            'tipo'          => $tipo
        ]);
    }

    /**
     * Obtener las declaraciones por ejercicio
     */
    public function getDeclaracionesTerminadas(){
        $ejercicio = $this->db_main
            ->table('osaf_ejercicios_cat')
            ->where('Year', '=', request()->ejercicio)
            ->first();
        $declaraciones = $this->dr->getDeclaracionesTerminadas($ejercicio->EjercicioID);
        return response()->json([
            'status' => 'Ok',
            'declaraciones' => $declaraciones
        ]);
    }

    /**
     * Obtener detalles de una declaracion
     */
    public function getEstadisticaDeclaracion()
    {
        $declaracion = $this->dr->getEstadisticaDeclaracion(request()->declaracion_id);
        $secciones = $this->dr->getSeccionesADeclarar(request()->declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'declaracion' => $declaracion,
            'secciones' => $secciones
        ]);
    }

    /**
     * generar acuse de recibido
     */
    public function generarAcuseDeclaracion()
    {
        try{
            $this->db_declaracion->beginTransaction();

        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error',
                'error' => $e
            ], 500);
        }
    }

    /**
     * Genera el hash de cada uno de los archivos que se suben al servidor
     */
    public function getHash($path)
    {
        $algoritmo = 'sha256';
        $content = Storage::disk('public')->get($path);
        $hash = hash($algoritmo, $content);
        return strtoupper($hash);
    }

    /**
     * Marcar sección como terminada
     */
    public function marcarTerminadoComo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionComo(request('declaracion_id'), request('seccionId'), request('terminadoComo'));
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error',
                'error' => $e
            ], 500);
        }
    }

    /**
     * Setear contraseñas
     */
    public function setAllPasswords()
    {
        $requestUsers = $this->getUsersToDeclare();
        $id_users = collect(json_decode($requestUsers->content())->users)->pluck('UsuarioID');
        $users = $this->db_main->table('osaf_usuarios')
            ->whereIn('UsuarioID', $id_users)
            ->where('Password', '=', '')
            ->orWhereNull('Password')
            ->get();
        foreach($users as $user){
            $password = Str::random(8);
            $password_hash = Hash::make($password);
            $password_encrypt = Crypt::encryptString($password);
            try {
                $this->db_main->beginTransaction();
                $this->db_main->table('osaf_usuarios')
                    ->where('UsuarioID', $user->UsuarioID)
                    ->update([
                        'Password'=>$password_hash,
                        'PasswordEncrypt'=>$password_encrypt,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                $this->db_main->commit();
            }catch(Exception $e){
                $this->db_main->rollBack();
                return response()->json([
                    'status' => 'error'
                ], 500);
            }
        }
        return response()->json([
            'status' => 'Ok'
        ], 200);
    }

    /**
     * Finalizar declaracion patrimonial
     */
    public function generarDeclaracion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $declaracion_id = request()->declaracion_id;
            $datos_declaracion = $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID','=', $declaracion_id)
                ->first();


            $ejercicio = $this->db_main->table('osaf_ejercicios_cat')
            ->where('EjercicioID','=',$datos_declaracion->EjercicioID)
            ->first();

            $datos_declaracion->AnioDeclarado = $ejercicio->Year;

            $espec_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID','=',$datos_declaracion->TipoDeclaracionID)
                ->first();

            $datos_declaracion->NombreDeclaracion = $espec_declaracion->Nombre;
            $datos_declaracion->NombreCorto = $espec_declaracion->NombreCorto;

            $usuario = $this->db_main->table('osaf_usuarios')
                ->select('Nombres','PrimerApellido','SegundoApellido','CURP','RFC','Email')
                ->where('UsuarioID','=',$datos_declaracion->UsuarioID)
                ->first();

            $nombre_completo = $usuario->Nombres."_".$usuario->PrimerApellido."_".$usuario->SegundoApellido;

            // Obtener tipo de declaracion patrimonial
            $tipo_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')->where('TipoDeclaracionID', '=', $datos_declaracion->TipoDeclaracionID)->first();
            // ------------------------------------------------------
            // GENERAR DECLARACION PATRIMONIAL
            // ------------------------------------------------------
            $secciones = $this->db_declaracion->select('call sp_estadoDeclaracion(?)',[
                $declaracion_id
            ]);
            $secciones_pdf = [];
            $indice = [];
            foreach($secciones as $seccion){
                if ($seccion->Aplica == 1){
                    array_push($indice, $seccion->Nombre);
                    $secciones_pdf[$seccion->Nombre] = $seccion;
                }
            }
            // -------------------------------------------Datos generales
            if(array_key_exists('DATOS GENERALES',$secciones_pdf)){
                $datos_generales = $this->db_declaracion->table('oic_datos_personales as odp')
                    ->join('oic_estado_civil_cat as oec','oec.EstadoCivilID','=','odp.EstadoCivilID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('UsuarioMainID','=',$datos_declaracion->UsuarioID)
                    ->first();
                    $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDatos        = $datos_generales;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }else{
                $secciones_pdf["DATOS GENERALES"] = (object)[
                    "SeccionID" => 1,
                    "Nombre" => "DATOS GENERALES",
                    "EstadoActual" => 1,
                    "Aplica" => 1,
                    "Parte"=> 1,
                ];
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }
            // -----------------------------------------Domicilio del declarante
            if(array_key_exists('DOMICILIO DECLARANTE',$secciones_pdf)){
                $domicilio = $this->db_declaracion->table('oic_direcciones as od')
                ->select('od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','od.Aclaraciones',
                'od.Aclaraciones','od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio')
                ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                ->where('DireccionID','=', $datos_declaracion->DireccionID)
                ->first();
                $secciones_pdf["DOMICILIO DECLARANTE"]->Info = $domicilio;
            }
            // ------------------------------------------Datos curriculares
            if(array_key_exists('DATOS CURRICULARES DEL DECLARANTE',$secciones_pdf)){
                $curriculares = $this->db_declaracion->table('oic_nivel_estudios_detalle as oned')
                ->join('oic_nivel_estudios_cat as onec','oned.NivelEstudioID','=','onec.NivelEstudioID')
                ->where('DeclaracionID','=', $declaracion_id)
                ->first();
                $secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info = $curriculares;
            }
            // -----------------------------------------Datos cargo
            if(array_key_exists('DATOS DEL EMPLEO, CARGO O COMISIÓN',$secciones_pdf)){
                $cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                    'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                    'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                    'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones',
                    'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo')
                    ->join('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->join('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('TipoCargo','=',1)
                    ->first();
                $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info = $cargo;
                $otro_cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones', 'oc.AreaEspecifica',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo', 'oc.PuestoEspecifico')
                    ->leftJoin('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->leftJoin('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('Oculto', '=', 0)
                    ->where('TipoCargo','=',2)
                    ->first();
                    $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo = (is_null($otro_cargo)) ? [] : $otro_cargo;
                }
            // -----------------------------------------Experiencia laboral
            if(array_key_exists('EXPERIENCIA LABORAL',$secciones_pdf)){
                $experiencias = $this->db_declaracion->table('oic_experiencia_laboral_detalle as exp')
                    ->leftJoin('oic_orden_gobierno as oog','exp.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->leftJoin('oic_ambito_cat as oac','exp.AmbitoID','=','oac.AmbitoID')
                    ->leftJoin('oic_sector_productivo_cat as osp','exp.SectorID','=','osp.SectorID')
                    ->where('exp.UsuarioID','=',$datos_declaracion->UsuarioID)
                    ->where('exp.Oculto','=',0)
                  ->get();
                    $secciones_pdf["EXPERIENCIA LABORAL"]->Info = $experiencias;
                }
            // ---------------------------------------Datos de la pareja
            if(array_key_exists('DATOS DE LA PAREJA',$secciones_pdf)){
                $pareja = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 1]);
                    $secciones_pdf["DATOS DE LA PAREJA"]->Info = $pareja;
                }
            // --------------------------------------Datos dependientes
            if(array_key_exists('DATOS DEL DEPENDIENTE ECONÓMICO',$secciones_pdf)){
                $dependientes = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 0]);
                    $secciones_pdf["DATOS DEL DEPENDIENTE ECONÓMICO"]->Info = $dependientes;
                }
            // ------------------------------------Ingresos
            // Obtener datos de ingreso
            if(array_key_exists('INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS',$secciones_pdf)){
                $ingresos['ingresos'] = $this->dr->getIngresosDeclaracion($declaracion_id);
                // actividades industriales
                $industriales = $this->dr->getIngresosActividadesIndustriales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['actividades_industriales'] = (is_null($ingresos['ingresos'])) ? [] : $industriales;
                // actividades financieras
                $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                ->where('oiaf.IngresoID','=',$ingresos['ingresos']->IngresoID)
                ->get();
                $ingresos['otros_ingresos']['actividades_financieras'] = (is_null($ingresos['ingresos'])) ? [] : $financieras;
                // servicios profesionales
                $profesionales = $this->dr->getIngresosServiciosProfesionales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['servicios_profesionales'] = (is_null($ingresos['ingresos'])) ? [] : $profesionales;
                // otros ingresos
                $otros = $this->dr->getIngresosOtros($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['otros_ingresos'] = (is_null($ingresos['ingresos'])) ? [] : $otros;
                // bienes enajenados
                $enajenados = $this->dr->getBienesEnajenados($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['bienes_enajenados'] = (is_null($ingresos['ingresos'])) ? [] : $enajenados;
                $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info = ($ingresos != []) ? $ingresos : [];
                }
            // ------------------------------------Servidor publico el año pasado
            if (array_search('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true){
                // Obtener datos de ingreso
                $servidor_pasado['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado($declaracion_id);
                if($servidor_pasado['ingresos'] != null){
                    // actividades industriales
                    $industriales = $this->dr->getIngresosActividadesIndustriales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['actividades_industriales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $industriales;
                    // actividades financieras
                    $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                        ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                        ->where('oiaf.IngresoID','=',$servidor_pasado['ingresos']->ServidorPasadoID)
                        ->get();
                    $servidor_pasado['otros_ingresos']['actividades_financieras'] = (is_null($servidor_pasado['ingresos'])) ? [] : $financieras;
                    // servicios profesionales
                    $servicios = $this->dr->getIngresosServiciosProfesionales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['servicios_profesionales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $servicios;
                    // otros ingresos
                    $otros = $this->dr->getIngresosOtros($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['otros_ingresos'] = (is_null($servidor_pasado['ingresos'])) ? [] : $otros;
                    // bienes enajenados
                    $bienes_enajenados = $this->dr->getBienesEnajenados($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['bienes_enajenados'] = (is_null($servidor_pasado['ingresos'])) ? [] : $bienes_enajenados;
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = $servidor_pasado;
                }
                else{
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = 'No aplica';
                }
            }

            // ------------------------------------BIENES INMUEBLES
            // obtener BIENES INMUEBLES
            if (array_search('BIENES INMUEBLES',$indice) == true){
                $bienes_inmuebles = $this->db_declaracion->select('call sp_obtenerBienesInmuebles(?)',[$declaracion_id]);
                $secciones_pdf["BIENES INMUEBLES"]->Info = $bienes_inmuebles;
            }
            // ------------------------------------VEHÍCULOS
            // obtener vehiculos
            if (array_search('VEHÍCULOS',$indice) == true){
                $vehiculos = $this->db_declaracion->select('call sp_obtenerVehiculos(?)',[$declaracion_id]);
                $secciones_pdf["VEHÍCULOS"]->Info = $vehiculos;
            }
            // ------------------------------------BIENES MUEBLES
            // obtener BIENES MUEBLES
            if (array_search('BIENES MUEBLES',$indice) == true){
                $bienes_muebles = $this->db_declaracion->select('call sp_obtenerBienesMueble(?)',[$declaracion_id]);
                $secciones_pdf["BIENES MUEBLES"]->Info = $bienes_muebles;
            }
            // ------------------------------------INVERSIONES
            // obtener INVERSIONES
            if (array_search('INVERSIONES',$indice) == true){
                $inversiones = $this->db_declaracion->select('call sp_obtenerInversiones(?)',[$declaracion_id]);
                $secciones_pdf["INVERSIONES"]->Info = $inversiones;
            }
            // ------------------------------------ADEUDOS/PASIVOS
            // obtener ADEUDOS/PASIVOS
            if (array_search('ADEUDOS/PASIVOS',$indice) == true){
                $adeudos = $this->db_declaracion->select('call sp_obtenerAdeudos(?)',[$declaracion_id]);
                $secciones_pdf["ADEUDOS/PASIVOS"]->Info = $adeudos;
            }
            // ------------------------------------PRESTAMO O COMODATO POR TERCEROS
            // obtener PRESTAMO O COMODATO POR TERCEROS
            if (array_search('PRESTAMO O COMODATO POR TERCEROS',$indice) == true){
                $prestamos = $this->db_declaracion->select('call sp_obtenerPrestamos(?)',[$declaracion_id]);
                    $secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info = $prestamos;
            }
            // ------------------------------------PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // obtener PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            if (array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice) !== false){
                $participacion_empresas = $this->db_declaracion->table('oic_participacion_empresas as ope')
                ->join('oic_sector_productivo_cat as ospc', 'ope.SectorID', '=' ,'ospc.SectorID')
                ->where('ope.DeclaracionID','=',$declaracion_id)
                ->where('ope.Oculto','=',0)
                ->get();
                $secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info = $participacion_empresas;
            }
            // ------------------------------------PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            // obtener PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            if (array_search('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN',$indice) == true){
                $participacion_instituciones = $this->db_declaracion->table('oic_participacion_instituciones as opi')
                    ->join('oic_tipo_institucion as oti', 'oti.TipoInstitucionID', '=' ,'opi.TipoInstitucionID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('opi.Oculto','=',0)
                    ->get();
                    $secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info = $participacion_instituciones;
            }
            // ------------------------------------APOYOS O BENEFICIOS PÚBLICOS
            // obtener APOYOS O BENEFICIOS PÚBLICOS
            if(array_search('APOYOS O BENEFICIOS PÚBLICOS',$indice) == true){
                $apoyos = $this->db_declaracion->table('oic_apoyos_beneficios as oab')
                    ->join('oic_ambito_cat as oac', 'oab.AmbitoID', '=' ,'oac.AmbitoID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('Oculto','=',0)
                    ->get();
                    $secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info = $apoyos;
            }
            // ------------------------------------REPRESENTACIÓN
            // obtener REPRESENTACIÓN
            if(array_search('REPRESENTACIÓN',$indice) == true){
                $representaciones = $this->db_declaracion->table('oic_representacion as rep')
                    ->join('oic_sector_productivo_cat as ospc', 'rep.SectorID', '=' ,'ospc.SectorID')
                    ->where('rep.DeclaracionID','=',$declaracion_id)
                    ->where('rep.Oculto','=',0)
                    ->get();
                    $secciones_pdf["REPRESENTACIÓN"]->Info = $representaciones;
            }
            // ------------------------------------CLIENTES PRINCIPALES
            // obtener CLIENTES PRINCIPALES
            if(array_search('CLIENTES PRINCIPALES',$indice) == true){
                $clientes = $this->db_declaracion->table('oic_clientes_principales as ocp')
                    ->join('oic_sector_productivo_cat as ospc', 'ocp.SectorID', '=' ,'ospc.SectorID')
                    ->where('ocp.DeclaracionID','=',$declaracion_id)
                    ->where('ocp.Oculto','=',0)
                    ->get();
                    $secciones_pdf["CLIENTES PRINCIPALES"]->Info = $clientes;
                }
            // ------------------------------------BENEFICIOS PRIVADOS
            // obtener BENEFICIOS PRIVADOS
            if(array_search('BENEFICIOS PRIVADOS',$indice) == true){
                $beneficios = $this->db_declaracion->table('oic_beneficios_privados as obp')
                    ->join('oic_sector_productivo_cat as ospc', 'obp.SectorID', '=' ,'ospc.SectorID')
                    ->where('obp.DeclaracionID','=',$declaracion_id)
                    ->where('obp.Oculto','=',0)
                    ->get();
                    $secciones_pdf["BENEFICIOS PRIVADOS"]->Info = $beneficios;
            }
            // ------------------------------------FIDEICOMISOS
            // obtener FIDEICOMISOS
            if(array_search('FIDEICOMISOS',$indice) == true){
                $fideicomisos = $this->db_declaracion->table('oic_fideicomisos as of2')
                    ->join('oic_sector_productivo_cat as ospc', 'of2.SectorID', '=' ,'ospc.SectorID')
                    ->where('of2.DeclaracionID','=',$declaracion_id)
                    ->where('of2.Oculto','=',0)
                    ->get();
                    $secciones_pdf["FIDEICOMISOS"]->Info = $fideicomisos;
            }
            ini_set('memory_limit', '-1');
            if($datos_declaracion->TipoDeclaracionID != 7){
                $pdf = \PDF::loadView('pdf.declaracion_patrimonial',compact("indice","secciones_pdf"));
            }
            else{
                $pdf = \PDF::loadView('pdf.declaracion_conflicto',compact("indice","secciones_pdf"));
            }
            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'declaracion.pdf');
            $path_acuse_declaracion = $ubicacion."declaracion.pdf";
            ini_set('memory_limit', '-1');


            // if($datos_declaracion->TipoDeclaracionID != 7){
            //     $pdf = \PDF::loadView('pdf.declaracion_patrimonial_publica',compact("indice","secciones_pdf"));
            // }
            // else{
            //     $pdf = \PDF::loadView('pdf.declaracion_conflicto_publica',compact("indice","secciones_pdf"));
            // }

            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'declaracion_publica.pdf');
            $path_acuse_declaracion = $ubicacion."declaracion_publica.pdf";
            // ------------------------------------------------------
            // GENERAR ACUSE DE RECIBO DE LA DECLARACIÓN
            // ------------------------------------------------------
            // Se debe generar el acuse
            $hash = $this->getHash($path_acuse_declaracion);
            $nombre_usuario = $usuario->Nombres.' '.$usuario->PrimerApellido.' '.$usuario->SegundoApellido;
            $fecha_declaracion = $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->DeclaracionFecha;
            // Generacion del ause
            ini_set('memory_limit', '-1');
            $ejercicio = \DB::connection('main')->table('osaf_ejercicios_cat')->where('Actual', '=', 1)->first();
            //Cargar el tipo de declaracion
            $pdf = \PDF::loadView('pdf.acuse_recibo_declaracion',compact("nombre_usuario","hash","fecha_declaracion", "ejercicio", "tipo_declaracion"));
            $ubicacion = "declaracion/{$declaracion_id}/";
            $ubicacion_real = public_path($ubicacion);
            if (!file_exists($ubicacion_real)) {
                mkdir($ubicacion_real, 0777, true);
            }
            $pdf->save($ubicacion_real.'acuse.pdf');
            $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'RutaDeclaracionCompleta' => $ubicacion.'declaracion.pdf',
                    'RutaDeclaracionPublica' => $ubicacion.'declaracion_publica.pdf',
                    'RutaAcuse'         => $ubicacion.'acuse.pdf',
                    'Hash'              => $hash,
                    'updated_at'       => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Regresar la declaración
     */
    public function habilitarDeclaracion()
    {
        try {
            $usuario = request()->user()->UsuarioID;
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->select('CALL sp_reactivarAuditoria(?,?)', [$usuario, request()->DeclaracionID]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener declaraciones de un usuario
     */
    public function obtenerDeclaraciones()
    {
        $usuario_id = request()->user()->UsuarioID;
        // $ejercicio  = $this->db_main->table('osaf_ejercicios_cat')->where('Year', '=', date('Y'))->first();

        $main_db_name = $this->db_main->getDatabaseName();

        $declaraciones_modificacion = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join('oic_tipo_declaracion_cat as otd','odp.TipoDeclaracionID', '=', 'otd.TipoDeclaracionID')
            // ->where('odp.EjercicioID', '=', $ejercicio->EjercicioID)
            ->join("{$main_db_name}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            ->where('odp.TipoDeclaracionID', '=', 2)
            ->where('odp.UsuarioID', '=', $usuario_id)
            ->where('odp.Eliminado', '=', false)
            ->where('odp.Finalizado', '=', false)
            ->select('odp.*', 'oec.Year', 'otd.*')
            ->get();

        $declaraciones_otras = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join('oic_tipo_declaracion_cat as otd','odp.TipoDeclaracionID', '=', 'otd.TipoDeclaracionID')
            ->join("{$main_db_name}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            // ->whereYear('odp.created_at', '=', $ejercicio->Year)
            ->where('odp.TipoDeclaracionID', '<>', 2)
            ->where('odp.UsuarioID', '=', $usuario_id)
            ->where('odp.Eliminado', '=', false)
            ->where('odp.Finalizado', '=', false)
            ->orderBy('odp.TipoDeclaracionID')
            ->select('odp.*', 'oec.Year', 'otd.*')
            ->get();
        $declaraciones = $declaraciones_modificacion->merge($declaraciones_otras);

        // Declaraciones presentadas
        $declaraciones_presentadas = $this->db_declaracion->table('v_estadoDeclaraciones as ved')
            ->join("{$main_db_name}.osaf_ejercicios_cat as oec", 'ved.EjercicioID', '=', 'oec.EjercicioID')
            ->where('ved.UsuarioID', '=', $usuario_id)
            ->where('ved.Finalizado', '=', 1)
            ->orderBy('ved.EjercicioID', 'desc')
            ->select('ved.DeclaracionID', 'ved.TipoDeclaracion', 'oec.Year', 'ved.RutaAcuse', 'ved.RutaDeclaracionCompleta','ved.created_at')
            ->get();
        return response()->json([
            'status'        => 'Ok',
            'declaraciones' => $declaraciones,
            'declaraciones_presentadas' => $declaraciones_presentadas
        ], 200);
    }

    /**
     * Obtener areas de adscripciones
     */
    public function obtenerAdscripciones()
    {
        return response()->json([
            'areas' => $this->dr->getAreasAdscripcion(),
        ]);
    }

    /**
     * Obtener los formatos de versiones publicas de declaracion
     */
    public function getFormatosPublicos()
    {
        $formatos = $this->dr->getFormatosPublicos();
        return response()->json([
            'status'   => 'Ok',
            'formatos' => $formatos
        ], 200);
    }

    /**
     * Crear formatos públicos
     */
    public function crearFormatoPublico()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $formato_id = $this->db_declaracion->table('oic_formato_declaracion')->insertGetId([
                'NombreFormato'        => trim(request('nombre')),
                'Descripcion'          => request('descripcion'),
                'Oculto'               => 0,
                'JustificacionFormato' => request('fundamentacion'),
                'created_at'           => date('Y-m-d H:i:s'),
                'updated_at'           => date('Y-m-d H:i:s'),
            ]);
            // Asignar clave del formato
            $this->db_declaracion->table('oic_formato_declaracion')
                ->where('FormatoID', $formato_id)
                ->update([
                    'ClaveFormato' => "F{$formato_id}"
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("Error | {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Borrar formato de declaración
     */
    public function borrarFormato()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_formato_declaracion')
                ->where('FormatoID', '=', request('formatoId'))
                ->update([
                    'Oculto' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("Error | {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Marcar formato como principal
     */
    public function marcarPrincipalFormato()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_formato_declaracion')
                ->where('Oculto', '=', 0)
                ->where('EnUso', '=', 1)
                ->update([
                    'EnUso' => 0,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->table('oic_formato_declaracion')
                ->where('FormatoID', '=', request('formatoId'))
                ->update([
                    'EnUso' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("Error | {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * editar formato ya existente
     */
    public function editarFormatoPublico()
    {
        try {
            $this->db_declaracion->beginTransaction();
            //Actualizar datos generales del formato
            $this->db_declaracion->table('oic_formato_declaracion')
                ->where('formatoID', '=', request('formatoID'))
                ->update([
                    'NombreFormato'        => trim(request('nombre')),
                    'Descripcion'          => request('descripcion'),
                    'JustificacionFormato' => request('fundamentacion'),
                    'SeccionesAgregadas'   => 1,
                    'CamposAgregados'      => 1,
                    'updated_at'           => date('Y-m-d H:i:s')
                ]);
            // Marcar las secciones del formato que se van a activar
            $secciones = array_unique(Arr::pluck(request('opciones_seleccionadas'), 'seccionID'));
            $this->db_declaracion->table('oic_secciones_formato')
                ->where('FormatoID', '=', request('formatoID'))
                ->update([
                    'Aplica' => false,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            //Obtener las secciones del formato que no van aplicar
            $secciones_no_aplica = $this->db_declaracion->table('oic_secciones_declaracion_cat')->whereNotIn('SeccionDeclaracionID', $secciones)->get();
            foreach($secciones_no_aplica as $seccion_no) {
                $seccion_temp = $this->db_declaracion->table('oic_secciones_formato')
                    ->where('FormatoID', '=', request('formatoID'))
                    ->where('SeccionID', '=', $seccion_no->SeccionDeclaracionID)
                    ->first();
                if(is_null($seccion_temp)) {
                    //Se registra la nueva seccion
                    $this->db_declaracion->table('oic_secciones_formato')
                        ->insert([
                            'FormatoID'  => request('formatoID'),
                            'SeccionID'  => $seccion_no->SeccionDeclaracionID,
                            'Aplica'     => false,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                } else {
                    //Se actualiza la sección
                    $this->db_declaracion->table('oic_secciones_formato')
                        ->where('FormatoID', '=', request('formatoID'))
                        ->where('SeccionID', '=', $seccion_no->SeccionDeclaracionID)
                        ->update([
                            'Aplica' => false,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                }
            }
            foreach ($secciones as $seccion) {
                $seccion_temp = $this->db_declaracion->table('oic_secciones_formato')
                    ->where('FormatoID', '=', request('formatoID'))
                    ->where('SeccionID', '=', $seccion)
                    ->first();
                if(is_null($seccion_temp)) {
                    //Se registra la nueva seccion
                    $this->db_declaracion->table('oic_secciones_formato')
                        ->insert([
                            'FormatoID'  => request('formatoID'),
                            'SeccionID'  => $seccion,
                            'Aplica'     => true,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                } else {
                    //Se actualiza la sección
                    $this->db_declaracion->table('oic_secciones_formato')
                        ->where('FormatoID', '=', request('formatoID'))
                        ->where('SeccionID', '=', $seccion)
                        ->update([
                            'Aplica' => true,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                }
            }
            // Consutar todos los campos disponibles para un formato
            $campos = $this->db_declaracion->table('oic_campos_secciones_cat')->where('VersionPublica', '=', 1)->get();
            foreach($campos as $campo) {
                $campo_temp = $this->db_declaracion->table('oic_campos_formato')
                    ->where('CampoID', '=', $campo->CampoID)
                    ->where('SeccionID', '=', $campo->SeccionID)
                    ->where('FormatoID', '=', request('formatoID'))
                    ->first();
                if(is_null($campo_temp)) {
                    $this->db_declaracion->table('oic_campos_formato')->insert([
                        'CampoID' => $campo->CampoID,
                        'Visible' => false,
                        'SeccionID' => $campo->SeccionID,
                        'FormatoID' => request('formatoID'),
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
            //Marcar las opciones del formato y de la seccion que se van activar
            $this->db_declaracion->table('oic_campos_formato')
                ->where('FormatoID', '=', request('formatoID'))
                ->update([
                    'Visible'    => false,
                    'updated_at' => date('Y-m-d H:i:s')
            ]);
            foreach(request('opciones_seleccionadas') as $opcion) {
                $opcion_temp = $this->db_declaracion->table('oic_campos_formato')
                    ->where('CampoID', '=', $opcion['opcionID'])
                    ->where('SeccionID', '=', $opcion['seccionID'])
                    ->where('FormatoID', '=', $opcion['formatoID'])
                    ->first();
                if(is_null($opcion_temp)) {
                    //Se registra el nuevo campo
                    $this->db_declaracion->table('oic_campos_formato')->insert([
                        'CampoID' => $opcion['opcionID'],
                        'Visible' => true,
                        'SeccionID' => $opcion['seccionID'],
                        'FormatoID' => $opcion['formatoID'],
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    //Se actualiza el nuevo campo
                    $this->db_declaracion->table('oic_campos_formato')
                        ->where('CampoID', '=', $opcion['opcionID'])
                        ->where('SeccionID', '=', $opcion['seccionID'])
                        ->where('FormatoID', '=', $opcion['formatoID'])
                        ->update([
                            'Visible' => true,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                }
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("Error | {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener secciones de las declaraciones
     */
    public function getSeccionesDeclaracion()
    {
        $secciones = $this->db_declaracion->table('oic_secciones_declaracion_cat')->get();
        return response()->json([
            'status'    => 'Ok',
            'secciones' => $secciones
        ], 200, [],JSON_NUMERIC_CHECK);
    }

    /**
     * Obtener secciones que pertenecen a un formato
     */
    public function getSeccionesFormato()
    {
        $secciones_formato = $this->db_declaracion->table('oic_secciones_formato')
            ->where('FormatoID', '=', request('id'))
            ->where('Aplica', '=', true)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'secciones' => $secciones_formato
        ], 200, [],JSON_NUMERIC_CHECK);
    }

    /**
     * Obtener campos del formato
     */
    public function getCamposFormato()
    {
        $campos = $this->db_declaracion->table('oic_campos_secciones_cat')->where('VersionPublica', '=', true)->get();
        return response()->json([
            'status' => 'Ok',
            'campos' => $campos
        ], 200, [],JSON_NUMERIC_CHECK);
    }

    /**
     * Obtener campos del formato seleccionados
     */
    public function getCamposFormatoSeccionSeleccionado()
    {
        $campos = $this->db_declaracion->table('oic_campos_formato')
            ->where('FormatoID', '=', request('id'))
            ->where('Visible', '=', true)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'campos' => $campos
        ], 200, [],JSON_NUMERIC_CHECK);
    }

    /**
     * Copiar un formato de declaración
     */
    public function copiarFormatoDeclaracion(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->select('CALL sp_copiarFormatoDeclaracion(?)',[request()->id]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
    /**
     * Ver catalogos disponibles
     */
    public function getListaCatalogos()
    {
        try {
            $catalogos = $this->db_declaracion->table('oic_catalogos')
                ->where('Editable', '=', 1)
                ->get();
            return response()->json([
                'status' => 'Ok',
                'catalogos' => $catalogos
            ], 200);
        } catch (Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener los ejercicios declarados
     */
    public function getEjerciciosDeclarados()
    {
        $db_main = $this->db_main->getDatabaseName();
        $ejercicios = $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$db_main}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            ->groupBy('odp.EjercicioID', 'oec.Year')
            ->get(['odp.EjercicioID', 'oec.Year']);
        return response()->json([
           'status'     => 'Ok',
           'ejercicios' => $ejercicios
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeclaracionesBacheo()
    {
        $ejercicioID = request('ejercicioID');
        $main_db = $this->db_main->getDatabaseName();
        $declaraciones = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$main_db}.osaf_usuarios as ou", 'odp.UsuarioID', '=', 'ou.UsuarioID')
            ->join('oic_tipo_declaracion_cat as otdc', 'odp.TipoDeclaracionID', '=', 'otdc.TipoDeclaracionID')
            ->join("{$main_db}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            ->where('odp.EjercicioID', '=', $ejercicioID)
            ->where('odp.Finalizado', '=', 1)
            ->where('odp.Eliminado', '=', 0)
            ->when(request('tipo_declaracion'), function ($query, $tipo) {
                return $query->where('otdc.Nombre', '=', $tipo);
            })
            ->when(request('ejercicios_presento'), function($query, $presento) {
                return $query->whereRaw("YEAR(odp.DeclaracionFecha) = ${presento}");
            })
            ->orderBy('Declarante', 'asc')
            ->select('odp.DeclaracionID', DB::raw("CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido) AS Declarante"),
                'otdc.Nombre as TipoDeclaracion', 'oec.Year as EjercicioDeclarado', DB::raw('YEAR(odp.DeclaracionFecha) as DeclaracionFecha'))
            ->paginate(50);

        $ejercicios_presento = $this->db_declaracion->table('oic_declaraciones_patrimoniales')
            ->where('Finalizado', '=', 1)
            ->where('Eliminado', '=', 0)
            ->where('EjercicioID', '=', $ejercicioID)
            ->select(DB::raw('YEAR(DeclaracionFecha) as Ejercicio'))
            ->groupBy('Ejercicio')
            ->get();
        return response()->json([
            'status'        => 'Ok',
            'declaraciones' => $declaraciones,
            'ejercicios_presento' => $ejercicios_presento
        ], 200);
    }

    public function getEjerciciosFiscales()
    {
        $ejercicios = $this->db_main->table('osaf_ejercicios_cat')->get();
        return response()->json([
           'status' => 'Ok',
           'ejercicios' => $ejercicios
        ], 200);
    }

    public function declaracionesDetalle()
    {
        $main_db = $this->db_main->getDatabaseName();
        $declaraciones = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$main_db}.osaf_usuarios as ou", 'odp.UsuarioID', '=', 'ou.UsuarioID')
            ->join('oic_tipo_declaracion_cat as otdc', 'odp.TipoDeclaracionID', '=', 'otdc.TipoDeclaracionID')
            ->whereIn('DeclaracionID', request('declaraciones'))
            ->select('odp.DeclaracionID', DB::raw("CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido) AS Declarante"),
                             'otdc.Nombre as TipoDeclaracion')
            ->get();
        return response()->json([
            'status' => 'OK',
            'declaraciones' => $declaraciones,
        ], 200);
    }

    public function cambiarDeclaracionEjercicio()
    {
        $declaraciones = request('declaraciones');
        $ejercicioID = request('ejercicioID');
        foreach($declaraciones as $DeclaracionID) {
            $this->db_declaracion
                ->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', $DeclaracionID)
                ->update([
                    'EjercicioID' => $ejercicioID,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
        }
        return response()->json([
            'status' => 'OK',
        ]);
    }

    public function usuariosDuplicados()
    {
        $usuarios = $this->db_main->table('osaf_usuarios as ou')
            ->where('ou.Activo', '=', 1)
            ->select(DB::raw("UPPER(CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido)) AS Nombre"))
            ->groupBy('Nombre')
            ->havingRaw('COUNT(*) > 1')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'usuarios' => $usuarios
        ], 200);
    }

    public function obtenerUsuarioNombre()
    {
        $nombre = request('nombre');
        $consulta1 = $this->db_main->table('osaf_usuarios as ou')
            ->where('ou.Activo', '=', 1)
            ->select(DB::raw("UPPER(CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido)) AS Nombre"))
            ->groupBy('Nombre')
            ->havingRaw('COUNT(*) > 1')
            ->get();

        $consulta2 = $this->db_main->table('osaf_usuarios as ou')
            ->where(DB::raw("UPPER(CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido)) IN (".$consulta1.")"))
            ->where('ou.Activo', '=', 1)
            ->select('UsuarioID')
            ->get();

        $main_db = $this->db_main->getDatabaseName();
        $consulta3 = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$main_db}.osaf_usuarios as ou", 'odp.UsuarioID', '=', 'ou.UsuarioID')
            ->join('oic_tipo_declaracion_cat as otd', 'odp.TipoDeclaracionID', '=', 'otd.TipoDeclaracionID')
            ->whereIn('odp.UsuarioID', $consulta2)
            ->where('odp.Eliminado', '=', false)
            ->where('odp.Finalizado', '=', true)
            ->whereRaw(DB::raw("UPPER(concat(ou.Nombres, ' ',ou.PrimerApellido, ' ',ou.SegundoApellido)) = {$nombre}"))
            ->select(DB::raw("UPPER(CONCAT(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido)) AS Declarante"), 'ou.Email', 'odp.DeclaracionID', 'otd.Nombre as TipoDeclaracion')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'declaraciones' => $consulta3
        ], 200);
    }

    /**
     * copiar declaración pasada
     */
    public function copiarDeclaracion()
    {
        $declaracion = $this->dr->getDeclaracionData(request()->declaracion_id);
        $declaracion_id = $declaracion->DeclaracionID;
        $user_id = request()->user()->UsuarioID;
        $tipo_declaracion = $declaracion->TipoDeclaracionID;
        $ejercicio_declaracion_id = $declaracion->EjercicioID;
        if($tipo_declaracion == 1) {
            // Si tipo de declaración es inicial, no se realizará la copia y se marca el bit de copia en 1
            $this->dr->marcarDeclaracionCopiada($declaracion_id);
        } else if($tipo_declaracion == 2) {
            // Declaración Inicio del ejercicio actual
            $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_declaracion_id, 1);
            if (is_object($declaracion_ref)) {
                $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
            } else {
                $ejercicio_id = $ejercicio_declaracion_id - 1;
                // Declaracion de modificación del ejercicio anterior
                $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_id, 2);
                if (is_object($declaracion_ref)) {
                    $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
                } else {
                    // Declaracion de inicio del ejercicio anterior
                    $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_id, 1);
                    if(is_object($declaracion_ref)) {
                        $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
                    }
                }
            }
            $this->dr->marcarDeclaracionCopiada($declaracion_id);
        } else {
            // Declaración Modificacion del ejercicio actual
            $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_declaracion_id, 2);
            if(is_object($declaracion_ref)) {
                $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
            } else {
                $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_declaracion_id, 1);
                if(is_object($declaracion_ref)) {
                    $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
                } else {
                    $ejercicio_id = $ejercicio_declaracion_id - 1;
                    // Declaracion de modificación del ejercicio anterior
                    $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_id, 2);
                    if (is_object($declaracion_ref)) {
                        $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
                    } else {
                        $declaracion_ref = $this->dr->getDeclaracionDataByUserYearType($user_id, $ejercicio_id, 1);
                        if (is_object($declaracion_ref)) {
                            $this->dr->copiarDatosDeclaracion($declaracion_ref->DeclaracionID, $declaracion_id, $user_id);
                        }
                    }
                }
            }
            $this->dr->marcarDeclaracionCopiada($declaracion_id);
        }
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Obtener ejercicios declaraciones publicas
     */
    public function getEjerciciosDeclaracionesPublica()
    {
        $main_db = $this->db_main->getDatabaseName();
        $ejercicios = $this->db_declaracion->table('oic_declaraciones_patrimoniales as odp')
            ->join("{$main_db}.osaf_ejercicios_cat as oec", 'odp.EjercicioID', '=', 'oec.EjercicioID')
            ->groupBy('odp.EjercicioID')
            ->select('odp.EjercicioID', 'oec.Year')
            ->get();
        return response()->json([
            'status'     => 'ok',
            'ejercicios' => $ejercicios
        ], 200);
    }
}
