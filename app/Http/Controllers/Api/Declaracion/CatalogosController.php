<?php

namespace App\Http\Controllers\Api\Declaracion;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Declaracion\CatalogosRepository;
use Illuminate\Support\Facades\{DB, Log};
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CatalogosController extends Controller{

    protected $dr; //Repositorio de declaracion
    protected $db_declaracion;
    protected $db_main;

    public function __construct(CatalogosRepository $dr){
        $this->dr = $dr;
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main = DB::connection('main');
    }

    /**
     * Ver catalogos disponibles
     */
    public function getInversiones(){
        try{
            $tipo_inversion = $this->db_declaracion->table('oic_tipo_inversion_cat')
                ->get();
            $activos_financieros = $this->db_declaracion->table('oic_activos_financieros')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'tipo_inversion' => $tipo_inversion,
                'activos_financieros' => $activos_financieros
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de adscripciones
     */
    public function getAdscripciones(){
        try{
            $adscripciones = $this->db_declaracion->table('oic_adscripciones_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'adscripciones' => $adscripciones
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Cambiar visibilidad de adscripcion
     */
    public function editVisibilidadAdscripciones(){
        $visible =  request('visibilidad');
        try{
            if($visible == 1){
                $adscripciones = $this->db_declaracion->table('oic_adscripciones_cat')
                    ->where('AdscripcionID',request('id_elemento'))
                    ->update(['Visible'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_adscripciones_cat')
                    ->where('AdscripcionID',request('id_elemento'))
                    ->update(['Visible'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de adscripcion
     */
    public function actualizarAdscripcion(){
        try{
            $this->db_declaracion->table('oic_adscripciones_cat')
            ->where('AdscripcionID',request('id_elemento'))
            ->update(['Nombre'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nueva adscripción
     */
    public function registrarAdscripcion(){
        try{
            $this->db_declaracion->table('oic_adscripciones_cat')
            ->insert([
                'Nombre'=> request('adscripcion')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de ambitos laborales
     */
    public function getAmbitos(){
        try{
            $ambitos = $this->db_declaracion->table('oic_ambito_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'ambitos' => $ambitos
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de ambitos laborales
     */
    public function editVisibilidadAmbitos(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_ambito_cat')
                    ->where('AmbitoID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_ambito_cat')
                    ->where('AmbitoID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de adscripcion
     */
    public function actualizarAmbito(){
        try{
            $this->db_declaracion->table('oic_ambito_cat')
            ->where('AmbitoID',request('id_elemento'))
            ->update(['NombreAmbito'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nueva adscripción
     */
    public function registrarAmbito(){
        try{
            $this->db_declaracion->table('oic_ambito_cat')
            ->insert([
                'NombreAmbito'=> request('ambito')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de beneficiarios
     */
    public function getBeneficiarios(){
        try{
            $beneficiarios = $this->db_declaracion->table('oic_beneficiarios_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'beneficiarios' => $beneficiarios
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de beneficiarios
     */
    public function editVisibilidadBeneficiarios(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_beneficiarios_cat')
                    ->where('BeneficiarioID',request('id_elemento'))
                    ->update(['Oculto'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_beneficiarios_cat')
                    ->where('BeneficiarioID',request('id_elemento'))
                    ->update(['Oculto'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de beneficiarios
     */
    public function actualizarBeneficiario(){
        try{
            $this->db_declaracion->table('oic_beneficiarios_cat')
            ->where('BeneficiarioID',request('id_elemento'))
            ->update(['Beneficiario'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo beneficiario
     */
    public function registrarBeneficiario(){
        try{
            $this->db_declaracion->table('oic_beneficiarios_cat')
            ->insert([
                'Beneficiario'=> request('beneficiario')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de formas de operacion
     */
    public function getFormasOperacion(){
        try{
            $formas = $this->db_declaracion->table('oic_forma_operacion_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'formas' => $formas
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de la forma de operacion
     */
    public function editVisibilidadFormaOperacion(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_forma_operacion_cat')
                    ->where('FormaOperacionID',request('id_elemento'))
                    ->update(['activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_forma_operacion_cat')
                    ->where('FormaOperacionID',request('id_elemento'))
                    ->update(['activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de beneficiarios
     */
    public function actualizarFormaOperacion(){
        try{
            $this->db_declaracion->table('oic_forma_operacion_cat')
            ->where('FormaOperacionID',request('id_elemento'))
            ->update(['Nombre'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo beneficiario
     */
    public function registrarFormaOperacion(){
        try{
            $this->db_declaracion->table('oic_forma_operacion_cat')
            ->insert([
                'Nombre'=> request('forma_operacion')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de niveles de empleo
     */
    public function getNivelesEmpleo(){
        try{
            $niveles_empleo = $this->db_declaracion->table('oic_nivel_empleo_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'niveles_empleo' => $niveles_empleo
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de la forma de operacion
     */
    public function editVisibilidadNivelEmpleo(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_nivel_empleo_cat')
                    ->where('NivelCargoID',request('id_elemento'))
                    ->update(['activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_nivel_empleo_cat')
                    ->where('NivelCargoID',request('id_elemento'))
                    ->update(['activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de beneficiarios
     */
    public function actualizarNivelEmpleo(){
        try{
            $this->db_declaracion->table('oic_nivel_empleo_cat')
            ->where('NivelCargoID',request('id_elemento'))
            ->update(['Nivel'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo nivel empleo
     */
    public function registrarNivelEmpleo(){
        try{
            $this->db_declaracion->table('oic_nivel_empleo_cat')
            ->insert([
                'Nivel'=> request('nivel')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de niveles de empleo
     */
    public function getNivelesEstudio(){
        try{
            $niveles_estudio = $this->db_declaracion->table('oic_nivel_estudios_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'niveles_estudio' => $niveles_estudio
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un nivel de estudio
     */
    public function editVisibilidadNivelEstudio(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_nivel_estudios_cat')
                    ->where('NivelEstudioID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_nivel_estudios_cat')
                    ->where('NivelEstudioID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de beneficiarios
     */
    public function actualizarNivelEstudio(){
        try{
            $this->db_declaracion->table('oic_nivel_estudios_cat')
            ->where('NivelEstudioID',request('id_elemento'))
            ->update(['Nombre'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo nivel empleo
     */
    public function registrarNivelEstudio(){
        try{
            $this->db_declaracion->table('oic_nivel_estudios_cat')
            ->insert([
                'Nombre'=> request('nivel')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de puestos
     */
    public function getPuestos(){
        try{
            $puestos = $this->db_main->table('osaf_puestos_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'puestos' => $puestos
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un puesto
     */
    public function editVisibilidadPuesto(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_main->table('osaf_puestos_cat')
                    ->where('PuestoID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_main->table('osaf_puestos_cat')
                    ->where('PuestoID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Cambiar formato de un puesto
     */
    public function editFormatoPuesto(){
        $formato =  request('formato');
        try{
            if($formato == 1){
                $adscripciones = $this->db_main->table('osaf_puestos_cat')
                    ->where('PuestoID',request('id_elemento'))
                    ->update(['Completo'=>0]);
            }else{
                $adscripciones = $this->db_main->table('osaf_puestos_cat')
                    ->where('PuestoID',request('id_elemento'))
                    ->update(['Completo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de beneficiarios
     */
    public function actualizarPuesto(){
        try{
            $this->db_main->table('osaf_puestos_cat')
            ->where('PuestoID',request('id_elemento'))
            ->update(['Nombre'=>request('nombre'), 'Completo'=>request('formato')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo nivel empleo
     */
    public function registrarPuesto(){
        try{
            $this->db_main->table('osaf_puestos_cat')
            ->insert([
                'Nombre'=> request('puesto'),
                'Completo'=>request('formato')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de sectores productivos
     */
    public function getSectores(){
        try{
            $sectores = $this->db_declaracion->table('oic_sector_productivo_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'sectores' => $sectores
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un sector productivo
     */
    public function editVisibilidadSectorProductivo(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_sector_productivo_cat')
                    ->where('SectorID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_sector_productivo_cat')
                    ->where('SectorID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un dato de sector
     */
    public function actualizarSectorProductivo(){
        try{
            $this->db_declaracion->table('oic_sector_productivo_cat')
            ->where('SectorID',request('id_elemento'))
            ->update(['Sector'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo sector
     */
    public function registrarSector(){
        try{
            $this->db_declaracion->table('oic_sector_productivo_cat')
            ->insert([
                'Sector'=> request('sector'),
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de tipos de adeudo
     */
    public function getTiposAdeudos(){
        try{
            $tipos_adeudo = $this->db_declaracion->table('oic_tipo_adeudo_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'tipos_adeudo' => $tipos_adeudo
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un sector productivo
     */
    public function editVisibilidadTipoAdeudo(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_tipo_adeudo_cat')
                    ->where('TipoAdeudoID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_tipo_adeudo_cat')
                    ->where('TipoAdeudoID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un tipo de adeudo
     */
    public function actualizarTipoAdeudo(){
        try{
            $this->db_declaracion->table('oic_tipo_adeudo_cat')
            ->where('TipoAdeudoID',request('id_elemento'))
            ->update(['Nombre'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo sector
     */
    public function registrarTipoAdeudo(){
        try{
            $this->db_declaracion->table('oic_tipo_adeudo_cat')
            ->insert([
                'Nombre'=> request('tipo_adeudo'),
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de tipos de bienes
     */
    public function getTiposBienes(){
        try{
            $tipos_bienes = $this->db_declaracion->table('oic_tipo_bien_cat')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'tipos_bienes' => $tipos_bienes
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un sector productivo
     */
    public function editVisibilidadTipoBien(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_tipo_bien_cat')
                    ->where('TipoBienID',request('id_elemento'))
                    ->update(['Activo'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_tipo_bien_cat')
                    ->where('TipoBienID',request('id_elemento'))
                    ->update(['Activo'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo tipo bien
     */
    public function registrarTipoBien(){
        try{
            $this->db_declaracion->table('oic_tipo_bien_cat')
            ->insert([
                'Nombre'=> request('bien'),
                'Categoria'=>request('categoria')
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de tipos de instituciones
     */
    public function getTiposInstituciones(){
        try{
            $tipos_instituciones = $this->db_declaracion->table('oic_tipo_institucion')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'tipos_instituciones' => $tipos_instituciones
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Cambiar visibilidad de un tipo de institucion
     */
    public function editVisibilidadTipoInstitucion(){
        $activo =  request('activo');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_tipo_institucion')
                    ->where('TipoInstitucionID',request('id_elemento'))
                    ->update(['Oculto'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_tipo_institucion')
                    ->where('TipoInstitucionID',request('id_elemento'))
                    ->update(['Oculto'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar un tipo de institución
     */
    public function actualizarTipoInstitucion(){
        try{
            $this->db_declaracion->table('oic_tipo_institucion')
            ->where('TipoInstitucionID',request('id_elemento'))
            ->update(['TipoInstitucion'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo tipo de institucion
     */
    public function registrarTipoInstitucion(){
        try{
            $this->db_declaracion->table('oic_tipo_institucion')
            ->insert([
                'TipoInstitucion'=> request('tipo_institucion'),
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Ver catalogo de tipos de participantes
     */
    public function getTiposParticipantes(){
        try{
            $tipos_participantes = $this->db_declaracion->table('oic_tipo_participante')
                ->get();
            return response()->json([
                'status' => 'Ok',
                'tipos_participantes' => $tipos_participantes
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Ver catalogo de tipos de trabajo
     */
    public function getTiposTrabajo()
    {
        try {
            $tipos_trabajo = $this->db_main->table('osaf_tipo_trabajador_cat')->get();
            return response()->json([
                'status' => 'Ok',
                'tipos_trabajo' => $tipos_trabajo
            ], 200);
        } catch (Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
     /**
     * Cambiar visibilidad de un tipo de institucion
     */
    public function editVisibilidadTipoParticipante(){
        $activo =  request('visibilidad');
        try{
            if($activo == 1){
                $adscripciones = $this->db_declaracion->table('oic_tipo_participante')
                    ->where('ParticipanteID',request('id_elemento'))
                    ->update(['Oculto'=>0]);
            }else{
                $adscripciones = $this->db_declaracion->table('oic_tipo_participante')
                    ->where('ParticipanteID',request('id_elemento'))
                    ->update(['Oculto'=>1]);
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Guardar tipo de trabajo
     */
    public function guardarTipoTrabajo()
    {
        try {
            $this->db_main->table('osaf_tipo_trabajador_cat')
                          ->insert([
                              'TipoTrabajador' => request('tipo')
                          ]);
            return response()->json([
               'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    public function ocultarTipo()
    {
        try {
            $this->db_main->table('osaf_tipo_trabajador_cat')
                ->where('TipoTrabajadorID' , '=', request('tipoId'))
                ->update([
                    'Visible' => 0
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    public function mostrarTipo()
    {
        try {
            $this->db_main->table('osaf_tipo_trabajador_cat')
                ->where('TipoTrabajadorID' , '=', request('tipoId'))
                ->update([
                    'Visible' => 1
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Actualizar un tipo de institución
     */
    public function actualizarTipoParticipante(){
        try{
            $this->db_declaracion->table('oic_tipo_participante')
            ->where('ParticipanteID',request('id_elemento'))
            ->update(['TipoParticipante'=>request('nombre')]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Registrar nuevo tipo de institucion
     */
    public function registrarTipoParticipante(){
        try{
            $this->db_declaracion->table('oic_tipo_participante')
            ->insert([
                'TipoParticipante'=> request('tipo_participante'),
            ]);
        }catch(Exception $e){
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Actualizar tipo
     */
    public function actualizarTipo()
    {
        try {
            $this->db_main->table('osaf_tipo_trabajador_cat')
                ->where('TipoTrabajadorID' , '=', request('tipoId'))
                ->update([
                    'TipoTrabajador' => request('tipo')
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Ocultar inversión
     */
    public function ocultarInversión()
    {
        try {
            $this->db_declaracion->table('oic_tipo_inversion_cat')
                ->where('TipoInversionID' , '=', request('inversionId'))
                ->update([
                    'Eliminado' => 1
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * mostrar inversion
     */
    public function mostrarInversión()
    {
        try {
            $this->db_declaracion->table('oic_tipo_inversion_cat')
                ->where('TipoInversionID' , '=', request('inversionId'))
                ->update([
                    'Eliminado' => 0
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * actualizar tipo de inversion
     */
    public function actualizarTipoInversion()
    {
        try {
            $this->db_declaracion->table('oic_tipo_inversion_cat')
                ->where('TipoInversionID' , '=', request('tipoId'))
                ->update([
                    'Nombre' => mb_strtoupper(request('tipo'), 'UTF-8')
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Ocultar activo
     */
    public function ocultarActivo()
    {
        try {
            $this->db_declaracion->table('oic_activos_financieros')
                ->where('ActivoID' , '=', request('activoId'))
                ->update([
                    'Activo' => 0
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    /**
     * Mostrar activo
     */
    public function mostrarActivo()
    {
        try {
            $this->db_declaracion->table('oic_activos_financieros')
                ->where('ActivoID' , '=', request('activoId'))
                ->update([
                    'Activo' => 1
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }

    public function guardarActivo()
    {
        try {
            $this->db_declaracion->table('oic_activos_financieros')
                ->where('ActivoID' , '=', request('activoId'))
                ->update([
                    'NombreActivo'    => mb_strtoupper(request('activo'), 'UTF-8'),
                    'TipoInversionID' => request('inversionId')
                ]);
            return response()->json([
                'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ],500);
        }
    }
}
