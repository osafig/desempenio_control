<?php

namespace App\Http\Controllers\Api\Declaracion;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Declaracion\DeclaracionRepository;
use App\Mail\NotificationDeclare;
use Carbon\Carbon;
use Illuminate\Support\Facades\{Crypt, DB, Hash, Log, Mail, Storage};
use Barryvdh\DomPDF\Facade as PDF;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use ZipArchive;

class PdfDeclaracionController extends Controller{

    protected $dr; //Repositorio de declaracion
    protected $db_declaracion;
    protected $db_main;

    public function __construct(DeclaracionRepository $dr){
        $this->dr = $dr;
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main = DB::connection('main');
    }

    /**
     * Finalizar declaracion patrimonial
     */
    public function previoDeclaracion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $previo = true;
            $declaracion_id = request()->declaracion_id;
            $datos_declaracion = $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID','=', $declaracion_id)
                ->first();

            $ejercicio = $this->db_main->table('osaf_ejercicios_cat')
            ->where('EjercicioID','=',$datos_declaracion->EjercicioID)
            ->first();

            $datos_declaracion->AnioDeclarado = $ejercicio->Year;
            $espec_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID','=',$datos_declaracion->TipoDeclaracionID)
                ->first();

            $datos_declaracion->NombreDeclaracion = $espec_declaracion->Nombre;
            $datos_declaracion->NombreCorto = $espec_declaracion->NombreCorto;

            $usuario = $this->db_main->table('osaf_usuarios')
                ->select('Nombres','PrimerApellido','SegundoApellido','CURP','RFC','Email')
                ->where('UsuarioID','=',$datos_declaracion->UsuarioID)
                ->first();
            // ------------------------------------------------------
            // GENERAR DECLARACION PATRIMONIAL
            // ------------------------------------------------------
            $secciones = $this->db_declaracion->select('call sp_estadoDeclaracion(?)',[
                $declaracion_id
            ]);
            $secciones_pdf = [];
            $indice = [];
            foreach($secciones as $seccion){
                if ($seccion->Aplica == 1){
                    array_push($indice, $seccion->Nombre);
                    $secciones_pdf[$seccion->Nombre] = $seccion;
                }
            }
            // -------------------------------------------Datos generales
            if(array_key_exists('DATOS GENERALES',$secciones_pdf)){
                $datos_generales = $this->db_declaracion->table('oic_datos_personales as odp')
                    ->join('oic_estado_civil_cat as oec','oec.EstadoCivilID','=','odp.EstadoCivilID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('UsuarioMainID','=',$datos_declaracion->UsuarioID)
                    ->first();
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDatos        = $datos_generales;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }else{
                $secciones_pdf["DATOS GENERALES"] = (object)[
                    "SeccionID" => 1,
                    "Nombre" => "DATOS GENERALES",
                    "EstadoActual" => 1,
                    "Aplica" => 1,
                    "Parte"=> 1,
                ];
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }

            // -----------------------------------------Domicilio del declarante
            if(array_key_exists('DOMICILIO DECLARANTE',$secciones_pdf)){
                $domicilio = $this->db_declaracion->table('oic_direcciones as od')
                    ->select('od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','od.Aclaraciones',
                        'od.Aclaraciones','od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DireccionID','=', $datos_declaracion->DireccionID)
                    ->first();
                $secciones_pdf["DOMICILIO DECLARANTE"]->Info = $domicilio;
            }
            // ------------------------------------------Datos curriculares
            if(array_key_exists('DATOS CURRICULARES DEL DECLARANTE',$secciones_pdf)){
                $curriculares = $this->db_declaracion->table('oic_nivel_estudios_detalle as oned')
                    ->join('oic_nivel_estudios_cat as onec','oned.NivelEstudioID','=','onec.NivelEstudioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->first();
                $secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info = $curriculares;
            }
            // -----------------------------------------Datos cargo
            if(array_key_exists('DATOS DEL EMPLEO, CARGO O COMISIÓN',$secciones_pdf)){
                $cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo')
                    ->join('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->join('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('TipoCargo','=',1)
                    ->first();
                $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info = $cargo;
                $otro_cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones', 'oc.AreaEspecifica',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo', 'oc.PuestoEspecifico')
                    ->leftJoin('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->leftJoin('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('Oculto', '=', 0)
                    ->where('TipoCargo','=',2)
                    ->first();
                    $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo = (is_null($otro_cargo)) ? [] : $otro_cargo;
            }
            // -----------------------------------------Experiencia laboral
            if(array_key_exists('EXPERIENCIA LABORAL',$secciones_pdf)){
                $experiencias = $this->db_declaracion->table('oic_experiencia_laboral_detalle as exp')
                    ->leftJoin('oic_orden_gobierno as oog','exp.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->leftJoin('oic_ambito_cat as oac','exp.AmbitoID','=','oac.AmbitoID')
                    ->leftJoin('oic_sector_productivo_cat as osp','exp.SectorID','=','osp.SectorID')
                    ->where('exp.UsuarioID','=',$datos_declaracion->UsuarioID)
                    ->where('exp.Oculto','=',0)
                  ->get();
                $secciones_pdf["EXPERIENCIA LABORAL"]->Info = $experiencias;
            }
            // ---------------------------------------Datos de la pareja
            if(array_key_exists('DATOS DE LA PAREJA',$secciones_pdf)){
                $pareja = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 1]);
                $secciones_pdf["DATOS DE LA PAREJA"]->Info = $pareja;
            }
            // --------------------------------------Datos dependientes
            if(array_key_exists('DATOS DEL DEPENDIENTE ECONÓMICO',$secciones_pdf)){
                $dependientes = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 0]);
                $secciones_pdf["DATOS DEL DEPENDIENTE ECONÓMICO"]->Info = $dependientes;
            }
            // ------------------------------------Ingresos
            // Obtener datos de ingreso
            if(array_key_exists('INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS',$secciones_pdf)){
                $ingresos['ingresos'] = $this->dr->getIngresosDeclaracion($declaracion_id);
                // actividades industriales
                $industriales = $this->dr->getIngresosActividadesIndustriales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['actividades_industriales'] = (is_null($ingresos['ingresos'])) ? [] : $industriales;
                // actividades financieras
                $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                    ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                    ->where('oiaf.IngresoID','=',$ingresos['ingresos']->IngresoID)
                    ->get();
                $ingresos['otros_ingresos']['actividades_financieras'] = (is_null($ingresos['ingresos'])) ? [] : $financieras;
                // servicios profesionales
                $profesionales = $this->dr->getIngresosServiciosProfesionales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['servicios_profesionales'] = (is_null($ingresos['ingresos'])) ? [] : $profesionales;
                // otros ingresos
                $otros = $this->dr->getIngresosOtros($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['otros_ingresos'] = (is_null($ingresos['ingresos'])) ? [] : $otros;
                // bienes enajenados
                $enajenados = $this->dr->getBienesEnajenados($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['bienes_enajenados'] = (is_null($ingresos['ingresos'])) ? [] : $enajenados;
                $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info = ($ingresos != []) ? $ingresos : [];
            }
            // ------------------------------------Servidor publico el año pasado
            if (array_search('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true){
                // Obtener datos de ingreso
                $servidor_pasado['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado($declaracion_id);
                if($servidor_pasado['ingresos'] != null){
                    // actividades industriales
                    $industriales = $this->dr->getIngresosActividadesIndustriales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['actividades_industriales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $industriales;
                    // actividades financieras
                    $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                        ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                        ->where('oiaf.IngresoID','=',$servidor_pasado['ingresos']->ServidorPasadoID)
                        ->get();
                    $servidor_pasado['otros_ingresos']['actividades_financieras'] = (is_null($servidor_pasado['ingresos'])) ? [] : $financieras;
                    // servicios profesionales
                    $servicios = $this->dr->getIngresosServiciosProfesionales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['servicios_profesionales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $servicios;
                    // otros ingresos
                    $otros = $this->dr->getIngresosOtros($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['otros_ingresos'] = (is_null($servidor_pasado['ingresos'])) ? [] : $otros;
                    // bienes enajenados
                    $bienes_enajenados = $this->dr->getBienesEnajenados($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['bienes_enajenados'] = (is_null($servidor_pasado['ingresos'])) ? [] : $bienes_enajenados;
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = $servidor_pasado;
                }
                else{
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = 'No aplica';
                }
            }

            // ------------------------------------BIENES INMUEBLES
            // obtener BIENES INMUEBLES
            if (array_search('BIENES INMUEBLES',$indice) == true){
                $bienes_inmuebles = $this->db_declaracion->select('call sp_obtenerBienesInmuebles(?)',[$declaracion_id]);
                $secciones_pdf["BIENES INMUEBLES"]->Info = $bienes_inmuebles;
            }
            // ------------------------------------VEHÍCULOS
            // obtener vehiculos
            if (array_search('VEHÍCULOS',$indice) == true){
                $vehiculos = $this->db_declaracion->select('call sp_obtenerVehiculos(?)',[$declaracion_id]);
                $secciones_pdf["VEHÍCULOS"]->Info = $vehiculos;
            }
            // ------------------------------------BIENES MUEBLES
            // obtener BIENES MUEBLES
            if (array_search('BIENES MUEBLES',$indice) == true){
                $bienes_muebles = $this->db_declaracion->select('call sp_obtenerBienesMueble(?)',[$declaracion_id]);
                $secciones_pdf["BIENES MUEBLES"]->Info = $bienes_muebles;
            }
            // ------------------------------------INVERSIONES
            // obtener INVERSIONES
            if (array_search('INVERSIONES',$indice) == true){
                $inversiones = $this->db_declaracion->select('call sp_obtenerInversiones(?)',[$declaracion_id]);
                $secciones_pdf["INVERSIONES"]->Info = $inversiones;
            }
            // ------------------------------------ADEUDOS/PASIVOS
            // obtener ADEUDOS/PASIVOS
            if (array_search('ADEUDOS/PASIVOS',$indice) == true){
                $adeudos = $this->db_declaracion->select('call sp_obtenerAdeudos(?)',[$declaracion_id]);
                $secciones_pdf["ADEUDOS/PASIVOS"]->Info = $adeudos;
            }
            // ------------------------------------PRESTAMO O COMODATO POR TERCEROS
            // obtener PRESTAMO O COMODATO POR TERCEROS
            if (array_search('PRESTAMO O COMODATO POR TERCEROS',$indice) == true){
                $prestamos = $this->db_declaracion->select('call sp_obtenerPrestamos(?)',[$declaracion_id]);
                $secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info = $prestamos;
            }
            // ------------------------------------PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // obtener PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // dd(array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice));
            if (array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice) !== false){
                $participacion_empresas = $this->db_declaracion->table('oic_participacion_empresas as ope')
                ->join('oic_sector_productivo_cat as ospc', 'ope.SectorID', '=' ,'ospc.SectorID')
                ->where('ope.DeclaracionID','=',$declaracion_id)
                ->where('ope.Oculto','=',0)
                ->get();
                $secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info = $participacion_empresas;
            }
            // ------------------------------------PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            // obtener PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            if (array_search('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN',$indice) == true){
                $participacion_instituciones = $this->db_declaracion->table('oic_participacion_instituciones as opi')
                    ->join('oic_tipo_institucion as oti', 'oti.TipoInstitucionID', '=' ,'opi.TipoInstitucionID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('opi.Oculto','=',0)
                    ->get();
                $secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info = $participacion_instituciones;
            }
            // ------------------------------------APOYOS O BENEFICIOS PÚBLICOS
            // obtener APOYOS O BENEFICIOS PÚBLICOS
            if(array_search('APOYOS O BENEFICIOS PÚBLICOS',$indice) == true){
                $apoyos = $this->db_declaracion->table('oic_apoyos_beneficios as oab')
                    ->join('oic_ambito_cat as oac', 'oab.AmbitoID', '=' ,'oac.AmbitoID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('Oculto','=',0)
                    ->get();
                $secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info = $apoyos;
            }
            // ------------------------------------REPRESENTACIÓN
            // obtener REPRESENTACIÓN
            if(array_search('REPRESENTACIÓN',$indice) == true){
                $representaciones = $this->db_declaracion->table('oic_representacion as rep')
                    ->join('oic_sector_productivo_cat as ospc', 'rep.SectorID', '=' ,'ospc.SectorID')
                    ->where('rep.DeclaracionID','=',$declaracion_id)
                    ->where('rep.Oculto','=',0)
                    ->get();
                $secciones_pdf["REPRESENTACIÓN"]->Info = $representaciones;
            }
            // ------------------------------------CLIENTES PRINCIPALES
            // obtener CLIENTES PRINCIPALES
            if(array_search('CLIENTES PRINCIPALES',$indice) == true){
                $clientes = $this->db_declaracion->table('oic_clientes_principales as ocp')
                    ->join('oic_sector_productivo_cat as ospc', 'ocp.SectorID', '=' ,'ospc.SectorID')
                    ->where('ocp.DeclaracionID','=',$declaracion_id)
                    ->where('ocp.Oculto','=',0)
                    ->get();
                $secciones_pdf["CLIENTES PRINCIPALES"]->Info = $clientes;
            }
            // ------------------------------------BENEFICIOS PRIVADOS
            // obtener BENEFICIOS PRIVADOS
            if(array_search('BENEFICIOS PRIVADOS',$indice) == true){
                $beneficios = $this->db_declaracion->table('oic_beneficios_privados as obp')
                    ->join('oic_sector_productivo_cat as ospc', 'obp.SectorID', '=' ,'ospc.SectorID')
                    ->where('obp.DeclaracionID','=',$declaracion_id)
                    ->where('obp.Oculto','=',0)
                    ->get();
                $secciones_pdf["BENEFICIOS PRIVADOS"]->Info = $beneficios;
            }
            // ------------------------------------FIDEICOMISOS
            // obtener FIDEICOMISOS
            if(array_search('FIDEICOMISOS',$indice) == true){
                $fideicomisos = $this->db_declaracion->table('oic_fideicomisos as of2')
                    ->join('oic_sector_productivo_cat as ospc', 'of2.SectorID', '=' ,'ospc.SectorID')
                    ->where('of2.DeclaracionID','=',$declaracion_id)
                    ->where('of2.Oculto','=',0)
                    ->get();
                $secciones_pdf["FIDEICOMISOS"]->Info = $fideicomisos;
            }
            ini_set('memory_limit', '-1');
            if($datos_declaracion->TipoDeclaracionID != 7){
                $pdf = PDF::loadView('pdf.declaracion_patrimonial',compact("indice","secciones_pdf","previo"))->save('prueba_declaracion_publica.pdf')->download("", array("Attachment" => false));
            }
            else{
                $pdf = PDF::loadView('pdf.declaracion_conflicto',compact("indice","secciones_pdf","previo"))->save('prueba_declaracion_publica.pdf')->download("", array("Attachment" => false));
            }
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    public function generarDeclaracionPublica($info_generar = null){
        if(isset($info_generar)){
            $declaracion_id = $info_generar["declaracion_id"];
            $formato_id = $info_generar["formato_id"];
        }else{
            $declaracion_id = request()->declaracion_id;
            $formato_id = request()->formato_id;
        }
        try {
            $this->db_declaracion->beginTransaction();
            $previo = true;
            $datos_declaracion = $this->db_declaracion->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID','=', $declaracion_id)
                ->first();

            $ejercicio = $this->db_main->table('osaf_ejercicios_cat')
            ->where('EjercicioID','=',$datos_declaracion->EjercicioID)
            ->first();

            $datos_declaracion->AnioDeclarado = $ejercicio->Year;
            $espec_declaracion = $this->db_declaracion->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID','=',$datos_declaracion->TipoDeclaracionID)
                ->first();

            $datos_declaracion->NombreDeclaracion = $espec_declaracion->Nombre;
            $datos_declaracion->NombreCorto = $espec_declaracion->NombreCorto;

            $usuario = $this->db_main->table('osaf_usuarios')
                ->select('Nombres','PrimerApellido','SegundoApellido','CURP','RFC','Email')
                ->where('UsuarioID','=',$datos_declaracion->UsuarioID)
                ->first();

            $nombre_completo = $usuario->Nombres."_".$usuario->PrimerApellido."_".$usuario->SegundoApellido;

            $formato_usado = $this->db_declaracion->table('oic_formato_declaracion')
                ->where('FormatoID','=',$formato_id)
                ->first();

            $datos_declaracion->Justificacion = $formato_usado->JustificacionFormato;

            $secciones_formato = $this->db_declaracion->table('oic_secciones_formato')
                ->where('FormatoID','=',$formato_usado->FormatoID)
                ->orderBy('SeccionID', 'asc')
                ->get();
            // Busqueda de campos por formato y filtrado
            $campos = $this->db_declaracion->table('oic_campos_formato')
                ->where('FormatoID','=',$formato_usado->FormatoID)
                ->get();
            // dd($campos);
            $secciones_formato[0]->Campos = array();
            $secciones_formato[1]->Campos = array();
            $secciones_formato[2]->Campos = array();
            $secciones_formato[3]->Campos = array();
            $secciones_formato[4]->Campos = array();
            $secciones_formato[5]->Campos = array();
            $secciones_formato[6]->Campos = array();
            $secciones_formato[7]->Campos = array();
            $secciones_formato[8]->Campos = array();
            $secciones_formato[9]->Campos = array();
            $secciones_formato[10]->Campos = array();
            $secciones_formato[11]->Campos = array();
            $secciones_formato[12]->Campos = array();
            $secciones_formato[13]->Campos = array();
            $secciones_formato[14]->Campos = array();
            $secciones_formato[15]->Campos = array();
            $secciones_formato[16]->Campos = array();
            $secciones_formato[17]->Campos = array();
            $secciones_formato[18]->Campos = array();
            $secciones_formato[19]->Campos = array();
            $secciones_formato[20]->Campos = array();
            $secciones_formato[21]->Campos = array();

            $secc_2 = array();
            foreach($campos as $campo){
                switch($campo->SeccionID){
                    case 1:
                        array_push($secciones_formato[0]->Campos, $campo);
                        break;
                    case 2:
                        array_push($secciones_formato[1]->Campos, $campo);
                        break;
                    case 3:
                        array_push($secciones_formato[2]->Campos, $campo);
                        break;
                    case 4:
                        array_push($secciones_formato[3]->Campos, $campo);
                        break;
                    case 5:
                        array_push($secciones_formato[4]->Campos, $campo);
                        break;
                    case 6:
                        array_push($secciones_formato[5]->Campos, $campo);
                        break;
                    case 7:
                        array_push($secciones_formato[6]->Campos, $campo);
                        break;
                    case 8:
                        array_push($secciones_formato[7]->Campos, $campo);
                        break;
                    case 9:
                        array_push($secciones_formato[8]->Campos, $campo);
                        break;
                    case 10:
                        array_push($secciones_formato[9]->Campos, $campo);
                        break;
                    case 11:
                        array_push($secciones_formato[10]->Campos, $campo);
                        break;
                    case 12:
                        array_push($secciones_formato[11]->Campos, $campo);
                        break;
                    case 13:
                        array_push($secciones_formato[12]->Campos, $campo);
                        break;
                    case 14:
                        array_push($secciones_formato[13]->Campos, $campo);
                        break;
                    case 15:
                        array_push($secciones_formato[14]->Campos, $campo);
                        break;
                    case 16:
                        array_push($secciones_formato[15]->Campos, $campo);
                        break;
                    case 17:
                        array_push($secciones_formato[16]->Campos, $campo);
                        break;
                    case 18:
                        array_push($secciones_formato[17]->Campos, $campo);
                        break;
                    case 19:
                        array_push($secciones_formato[18]->Campos, $campo);
                        break;
                    case 20:
                        array_push($secciones_formato[19]->Campos, $campo);
                        break;
                    case 21:
                        array_push($secciones_formato[20]->Campos, $campo);
                        break;
                    case 22:
                        array_push($secciones_formato[21]->Campos, $campo);
                        break;
                }
            }

            // dd($secciones_formato);

            // ------------------------------------------------------
            // GENERAR DECLARACION PATRIMONIAL
            // ------------------------------------------------------
            $secciones = $this->db_declaracion->select('call sp_estadoDeclaracion(?)',[
                $declaracion_id
            ]);
            $secciones_pdf = [];
            $indice = [];
            foreach($secciones as $seccion){
                if ($seccion->Aplica == 1){
                    array_push($indice, $seccion->Nombre);
                    $secciones_pdf[$seccion->Nombre] = $seccion;
                }
            }
            // -------------------------------------------Datos generales
            if(array_key_exists('DATOS GENERALES',$secciones_pdf)){
                $datos_generales = $this->db_declaracion->table('oic_datos_personales as odp')
                    ->join('oic_estado_civil_cat as oec','oec.EstadoCivilID','=','odp.EstadoCivilID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('UsuarioMainID','=',$datos_declaracion->UsuarioID)
                    ->first();

                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDatos        = $datos_generales;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }else{
                $secciones_pdf["DATOS GENERALES"] = (object)[
                    "SeccionID" => 1,
                    "Nombre" => "DATOS GENERALES",
                    "EstadoActual" => 1,
                    "Aplica" => 1,
                    "Parte"=> 1,
                ];
                $secciones_pdf["DATOS GENERALES"]->InfoUsuario      = $usuario;
                $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion  = $datos_declaracion;
            }

            // -----------------------------------------Domicilio del declarante
            if(array_key_exists('DOMICILIO DECLARANTE',$secciones_pdf)){
                $domicilio = $this->db_declaracion->table('oic_direcciones as od')
                    ->select('od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','od.Aclaraciones',
                        'od.Aclaraciones','od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DireccionID','=', $datos_declaracion->DireccionID)
                    ->first();
                $secciones_pdf["DOMICILIO DECLARANTE"]->Info = $domicilio;
            }
            // ------------------------------------------Datos curriculares
            if(array_key_exists('DATOS CURRICULARES DEL DECLARANTE',$secciones_pdf)){
                $curriculares = $this->db_declaracion->table('oic_nivel_estudios_detalle as oned')
                    ->join('oic_nivel_estudios_cat as onec','oned.NivelEstudioID','=','onec.NivelEstudioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->first();
                $secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info = $curriculares;
            }
            // -----------------------------------------Datos cargo
            if(array_key_exists('DATOS DEL EMPLEO, CARGO O COMISIÓN',$secciones_pdf)){
                $cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo')
                    ->join('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->join('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('TipoCargo','=',1)
                    ->first();
                $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info = $cargo;
                $otro_cargo = $this->db_declaracion->table('oic_cargos as oc')
                    ->select('oog.NombreOrden','oac.NombreAmbito','oc.Dependencia','aoc.Nombre',
                        'apc.Nombre as Puesto','oc.Honorarios','onec.Nivel','oc.FechaIngreso',
                        'oc.FuncionPrincipal','oc.Telefono','od.DireccionID','od.Calle','od.NumeroExterior','od.NumeroInterior','od.Colonia',
                        'Ciudad','od.MunicipioID','od.EstadoID','od.Pais','od.CodigoPostal','oc.Observaciones', 'oc.AreaEspecifica',
                        'od.EsExtranjero','od.EstadoExtranjero','oec.Nombre as Estado','omc.Nombre as Municipio', 'oc.TipoCargo', 'oc.PuestoEspecifico')
                    ->leftJoin('oic_adscripciones_cat as aoc','oc.AdscripcionID','=','aoc.AdscripcionID')
                    ->leftJoin('oic_puestos_cat as apc','oc.PuestoID','=','apc.PuestoID')
                    ->join('oic_orden_gobierno as oog','oc.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->join('oic_ambito_cat as oac','oc.AmbitoPublicoID','=','oac.AmbitoID')
                    ->join('oic_nivel_empleo_cat as onec','onec.NivelCargoID','=','oc.NivelCargo')
                    ->join('oic_direcciones as od','oc.DireccionID','=','od.DireccionID')
                    ->leftJoin('oic_estados_cat as oec','od.EstadoID','=','oec.EstadoID')
                    ->leftJoin('oic_municipios_cat as omc','od.MunicipioID','=','omc.MunicipioID')
                    ->where('DeclaracionID','=', $declaracion_id)
                    ->where('Oculto', '=', 0)
                    ->where('TipoCargo','=',2)
                    ->first();
                    $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo = (is_null($otro_cargo)) ? [] : $otro_cargo;
            }
            // -----------------------------------------Experiencia laboral
            if(array_key_exists('EXPERIENCIA LABORAL',$secciones_pdf)){
                $experiencias = $this->db_declaracion->table('oic_experiencia_laboral_detalle as exp')
                    ->leftJoin('oic_orden_gobierno as oog','exp.OrdenGobiernoID','=','oog.NivelOrdenID')
                    ->leftJoin('oic_ambito_cat as oac','exp.AmbitoID','=','oac.AmbitoID')
                    ->leftJoin('oic_sector_productivo_cat as osp','exp.SectorID','=','osp.SectorID')
                    ->where('exp.UsuarioID','=',$datos_declaracion->UsuarioID)
                    ->where('exp.Oculto','=',0)
                  ->get();
                $secciones_pdf["EXPERIENCIA LABORAL"]->Info = $experiencias;
            }
            // ---------------------------------------Datos de la pareja
            if(array_key_exists('DATOS DE LA PAREJA',$secciones_pdf)){
                $pareja = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 1]);
                $secciones_pdf["DATOS DE LA PAREJA"]->Info = $pareja;
            }
            // --------------------------------------Datos dependientes
            if(array_key_exists('DATOS DEL DEPENDIENTE ECONÓMICO',$secciones_pdf)){
                $dependientes = $this->db_declaracion->select('call sp_obtenerDependientesPareja(?,?)',[$datos_declaracion->UsuarioID, 0]);
                $secciones_pdf["DATOS DEL DEPENDIENTE ECONÓMICO"]->Info = $dependientes;
            }
            // ------------------------------------Ingresos
            // Obtener datos de ingreso
            if(array_key_exists('INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS',$secciones_pdf)){
                $ingresos['ingresos'] = $this->dr->getIngresosDeclaracion($declaracion_id);
                // actividades industriales
                $industriales = $this->dr->getIngresosActividadesIndustriales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['actividades_industriales'] = (is_null($ingresos['ingresos'])) ? [] : $industriales;
                // actividades financieras
                $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                    ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                    ->where('oiaf.IngresoID','=',$ingresos['ingresos']->IngresoID)
                    ->get();
                $ingresos['otros_ingresos']['actividades_financieras'] = (is_null($ingresos['ingresos'])) ? [] : $financieras;
                // servicios profesionales
                $profesionales = $this->dr->getIngresosServiciosProfesionales($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['servicios_profesionales'] = (is_null($ingresos['ingresos'])) ? [] : $profesionales;
                // otros ingresos
                $otros = $this->dr->getIngresosOtros($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['otros_ingresos'] = (is_null($ingresos['ingresos'])) ? [] : $otros;
                // bienes enajenados
                $enajenados = $this->dr->getBienesEnajenados($ingresos['ingresos']->IngresoID);
                $ingresos['otros_ingresos']['bienes_enajenados'] = (is_null($ingresos['ingresos'])) ? [] : $enajenados;
                $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info = ($ingresos != []) ? $ingresos : [];
            }
            // ------------------------------------Servidor publico el año pasado
            if (array_search('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true){
                // Obtener datos de ingreso
                $servidor_pasado['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado($declaracion_id);
                if($servidor_pasado['ingresos'] != null){
                    // actividades industriales
                    $industriales = $this->dr->getIngresosActividadesIndustriales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['actividades_industriales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $industriales;
                    // actividades financieras
                    $financieras = $this->db_declaracion->table('oic_ingresos_actividad_financiera as oiaf')
                        ->join('oic_tipo_instrumento_cat as otic','otic.IntrumentoID','=','oiaf.InstrumentoID')
                        ->where('oiaf.IngresoID','=',$servidor_pasado['ingresos']->ServidorPasadoID)
                        ->get();
                    $servidor_pasado['otros_ingresos']['actividades_financieras'] = (is_null($servidor_pasado['ingresos'])) ? [] : $financieras;
                    // servicios profesionales
                    $servicios = $this->dr->getIngresosServiciosProfesionales($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['servicios_profesionales'] = (is_null($servidor_pasado['ingresos'])) ? [] : $servicios;
                    // otros ingresos
                    $otros = $this->dr->getIngresosOtros($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['otros_ingresos'] = (is_null($servidor_pasado['ingresos'])) ? [] : $otros;
                    // bienes enajenados
                    $bienes_enajenados = $this->dr->getBienesEnajenados($servidor_pasado['ingresos']->ServidorPasadoID);
                    $servidor_pasado['otros_ingresos']['bienes_enajenados'] = (is_null($servidor_pasado['ingresos'])) ? [] : $bienes_enajenados;
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = $servidor_pasado;
                }
                else{
                    $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info = 'No aplica';
                }
            }

            // ------------------------------------BIENES INMUEBLES
            // obtener BIENES INMUEBLES
            if (array_search('BIENES INMUEBLES',$indice) == true){
                $bienes_inmuebles = $this->db_declaracion->select('call sp_obtenerBienesInmuebles(?)',[$declaracion_id]);
                $bienes_inmuebles_filtrado = array();
                foreach($bienes_inmuebles as $bien){
                    if($bien->Titular == "DECLARANTE"){
                        array_push($bienes_inmuebles_filtrado, $bien);
                    } else {
                        array_push($bienes_inmuebles_filtrado, $bien);
                    }
                }
                $bienes_inmuebles = $bienes_inmuebles_filtrado;
                $secciones_pdf["BIENES INMUEBLES"]->Info = $bienes_inmuebles;
            }


            // ------------------------------------VEHÍCULOS
            // obtener vehiculos
            if (array_search('VEHÍCULOS',$indice) == true){
                $vehiculos = $this->db_declaracion->select('call sp_obtenerVehiculos(?)',[$declaracion_id]);
                $vehiculos_filtrado = array();
                foreach($vehiculos as $vehiculo){
                    if($vehiculo->Titular == "DECLARANTE"){
                        array_push($vehiculos_filtrado, $vehiculo);
                    }
                }
                $vehiculos = $vehiculos_filtrado;
                $secciones_pdf["VEHÍCULOS"]->Info = $vehiculos;
            }


            // ------------------------------------BIENES MUEBLES
            // obtener BIENES MUEBLES
            if (array_search('BIENES MUEBLES',$indice) == true){
                $bienes_muebles = $this->db_declaracion->select('call sp_obtenerBienesMueble(?)',[$declaracion_id]);
                $bienes_muebles_filtrado = array();
                foreach($bienes_muebles as $bien){
                    if($bien->Titular == "DECLARANTE"){
                        array_push($bienes_muebles_filtrado,$bien);
                    } else {
                        array_push($bienes_muebles_filtrado,$bien);
                    }
                }
                $bienes_muebles = $bienes_muebles_filtrado;
                $secciones_pdf["BIENES MUEBLES"]->Info = $bienes_muebles;
            }


            // ------------------------------------INVERSIONES
            // obtener INVERSIONES
            if (array_search('INVERSIONES',$indice) == true){
                $inversiones = $this->db_declaracion->select('call sp_obtenerInversiones(?)',[$declaracion_id]);
                $inversiones_filtrado = array();
                foreach($inversiones as $inversion){
                    if($inversion->Titular == "DECLARANTE"){
                        array_push($inversiones_filtrado,$inversion);
                    }
                }
                $inversiones = $inversiones_filtrado;
                $secciones_pdf["INVERSIONES"]->Info = $inversiones;
            }


            // ------------------------------------ADEUDOS/PASIVOS
            // obtener ADEUDOS/PASIVOS
            if (array_search('ADEUDOS/PASIVOS',$indice) == true){
                $adeudos = $this->db_declaracion->select('call sp_obtenerAdeudos(?)',[$declaracion_id]);
                $adeudos_filtrado = array();
                foreach($adeudos as $adeudo){
                    if($adeudo->Titular == "DECLARANTE"){
                        array_push($adeudos_filtrado,$adeudo);
                    }
                }
                $adeudos = $adeudos_filtrado;
                $secciones_pdf["ADEUDOS/PASIVOS"]->Info = $adeudos;
            }


            // ------------------------------------PRESTAMO O COMODATO POR TERCEROS
            // obtener PRESTAMO O COMODATO POR TERCEROS
            if (array_search('PRESTAMO O COMODATO POR TERCEROS',$indice) == true){
                $prestamos = $this->db_declaracion->select('call sp_obtenerPrestamos(?)',[$declaracion_id]);
                $secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info = $prestamos;
            }
            // ------------------------------------PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // obtener PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES
            // dd(array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice));
            if (array_search('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES',$indice) !== false){
                $participacion_empresas = $this->db_declaracion->table('oic_participacion_empresas as ope')
                ->join('oic_sector_productivo_cat as ospc', 'ope.SectorID', '=' ,'ospc.SectorID')
                ->where('ope.DeclaracionID','=',$declaracion_id)
                ->where('ope.Oculto','=',0)
                ->get();
                $participacion_empresas_filtrado = array();
                foreach($participacion_empresas as $participacion){
                    if($participacion->Participante == "Declarante"){
                        array_push($participacion_empresas_filtrado,$participacion);
                    }
                }
                $participacion_empresas = $participacion_empresas_filtrado;
                $secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info = $participacion_empresas;
            }
            // ------------------------------------PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            // obtener PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN
            if (array_search('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN',$indice) == true){
                $participacion_instituciones = $this->db_declaracion->table('oic_participacion_instituciones as opi')
                    ->join('oic_tipo_institucion as oti', 'oti.TipoInstitucionID', '=' ,'opi.TipoInstitucionID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('opi.Oculto','=',0)
                    ->get();
                $participacion_instituciones_filtrado = array();
                foreach($participacion_instituciones as $participacion){
                    if($participacion->Participante == "Declarante"){
                        array_push($participacion_instituciones_filtrado,$participacion);
                    }
                }
                $participacion_instituciones = $participacion_instituciones_filtrado;
                $secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info = $participacion_instituciones;
            }
            // ------------------------------------APOYOS O BENEFICIOS PÚBLICOS
            // obtener APOYOS O BENEFICIOS PÚBLICOS
            if(array_search('APOYOS O BENEFICIOS PÚBLICOS',$indice) == true){
                $apoyos = $this->db_declaracion->table('oic_apoyos_beneficios as oab')
                    ->join('oic_ambito_cat as oac', 'oab.AmbitoID', '=' ,'oac.AmbitoID')
                    ->where('DeclaracionID','=',$declaracion_id)
                    ->where('Oculto','=',0)
                    ->get();
                $apoyos_filtrado = array();
                foreach($apoyos as $apoyo){
                    if($apoyo->Beneficiario == "DECLARANTE"){
                        array_push($apoyos_filtrado,$apoyo);
                    }
                }
                $apoyos = $apoyos_filtrado;
                $secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info = $apoyos;
            }
            // ------------------------------------REPRESENTACIÓN
            // obtener REPRESENTACIÓN
            if(array_search('REPRESENTACIÓN',$indice) == true){
                $representaciones = $this->db_declaracion->table('oic_representacion as rep')
                    ->join('oic_sector_productivo_cat as ospc', 'rep.SectorID', '=' ,'ospc.SectorID')
                    ->where('rep.DeclaracionID','=',$declaracion_id)
                    ->where('rep.Oculto','=',0)
                    ->get();
                $representaciones_filtrado = array();
                foreach($representaciones as $representacion){
                    if($representacion->Representante == "Declarante"){
                        array_push($representaciones_filtrado,$representacion);
                    }
                }
                $representaciones = $representaciones_filtrado;
                $secciones_pdf["REPRESENTACIÓN"]->Info = $representaciones;
            }
            // ------------------------------------CLIENTES PRINCIPALES
            // obtener CLIENTES PRINCIPALES
            if(array_search('CLIENTES PRINCIPALES',$indice) == true){
                $clientes = $this->db_declaracion->table('oic_clientes_principales as ocp')
                    ->join('oic_sector_productivo_cat as ospc', 'ocp.SectorID', '=' ,'ospc.SectorID')
                    ->where('ocp.DeclaracionID','=',$declaracion_id)
                    ->where('ocp.Oculto','=',0)
                    ->get();
                $clientes_filtrado = array();
                foreach($clientes as $cliente){
                    if($cliente->Titular == "Declarante"){
                        array_push($clientes_filtrado,$cliente);
                    }
                }
                $clientes = $clientes_filtrado;
                $secciones_pdf["CLIENTES PRINCIPALES"]->Info = $clientes;
            }
            // ------------------------------------BENEFICIOS PRIVADOS
            // obtener BENEFICIOS PRIVADOS
            if(array_search('BENEFICIOS PRIVADOS',$indice) == true){
                $beneficios = $this->db_declaracion->table('oic_beneficios_privados as obp')
                    ->join('oic_sector_productivo_cat as ospc', 'obp.SectorID', '=' ,'ospc.SectorID')
                    ->where('obp.DeclaracionID','=',$declaracion_id)
                    ->where('obp.Oculto','=',0)
                    ->get();
                $beneficios_filtrado = array();
                foreach($beneficios as $beneficio){
                    if($beneficio->Titular == "DECLARANTE"){
                        array_push($beneficios_filtrado,$beneficio);
                    }
                }
                $beneficios = $beneficios_filtrado;
                $secciones_pdf["BENEFICIOS PRIVADOS"]->Info = $beneficios;
            }
            // ------------------------------------FIDEICOMISOS
            // obtener FIDEICOMISOS
            if(array_search('FIDEICOMISOS',$indice) == true){
                $fideicomisos = $this->db_declaracion->table('oic_fideicomisos as of2')
                    ->join('oic_sector_productivo_cat as ospc', 'of2.SectorID', '=' ,'ospc.SectorID')
                    ->where('of2.DeclaracionID','=',$declaracion_id)
                    ->where('of2.Oculto','=',0)
                    ->get();
                $fideicomisos_filtrado = array();
                foreach($fideicomisos as $fideicomiso){
                    if($fideicomiso->Participante == "Declarante"){
                        array_push($fideicomisos_filtrado,$fideicomiso);
                    }
                }
                $fideicomisos = $fideicomisos_filtrado;
                $secciones_pdf["FIDEICOMISOS"]->Info = $fideicomisos;
            }

            // dd($secciones_formato);

            ini_set('memory_limit', '-1');
            $TipoDeclaracion = $datos_declaracion->TipoDeclaracionID < 7 ? 'Patrimonial' : 'Conflicto intereses';

            $declaracion_publica = $this->db_declaracion->table('oic_declaraciones_publicas')
                ->where('DeclaracionID', '=', $declaracion_id)
                ->where('FormatoID', '=', $formato_usado->FormatoID)
                ->first();
            if($datos_declaracion->TipoDeclaracionID != 7){
                if(is_null($declaracion_publica)) {
                    $pdf = \PDF::loadView('pdf.declaracion_patrimonial_publica_modif',compact("indice","secciones_pdf","previo","secciones_formato"));
                    $ubicacion = "declaracion/{$declaracion_id}/";
                    $ubicacion_real = public_path($ubicacion);
                    if (!file_exists($ubicacion_real)) {
                        mkdir($ubicacion_real, 0777, true);
                    }
                    $pdf->save($ubicacion_real.$formato_usado->ClaveFormato.'-'.$nombre_completo.'-'.$TipoDeclaracion.'.pdf');
                    $this->db_declaracion->table('oic_declaraciones_publicas')->insert([
                        'DeclaracionID'         => $declaracion_id,
                        'UsuarioID'             => $datos_declaracion->UsuarioID,
                        'FormatoID'             => $formato_usado->FormatoID,
                        'Generado'              => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'RutaDeclaracion'       => $ubicacion.$formato_usado->ClaveFormato.'-'.$nombre_completo.'-'.$TipoDeclaracion.'.pdf'
                    ]);
                    $this->db_declaracion->commit();
                }
            }
            else{
                if(is_null($declaracion_publica)) {
                    $pdf = \PDF::loadView('pdf.declaracion_patrimonial_publica_modif', compact("indice", "secciones_pdf", "previo", "secciones_formato"));
                    $ubicacion = "declaracion/{$declaracion_id}/";
                    $ubicacion_real = public_path($ubicacion);
                    if (!file_exists($ubicacion_real)) {
                        mkdir($ubicacion_real, 0777, true);
                    }
                    $pdf->save($ubicacion_real . $formato_usado->ClaveFormato . '-' . $nombre_completo . '-' . $TipoDeclaracion . '.pdf');
                    $this->db_declaracion->table('oic_declaraciones_publicas')->insert([
                        'DeclaracionID' => $declaracion_id,
                        'UsuarioID' => $datos_declaracion->UsuarioID,
                        'FormatoID' => $formato_usado->FormatoID,
                        'Generado' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'RutaDeclaracion' => $ubicacion . $formato_usado->ClaveFormato . '-' . $nombre_completo . '-' . $TipoDeclaracion . '.pdf'
                    ]);
                    $this->db_declaracion->commit();
                }
            }
            return [
                'status' => 'Ok',
            ];
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return [
                'status' => 'Error',
                'error'  => $e->getMessage()
            ];
        }
    }

    /**
     * Obtener detalles de una declaracion
     */
    public function listaDocumentosGenerados(){
        $lista_documentos = $this->dr->listaDocumentosGenerados(request()->declaracion_id);
        return response()->json([
            'status' => 'Ok',
            'documentos' => $lista_documentos,
        ]);
    }

    /**
     * Generacion de multiples declaraciones con formato especifico
     */
    public function generarMultiplesDeclaraciones(){
        try{
            $declaraciones = request()->declaraciones_id;
            foreach($declaraciones as $declaracion){
                $declaracion_id = $declaracion;
                $info_generar = array(
                    "formato_id" => request()->formato_id,
                    "declaracion_id"=> $declaracion_id
                );
                $respuesta = $this->generarDeclaracionPublica($info_generar);
                Log::info(json_encode($respuesta));
            }
            return response()->json([
               'status' => 'Ok'
            ]);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener lista de las rutas de declaraciones, por formato publico
     */
    public function getRutasDeclaracion(){
        try {
            $declaraciones = request()->declaraciones_id;
            $formato = request()->formato_id;
            $lista_documentos = $this->dr->getRutasDeclaracion($declaraciones,$formato);
            $zip = new ZipArchive();
            // Ruta absoluta
            $nombreArchivoZip = public_path('/declaraciones_publicas.zip');
            if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
                throw new Exception("Error al generar el archivo zip");
            }
            foreach ($lista_documentos as $documento) {
                $rutaAbsoluta = public_path($documento->RutaDeclaracion);
                $nombre = basename($rutaAbsoluta);
                $zip->addFile($rutaAbsoluta, $nombre);
            }
            $resultado = $zip->close();
            if ($resultado) {
                return response()->json([
                    'status' => 'Ok'
                ], 200);
            } else {
                throw new Exception("Error al generar el archivo zip");
            }
        } catch(\Exception $e) {
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Borrar una declaracion generada
     */
    public function borrarDeclaracionPublica(){
        try{
            $declaracion_id = request()->declaracion_id;
            $formato = request()->formato_id;

            $this->dr->borarFormatoGenerado($declaracion_id,$formato);

            return response()->json([
                'status' => 'Ok'
             ]);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Descarga de documentos de monitorei
     */
    public function getDescargaDocsMonitoreo(){
        try {
            $documento_requerido = request()->docs_seleccionados;
            $lista_documentos = request()->docs;
            $ejercicio = request()->ejercicio;
            $zip = new ZipArchive();

            // dd($lista_documentos);

            $nombreArchivoZip = public_path('/Declaraciones - '.$ejercicio.'.zip');
            if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
                throw new Exception("Error al generar el archivo zip");
            }

            switch($documento_requerido){
                case "1":
                    foreach ($lista_documentos as $documento){
                        $rutaAbsoluta = public_path($documento["RutaDeclaracionCompleta"]);
                        $nombre = $documento["SiglaArchivo"]."-".$documento["Declarante"]."-".$ejercicio.".pdf";
                        $zip->addFile($rutaAbsoluta, $nombre);
                    }
                    break;
                case "2":
                    foreach ($lista_documentos as $documento){
                        $rutaAbsoluta = public_path($documento["RutaArchivoSat"]);
                        $nombre = "DF -".$documento["Declarante"]."-".$ejercicio.".pdf";
                        $zip->addFile($rutaAbsoluta, $nombre);
                    }
                    break;
                case "3":
                    foreach ($lista_documentos as $documento){
                        $rutaAbsoluta = public_path($documento["RutaAcuse"]);
                        $nombre = "A".$documento["SiglaArchivo"]."-".$documento["Declarante"]."-".$ejercicio.".pdf";
                        $zip->addFile($rutaAbsoluta, $nombre);
                    }
                    break;
                case "4":
                    foreach ($lista_documentos as $documento){
                        $rutaAbsoluta = public_path($documento["RutaDeclaracionCompleta"]);
                        $rutaAbsolutaAcuse = public_path($documento["RutaAcuse"]);
                        $nombre = $documento["SiglaArchivo"]."-".$documento["Declarante"]."-".$ejercicio.".pdf";
                        $nombreAcuse = "A".$documento["SiglaArchivo"]."-".$documento["Declarante"]."-".$ejercicio.".pdf";
                        $zip->addFile($rutaAbsoluta, $nombre);
                        $zip->addFile($rutaAbsolutaAcuse, $nombreAcuse);
                    }
                    break;
            }

            $resultado = $zip->close();

            if ($resultado) {
                return response()->json([
                    'status' => 'Ok'
                ], 200);
            } else {
                throw new Exception("Error al generar el archivo zip");
            }
        } catch(Throwable $e) {
            return response()->json([
                'status' => 'Error',
                'error' => $e
            ], 500);
        }
    }
    /**
     * Borrar una declaracion generada
     */
    public function borrarDeclaracionesPublicas(){
        try{
            $declaraciones_id = request()->declaraciones_id;
            $formato = request()->formato_id;

            $this->dr->borarDeclaracionesGeneradas($declaraciones_id,$formato);

            return response()->json([
                'status' => 'Ok'
             ]);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()} | {$e->getFile()} | {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
}
