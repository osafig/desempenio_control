<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;


class WebServicesController extends Controller{
    public function __construct()
    {
       
    }
    public function ObtenerDcrpEnlace($idEnte, $anioSeleccionado)
    {

        $datos = DB::connection('siaMovil')
        ->table ('vResultadosDesempenioCrp')
        ->where('EntidadID', $idEnte)
        ->where('Ejercicio', $anioSeleccionado) 
        ->select('Preguntas', 'Contestadas','SinContestar')
        ->get();

        return response()->json($datos);
    }

    public function ObtenerDcrpRevision($idEnte, $anioSeleccionado){
        $datos =  DB::connection('siaMovil')
        ->table ('vRevisadasDesempenioAuditorCrp')
        ->where('EnteID', $idEnte)
        ->where('Ejercicio', $anioSeleccionado)
        ->whereIn('Nivel', ['responsable', 'auditor'])
        ->select('Nivel','Preguntas', 'Revisadas','SinRevisar')
        ->get();

         return response()->json($datos);
    }
}