<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationDeclare extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $ejercicio;
    public $formato_email_osafig;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $ejercicio, $formato_email_osafig)
    {
        $this->user = $user;
        $this->ejercicio = $ejercicio;
        $this->formato_email_osafig = $formato_email_osafig;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS', 'webmaster@mail.com'))
            ->subject(env('MAIL_FROM_NAME'))
            ->markdown('emails.notificacion_declaracion');
    }
}
