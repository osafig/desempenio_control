<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SidepatConfigProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $config_sidepat = \DB::connection('declaracion')
            ->table('oic_configuraciones')
            ->where('ConfigID', '=', 1)
            ->first();
        $this->app['config']->set('sidepat.color_primario', $config_sidepat->ColorPlataforma);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
