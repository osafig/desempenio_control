-- ----------------------------EJECUTAR EN DECLARACION------------------------------------
DELIMITER ;;
CREATE PROCEDURE osafgobm_declaracion.sp_obtenerDependientesPareja(
	in usuario_id int,
	in pareja bit
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener dependientes o pareja
	-- Fecha de creacion:		 31/05/2021
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 31/05/2021
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------

	select oud.Nombres, oud.PrimerApellido, oud.SegundoApellido,
		oud.FechaNacimiento, oud.RFC, ovc.Nombre, oud.CiudadanoExtranjero,
		oud.Curp, oud.DependienteEconomico, oud.DomicilioDeclarante, od.EsExtranjero,
		od.Calle, od.NumeroExterior, od.NumeroInterior,od.Colonia, od.Ciudad,
		oec.Nombre as Estado, omc.Nombre as Municipio, od.CodigoPostal,
		od.EstadoExtranjero, od.Pais, oald.ActividadLaboral, oog.NombreOrden,
		oac.NombreAmbito, oald.NombreEntePublico, oald.AreaAdscripcion,
		oald.Empleo, oald.FuncionPrincipal, oald.SalarioMensual, oald.FechaIngreso,
		oald.RFCInstitucion, oald.ProveedorContratista, oud.Observaciones, ospc.Sector 
	from oic_usuarios_dependientes oud 
	left join oic_vinculo_cat ovc ON oud.RelacionID = ovc.VinculoID 
	left join oic_direcciones od on oud.DireccionID = od.DireccionID 
	left join oic_estados_cat oec on od.EstadoID = oec.EstadoID
	left join oic_municipios_cat omc on oec.EstadoID = omc.EstadoID and omc.MunicipioID = od.MunicipioID 
	left join oic_actividad_laboral_detalle oald on oud.ActividadLaboralID = oald.ActividadLaboralDetalleID 
	left join oic_orden_gobierno oog on oald.NivelGobierno = oog.NivelOrdenID
	left join oic_ambito_cat oac on oald.AmbitoPublico = oac.AmbitoID 
	left join oic_sector_productivo_cat ospc on ospc.SectorID = oald.SectorID 
	where oud.UsuarioID = usuario_id and oud.Oculto = 0 and oud.Pareja = pareja;
END;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE osafgobm_declaracion.sp_obtenerBienesInmuebles(
	in declaracion_id bigint
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener los bienes inmuebles de una declaración
	-- Fecha de creacion:		 08/06/2020
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 08/06/2020
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	select otbc.Nombre as TipoInmueble, obi.Titular, obi.PorcentajePropiedad, obi.SuperficieTerreno,
		obi.SuperficieConstruida, obi.TipoPersonaTercero, obi.NombreTercero, obi.RFCTercero,
		ofoc.Nombre as FormaAdquisicion, obi.FormaPago, obi.TipoPersonaTransmisor, obi.NombreTransmisor,
		obi.RFCTransmisor, obi.RelacionTransmisor, obi.Valor, obi.TipoMoneda, obi.FechaOperacion, obi.ValorConformeA,
		od.EsExtranjero, od.Calle, od.NumeroExterior, od.NumeroInterior, od.Colonia,
		omc.Nombre as Municipio, oec.Nombre as Estado, od.CodigoPostal, od.Ciudad, od.EstadoExtranjero,
		od.Pais, obi.MotivoBaja, obi.Observaciones 
	from oic_bienes_inmubles obi
	join oic_tipo_bien_cat otbc on obi.TipoBienID = otbc.TipoBienID
	join oic_forma_operacion_cat ofoc on obi.FormaOperacionID = ofoc.FormaOperacionID 
	join oic_direcciones od on obi.DireccionID = od.DireccionID 
	join oic_municipios_cat omc on od.MunicipioID = omc.MunicipioID 
	join oic_estados_cat oec on od.EstadoID = oec.EstadoID 
	where obi.DeclaracionID = declaracion_id;
END;
DELIMITER ;

DELIMITER ;;
create procedure sp_actualizarDatosPersonales(
	in nombres varchar(250),
	in primer_apellido varchar(250),
	in segundo_apellido varchar(250),
	in email varchar(250),
	in rfc varchar(250),
	in curp varchar(250),
	in usuario_id int
)
begin 
	-- --------------------------------------------------------
	-- Procedimiento para actualizar los datos personales del usuario en main
	-- Fecha de creacion:		 09/06/2021
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 09/06/2021
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	UPDATE osaf_main_db.osaf_usuarios
	SET Nombres=nombres, PrimerApellido=primer_apellido, SegundoApellido=segundo_apellido, 
		Email=email, RFC=rfc, CURP=curp, updated_at=NOW()
	WHERE UsuarioID = usuario_id;
end;
DELIMITER ;


CREATE PROCEDURE `sp_obtenerDependientesPareja`(
	in usuario_id int,
	in pareja bit
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener dependientes o pareja
	-- Fecha de creacion:		 31/05/2021
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 31/05/2021
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------

	select oud.Nombres, oud.PrimerApellido, oud.SegundoApellido,
		oud.FechaNacimiento, oud.RFC, ovc.Nombre, oud.CiudadanoExtranjero,
		oud.Curp, oud.DependienteEconomico, oud.DomicilioDeclarante, od.EsExtranjero,
		od.Calle, od.NumeroExterior, od.NumeroInterior,od.Colonia, od.Ciudad,
		oec.Nombre as Estado, omc.Nombre as Municipio, od.CodigoPostal,
		od.EstadoExtranjero, od.Pais, oald.ActividadLaboral, oog.NombreOrden,
		oac.NombreAmbito, oald.NombreEntePublico, oald.AreaAdscripcion,
		oald.Empleo, oald.FuncionPrincipal, oald.SalarioMensual, oald.FechaIngreso,
		oald.RFCInstitucion, oald.ProveedorContratista, oud.Observaciones, ospc.Sector 
	from oic_usuarios_dependientes oud 
	left join oic_vinculo_cat ovc ON oud.RelacionID = ovc.VinculoID 
	left join oic_direcciones od on oud.DireccionID = od.DireccionID 
	left join oic_estados_cat oec on od.EstadoID = oec.EstadoID
	left join oic_municipios_cat omc on oec.EstadoID = omc.EstadoID and omc.MunicipioID = od.MunicipioID 
	left join oic_actividad_laboral_detalle oald on oud.ActividadLaboralID = oald.ActividadLaboralDetalleID 
	left join oic_orden_gobierno oog on oald.NivelGobierno = oog.NivelOrdenID
	left join oic_ambito_cat oac on oald.AmbitoPublico = oac.AmbitoID 
	left join oic_sector_productivo_cat ospc on ospc.SectorID = oald.SectorID 
	where oud.UsuarioID = usuario_id and oud.Oculto = 0 and oud.Pareja = pareja;
END

CREATE PROCEDURE `sp_obtenerBienesInmuebles`(
	in declaracion_id bigint
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener los bienes inmuebles de una declaración
	-- Fecha de creacion:		 08/06/2020
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 10/06/2020
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	select otbc.Nombre as TipoInmueble, obi.Titular, obi.PorcentajePropiedad, obi.SuperficieTerreno,
		obi.SuperficieConstruida, obi.TipoPersonaTercero, obi.NombreTercero, obi.RFCTercero,
		ofoc.Nombre as FormaAdquisicion, obi.FormaPago, obi.TipoPersonaTransmisor, obi.NombreTransmisor,
		obi.RFCTransmisor, obi.RelacionTransmisor, obi.Valor, obi.TipoMoneda, obi.FechaOperacion, obi.ValorConformeA,
		obi.FolioReal, od.EsExtranjero, od.Calle, od.NumeroExterior, od.NumeroInterior, od.Colonia,
		omc.Nombre as Municipio, oec.Nombre as Estado, od.CodigoPostal, od.Ciudad, od.EstadoExtranjero,
		od.Pais, obi.MotivoBaja, obi.Observaciones 
	from oic_bienes_inmubles obi
	join oic_tipo_bien_cat otbc on obi.TipoBienID = otbc.TipoBienID
	join oic_forma_operacion_cat ofoc on obi.FormaOperacionID = ofoc.FormaOperacionID 
	join oic_direcciones od on obi.DireccionID = od.DireccionID 
	join oic_municipios_cat omc on od.MunicipioID = omc.MunicipioID 
	join oic_estados_cat oec on od.EstadoID = oec.EstadoID 
	where obi.DeclaracionID = declaracion_id and obi.Oculto = 0;
END

CREATE PROCEDURE `sp_obtenerVehiculos`(
	in declaracion_id bigint
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener los vehiculos de una declaración
	-- Fecha de creacion:		 22/06/2021
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 22/06/2021
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	SELECT otbc.Nombre as TipoVehiculo, obm.Titular, obm.TipoPersonaTransmisor,
		obm.NombreRazonSocial, obm.RFCTRansmisor, obm.RelacionTransmisor, obm.VehiculoMarca,
		obm.VehiculoModelo, obm.VehiculoAnio, VehiculoNumeroSerie, obm.TipoPersonaTercero,
		obm.NombreTercero, obm.RFCTercero, obm.EnExtranjero, ofoc.Nombre as FormaAdquisicion,
		obm.FormaPago, obm.Valor, obm.TipoMoneda, obm.FechaOperacion, obm.MotivoBaja, obm.Observaciones 
	FROM oic_bienes_muebles obm
	join oic_tipo_bien_cat otbc on obm.TipoBienID = otbc.TipoBienID and otbc.Categoria = 'Vehículo'
	join oic_forma_operacion_cat ofoc on obm.FormaAdquisicionID = ofoc.FormaOperacionID 
	where obm.DeclaracionID = declaracion_id and obm.Oculto = 0;
END

CREATE PROCEDURE `sp_obtenerBienesMueble`(
	in declaracion_id bigint 
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener los bienes muebles de una declaración
	-- Fecha de creacion:		 08/06/2020
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 22/06/2020
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	
	SELECT otbc.Nombre as TipoBien, obm.Titular, obm.TipoPersonaTransmisor,
		obm.NombreRazonSocial, obm.RFCTRansmisor, obm.RelacionTransmisor, 
		obm.NombreTercero, obm.RFCTercero, obm.TipoPersonaTercero, obm.DescripcionBien,
		obm.FormaPago, ofoc.Nombre as TipoAdquisicion, obm.TipoMoneda, obm.FechaOperacion,
		obm.MotivoBaja, obm.Observaciones, obm.OtroTipoBien, obm.Valor 
	FROM oic_bienes_muebles obm
	join oic_tipo_bien_cat otbc on obm.TipoBienID = otbc.TipoBienID and otbc.Categoria = 'Mueble'
	join oic_forma_operacion_cat ofoc on obm.FormaAdquisicionID = ofoc.FormaOperacionID 
	where obm.DeclaracionID = declaracion_id and obm.Oculto = 0;
END

CREATE PROCEDURE `sp_obtenerInversiones`(
	in declaracion_id bigint
)
begin
	-- --------------------------------------------------------
	-- Procedimiento para obtener las inversiones de una declaración
	-- Fecha de creacion:		 22/06/2020
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 22/06/2020
	-- Actualizado por: 		 DEH
	-- --------------------------------------------------------
	select otic.Nombre as TipoInversion, orf.Titular, otic2.TipoInstrumento,
		orf.RFCTercero, orf.NombreTercero, orf.TipoPersonaTercero, orf.NumeroContrato,
		orf.EsExtranjero, orf.Institucion, orf.RFCIntitucion, orf.Saldo, orf.Moneda, 
		orf.Aclaraciones, orf.Ubicacion
	from oic_recursos_financieros orf
	join oic_tipo_inversion_cat otic on orf.TipoInversionID = otic.TipoInversionID
	join oic_tipo_instrumento_cat otic2 on orf.RecursoFinancieroID = otic2.IntrumentoID
	where orf.DeclaracionID = declaracion_id and orf.Oculto = 0;
END

CREATE PROCEDURE `sp_obtenerAdeudos`(
	in declaracion_id bigint
)
begin
	-- ---------------------------------------------------------
	-- Procedimiento para obtener los adeudos de una declaracion
	-- Fecha de creacion:		 22/06/2020
	-- Creado por:				 DEH
	-- Ultima Actualizacion:	 22/06/2020
	-- Actualizado por: 		 DEH
	-- ---------------------------------------------------------
	select oa.Titular, otac.Nombre as TipoAdeudo, oa.NoCuenta,
		oa.FechaOtorgamiento, oa.MontoOriginal, oa.Moneda, oa.SaldoInsoluto,
		oa.Ubicacion, oa.TipoPersonaTercero, oa.NombreTercero, oa.RFCTercero,
		oa.RFCOtorgante, oa.TipoPersonaOtorgante, oa.NombreRazonSocial, oa.Aclaraciones
	from oic_adeudos oa 
	join oic_tipo_adeudo_cat otac on oa.TipoAdeudoID = otac.TipoAdeudoID 
	where oa.Oculto = 0 and oa.DeclaracionID = declaracion_id;
END

CREATE PROCEDURE `sp_obtenerPrestamos`(
	in declaracion_id bigint
)
begin
	select opc.TipoDeBien, otbc.Nombre as BienInmueble, vehiculo.Nombre as BienMueble, od.EsExtranjero, od.Calle, od.NumeroExterior, 
		od.NumeroInterior, od.Colonia, omc.Nombre as Municipio, oec.Nombre as Estado,
		od.CodigoPostal, od.Ciudad, od.EstadoExtranjero, od.Pais, opc.Marca, opc.Modelo,
		opc.Anio, opc.NoSerie, opc.EntidadFederativa, opc.Pais, opc.Titular, opc.TipoPersonaTitular,
		opc.RFCTitular, opc.Vinculo, opc.Observaciones
	from oic_prestamo_comodato opc
	left join oic_tipo_bien_cat otbc on opc.BienInmuebleID = otbc.TipoBienID
	left join oic_tipo_bien_cat vehiculo on opc.BienMuebleID = vehiculo.TipoBienID 
	left join oic_direcciones od on opc.DireccionID = od.DireccionID 
	left join oic_municipios_cat omc on od.MunicipioID = omc.MunicipioID 
	left join oic_estados_cat oec on oec.EstadoID = od.EstadoID 
	where opc.DeclaracionID = declaracion_id and opc.Oculto = 0;
END

-- ----------------------------EJECUTAR EN MAIN------------------------------------