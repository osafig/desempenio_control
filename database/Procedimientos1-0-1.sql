DELIMITER ;;
CREATE PROCEDURE osafgobm_declaracion.sp_detalleDeclaracion(
	in declaracion_id bigint
)
begin
	select ved.DeclaracionID, ved.EjercicioID, TipoDeclaracion,
		Declarante, TotalSecciones, SeccionesTerminadas, ved.UsuarioID,
		oi.RemuneracionMensualNeta, oi.IngNetoParejaDependientes, oi.TotalOtrosIngresos,
		oec.`Year`
	from v_estadoDeclaraciones ved
	left join oic_ingresos oi on ved.DeclaracionID = oi.DeclaracionID
	join osafgobm_main_db.osaf_ejercicios_cat oec on ved.EjercicioID = oec.EjercicioID
	where ved.DeclaracionID = declaracion_id;
END;
DELIMITER ;

CREATE VIEW v_estadoDeclaraciones as
select odp.DeclaracionID, odp.EjercicioID, otdc.Nombre as TipoDeclaracion, `ou`.`UsuarioID` AS `UsuarioID`,
	concat(ou.Nombres, ' ', ou.PrimerApellido, ' ', ou.SegundoApellido) as Declarante,
	(Select count(oded.EstadoDeclaracionID)
		from oic_declaracion_estatus_det oded
		where oded.DeclaracionID = odp.DeclaracionID
			and oded.Aplica = 1) as TotalSecciones,
	(Select count(oded.EstadoDeclaracionID)
		from oic_declaracion_estatus_det oded
		where oded.DeclaracionID = odp.DeclaracionID
			and oded.Aplica = 1
			and oded.EstadoActual = 1) as SeccionesTerminadas
from oic_declaraciones_patrimoniales odp
join osafgobm_main_db.osaf_usuarios ou on odp.UsuarioID = ou.UsuarioID
join oic_tipo_declaracion_cat otdc on odp.TipoDeclaracionID = otdc.TipoDeclaracionID
where odp.Eliminado = 0
order by odp.DeclaracionID
DELIMITER
