select `trsm`.`Id_Registro` AS `Id_Registro`,
`oef`.`EntidadID` AS `EntidadID`,
`oef`.`NombreCorto` AS `Ente`,
`trsm`.`Id_Tipo_Auditoria` AS `Id_Tipo_Auditoria`,
`cta`.`Descripcion` AS `Tipo_Auditoria`,
`trsm`.`Id_Rol` AS `Id_Rol`,
`cr`.`Nombre` AS `Nivel`,
`trsm`.`No_En_Revision` AS `No_En_Revision`,
`trsm`.`Cve_Res` AS `Resultados`,
`trsm`.`Ejercicio` AS `Ejercicio`
from (((`osafgobm_bd_siaMovil`.`te_ResSiaMovil` `trsm` join `osafgobm_main_db`.`osaf_entidades_fiscalizables` `oef` on((`oef`.`IdExterno` = `trsm`.`Id_Ente`))) 
join `osafgobm_bd_siaMovil`.`Cat_Tipo_Auditoria` `cta` on((`trsm`.`Id_Tipo_Auditoria` = `cta`.`Id_Tipo_Auditoria`))) 
join `osafgobm_bd_siaMovil`.`Cat_Roles` `cr` on((`cr`.`Id_registro` = `trsm`.`Id_Rol`)))




select `oef`.`EntidadID` AS `EntidadID`,
`oef`.`NombreCorto` AS `Ente`,
`trsm`.`Id_Tipo_Auditoria` AS `Id_Tipo_Auditoria`,
`cta`.`Descripcion` AS `Tipo_Auditoria`,
`trsm`.`Total_Resultados` AS `Total_Resultados`,
`trsm`.`Total_EnRevision` AS `Total_EnRevision`,
`trsm`.`Total_Validados` AS `Total_Validados`,
`trsm`.`Ejercicio` AS `Ejercicio`
from (((`osafgobm_bd_siaMovil`.`te_ResSiaMovil` `trsm` 
join `osafgobm_main_db`.`osaf_entidades_fiscalizables` `oef` 
on((`oef`.`IdExterno` = `trsm`.`Id_Ente`))) 
join `osafgobm_bd_siaMovil`.`Cat_Tipo_Auditoria` `cta` 
on((`trsm`.`Id_Tipo_Auditoria` = `cta`.`Id_Tipo_Auditoria`))) 
join `osafgobm_bd_siaMovil`.`Cat_Roles` `cr` on((`cr`.`Id_registro` = `trsm`.`Id_Rol`))) 
group by `oef`.`EntidadID`,`oef`.`NombreCorto`,`trsm`.`Id_Tipo_Auditoria`,`cta`.`Descripcion`,`trsm`.`Total_Resultados`,`trsm`.`Total_EnRevision`,`trsm`.`Total_Validados`,`trsm`.`Ejercicio`

/*vrespuestasresumen*/
select `oef`.`EntidadID` AS `EntidadID`,
`oef`.`NombreCorto` AS `Ente`,
`trsm`.`Id_Tipo_Auditoria` AS `Id_Tipo_Auditoria`,
`cta`.`Descripcion` AS `Tipo_Auditoria`,
`trsm`.`Total_Resultados` AS `Total_Resultados`,
`trsm`.`Total_EnRevision` AS `Total_EnRevision`,
`trsm`.`Total_Validados` AS `Total_Validados`,
`trsm`.`Ejercicio` AS `Ejercicio`
from (((`osafgobm_bd_siaMovil`.`te_ResSiaMovil` `trsm`
join `osafgobm_main_db`.`osaf_entidades_fiscalizables` `oef` on((`oef`.`IdExterno` = `trsm`.`Id_Ente`)))
join `osafgobm_bd_siaMovil`.`Cat_Tipo_Auditoria` `cta` on((`trsm`.`Id_Tipo_Auditoria` = `cta`.`Id_Tipo_Auditoria`))) 
join `osafgobm_bd_siaMovil`.`Cat_Roles` `cr` on((`cr`.`Id_registro` = `trsm`.`Id_Rol`))) 
group by `oef`.`EntidadID`,`oef`.`NombreCorto`,`trsm`.`Id_Tipo_Auditoria`,`cta`.`Descripcion`,`trsm`.`Total_Resultados`,`trsm`.`Total_EnRevision`,`trsm`.`Total_Validados`,`trsm`.`Ejercicio`


CREATE VIEW vResultadosDesempenioCrp AS
select `oef`.`EntidadID` AS `EntidadID`,
if((trim(`oef`.`NombreCorto`) = ''),`oef`.`Nombre`,`oef`.`NombreCorto`) AS `Entidad`,
`oec`.`Year` AS `Ejercicio`,
count(`spc`.`CuestionarioID`) AS `Preguntas`,
(select count(distinct `sr`.`RespuestaID`) 
from (((`zpjeugub_desempenio`.`sad_aplicacion_cuestionario` `sac` 
join `zpjeugub_desempenio`.`sad_preguntas_cat` `spc` 
on((`sac`.`CuestionarioID` = `spc`.`CuestionarioID`))) 
left join `zpjeugub_desempenio`.`sad_respuestas` `sr` 
on((`sac`.`AplicacionID` = `sr`.`AplicacionID`))) 
join `zpjeugub_desempenio`.`sad_auditoria_etapa_det` `saed`
on((`sr`.`AuditoriaEtapaID` = `saed`.`AuditoriaEtapaID`))) 
where ((`sac`.`AuditoriaID` = `sa`.`AuditoriaID`)
and (`saed`.`EtapaID` = 1))) AS `Contestadas`,
(count(`spc`.`CuestionarioID`) - (select count(distinct `sr`.`RespuestaID`) 
from (((`zpjeugub_desempenio`.`sad_aplicacion_cuestionario` `sac` 
join `zpjeugub_desempenio`.`sad_preguntas_cat` `spc` on((`sac`.`CuestionarioID` = `spc`.`CuestionarioID`))) 
left join `zpjeugub_desempenio`.`sad_respuestas` `sr` on((`sac`.`AplicacionID` = `sr`.`AplicacionID`))) 
join `zpjeugub_desempenio`.`sad_auditoria_etapa_det` `saed` on((`sr`.`AuditoriaEtapaID` = `saed`.`AuditoriaEtapaID`)))
where ((`sac`.`AuditoriaID` = `sa`.`AuditoriaID`) and (`saed`.`EtapaID` = 1)))) AS `SinContestar`
from ((((`zpjeugub_desempenio`.`sad_auditorias` `sa` join `zpjeugub_main`.`osaf_ejercicios_cat` `oec`
on((`sa`.`EjercicioID` = `oec`.`EjercicioID`))) join `zpjeugub_main`.`osaf_entidades_fiscalizables` `oef` on((`sa`.`EnteID` = `oef`.`EntidadID`))) 
join `zpjeugub_desempenio`.`sad_aplicacion_cuestionario` `sac` on((`sa`.`AuditoriaID` = `sac`.`AuditoriaID`))) 
join `zpjeugub_desempenio`.`sad_preguntas_cat` `spc` on((`sac`.`CuestionarioID` = `spc`.`CuestionarioID`)))
group by `sac`.`CuestionarioID`,`oef`.`NombreCorto`,`oec`.`Year`,`oef`.`EntidadID`,`sa`.`AuditoriaID`


CREATE VIEW vRevisadasDesempenioAuditorCrp AS
select `sa`.`EnteID` AS `EnteID`,
if((`oef`.`NombreCorto` = ''),`oef`.`Nombre`,`oef`.`NombreCorto`) AS `NombreCorto`,
`oec`.`Year` AS `Ejercicio`,
`sec`.`RolEncargado` AS `Nivel`,
count(`spc`.`PreguntaID`) AS `Preguntas`,
(select count(`zpjeugub_desempenio`.`sad_recomendaciones_det`.`RecomendacionID`)
from `zpjeugub_desempenio`.`sad_recomendaciones_det` 
where (`zpjeugub_desempenio`.`sad_recomendaciones_det`.`AuditoriaEtapaID` = `saed`.`AuditoriaEtapaID`)) AS `Revisadas`,
(count(`spc`.`PreguntaID`) - (select count(`zpjeugub_desempenio`.`sad_recomendaciones_det`.`RecomendacionID`)
from `zpjeugub_desempenio`.`sad_recomendaciones_det`
where (`zpjeugub_desempenio`.`sad_recomendaciones_det`.`AuditoriaEtapaID` = `saed`.`AuditoriaEtapaID`)))
AS `SinRevisar` from (((((((`zpjeugub_desempenio`.`sad_auditorias` `sa` join `zpjeugub_main`.`osaf_ejercicios_cat` `oec` on((`sa`.`EjercicioID` = `oec`.`EjercicioID`)))
join `zpjeugub_main`.`osaf_entidades_fiscalizables` `oef` on((`sa`.`EnteID` = `oef`.`EntidadID`)))
join `zpjeugub_desempenio`.`sad_aplicacion_cuestionario` `sac` on((`sa`.`AuditoriaID` = `sac`.`AuditoriaID`)))
left join `zpjeugub_desempenio`.`sad_cuestionario` `sc` on((`sac`.`CuestionarioID` = `sc`.`CuestionarioID`)))
left join `zpjeugub_desempenio`.`sad_preguntas_cat` `spc` on((`sc`.`CuestionarioID` = `spc`.`CuestionarioID`)))
join `zpjeugub_desempenio`.`sad_auditoria_etapa_det` `saed` on(((`sa`.`AuditoriaID` = `saed`.`AuditoriaID`) and (`saed`.`EtapaID` = `sa`.`EtapaID`))))
join `zpjeugub_desempenio`.`sad_etapa_cat` `sec` on((`sa`.`EtapaID` = `sec`.`EtapaID`)))
where (`oec`.`Year` = 2023)
group by `sa`.`AuditoriaID`,`sac`.`AplicacionID`,`sc`.`CuestionarioID`,`sa`.`EnteID`,`oef`.`NombreCorto`,`oec`.`Year`,`saed`.`AuditoriaEtapaID`