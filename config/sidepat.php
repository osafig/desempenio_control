<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Archivo de configuraciones de SiDePat
    |--------------------------------------------------------------------------
    */

    // Nombre de aplicación
    'nombre_aplicacion' => env('APPLICATION_NAME',  'Sidepat'),
    'nombre_aplicacion_titulo' => env('APPLICATION_NAME_TITLE', 'SIDEPAT'),

    // Logos de la institución
    'logo'             => env('CONF_LOGO'),
    'logo2'            => env('CONF_LOGO2'),

    // Paleta de colores
    'color_primario'   => env('CONF_COLOR_PRIMARY'),
    'color_dark1'      => env('CONF_COLOR_DARK1'),
    'color_dark2'      => env('CONF_COLOR_DARK2'),

    // Editar datos generales
    'editar_generales' => env('CONF_EDITAR_GENERALES', true),

    // Driver de correos
    'driver_correos'   => env('CONF_DRIVER_EMAIL', 'sidepat'),

    // Declaracion terminada
    'declaracion_terminada' => env('CONF_DECLARACION_TERMINADA', false),

    // Version
    'version_actual' => 'v.2.0.6',

    // Mail osafig
    'mail_formato_osafig' => env('CONF_MAIL_FORMATO_OSAFIG', false),

    // Modulo de limpieza
    'limpieza_auditorias' => env('CONF_LIMPIEZA_AUDITORIAS', false),

    // Mostrar aviso de privacidad
    'aviso_privacidad' => env('CONF_AVISO_PROVACIDAD', false),
];
