import Vue from 'vue';
import Vuex from 'vuex';

import user                  from '@/js/store/modules/user.js';
import auditoria             from '@/js/store/modules/auditoria.js';
import cuestionario          from '@/js/store/modules/cuestionario.js';
import declaracion           from '@/js/store/modules/declaracion.js';
import direccion_declaracion from '@/js/store/modules/direccion_declaracion.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        // Estado principal que se comparte por todo los componentes
    },
    mutations: {
        // Solo se puede modificar el estado a traves de mutaciones
        // las mutaciones son las uicas en hablar al estado
    },
    actions: {
        // Las acciones son similares a las mutaciones (No pueden hablar al estado)
        // Pueden tener un comportamiento asincrono, realizar peticiones, y realizar
        // operaciones. Para despues lanzar mutaciones.
    },
    getters: {
        // Podemos pensar en los getters como propiedades computadas para el store
    },
    modules: {
        // Podemos agregar diferentes modulos
        user,
        auditoria,
        cuestionario,
        declaracion,
        direccion_declaracion
    }
});
