export default  {
    state: {
        datos_declaracion: {},
        users: [],
        tipoDeclaracionConsultado: {},
        usersPast: [],
        usersConflicto:[]
    },
    mutations: {
        setUsersToDeclare(state, users) {
            state.users = users;
        },
        setUsersToDeclareConflicto(state, usersConflicto) {
            state.usersConflicto = usersConflicto;
        },
        setTipoDeclaracionConsultado(state, tipo_consultado) {
            state.tipoDeclaracionConsultado = tipo_consultado;
        },
        setDatosDeclaracion(state, datos){
            state.datos_declaracion = datos;
        },
        setUsersPast(state, datos) {
            state.usersPast = datos;
        }
    },
    actions: {
        getUsersToDeclare({ commit }) {
            return new Promise((resolve) => {
                axios.get('/api/declaracion/admin/get-users-declaracion').then(resp => {
                    commit('setUsersToDeclare', resp.data.users);
                    commit('setUsersPast', resp.data.usersPast);
                    commit('setUsersToDeclareConflicto',resp.data.usersConflicto);
                    resolve();
                }).catch(e => {
                    console.log(e.response.data.message);
                });
            });
        },
        getDatosDeclaracion({commit}){
            return new Promise((resolve) => {
                let declaracion_id = sessionStorage.getItem('declaracion_id');
                axios.post('/api/declaracion/get-data-declaracion', {declaracion_id: declaracion_id}).then(resp => {
                    commit('setDatosDeclaracion', resp.data.declaracion);
                    resolve();
                }).catch(e => {
                    console.log(e);
                })
            })
        },
    },
    getters: {
        getUsersToDeclare(state) {
            return state.users;
        },
        getUsersToDeclareConflicto(state) {
            return state.usersConflicto;
        },
        getUsersToDeclarePast(state) {
            return state.usersPast;
        },
        getTipoDeclaracionConsultado(state)
        {
            return state.tipoDeclaracionConsultado;
        },
        getDatosDeclaracion(state)
        {
            return state.datos_declaracion;
        },
    }
}
