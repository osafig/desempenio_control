export default  {
    state: {
        user: {}
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        }
    },
    actions: {
        getUser({ commit }) {
            if(localStorage.getItem('tipo_usuario') === 'osafig'){
                return new Promise((resolve) => {
                    axios.get('/api/user').then((res) => {
                        commit('setUser', res.data);
                        resolve();
                    }).catch((err) => {
                        console.log(err.response.data.message);
                    });
                });
            } else {
                return new Promise((resolve) => {
                    let data = {
                        api_token: localStorage.getItem('api_token')
                    };
                    axios.post('/api/enlace', data).then((res) => {
                        commit('setUser', res.data.enlace);
                        resolve();
                    }).catch((err) => {
                        console.log(err.response.data.message);
                    });
                });
            }
        },
        logoutUser({ commit }) {
            commit('setUser', {});
        }
    },
    getters: {
        fullNameUser(state) {
            if(typeof state.user.Nombres === 'undefined') {
                return undefined;
            } else {
                return state.user.Nombres + ' ' + state.user.PrimerApellido + ' ' + state.user.SegundoApellido;
            }
        },
        dataUser(state) {
            return state.user;
        }
    }
}
