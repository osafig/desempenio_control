export default  {
    state: {
        //Expedientes
        hasExpedientesCrp: false,
        hasExpedientesSolventacion: false,
        datosAuditoria: null,
        hasExpedientesCrpSad: false,
        hasExpedientesSolventacionSad: false,
        datosAuditoriaSad: null
    },
    mutations: {
        setHasExpedientesCrp(state, value) {
            state.hasExpedientesCrp = value;
        },
        setHasExpedientesSolventacion(state,value) {
            state.hasExpedientesSolventacion = value;
        },
        setDatosAuditoria(state,value) {
            state.datosAuditoria = value;
        },
        setHasExpedientesCrpSad(state, value) {
            state.hasExpedientesCrpSad = value;
        },
        setHasExpedientesSolventacionSad(state,value) {
            state.hasExpedientesSolventacionSad = value;
        },
        setDatosAuditoriaSad(state,value) {
            state.datosAuditoriaSad = value;
        }
    },
    actions: {
        getExpedientesAuditoria({ commit }, payload) {
            return new Promise((resolve) => {
                let data = payload;
                axios.post('/api/get-expedientes', data).then(resp => {
                    commit('setHasExpedientesCrp', resp.data.hasExpedienteCrp);
                    commit('setHasExpedientesSolventacion', resp.data.hasExpedientesSolventacion);
                    commit('setDatosAuditoria',data);
                    resolve();
                }).catch((err) => {
                    console.log(err);
                });
            });
        },
        getExpedientesAuditoriaSad({ commit }, payload) {
            return new Promise((resolve) => {
                let data = payload;
                axios.post('/api/desempenio/get-expedientes-sad', data).then(resp => {
                    commit('setHasExpedientesCrpSad', resp.data.hasExpedientesCrpSad);
                    commit('setHasExpedientesSolventacionSad', resp.data.hasExpedientesSolventacionSad);
                    commit('setDatosAuditoriaSad',data);
                    resolve();
                }).catch((err) => {
                    console.log(err);
                });
            });
        },
    },
    getters: {
        getHasExpedientesCrp(state) {
            return state.hasExpedientesCrp;
        },
        getHasExpedientesSolventacion(state) {
            return state.hasExpedientesSolventacion;
        },
        getDatosAuditoria(state){
            return state.datosAuditoria;
        },
        getHasExpedientesCrpSad(state) {
            return state.hasExpedientesCrpSad;
        },
        getHasExpedientesSolventacionSad(state) {
            return state.hasExpedientesSolventacionSad;
        },
        getDatosAuditoriaSad(state){
            return state.datosAuditoriaSad;
        }
    } 
}
