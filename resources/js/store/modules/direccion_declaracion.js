export default {
    state: {
        direccion: {
            residencia:         0,
            calle:             '',
            numero_exterior:   '',
            numero_interior:   '',
            colonia:           '',
            municipio_id:      '',
            estado_id:         '',
            codigo_postal:     '',
            ciudad:            '',
            estado_extranjero: '',
            pais:              '',
            observaciones:     '',
            direccion_id:      '',
        }
    },
    mutations: {
        updateResidencia(state, value) {
            state.direccion.residencia = value;
        },
        updateCalle(state, value) {
            state.direccion.calle = value;
        },
        updateNumeroExterior(state, value) {
            state.direccion.numero_exterior = value;
        },
        updateNumeroInterior(state, value) {
            state.direccion.numero_interior = value;
        },
        updateColonia(state, value) {
            state.direccion.colonia = value;
        },
        updateMunicipioId(state, value) {
            state.direccion.municipio_id = value;
        },
        updateEstadoId(state, value) {
            state.direccion.estado_id = value;
        },
        updateCodigoPostal(state, value) {
            state.direccion.codigo_postal = value;
        },
        updateCiudad(state, value) {
            state.direccion.ciudad = value;
        },
        updateEstadoExtranjero(state, value) {
            state.direccion.estado_extranjero = value;
        },
        updatePais(state, value) {
            state.direccion.pais = value;
        },
        updateObservaciones(state, value) {
            state.direccion.observaciones = value;
        },
        updateDireccionId(state, value) {
            state.direccion.direccion_id = value;
        },
        resetDireccion(state) {
            state.direccion.residencia        = 0;
            state.direccion.calle             = '';
            state.direccion.numero_exterior   = '';
            state.direccion.numero_interior   = '';
            state.direccion.colonia           = '';
            state.direccion.municipio_id      = '';
            state.direccion.estado_id         = '';
            state.direccion.codigo_postal     = '';
            state.direccion.ciudad            = '';
            state.direccion.estado_extranjero = '';
            state.direccion.pais              = '';
            state.direccion.observaciones     = '';
            state.direccion.direccion_id      = '';
        }
    },
    actions: {

    },
    getters: {
        direccionDeclaracion(state) {
            return state.direccion;
        }
    }
}
