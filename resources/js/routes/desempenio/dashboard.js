// Rutas Extendidas de Dashboard
import { routesDesempenioResponsable } from './responsable';
import { routesDesempenioAuditor } from './auditor';
import { routesDesempenioEnlace } from './enlace';

export default [
    ...routesDesempenioResponsable,
    ...routesDesempenioAuditor,
    ...routesDesempenioEnlace
]
