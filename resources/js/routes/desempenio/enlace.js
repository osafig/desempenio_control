// ------------------------------------------------------------------------------------------------------
//                      SECCION DE USUARIO RESPONSABLE DE AUDITORIA
// ------------------------------------------------------------------------------------------------------
import ModuloCuestionario from '@/js/views/dashboard/auditoria_desempenio/Enlace/ModuloCuestionario.vue';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';
import authEnlaceMiddleware  from '@/js/middleware/authEnlaceMiddleware';

const routesDesempenioEnlace = [
    {
        path: '/panel/enlace/auditoria-desempenio/cuestionario',
        name: 'cuestionario-auditoria-desempenio-enlace',
        component: ModuloCuestionario,
        meta: {
            middleware: [
                authMiddleware,
                authEnlaceMiddleware
            ]
        },
    },
]

export { routesDesempenioEnlace };
