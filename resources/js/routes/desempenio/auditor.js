// ----------------------------------------------
//              SECCION DE AUDITOR
// ----------------------------------------------
import IndexDesempenio from "@/js/views/dashboard/auditoria_desempenio/Auditor/IndexDesempenio";
import RespuestaInicialDesempenio from '@/js/views/dashboard/auditoria_desempenio/Auditor/RespuestaInicialDesempenio';
import RespuestaInicialDesempenioDetalleAuditoria from "@/js/views/dashboard/auditoria_desempenio/Auditor/RespuestaInicialDesempenioDetalleAuditoria";
import DesempenioPreviewAnexo from "@/js/views/dashboard/auditoria_desempenio/Auditor/DesempenioPreviewAnexo";
import RespuestaSolventacionDesempenio from '@/js/views/dashboard/auditoria_desempenio/Auditor/RespuestaSolventacionDesempenio';
import RespuestaSolventacionDesempenioDetalleAuditoria from "@/js/views/dashboard/auditoria_desempenio/Auditor/RespuestaSolventacionDesempenioDetalleAuditoria";

// Importamos middlewares
import authMiddleware        from '@/js/middleware/auth';
import authAuditorMiddleware from '@/js/middleware/authAuditorMiddleware';

let middleware_rules = [
    authMiddleware,
    authAuditorMiddleware
];

const basePath = '/panel/sad/auditor';

const routesDesempenioAuditor = [
    {
        path: `${basePath}`,
        name: 'dashboard_auditor_desempenio',
        component: IndexDesempenio,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/respuesta-inicial`,
        name: 'dashboard_auditor_respuesta_inicial_desempenio',
        component: RespuestaInicialDesempenio,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/respuesta-solventacion`,
        name: 'dashboard_auditor_modulo_solventacion_recomendaciones_desempenio',
        component: RespuestaSolventacionDesempenio,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/preview-anexo/:anexo_hash`,
        name: 'dashboard_auditor_desempenio_preview_anexo',
        component: DesempenioPreviewAnexo,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/respuesta-inicial/:auditoria`,
        name: 'dashboard_auditor_respuesta_inicial_desempenio_detalle',
        component: RespuestaInicialDesempenioDetalleAuditoria,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/respuesta-solventacion/:auditoria`,
        name: 'dashboard_auditor_respuesta_solventacion_desempenio_detalle',
        component: RespuestaSolventacionDesempenioDetalleAuditoria,
        meta: {
            middleware: middleware_rules
        },
    },
];

export { routesDesempenioAuditor };
