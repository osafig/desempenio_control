// Componentes de Dashboard
import ModuloMonitoreoRoot from '@/js/views/dashboard/auditoria_control_interno/ModuloMonitoreoRoot.vue';
import ErrorDashboard from '@/js/views/404_Dashboard.vue';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

// Rutas Extendidas de Dashboard
import { routesControlInternoResponsable } from './responsable';
import { routesControlInternoAuditor } from './auditor';
import { routesControlInternoEnlace } from './enlace';

export default [
    ...routesControlInternoResponsable,
    ...routesControlInternoEnlace,
    ...routesControlInternoAuditor,
    {
        path: '/panel/ci/monitoreo-root',
        name: 'auditoria-control-interno-monitoreo-root',
        component: ModuloMonitoreoRoot,
        meta: {
            middleware: authMiddleware
        }
    },
    {
        path: '*',
        name: '404-dashboard',
        component: ErrorDashboard,
        meta: {
            middleware: authMiddleware,
        }
    },
]
