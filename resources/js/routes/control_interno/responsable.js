// ------------------------------------------------------------------------------------------------------
//                        SECCION DE USUARIO RESPONSABLE DE AUDITORIA
// ------------------------------------------------------------------------------------------------------
import AuditoriaControlInternoIndex             from '@/js/views/dashboard/auditoria_control_interno/Index.vue';
import ModuloAuditorias                         from '@/js/views/dashboard/auditoria_control_interno/ModuloAuditorias.vue';
import ModuloAuditoriasDetalle                  from '@/js/views/dashboard/auditoria_control_interno/ModuloAuditoriasDetalle.vue';
import ModuloEnlaces                            from '@/js/views/dashboard/auditoria_control_interno/ModuloEnlaces.vue';
import ModuloEnlacesCrear                       from '@/js/views/dashboard/auditoria_control_interno/ModuloEnlacesCrear.vue';
import ModuloEnlacesEditar                      from '@/js/views/dashboard/auditoria_control_interno/ModuloEnlacesEditar.vue';
import ModuloRespuestasIniciales                from '@/js/views/dashboard/auditoria_control_interno/ModuloRespuestasIniciales.vue';
import ModuloRespuestasInicialesDetalle         from '@/js/views/dashboard/auditoria_control_interno/ModuloRespuestasInicialesDetalle.vue';
import ModuloSolventacionRecomendaciones        from '@/js/views/dashboard/auditoria_control_interno/ModuloSolventacionRecomendaciones.vue';
import ModuloSolventacionRecomendacionesDetalle from '@/js/views/dashboard/auditoria_control_interno/ModuloSolventacionRecomendacionesDetalle.vue';
import ModuloMonitoreo                          from '@/js/views/dashboard/auditoria_control_interno/ModuloMonitoreo.vue';
import ControlInternoPreviewAnexo               from "@/js/views/dashboard/auditoria_control_interno/Auditor/ControlInternoPreviewAnexo";
import ControlInternoPreviewRespuestas          from "@/js/views/dashboard/auditoria_control_interno/PreviewRespuestas";
import ControlInternoPreviewAcuse               from "@/js/views/dashboard/auditoria_control_interno/ControlInternoPreviewAcuse";
import AdministrarCuestionarios                 from '@/js/views/dashboard/auditoria_control_interno/Auditor/AdministrarCuestionarios';
import DetalleCuestionario                      from '@/js/views/dashboard/auditoria_control_interno/Auditor/DetalleCuestionario';
import PreguntasTema                            from '@/js/views/dashboard/auditoria_control_interno/Auditor/PreguntasTema';
import FormularioPregunta                       from '@/js/views/dashboard/auditoria_control_interno/Auditor/FormularioPregunta';
import FormularioCuestionario                   from '@/js/views/dashboard/auditoria_control_interno/Auditor/FormularioCuestionario';
// import NuevaPreguntaCuestionario                from '@/js/views/dashboard/auditoria_control_interno/';


// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';
import authResponsableMiddleware from '@/js/middleware/authResponsableMiddleware';

const basePath = '/panel/ci/responsable';

const routesControlInternoResponsable = [
    {
        path: `${basePath}`,
        name: 'auditoria-control-interno',
        component: AuditoriaControlInternoIndex,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/auditorias`,
        name: 'modulo-auditorias-control-interno',
        component: ModuloAuditorias,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/auditorias/:ejercicio/:auditoria`,
        name: 'modulo-auditorias-control-interno-detalle',
        component: ModuloAuditoriasDetalle,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/enlaces/crear/:EnteID?`,
        name: 'modulo-enlaces-control-interno-crear',
        props: true,
        component: ModuloEnlacesCrear,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/enlaces`,
        name: 'modulo-enlaces-control-interno',
        component: ModuloEnlaces,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/enlaces/:enlace`,
        name: 'modulo-enlaces-control-interno-editar',
        component: ModuloEnlacesEditar,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/respuestas-iniciales`,
        name: 'modulo-respuesta-inicial-control-interno',
        component: ModuloRespuestasIniciales,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/respuestas-iniciales/:auditoria`,
        name: 'modulo-respuesta-inicial-control-interno-detalle',
        component: ModuloRespuestasInicialesDetalle,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/solventacion-recomendaciones`,
        name: 'modulo-solventacion-recomendaciones-control-interno',
        component: ModuloSolventacionRecomendaciones,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/solventacion-recomendaciones/:auditoria`,
        name: 'modulo-solventacion-control-interno-detalle',
        component: ModuloSolventacionRecomendacionesDetalle,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/monitoreo`,
        name: 'modulo-monitoreo-control-interno',
        component: ModuloMonitoreo,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/preview-anexo/:anexo_hash`,
        name: 'dashboard_control_interno_preview_anexo',
        component: ControlInternoPreviewAnexo,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/preview-respuestas/:ejercicio/:auditoria/:tipo_auditoria`,
        name: 'dashboard_control_interno_preview_respuestas',
        component: ControlInternoPreviewRespuestas,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path: `${basePath}/preview-acuse/:ejercicio/:auditoria/:tipo_auditoria`,
        name: 'dashboard_control_interno_preview_acuse',
        component: ControlInternoPreviewAcuse,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path:  `${basePath}/admin-cuestionarios`,
        name: 'admin-cuestionarios',
        component: AdministrarCuestionarios,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path:  `${basePath}/modulo-cuestionarios-editar/:cuestionario_id`,
        name: 'modulo-cuestionarios-editar',
        component: DetalleCuestionario,
        props:true,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path:  `${basePath}/preguntas-tema/:tema_id`,
        name: 'preguntas-tema',
        props: true,
        component: PreguntasTema,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path:  `${basePath}/pregunta-form`,
        name: 'pregunta-form',
        component: FormularioPregunta,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
    {
        path:  `${basePath}/cuestionario-form`,
        name: 'cuestionario-form',
        component: FormularioCuestionario,
        meta: {
            middleware: [
                authMiddleware,
                authResponsableMiddleware
            ]
        },
    },
]

export { routesControlInternoResponsable };
