// ----------------------------------------------
//              SECCION DE ENLACE
// ----------------------------------------------
import IndexEnlace              from '@/js/views/dashboard/indexEnlace.vue';
import EnlaceModuloCuestionario from '@/js/views/dashboard/auditoria_control_interno/Enlace/ModuloCuestionario.vue';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';
import authEnlaceMiddleware     from '@/js/middleware/authEnlaceMiddleware';

const routesControlInternoEnlace = [
    {
        path: '/panel/enlace',
        name: 'dashboard_enlace',
        component: IndexEnlace,
        meta: {
            middleware: [
                authMiddleware,
                authEnlaceMiddleware
            ]
        },
    },
    {
        path: '/panel/enlace/auditoria-control-interno/cuestionario',
        name: 'cuestionario-auditoria-control-interno-enlace',
        component: EnlaceModuloCuestionario,
        meta: {
            middleware: [
                authMiddleware,
                authEnlaceMiddleware
            ]
        }
    }
];

export { routesControlInternoEnlace };
