import Index                    from '@/js/views/dashboard/administracion_ti/index';
import Privilegios              from '@/js/views/dashboard/administracion_ti/privilegios';
import PrivilegiosPlataforma    from '@/js/views/dashboard/administracion_ti/privilegiosPlataforma';
import RolesUsuario             from '@/js/views/dashboard/administracion_ti/RolesUsuario';
import Usuarios                 from '@/js/views/dashboard/administracion_ti/Usuarios';
import UsuarioForm              from '@/js/views/dashboard/administracion_ti/UsuarioForm';
import Catalogos                from '@/js/views/dashboard/administracion_ti/Catalogos';
import Inversiones              from '@/js/views/dashboard/administracion_ti/catalogos/Inversiones';
import Adscripciones            from '@/js/views/dashboard/administracion_ti/catalogos/Adscripciones';
import Ambito                   from '@/js/views/dashboard/administracion_ti/catalogos/Ambito';
import Beneficiarios            from '@/js/views/dashboard/administracion_ti/catalogos/Beneficiarios';
import FormasOperacion          from '@/js/views/dashboard/administracion_ti/catalogos/FormasOperacion';
import NivelEmpleo              from '@/js/views/dashboard/administracion_ti/catalogos/NivelEmpleo';
import NivelEstudio             from '@/js/views/dashboard/administracion_ti/catalogos/NivelEstudio';
import Puestos                  from '@/js/views/dashboard/administracion_ti/catalogos/Puestos';
import Sectores                 from '@/js/views/dashboard/administracion_ti/catalogos/SectoresProductivos';
import TipoAdeudo               from '@/js/views/dashboard/administracion_ti/catalogos/TipoAdeudo';
import TipoBienes               from '@/js/views/dashboard/administracion_ti/catalogos/TipoBienes';
import TipoInstituciones        from '@/js/views/dashboard/administracion_ti/catalogos/TipoInstituciones';
import TipoParticipantes        from '@/js/views/dashboard/administracion_ti/catalogos/TipoParticipantes';
import TipoTrabajador           from '@/js/views/dashboard/administracion_ti/catalogos/TipoTrabajador';
import LimpiezaAuditorias       from '@/js/views/dashboard/administracion_ti/LimpiezaAuditorias';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

const basePath = '/panel/sati';

const routesAdministradorTi = [
    {
        path: `${basePath}`,
        name: 'plataforma-admon',
        component: Index,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/privilegios`,
        name: 'administracion-ti-privilegios',
        component: Privilegios,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/privilegios/plataforma/:platform`,
        name: 'administracion-ti-privilegios-plataforma',
        component: PrivilegiosPlataforma,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/privilegios/roles`,
        name: 'administracion-roles-usuario',
        component: RolesUsuario,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/usuarios`,
        name: 'administracion-ti-usuarios',
        component: Usuarios,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/usuario-form/:user_id?`,
        name: 'formulario-usuario',
        component: UsuarioForm,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/administracion-catalogos`,
        name: 'administracion-catalogos',
        component: Catalogos,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-inversiones`,
        name: 'catalogo-inversiones',
        component: Inversiones,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-adscripciones`,
        name: 'catalogo-adscripciones',
        component: Adscripciones,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-ambito`,
        name: 'catalogo-ambito',
        component: Ambito,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-beneficiarios`,
        name: 'catalogo-beneficiarios',
        component: Beneficiarios,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-forma-operacion`,
        name: 'catalogo-forma-operacion',
        component: FormasOperacion,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-nivel-empleo`,
        name: 'catalogo-nivel-empleo',
        component: NivelEmpleo,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-nivel-estudios`,
        name: 'catalogo-nivel-estudios',
        component: NivelEstudio,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-puestos`,
        name: 'catalogo-puestos',
        component: Puestos,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-sectores-productivos`,
        name: 'catalogo-sectores-productivos',
        component: Sectores,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-tipo-adeudos`,
        name: 'catalogo-tipo-adeudos',
        component: TipoAdeudo,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-tipo-bienes`,
        name: 'catalogo-tipo-bienes',
        component: TipoBienes,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-tipo-institucion`,
        name: 'catalogo-tipo-institucion',
        component: TipoInstituciones,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-tipo-participante`,
        name: 'catalogo-tipo-participante',
        component: TipoParticipantes,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },{
        path: `${basePath}/catalogo-tipo-trabajador`,
        name: 'catalogo-tipo-trabajador',
        component: TipoTrabajador,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/limpieza-auditorias`,
        name: 'limpieza-auditorias',
        component: LimpiezaAuditorias,
        meta: {
            middleware: [
                authMiddleware,
            ]
        }
    }
]

export { routesAdministradorTi };
