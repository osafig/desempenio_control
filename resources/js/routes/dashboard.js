import Index from '@/js/views/dashboard/index.vue';
import PlataformaSad from '@/js/views/dashboard/PlataformaSad.vue';
import PlataformaControlInterno from '@/js/views/dashboard/PlataformaControlInterno.vue';
import PlataformaDeclaracion from '@/js/views/dashboard/PlataformaDeclaracion.vue';


// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

// Rutas extendidas de administracion ti
import { routesAdministradorTi } from '@/js/routes/administrador_ti/dashboard';

const basePath = '/panel';

export default [
    {
        path: `${basePath}`,
        name: 'dashboard',
        component: Index,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/sad/`,
        name: 'plataforma-sad',
        component: PlataformaSad,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/ci/`,
        name: 'plataforma-ci',
        component: PlataformaControlInterno,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/sdp/`,
        name: 'plataforma-sdp',
        component: PlataformaDeclaracion,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    ...routesAdministradorTi
];
