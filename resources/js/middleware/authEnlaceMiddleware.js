// Middleware encargado de validar que solo el enlace acceda a sus rutas
export default function auth({next, router}) {
    let rol = localStorage.getItem('user_rol');
    if(rol === null) {
        return next();
    } else {
        if(rol === 'auditor') {
            return router.push({name: 'dashboard_auditor'});
        } else {
            return router.push({name: 'dashboard'});
        }
    }
}
