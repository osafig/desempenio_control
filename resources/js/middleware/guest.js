// Middleware encargado de validar que la sesión exista
export default function auth({next, router}) {
    if (localStorage.getItem('api_token')) {
        if(localStorage.getItem('tipo_usuario') === 'osafig') {
            return router.push({name: 'dashboard'});
        } else {
            return router.push({name: 'dashboard_enlace'});
        }
    }
    return next();
}
