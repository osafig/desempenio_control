<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="sidepat" content="{{ json_encode(config('sidepat')) }}">
        <meta name="ejercicio_seleccionado" content="{{ json_encode(\Illuminate\Support\Facades\DB::connection('main')->table('osaf_ejercicios_cat')->where('Actual', '=', true)->first()) }}">
        <meta name="version" content="{{ json_encode(\Illuminate\Support\Facades\DB::connection('declaracion')->table('oic_update_history')->orderBy('UpdateHistoryID', 'desc')->first('Version')->Version) }}">
        <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
        <title>{{ config('sidepat.nombre_aplicacion_titulo')  }}</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div id="app"></div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
