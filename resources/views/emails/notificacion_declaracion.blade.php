@component('mail::message')
    <center>
        <table style="color: #000000 !important;">
            @if($formato_email_osafig == false)
                <tr>
                    <td style="text-align: center">
                        <img src="{{asset(config('sidepat.logo'))}}" width="200">
                    </td>
                </tr>
            @else
                <tr>
                    <td style="text-align: center">
                        <img src="{{asset('/img/logos/logo3.jpg')}}" style="width: 100%;">
                    </td>
                </tr>
            @endif
            <tr>
                <td style="text-align: center;"><strong></strong></td>
            </tr>
        </table>
        <br>
        <table style="color: #000000 !important;">
            @if($formato_email_osafig == false)
                <tr>
                    <p style="text-align: left; font-weight: bold;">
                        TRABAJADORES, SERVIDORES PÚBLICOS
                    </p>
                    <p style="text-align: left; font-weight: bold;">
                        P R E S E N T E S.-
                    </p>
                    <p style="text-align: justify">
                        Por medio del presente y con fundamento en lo dispuesto por los artículos 2 fracción X, 49 fracción I y 51 de la Ley General del Sistema Nacional Anticorrupción; 9 fracción II, 10 fracción I, 15, 30, 31, 32, 33, 34, 46 y 48 de la Ley General de Responsabilidades Administrativas; Normas Quinta y Vigésimo primera del Anexo Segundo "ACUERDO por el que se modifican los Anexos Primero y Segundo del Acuerdo por el que el Comité Coordinador del Sistema Nacional Anticorrupción emite el formato de declaraciones: de situación patrimonial y de intereses; y expide las normas e instructivo para su llenado y presentación".
                        Se les recuerda la obligación de presentar ante esta Contraloría Interna la Declaración de Situación Patrimonial y de Intereses, de modificación; toda vez que de no hacerlo se harán acreedores a las sanciones señaladas en el artículo 75 fracciones I, II, III, y IV, y artículo 78 fracciones I, II, III, y IV de la Ley General de Responsabilidades Administrativas.
                    </p>
                    <p style="text-align: justify">
                        Podrán ingresar con su correo oficial como usuario y contraseña.
                    </p>
                    <p style="text-align: center; margin-top: 10px; margin-bottom: 10px;">
                        Enlace: <strong><a href="{{env('APP_URL')}}" target="_blank">{{env('APP_URL')}}</a></strong> <br>
                        Correo: <strong>{{ $user->Email }}</strong> <br>
                        Contraseña: <strong>{{ Illuminate\Support\Facades\Crypt::decryptString(($user->PasswordEncrypt)) }}</strong>
                    </p>
                    <p style="text-align: justify">
                        Así mismo, se solicita a quienes estén obligados a presentar la declaración de anual de sueldos y salarios presenten constancia del acuse de la presentación de la declaración del Impuesto Sobre la Renta, correspondiente al ejercicio fiscal
                        de {{ $ejercicio->Year - 1 }}, en caso de encontrarse en los supuestos del artículo 150 de la Ley del Impuesto Sobre la Renta y artículos 31 y 31 Código Fiscal de la Federación, y tener la obligación de presentarla ante el SAT.
                    </p>
                    <p style="text-align: justify">
                        Sin otro particular, reciban un cordial saludo.
                    </p>
                </tr>
            @else
                <tr>
                    <p style="text-align: center; font-weight: bold;">
                        TE INVITA A PRESENTAR
                    </p>
                    <p style="text-align: center; font-weight: bold;">
                        TU DECLARCIÓN <span style="color: #7E042E;">SITUACIÓN</span> <br>
                        PATRIMONIAL <span style="color: #7E042E;">Y DE INTERES</span>
                    </p>
                    <p style="text-align: justify">
                        Como servidores públicos se debe declarar las modificaciones realizadas en su patrimonio, del 1 de enero al 31 de diciembre del 2023, a través de la plataforma SiDePat, su incumplimiento podrá ser sancionado conforme lo marca la Ley General de Responsabilidades administrativas.
                    </p>
                    <p style="font-weight: bold;">
                        Ingresa a declara SiDePat OSAFIG en el síguete liga:
                    </p>
                    <p style="text-align: justify">
                        <a href="https://auditoriaenlinea.osaf.gob.mx" target="_blank">https://auditoriaenlinea.osaf.gob.mx</a>
                        <br>
                        Correo: <strong>{{ $user->Email }}</strong> <br>
                        Contraseña: <strong>{{ Illuminate\Support\Facades\Crypt::decryptString(($user->PasswordEncrypt)) }}</strong>
                    </p>
                    <p style="font-weight: bold">
                        Solo tienes hasta el 31 de mayo de 2024, para presentar tu declaración. <br>
                        Estamos para servirte <br>
                        Dudas o aclaraciones: <a href="mailto:oic@osaf.gob.mx">oic@osaf.gob.mx</a>
                    </p>
                </tr>
            @endif
        </table>
    </center>
@endcomponent
