<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Declaración Patrimonial completa - Sistema de Declaracion Patrimonial</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        footer {
                position: fixed;
                bottom: -30px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                text-align: right;
                line-height: 10px;
            }
    </style>
</head>
<body>
    <div style="text-align: center;">
        <img src="{{ url(config('sidepat.logo2')) }}" width="300" height="120">
    </div>
    <div style="text-align: center; margin-top: 90px; margin-bottom: 2px; font-size: 25px; border: 1px solid; font-weight:bold">
        DECLARACIÓN DE SITUACIÓN PATRIMONIAL<br> VERSIÓN PÚBLICA
    </div>
    <br>
    <div>
        <table style="width: 100%">
            <tr>
                <td style="width: 75%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                            @case(1)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL INICIAL
                            @break
                            @case(2)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL MODIFICACIÓN
                            @break
                            @case(3)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL CONCLUSIÓN
                            @break
                            @case(4)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO INICIAL
                            @break
                            @case(5)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO MODIFICACIÓN
                            @break
                            @case(6)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO CONCLUSIÓN
                            @break
                        @endswitch
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->DeclaracionFecha,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 75%; text-align: center;">Tipo de declaracion</td>
                <td style="width: 25%; text-align: center;">Fecha de presentación</td>
            </tr>
        </table>
    </div>
    <br>
    <div style="text-align: center;">
        @if($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->NombreCorto == 'MODIFICACIÓN')
            <div>
                Bajo protesta de decir verdad, presento a usted mi declaración de situación patrimonial del periodo {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->AnioDeclarado,'UTF-8') }}, conforme a lo dispuesto
                en la Ley General de Responsabilidades Administrativas, la Ley General del Sistema Nacional Anticorrupción  y la normatividad
                aplicable.
            </div>
        @else
            <div>
                Bajo protesta de decir verdad, presento a usted mi declaración de situación patrimonial a la fecha, conforme a lo dispuesto
                en la Ley General de Responsabilidades Administrativas, la Ley General del Sistema Nacional Anticorrupción  y la normatividad
                aplicable.
            </div>
        @endif
    </div>

    <!-- INICIAN DATOS GENERALES -->
    <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS GENERALES</span></h2>
    <table style="width: 100%">
        <tr>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->PrimerApellido, 'UTF-8') }}
                </div>
            </td>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->SegundoApellido,'UTF-8') }}
                </div>
            </td>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->Nombres,'UTF-8') }}
                </div>
            </td>
        </tr>
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 33%; text-align: center;"> Primer apellido</td>
            <td style="width: 33%; text-align: center;"> Segundo apellido</td>
            <td style="width: 33%; text-align: center;"> Nombre(s) </td>
        </tr>
    </table>
    <br>
    <table class="row-table">
        <tr>
            <td style="width: 50%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ $secciones_pdf["DATOS GENERALES"]->InfoUsuario->Email }}
                </div>
            </td>
        </tr>
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 50%; text-align: center;"> Correo electrónico Institucional</td>
        </tr>
    </table>

    <!-- DATOS CURRICULARES DEL DECLARANTE -->
    @if (in_array('DATOS CURRICULARES DEL DECLARANTE',$indice) == true)
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS CURRICULARES DEL DECLARANTE</span></h2>
        <table style="width: 100%">
            <tr>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Nombre,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Institucion,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Carrera,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 33%; text-align: center;">Nivel</td>
                <td style="width: 33%; text-align: center;">Institución</td>
                <td style="width: 33%; text-align: center;">Carrera o área de conocimiento</td>
            </tr>
        </table>
        <br>
        <table style="width: 100%">
            <tr>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Status,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->DocumentoObtenido,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->FechaDocumento,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        @if ($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->EsExtranjero == 0)
                            EN MÉXICO
                        @else
                            EN EL EXTRANJERO
                        @endif
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 25%; text-align: center;">Estatus</td>
                <td style="width: 25%; text-align: center;">Documento obtenido</td>
                <td style="width: 25%; text-align: center;">Fecha Obtención del documento</td>
                <td style="width: 25%; text-align: center;">Lugar donde se ubica la institución educativa</td>
            </tr>
        </table>
        <div style="page-break-after:always;"></div>
    @endif
    <!-- DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA -->
    @if (in_array('DATOS DEL EMPLEO, CARGO O COMISIÓN',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(1)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA</span></h2>
            @break
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN ACTUAL</span></h2>
            @break
            @case(3)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</span></h2>
            @break
            @case(4)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA</span></h2>
            @break
            @case(5)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN ACTUAL</span></h2>
            @break
            @case(6)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</span></h2>
            @break
        @endswitch

        <table style="width: 100%">
            <tr>
                <td style="width: 50%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NombreOrden,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 50%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NombreAmbito,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 50%; text-align: center;">Nivel/ Orden de gobierno</td>
                <td style="width: 50%; text-align: center;">Ámbito público</td>
            </tr>
        </table>
        <br>
        <table style="width: 100%">
            <tr>
                <td style="width: 100%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Dependencia,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 100%; text-align: center;">Nombre del ente público</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="width: 50%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Nombre,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 50%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Puesto,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: center;">Área de adscripción</td>
                <td style="width: 50%; text-align: center;">Empleo, cargo o comisión</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Honorarios == 1)
                            SÍ
                        @else
                            NO
                        @endif
                    </div>
                </td>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Nivel,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 33%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->FechaIngreso,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 33%; text-align: center;">¿Está contratado por honorarios</td>
                <td style="width: 33%; text-align: center;">Nivel del empleo, cargo o comisión</td>
                <td style="width: 33%; text-align: center;">Fecha de toma de posesión del empleo, cargo o comisión</td>
            </tr>
        </table>
        <br>
        <table style="width: 100%">
            <tr>
                <td style="width: 75%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->FuncionPrincipal,'UTF-8') }}
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Telefono == null)
                            N/A
                        @else
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Telefono,'UTF-8') }}
                        @endif
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 75%; text-align: center;">Especifique función principal</td>
                <td style="width: 25%; text-align: center;">Teléfono de oficina y extensión</td>
            </tr>
        </table>
        <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DOMICILIO DEL EMPLEO, CARGO O COMISIÓN</span></h3>
        @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->EsExtranjero == 0)
            <!-- cargo en mexico -->
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Calle,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroExterior,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior == null)
                                N/A
                            @else
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior,'UTF-8') }}
                            @endif
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Calle</td>
                    <td style="width: 33%; text-align: center;">Número exterior</td>
                    <td style="width: 33%; text-align: center;">Número interior</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Colonia,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Municipio,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Estado,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Colonia/Localidad</td>
                    <td style="width: 33%; text-align: center;">Municipio/Alcaldía</td>
                    <td style="width: 33%; text-align: center;">Entidad Federativa</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->CodigoPostal,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Código postal</td>
                </tr>
            </table>
            <br>
        @else
            <!-- cargo en el extranjero -->
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Calle,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroExterior,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior == null)
                                N/A
                            @else
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior,'UTF-8') }}
                            @endif
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Calle</td>
                    <td style="width: 33%; text-align: center;">Número exterior</td>
                    <td style="width: 33%; text-align: center;">Número interior</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Ciudad,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->EstadoExtranjero,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Ciudad/Localidad</td>
                    <td style="width: 33%; text-align: center;">Estado/Provincia</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Pais,'UTF-8')}}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->CodigoPostal,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">País</td>
                    <td style="width: 33%; text-align: center;">Código postal</td>
                </tr>
            </table>
        @endif

        @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo != [])
            <br>
            <br>
            <div style="text-align: center;">
                El declarante manifestó que cuenta con otro empleo, cargo o comisión distinto al declarado.
            </div>
            <br>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NombreOrden,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 50%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NombreAmbito,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 50%; text-align: center;">Nivel/ Orden de gobierno</td>
                    <td style="width: 50%; text-align: center;">Ámbito público</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Dependencia,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 100%; text-align: center;">Nombre del ente público</td>
                </tr>
            </table>
            <table style="width:100%">
                <tr>
                    <td style="width: 50%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->AreaEspecifica,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 50%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->PuestoEspecifico,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: center;">Área de adscripción</td>
                    <td style="width: 50%; text-align: center;">Empleo, cargo o comisión</td>
                </tr>
            </table>
            <table style="width:100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Honorarios == 1)
                                SÍ
                            @else
                                NO
                            @endif
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Nivel,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->FechaIngreso,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; text-align: center;">¿Está contratado por honorarios</td>
                    <td style="width: 33%; text-align: center;">Nivel del empleo, cargo o comisión</td>
                    <td style="width: 33%; text-align: center;">Fecha de toma de posesión del empleo, cargo o comisión</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 75%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->FuncionPrincipal,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 25%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono }}
                            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono == null)
                                N/A
                            @else
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono,'UTF-8') }}
                            @endif
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 75%; text-align: center;">Especifique función principal</td>
                    <td style="width: 25%; text-align: center;">Teléfono de oficina y extención</td>
                </tr>
            </table>
            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DOMICILIO DEL EMPLEO, CARGO O COMISIÓN</span></h3>
            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->EsExtranjero == 0)
                <!-- cargo en mexico -->
                <table style="width: 100%">
                    <tr>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Calle,'UTF-8') }}
                            </div>
                        </td>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroExterior,'UTF-8') }}
                            </div>
                        </td>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior == null)
                                    N/A
                                @else
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior,'UTF-8') }}
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr style="font-size: 10px; font-style: italic;">
                        <td style="width: 33%; text-align: center;">Calle</td>
                        <td style="width: 33%; text-align: center;">Número exterior</td>
                        <td style="width: 33%; text-align: center;">Número interior</td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Colonia,'UTF-8') }}
                            </div>
                        </td>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Municipio,'UTF-8') }}
                            </div>
                        </td>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Estado,'UTF-8') }}
                            </div>
                        </td>
                    </tr>
                    <tr style="font-size: 10px; font-style: italic;">
                        <td style="width: 33%; text-align: center;">Colonia/Localidad</td>
                        <td style="width: 33%; text-align: center;">Municipio/Alcaldía</td>
                        <td style="width: 33%; text-align: center;">Entidad Federativa</td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 33%; text-align: center;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->CodigoPostal,'UTF-8') }}
                            </div>
                        </td>
                    </tr>
                    <tr style="font-size: 10px; font-style: italic;">
                        <td style="width: 33%; text-align: center;">Código postal</td>
                    </tr>
                </table>
                <br>
            @else
            <!-- cargo en el extranjero -->
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Calle,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroExterior,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior == null)
                                N/A
                            @else
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior,'UTF-8') }}
                            @endif
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Calle</td>
                    <td style="width: 33%; text-align: center;">Número exterior</td>
                    <td style="width: 33%; text-align: center;">Número interior</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Ciudad,'UTF-8') }}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->EstadoExtranjero,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">Ciudad/Localidad</td>
                    <td style="width: 33%; text-align: center;">Estado/Provincia</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Pais,'UTF-8')}}
                        </div>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->CodigoPostal,'UTF-8') }}
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 10px; font-style: italic;">
                    <td style="width: 33%; text-align: center;">País</td>
                    <td style="width: 33%; text-align: center;">Código postal</td>
                </tr>
            </table>
            <br>
            @endif
            <br>
        @else
            <br>
            <br>
            <div style="text-align: center;">
                El declarante manifestó que no cuenta con otro empleo, cargo o comisión distinto al declarado
            </div>
        @endif
        <div style="page-break-after:always;"></div>
    @endif
    <!-- EXPERIENCIA LABORAL (ÙLTIMOS CINCO EMPLEOS) -->
    @if(in_array('EXPERIENCIA LABORAL',$indice) == true)
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">EXPERIENCIA LABORAL (ÙLTIMOS CINCO EMPLEOS)</span></h2>
        @if($secciones_pdf["EXPERIENCIA LABORAL"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ EXPERIENCIA LABORAL PREVIA EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["EXPERIENCIA LABORAL"]->Info as $experiencia)
                @if($experiencia->AmbitoSector == 'Público')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->AmbitoSector,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->NombreOrden,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->NombreAmbito,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Ámbito/Sector en el que laboraste</td>
                                <td style="width: 33%; text-align: center;">Nivel/Orden de gobierno</td>
                                <td style="width: 33%; text-align: center;">Ámbito público</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Institucion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Area,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Nombre del ente público</td>
                                <td style="width: 33%; text-align: center;">Área de adscripción</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 75%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Puesto,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 25%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($experiencia->EnExtranjero == 0)
                                            EN MÉXICO
                                        @else
                                            EN EL EXTRANJERO
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 75%; text-align: center;">Empleo, cargo o comisión</td>
                                <td style="width: 25%; text-align: center;">Lugar donde se ubica</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->FuncionPrincipal,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 100%; text-align: center;">Función principal</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ $experiencia->FechaIngreso }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ $experiencia->FechaTermino }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Fecha de ingreso</td>
                                <td style="width: 33%; text-align: center;">Fecha de termino</td>
                            </tr>
                        </table>
                    </div>
                @elseif($experiencia->AmbitoSector == 'Privado')
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->AmbitoSector,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Institucion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->RFCInstitucion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Ámbito/Sector en el que laboraste</td>
                                <td style="width: 33%; text-align: center;">Nombre de la empresa, sociedad o asociación</td>
                                <td style="width: 33%; text-align: center;">RFC</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Area,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Puesto,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->Sector,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 33%; text-align: center;">Área</td>
                            <td style="width: 33%; text-align: center;">Puesto</td>
                                <td style="width: 33%; text-align: center;">Sector al que pertenece</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($experiencia->EnExtranjero == 0)
                                            EN MÉXICO
                                        @else
                                            EN EL EXTRANJERO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->FechaIngreso,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($experiencia->FechaTermino,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Lugar donde se ubica</td>
                                <td style="width: 33%; text-align: center;">Fecha de ingreso</td>
                                <td style="width: 33%; text-align: center;">Fecha de termino</td>
                            </tr>
                        </table>
                    </div>
                @else
                    <!-- AQUÍ VA TODO LO DE EL REGRISTO DE EXPERIENCIA EN UN AMBITO SOCIAL QUE NO ES PÚBLICO O PRIVADO -->
                    otro
                @endif
                <br>
            @endforeach
        @endif
    <div style="page-break-after:always;"></div>
    @endif

    <!-- INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS -->
    <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS</span></h2>
    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
        @case(2)
            <div style="text-align: left;">Ingresos netos del declarante y/o dependientes económicos (Entre el 1 de enero y 31 diciembre del año inmediato anterior)</div>
        @break
        @case(5)
            <div style="text-align: left;">Ingresos netos del declarante y/o dependientes económicos (Entre el 1 de enero y 31 diciembre del año inmediato anterior)</div>
        @break
        @default
            <div style="text-align: left;">Ingresos netos del declarante y/o dependientes económicos (Situación actual)</div>
        @break
    @endswitch
    <br>
    <div style="text-align: left;">
        <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; ">
            <tr>
                <td style="width: 75%; text-align: left; font-weight:bold;">
                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                        @case(1)
                            I. Remuneración mensual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                            prestaciones) (Cantidades netas después de impuestos)
                        @break
                        @case(2)
                            I. Remuneración anual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                            prestaciones) (Cantidades netas después de impuestos)
                        @break
                        @case(3)
                            I. Remuneración neta del año en curso a la fecha de conclusión del empleo, cargo o comisión del declarante por su cargo público (por concepto
                            de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras prestaciones) (Cantidades netas después de impuestos)
                        @break
                        @case(4)
                            I. Remuneración mensual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                            prestaciones) (Cantidades netas después de impuestos)
                        @break
                        @case(5)
                            I. Remuneración anual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                            prestaciones) (Cantidades netas después de impuestos)
                        @break
                        @case(6)
                            I. Remuneración neta del año en curso a la fecha de conclusión del empleo, cargo o comisión del declarante por su cargo público (por concepto
                            de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras prestaciones) (Cantidades netas después de impuestos)
                        @break
                    @endswitch
                </td>
                <td style="width: 25%; text-align: center; font-weight:bold;">
                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta}}
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; ">
            <tr>
                <td style="width: 75%; text-align: left; font-weight:bold;" >
                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                        @case(1)
                            II. Otros ingresos mensuales del declarante (Suma del II.1 al II.5)
                        @break
                        @case(4)
                            II. Otros ingresos mensuales del declarante (Suma del II.1 al II.5)
                        @break
                        @default
                            II. Otros ingresos del declarante (Suma del II.1 al II.5)
                        @break
                    @endswitch
                </td>
                <td style="width: 25%; text-align: center; font-weight:bold;">
                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos}}
                </td>
            </tr>
        </table>
        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
            <tr>
                <td style="width: 75%; text-align: left;">
                    II.1 Por actividad industrial, comercial y/o empresarial
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        <thead>
                            <td style="width: 40%; text-align: left;">Nombre o razón social</td>
                            <td style="width: 40%; text-align: left;">Tipo de negocio</td>
                            <td style="width: 20%; text-align: left;">Monto</td>
                        </thead>
                    </table>
                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_industriales'] == [])
                            <tr>
                                <td style="text-align: center;">
                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                </td>
                            </tr>
                        @else
                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_industriales'] as $actividad_industrial)
                                    <tr>
                                        <td style="width: 40%; text-align: left;">
                                            {{ mb_strtoupper($actividad_industrial->NombreRazonSocial,'UTF-8')}}
                                        </td>
                                        <td style="width: 40%; text-align: left;">
                                            {{ mb_strtoupper($actividad_industrial->TipoNegocio,'UTF-8')}}
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            {{$actividad_industrial->Monto}}
                                        </td>
                                    </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
            <tr>
                <td style="width: 75%; text-align: left;">
                    II.2 Por actividad financiera (rendimientos o ganancias) (después de impuestos)
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        <thead>
                            <td style="width: 80%; text-align: left;">Tipo de instrumento que generó el rendimiento o ganancia</td>
                            <td style="width: 20%; text-align: left;">Monto</td>
                        </thead>
                    </table>
                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_financieras'] == [])
                            <tr>
                                <td style="text-align: center;">
                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                </td>
                            </tr>
                        @else
                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_financieras'] as $actividad_financiera)
                                    <tr>
                                        <td style="width: 80%; text-align: left;">
                                            @if($actividad_financiera->IntrumentoID == 99)
                                                {{ mb_strtoupper($actividad_financiera->OtroInstrumento,'UTF-8')}}
                                            @else
                                                {{ mb_strtoupper($actividad_financiera->TipoInstrumento,'UTF-8')}}
                                            @endif
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            {{$actividad_financiera->Monto}}
                                        </td>
                                    </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
            <tr>
                <td style="width: 75%; text-align: left;">
                    II.3 Por servicios profesionales, consejos, consultorías, y/o asesorías (Después de impuestos)
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        <thead>
                            <td style="width: 80%; text-align: left;">Tipo de servicio prestado</td>
                            <td style="width: 20%; text-align: left;">Monto</td>
                        </thead>
                    </table>
                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['servicios_profesionales'] == [])
                            <tr>
                                <td style="text-align: center;">
                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                </td>
                            </tr>
                        @else
                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['servicios_profesionales'] as $servicio_profesional)
                                    <tr>
                                        <td style="width: 80%; text-align: left;">
                                            {{ mb_strtoupper($servicio_profesional->TipoServicioPrestado,'UTF-8')}}
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            {{$servicio_profesional->Monto}}
                                        </td>
                                    </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
            <tr>
                <td style="width: 75%; text-align: left;">
                    II.4 Por enajenacion de bienes (Después de impuestos)
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        <thead>
                            <td style="width: 80%; text-align: left;">Tipo de bien enajenado</td>
                            <td style="width: 20%; text-align: left;">Monto</td>
                        </thead>
                    </table>
                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['bienes_enajenados'] == [])
                            <tr>
                                <td style="text-align: center;">
                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                </td>
                            </tr>
                        @else
                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['bienes_enajenados'] as $otro_ingreso)
                                    <tr>
                                        <td style="width: 80%; text-align: left;">
                                            {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            {{$otro_ingreso->Monto}}
                                        </td>
                                    </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
            <tr>
                <td style="width: 75%; text-align: left;">
                    II.5 Otros ingresos no considerados anteriormente (Después de impuestos)
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        <thead>
                            <td style="width: 80%; text-align: left;">Especificar tipo de ingreso (Arrendamiento, regalía, sorteos, concursos, donaciones, seguro de vida, etc.)</td>
                            <td style="width: 20%; text-align: left;">Monto</td>
                        </thead>
                    </table>
                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['otros_ingresos'] == [])
                            <tr>
                                <td style="text-align: center;">
                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                </td>
                            </tr>
                        @else
                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['otros_ingresos'] as $otro_ingreso)
                                    <tr>
                                        <td style="width: 80%; text-align: left;">
                                            {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                        </td>
                                        <td style="width: 20%; text-align: left;">
                                            {{$otro_ingreso->Monto}}
                                        </td>
                                    </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
            <tr>
                <td style="width: 75%; text-align: left; font-weight:bold;" >
                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                        @case(1)
                            A.- Ingreso mensual neto del declarante (Suma del numeral I y II)
                        @break
                        @case(2)
                            A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                        @break
                        @case(3)
                            A.- Ingresos del declarante del año en curso a la fecha de conclusión del empleo, cargo o comisión (Suma del numeral I y II)
                        @break
                        @case(4)
                            A.- Ingreso mensual neto del declarante (Suma del numeral I y II)
                        @break
                        @case(5)
                            A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                        @break
                        @case(6)
                            A.- Ingresos del declarante del año en curso a la fecha de conclusión del empleo, cargo o comisión (Suma del numeral I y II)
                        @break
                    @endswitch
                </td>
                <td style="width: 25%; text-align: center; font-weight:bold;">
                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta +
                        $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos
                    }}
                </td>
            </tr>
        </table>
        <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
            <tr>
                <td style="width: 75%; text-align: left; font-weight:bold;" >
                @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                    @case(1)
                        C.- Total de ingresos mensuales netos percibidos por el declarante, pareja y/o dependientes económicos (suma de los apartados A y B)
                    @break
                    @case(2)
                        C.- Total de ingresos anuales netos percibidos por el declarante, pareja y/o dependientes económicos (suma de los apartados A y B)
                    @break
                    @case(3)
                        C.- Total de ingresos del año en curso a la fecha de conclusión del empleo, cargo o comisión percibidos por el declarante, pareja y/o dependientes
                        económicos (suma de los apartados A y B)
                    @break
                    @case(4)
                        C.- Total de ingresos mensuales netos percibidos por el declarante, pareja y/o dependientes económicos (suma de los apartados A y B)
                    @break
                    @case(5)
                        C.- Total de ingresos anuales netos percibidos por el declarante, pareja y/o dependientes económicos (suma de los apartados A y B)
                    @break
                    @case(6)
                        C.- Total de ingresos del año en curso a la fecha de conclusión del empleo, cargo o comisión percibidos por el declarante, pareja y/o dependientes
                        económicos (suma de los apartados A y B)
                    @break
                @endswitch
                </td>
                <td style="width: 25%; text-align: center; font-weight:bold;">
                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta +
                        $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos +
                        $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->IngNetoParejaDependientes
                    }}
                </td>
            </tr>
        </table>
    </div>
    @if (in_array('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true)
        <!-- DESEMPEÑO COMO SERVIDOR EL AÑO PASADO -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DESEMPEÑO COMO SERVIDOR EL AÑO PASADO</span></h2>
        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info == 'No aplica')
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO SE DESEMPEÑÓ COMO SERVIDOR PÚBLICO EL AÑO PASADO
            </div>
            <br>
            <br>
        @else
            <div style="text-align: left;">
                Ingresos netos, recibidos durante el tiempo en el que se desempeñó como servidor público el año inmediato anterior
            </div>
            <br>
            <div style="text-align: left;">
                <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; ">
                    <tr>
                        <td style="width: 50%; text-align: left; font-weight:bold;">
                            Fecha de inicio: {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->FechaInicio}}
                        </td>
                        <td style="width: 50%; text-align: left; font-weight:bold;">
                            Fecha de conclusión: {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->FechaConclusion}}
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; ">
                    <tr>
                        <td style="width: 75%; text-align: left; font-weight:bold;">
                            I. Remuneración neta del declarante, recibida durante el tiempo en el que se desempeñó como servidor público en el año inmediato anterior (por concepto de sueldos, honorarios, compensaciones, bonos, aqguinaldos y otras
                            prestaciones) (Cantidades netas después de impuestos)
                        </td>
                        <td style="width: 25%; text-align: center; font-weight:bold;">
                            {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta }}
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; ">
                    <tr>
                        <td style="width: 75%; text-align: left; font-weight:bold;" >
                            II. Otros ingresos del declarante, recibidos durante el tiempo en el que se desempeñó como servidor público en el año inmediato amnterior (Suma del II.1 al II.5)
                        </td>
                        <td style="width: 25%; text-align: center; font-weight:bold;">
                            {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos}}
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                    <tr>
                        <td style="width: 75%; text-align: left;">
                            II.1 Por actividad industrial, comercial y/o empresarial
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                <thead>
                                    <td style="width: 40%; text-align: left;">Nombre o razón social</td>
                                    <td style="width: 40%; text-align: left;">Tipo de negocio</td>
                                    <td style="width: 20%; text-align: left;">Monto</td>
                                </thead>
                            </table>
                            <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_industriales'] == [])
                                    <tr>
                                        <td style="text-align: center;">
                                            NO DECLARÓ NINGUN INGRESO DE ESTE TIPO
                                        </td>
                                    </tr>
                                @else
                                    @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_industriales'] as $actividad_industrial)
                                            <tr>
                                                <td style="width: 40%; text-align: left;">
                                                    {{ mb_strtoupper($actividad_industrial->NombreRazonSocial,'UTF-8')}}
                                                </td>
                                                <td style="width: 40%; text-align: left;">
                                                    {{ mb_strtoupper($actividad_industrial->TipoNegocio,'UTF-8')}}
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    {{$actividad_industrial->Monto}}
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
                    <tr>
                        <td style="width: 75%; text-align: left;">
                            II.2 Por actividad financiera (rendimientos o ganancias) (después de impuestos)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                <thead>
                                    <td style="width: 80%; text-align: left;">Tipo de instrumento que generó el rendimiento o ganancia</td>
                                    <td style="width: 20%; text-align: left;">Monto</td>
                                </thead>
                            </table>
                            <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_financieras'] == [])
                                    <tr>
                                        <td style="text-align: center;">
                                            NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                        </td>
                                    </tr>
                                @else
                                    @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_financieras'] as $actividad_financiera)
                                            <tr>
                                                <td style="width: 80%; text-align: left;">
                                                    @if($actividad_financiera->IntrumentoID == 99)
                                                        {{ mb_strtoupper($actividad_financiera->OtroInstrumento,'UTF-8')}}
                                                    @else
                                                        {{ mb_strtoupper($actividad_financiera->TipoInstrumento,'UTF-8')}}
                                                    @endif
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    {{$actividad_financiera->Monto}}
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
                    <tr>
                        <td style="width: 75%; text-align: left;">
                            II.3 Por servicios profesionales, consejos, consultorías, y/o asesorías (Después de impuestos)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                <thead>
                                    <td style="width: 80%; text-align: left;">Tipo de servicio prestado</td>
                                    <td style="width: 20%; text-align: left;">Monto</td>
                                </thead>
                            </table>
                            <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['servicios_profesionales'] == [])
                                    <tr>
                                        <td style="text-align: center;">
                                            NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                        </td>
                                    </tr>
                                @else
                                    @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['servicios_profesionales'] as $servicio_profesional)
                                            <tr>
                                                <td style="width: 80%; text-align: left;">
                                                    {{ mb_strtoupper($servicio_profesional->TipoServicioPrestado,'UTF-8')}}
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    {{$servicio_profesional->Monto}}
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
                    <tr>
                        <td style="width: 75%; text-align: left;">
                            II.4 Por enajenacion de bienes (Después de impuestos)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                <thead>
                                    <td style="width: 80%; text-align: left;">Tipo de bien enajenado</td>
                                    <td style="width: 20%; text-align: left;">Monto</td>
                                </thead>
                            </table>
                            <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['bienes_enajenados'] == [])
                                    <tr>
                                        <td style="text-align: center;">
                                            NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                        </td>
                                    </tr>
                                @else
                                    @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['bienes_enajenados'] as $otro_ingreso)
                                            <tr>
                                                <td style="width: 80%; text-align: left;">
                                                    {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    {{$otro_ingreso->Monto}}
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 20px;">
                    <tr>
                        <td style="width: 75%; text-align: left;">
                            II.5 Otros ingresos no considerados anteriormente (Después de impuestos)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                <thead>
                                    <td style="width: 80%; text-align: left;">Especificar tipo de ingreso (Arrendamiento, regalía, sorteos, concursos, donaciones, seguro de vida, etc.)</td>
                                    <td style="width: 20%; text-align: left;">Monto</td>
                                </thead>
                            </table>
                            <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['otros_ingresos'] == [])
                                    <tr>
                                        <td style="text-align: center;">
                                            NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                        </td>
                                    </tr>
                                @else
                                    @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['otros_ingresos'] as $otro_ingreso)
                                            <tr>
                                                <td style="width: 80%; text-align: left;">
                                                    {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    {{$otro_ingreso->Monto}}
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    <tr>
                        <td style="width: 75%; text-align: left; font-weight:bold;" >
                            A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                        </td>
                        <td style="width: 25%; text-align: center; font-weight:bold;">
                            {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta +
                                $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos
                            }}
                        </td>
                    </tr>
                </table>
                <table class="row-table" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    <tr>
                        <td style="width: 75%; text-align: left; font-weight:bold;" >
                            C.- Total de ingresos anuales netos percibidos por el declarante, pareja y/o dependients económicos (suma de los apartados A y B)
                        </td>
                        <td style="width: 25%; text-align: center; font-weight:bold;">
                            {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta +
                                $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos +
                                $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->IngNetoParejaDependientes
                            }}
                        </td>
                    </tr>
                </table>
            </div>
        @endif

    @endif
    <!-- BIENES INMUEBLES -->
    @if (in_array('BIENES INMUEBLES',$indice) == true)
        <div style="page-break-after:always;"></div>
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(1)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES INMUEBLES (SITUACIÓN ACTUAL)</span></h2>
            @break
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES INMUEBLES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @case(3)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES INMUEBLES (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        <div style="text-align: center;">
            BIENES DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS
        </div>
        @if($secciones_pdf["BIENES INMUEBLES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ BIEN INMUEBLE ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach($secciones_pdf["BIENES INMUEBLES"]->Info as $bien_inmueble)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    @if($bien_inmueble->Titular == 'DECLARANTE')
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_inmueble->TipoInmueble,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_inmueble->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo inmueble</td>
                                <td style="width: 50%; text-align: center;">Titular inmueble</td>
                            </tr>
                        </table>
                        <br>
                    @else
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_inmueble->TipoInmueble,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 100%; text-align: center;">Tipo inmueble</td>
                            </tr>
                        </table>
                        <br>
                    @endif
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($bien_inmueble->PorcentajePropiedad == null)
                                        EL TITULAR TIENE LA TOTALIDAD DE LA PROPIEDAD
                                    @else
                                        {{ mb_strtoupper($bien_inmueble->PorcentajePropiedad,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->SuperficieTerreno,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Porcentaje de propiedad del declarante conforme a escrituración o contrato</td>
                            <td style="width: 50%; text-align: center;">Superficie del terreno (en metros cuadrados)</td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->SuperficieConstruida,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->FormaAdquisicion,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Superficie de construcción (en metros cuadrados)</td>
                            <td style="width: 50%; text-align: center;">Forma de adquisición</td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->FormaPago,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->Valor,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Forma de pago</td>
                            <td style="width: 50%; text-align: center;">Valor de adquisición</td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->TipoMoneda,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->ValorConformeA,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Tipo de moneda</td>
                            <td style="width: 50%; text-align: center;">El valor de adquisición del inmueble es conforme a</td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 100%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_inmueble->FechaOperacion,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 100%; text-align: center;">Fecha de adquisición</td>
                        </tr>
                    </table>
                    <br>
                    @if($bien_inmueble->MotivoBaja != null && $bien_inmueble->MotivoBaja != ' ')
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_inmueble->MotivoBaja,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Motivo de baja del inmueble</td>
                            </tr>
                        </table>
                    @endif
                    @if($bien_inmueble->NombreTercero != null and $bien_inmueble->Titular != 'DECLARANTE')
                        @if($bien_inmueble->TipoPersonaTercero != 1)
                            <br>
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->NombreTercero,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->RFCTercero,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        Tipo persona legal del tercero
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        Nombre del tercero
                                    </td><td style="width: 33%; text-align: center;">
                                        RFC del tercero
                                    </td>
                                </tr>
                            </table>
                        @endif
                    @endif
                    @if($bien_inmueble->NombreTransmisor != null && $bien_inmueble->TipoPersonaTransmisor == 1)
                        <br>
                        <div style="border: 1px solid;">
                            <table class="row-table" style=" width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DE LA PROPIEDAD</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->NombreTransmisor,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->RFCTransmisor,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        Tipo persona legal del transmisor
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        Nombre del transmisor de la propiedad
                                    </td><td style="width: 33%; text-align: center;">
                                        RFC del transmisor de propiedad
                                    </td>
                                </tr>
                            </table>
                            @php
                                $relaciones_no_mostrables = array(
                                    "Abuelo (a)",
                                    "Bisabuelo (a)",
                                    "Bisnieto (a)",
                                    "Concubino (a) / Concubinario / Unión libre",
                                    "Concuño (a)",
                                    "Cuñado (a)",
                                    "Cónyuge",
                                    "Hermano (a)",
                                    "Hijo (a)",
                                    "Madre",
                                    "Padre",
                                    "Primo (a)",
                                    "Sobrino (a)",
                                    "Suegro (a)",
                                    "Tatarabuelo (a)",
                                    "Tataranieto (a)",
                                    "Tío (a)"
                                );

                            @endphp
                            @if(in_array($bien_inmueble->RelacionTransmisor,$relaciones_no_mostrables) == false)
                                <table class="row-table" style=" width: 100%;">
                                    <tr>
                                        <td style="width: 100%; text-align: center;">
                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->RelacionTransmisor,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: center;">
                                            Tipo de relación con el titular
                                        </td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                    @endif
                </div>
            @endforeach
            <br>
            <br>
        @endif
    @endif

    <!-- VEHÍCULOS -->
    @if (in_array('VEHÍCULOS',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(1)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">VEHÍCULOS (SITUACIÓN ACTUAL)</span></h2>
            @break
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">VEHÍCULOS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @case(3)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">VEHÍCULOS (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        <div style="text-align: center;">
            VEHÍCULOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS
        </div>
        @if($secciones_pdf["VEHÍCULOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ VEHÍCULO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach($secciones_pdf["VEHÍCULOS"]->Info as $vehiculo)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    <table style="width: 100%">
                        @if($vehiculo->Titular != 'DECLARANTE')
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->TipoVehiculo,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo Vehículo</td>
                                <td style="width: 50%; text-align: center;">Titular</td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->TipoVehiculo,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo Vehículo</td>
                            </tr>
                        @endif
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 25%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->VehiculoMarca,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 25%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->VehiculoModelo,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 25%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->VehiculoAnio,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 25%; text-align: center;">Marca</td>
                            <td style="width: 25%; text-align: center;">Modelo</td>
                            <td style="width: 25%; text-align: center;">Año</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 33%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->FormaAdquisicion,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 33%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->FormaPago,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 33%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->Valor,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 33%; text-align: center;">Forma de adquisición</td>
                            <td style="width: 33%; text-align: center;">Forma de pago</td>
                            <td style="width: 33%; text-align: center;">Valor de adquisición</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 33%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->TipoMoneda,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 33%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($vehiculo->FechaOperacion,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 33%; text-align: center;">Tipo de moneda</td>
                            <td style="width: 33%; text-align: center;">Fecha de adquisición</td>
                        </tr>
                    </table>
                    @if($vehiculo->NombreRazonSocial != null && $vehiculo->TipoPersonaTransmisor != 0)
                        <br>
                        <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DEL VEHÍCULO</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->NombreRazonSocial,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->RFCTRansmisor,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal del transmisor
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre del transmisor de la propiedad
                                </td><td style="width: 33%; text-align: center;">
                                    RFC del transmisor de propiedad
                                </td>
                            </tr>
                        </table>

                        @php
                            $relaciones_no_mostrables = array(
                                "Abuelo (a)",
                                "Bisabuelo (a)",
                                "Bisnieto (a)",
                                "Concubino (a) / Concubinario / Unión libre",
                                "Concuño (a)",
                                "Cuñado (a)",
                                "Cónyuge",
                                "Hermano (a)",
                                "Hijo (a)",
                                "Madre",
                                "Padre",
                                "Primo (a)",
                                "Sobrino (a)",
                                "Suegro (a)",
                                "Tatarabuelo (a)",
                                "Tataranieto (a)",
                                "Tío (a)"
                            );
                        @endphp
                        @if(in_array($vehiculo->RelacionTransmisor,$relaciones_no_mostrables) == false)
                            <table class="row-table" style="border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid; width: 100%;">
                                <tr>
                                    <td style="width: 100%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($vehiculo->RelacionTransmisor,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; text-align: center;">
                                        Tipo de relación con el titular
                                    </td>
                                </tr>
                            </table>
                        @endif

                    @endif
                    @if($vehiculo->NombreTercero != null && $vehiculo->TipoPersonaTercero != 0)
                        <br>
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->NombreTercero,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($vehiculo->RFCTercero,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal del tercero
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre del tercero
                                </td><td style="width: 33%; text-align: center;">
                                    RFC del tercero
                                </td>
                            </tr>
                        </table>
                        <br>
                    @endif
                    @if($vehiculo->MotivoBaja != null)
                        <table class="row-table" style="border-bottom: 1px solid;">
                            <tr>
                                <td style="width: 100%; text-align: left; font-weight:bold;" >
                                    Motivo de baja
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; text-align: center;">
                                    {{ mb_strtoupper($vehiculo->MotivoBaja,'UTF-8') }}
                                </td>
                            </tr>
                        </table>
                    @endif
                </div>
            @endforeach
            <br>
            <br>
        @endif
    @endif
    <!-- BIENES MUEBLES -->
    @if(in_array('BIENES MUEBLES',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(1)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES MUEBLES (SITUACIÓN ACTUAL)</span></h2>
            @break
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES MUEBLES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @case(3)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BIENES MUEBLES (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        <div style="text-align: center;">
            BIENES DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS
        </div>
        @if($secciones_pdf["BIENES MUEBLES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ BIEN MUEBLE ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach($secciones_pdf["BIENES MUEBLES"]->Info as $bien_mueble)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    <table style="width: 100%">
                        @if($bien_mueble->Titular == 'DECLARANTE')
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->TipoBien,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo del bien</td>
                                <td style="width: 50%; text-align: center;">Titular</td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->TipoBien,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo del bien</td>
                            </tr>
                        @endif
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->DescripcionBien,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->TipoAdquisicion,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Descripción</td>
                            <td style="width: 50%; text-align: center;">Forma de adquisición</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->DescripcionBien,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->Valor,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->TipoMoneda,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($bien_mueble->FechaOperacion,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Forma de pago</td>
                            <td style="width: 50%; text-align: center;">Valor adquisición</td>
                            <td style="width: 50%; text-align: center;">Tipo moneda</td>
                            <td style="width: 50%; text-align: center;">Fecha adquisición</td>
                        </tr>
                    </table>
                    @if($bien_mueble->NombreRazonSocial != null && $bien_mueble->TipoPersonaTransmisor == 2)
                        <br>
                        <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DEL BIEN MUEBLE</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->NombreRazonSocial,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->RFCTRansmisor,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal del transmisor
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre del transmisor de la propiedad
                                </td><td style="width: 33%; text-align: center;">
                                    RFC del transmisor de propiedad
                                </td>
                            </tr>
                        </table>

                        @php
                            $relaciones_no_mostrables = array(
                                "Abuelo (a)",
                                "Bisabuelo (a)",
                                "Bisnieto (a)",
                                "Concubino (a) / Concubinario / Unión libre",
                                "Concuño (a)",
                                "Cuñado (a)",
                                "Cónyuge",
                                "Hermano (a)",
                                "Hijo (a)",
                                "Madre",
                                "Padre",
                                "Primo (a)",
                                "Sobrino (a)",
                                "Suegro (a)",
                                "Tatarabuelo (a)",
                                "Tataranieto (a)",
                                "Tío (a)"
                            );
                        @endphp
                        @if(in_array($bien_mueble->RelacionTransmisor,$relaciones_no_mostrables) == false)
                            <table class="row-table" style="border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid; width: 100%;">
                                <tr>
                                    <td style="width: 100%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_mueble->RelacionTransmisor,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; text-align: center;">
                                        Tipo de relación con el titular
                                    </td>
                                </tr>
                            </table>
                        @endif
                    @endif
                    @if($bien_mueble->NombreTercero != null && $bien_mueble->TipoPersonaTercero == 1)
                        <br>
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->NombreTercero,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($bien_mueble->RFCTercero,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal del tercero
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre del tercero
                                </td><td style="width: 33%; text-align: center;">
                                    RFC del tercero
                                </td>
                            </tr>
                        </table>
                        <br>
                    @endif
                    @if($bien_mueble->MotivoBaja != null)
                        <table class="row-table" style="border-bottom: 1px solid;">
                            <tr>
                                <td style="width: 100%; text-align: left; font-weight:bold;" >
                                    Motivo de baja
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; text-align: center;">
                                    {{ mb_strtoupper($bien_mueble->MotivoBaja,'UTF-8') }}
                                </td>
                            </tr>
                        </table>
                    @endif
                </div>
            @endforeach
            <br>
            <br>
        @endif
    @endif

    <!-- INVERSIONES -->
    @if(in_array('INVERSIONES',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">INVERSIONES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @default
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">INVERSIONES (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        <div style="text-align: center;">
            INVERSIONES, CUENTAS BANCARIAS Y OTRO TIPO DE VALORES DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS
        </div>
        @if($secciones_pdf["INVERSIONES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ INVERSIÓN ALGUNA EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach($secciones_pdf["INVERSIONES"]->Info as $inversion)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    @if($inversion->Titular == 'DECLARANTE')
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->TipoInversion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de inversión / activo</td>
                                <td style="width: 50%; text-align: center;">Titular de la inversión</td>
                            </tr>
                        </table>
                    @else
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->TipoInversion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de inversión / activo</td>
                            </tr>
                        </table>
                    @endif
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($inversion->TipoInstrumento,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($inversion->Moneda,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Activo</td>
                            <td style="width: 50%; text-align: center;">Tipo de moneda</td>
                        </tr>
                    </table>
                    @if($inversion->NombreTercero != null && $inversion->TipoPersonaTercero == 1)
                        <br>
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->NombreTercero,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->RFCTercero,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal del tercero
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre del tercero
                                </td><td style="width: 33%; text-align: center;">
                                    RFC del tercero
                                </td>
                            </tr>
                        </table>
                        <br>
                    @endif
                    @if($inversion->Institucion != null)
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DE LA INSTITUCIÓN CON DONDE SE REALIZA LA INVERSIÓN</span></h3>
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->Institucion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->RFCIntitucion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    @if($inversion->EsExtranjero == 0)
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            MÉXICO
                                        </div>
                                    @else
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($inversion->Ubicacion,'UTF-8') }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    Institución o razón social
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    RFC
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    País de localización
                                </td>
                            </tr>
                        </table>
                        <br>
                    @endif
                </div>
            @endforeach
            <br>
            <br>
        @endif
    @endif

    <!-- ADEUDOS/PASIVOS -->
    @if(in_array('ADEUDOS/PASIVOS',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">ADEUDOS/PASIVOS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @default
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">ADEUDOS/PASIVOS (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        <div style="text-align: center;">
            ADEUDOS/PASIVOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS
        </div>
        @if($secciones_pdf["ADEUDOS/PASIVOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ ADEUDO O PASIVO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach($secciones_pdf["ADEUDOS/PASIVOS"]->Info as $adeudo)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    <table style="width: 100%">
                        @if($adeudo->Titular == 'DECLARANTE')
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($adeudo->TipoAdeudo,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($adeudo->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de adeudo</td>
                                <td style="width: 50%; text-align: center;">Titular del adeudo</td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($adeudo->TipoAdeudo,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de adeudo</td>
                            </tr>
                        @endif
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($adeudo->FechaOtorgamiento,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($adeudo->MontoOriginal,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Fecha de adquisicíon</td>
                            <td style="width: 50%; text-align: center;">Monto original del adeudo / pasivo</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($adeudo->Moneda,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Tipo de moneda</td>
                    </table>
                    @if($adeudo->NombreTercero != null)
                        @if($adeudo->TipoPersonaTercero == 0)
                            <br>
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA FISÍCA
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($adeudo->NombreTercero,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($adeudo->RFCTercero,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        Tipo persona legal del tercero
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        Nombre del tercero
                                    </td><td style="width: 33%; text-align: center;">
                                        RFC del tercero
                                    </td>
                                </tr>
                            </table>
                            <br>
                        @endif
                    @endif
                    @if($adeudo->TipoPersonaOtorgante != 0)
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL OTORGANTE</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                    PERSONA MORAL
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($adeudo->NombreRazonSocial,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($adeudo->RFCOtorgante,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    Tipo persona legal
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    Nombre/ Institución o razón social
                                </td><td style="width: 33%; text-align: center;">
                                    RFC
                                </td>
                            </tr>
                        </table>
                    @endif
                </div>
            @endforeach
        @endif
    @endif

    <!-- PRESTAMO O COMODATO POR TERCEROS -->
    @if(in_array('PRESTAMO O COMODATO POR TERCEROS',$indice) == true)
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(2)
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PRESTAMO O COMODATO POR TERCEROS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
            @break
            @default
                <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PRESTAMO O COMODATO POR TERCEROS (SITUACIÓN ACTUAL)</span></h2>
            @break
        @endswitch
        @if($secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ PRESTAMO O COMODATO POR TERCEROS EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info as $prestamo)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    @if($prestamo->TipoDeBien == 1)
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        INMUEBLE
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($prestamo->BienInmueble,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de bien</td>
                                <td style="width: 50%; text-align: center;">Especificación del bien</td>
                            </tr>
                        </table>
                        <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DOMICILIO</span></h3>
                    @else
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        VEHÍCULO
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($prestamo->BienMueble,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Tipo de bien</td>
                                <td style="width: 50%; text-align: center;">Especificación del bien</td>
                            </tr>
                        </table>
                        <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">Datos del vehículo</span></h3>
                        <div style="border: 1px solid">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->Marca,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->Modelo,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->Anio,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 33%; text-align: center;">Marca</td>
                                    <td style="width: 33%; text-align: center;">Modelo</td>
                                    <td style="width: 33%; text-align: center;">Año</td>
                                </tr>
                            </table>
                            <br>
                        </div>
                    @endif
                    @if($prestamo->TipoPersonaTitular == 2)
                        <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">Datos del dueño o titular</span></h3>
                        <div style="border: 1px solid">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            PERSONA MORAL
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->Titular,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 33%; text-align: center;">Tipo persona legal</td>
                                    <td style="width: 33%; text-align: center;">Nombre</td>
                                </tr>
                            </table>
                            <br>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->RFCTitular,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->Vinculo,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 33%; text-align: center;">RFC</td>
                                    <td style="width: 33%; text-align: center;">Relación con el dueño o titular</td>
                                </tr>
                            </table>
                        </div>
                    @endif
                </div>
                <br>
            @endforeach
        @endif
    @endif

    <div style="page-break-after:always;"></div>
    <!-- SEGUNDA PARTE DE LA DECLARACION -->
    <div style="text-align: center; margin-bottom: 2px; font-size: 25px; border: 1px solid; font-weight:bold">
        DECLARACIÓN DE INTERESES
    </div>
    <!-- PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES -->
    @if(array_key_exists('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES', $secciones_pdf))
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info as $participacion_empresa)
                @if($participacion_empresa->Participante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Participante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->TipoParticipacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Participante</td>
                                <td style="width: 50%; text-align: center;">Tipo de participación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->NombreEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->RFCEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->PorcentajeParticipacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Nombre de la empresa, sociedad o asociación</td>
                                <td style="width: 50%; text-align: center;">RFC</td>
                                <td style="width: 50%; text-align: center;">Porcentaje de participación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->MontoMensual != null)
                                            {{$participacion_empresa->MontoMensual}}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->EsExtranjero == 0)
                                            EN MÉXICO
                                        @else
                                            EN EL EXTRANJERO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Es extranjero</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Sector,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Sector productivo</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN', $secciones_pdf))
        <!-- PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info as $participacion_institucion)
                @if($participacion_institucion->Participante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->Participante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->TipoInstitucion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Participante</td>
                                <td style="width: 50%; text-align: center;">Tipo de institución</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->PuestoRol,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Puesto/Rol</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->FechaInicio,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->MontoMensual != null)
                                            {{$participacion_institucion->MontoMensual}}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Fecha de inicio de participación</td>
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->EsExtranjero == 0)
                                            EN MÉXICO
                                        @else
                                            EN EL EXTRANJERO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Es extranjero</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif

    @if(array_key_exists('APOYOS O BENEFICIOS PÚBLICOS', $secciones_pdf))
        <!-- APOYOS O BENEFICIOS PÚBLICOS -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">APOYOS O BENEFICIOS PÚBLICOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ APOYO O BENEFICIO PÚBLICO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info as $apoyo)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    <table style="width: 100%">
                        @if($apoyo->Beneficiario == 'DECLARANTE')
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->Beneficiario,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->NombrePrograma,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Beneficiario</td>
                                <td style="width: 50%; text-align: center;">Nombre del programa</td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->NombrePrograma,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Nombre del programa</td>
                            </tr>
                        @endif
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->NombreInstitucion,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->NombreAmbito,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Institucion que otorga el apoyo</td>
                            <td style="width: 50%; text-align: center;">Nivel u orden de gobierno</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->TipoApoyo,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($apoyo->MontoApoyo == null)
                                        NO APLICA
                                    @else
                                        {{ mb_strtoupper($apoyo->MontoApoyo,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($apoyo->Especificacion == null)
                                        NO APLICA
                                    @else
                                        {{ mb_strtoupper($apoyo->Especificacion,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Tipo de apoyo</td>
                            <td style="width: 50%; text-align: center;">Monto aproximado</td>
                            <td style="width: 50%; text-align: center;">Especificacion del apoyo</td>
                        </tr>
                    </table>
                </div>
            @endforeach
        @endif
    @endif

    @if(array_key_exists('REPRESENTACIÓN', $secciones_pdf))
        <!-- REPRESENTACIÓN -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">REPRESENTACIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["REPRESENTACIÓN"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ REPRESENTACIÓN ALGUNA EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["REPRESENTACIÓN"]->Info as $representacion)
                @if($representacion->Representante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Representante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->TipoRepresentacion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->FechaInicio,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Representante</td>
                                <td style="width: 50%; text-align: center;">Tipo de representacion</td>
                                <td style="width: 50%; text-align: center;">Fecha de inicio de la representación</td>
                            </tr>
                        </table>
                        @if($representacion->NombreRazonSocial != null && $representacion->RepresentateTipoPersona == 'moral')
                            <br>
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL REPRESENTANTE / REPRESENTADO</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->NombreRazonSocial,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->RFCRepresentante,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">Tipo de persona</td>
                                    <td style="width: 33%; text-align: center;">Razón social del representante</td>
                                    <td style="width: 33%; text-align: center;">RFC</td>
                                </tr>
                            </table>
                            <br>
                        @endif
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->RecibeRemuneracion == 1)
                                            {{ mb_strtoupper($representacion->MontoMensual,'UTF-8') }}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Sector,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                                <td style="width: 50%; text-align: center;">Sector</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->EsExtranjero == 1)
                                            EN EL EXTRANJERO
                                        @else
                                            EN MÉXICO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Lugar donde se ubica</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('CLIENTES PRINCIPALES', $secciones_pdf))
        <!-- CLIENTES PRINCIPALES -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">CLIENTES PRINCIPALES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["CLIENTES PRINCIPALES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ CLIENTE PRINCIPAL ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            <div style="text-align: center;">
                SE MANIFESTARÁ EN BENEFICIO O GANANCIA DEL DECLARANTE SI SUPERA MENSUALMENTE 250 UNIDADES DE MEDIDA Y ACTUALIZACIÓN (UMA)
            </div>
            @foreach ($secciones_pdf["CLIENTES PRINCIPALES"]->Info as $cliente)
                @if($cliente->Titular == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($cliente->ActLucrativaDependiente == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Realiza alguna actividad independiente al empleo, cargo o comisión</td>
                                <td style="width: 50%; text-align: center;">Participante</td>
                            </tr>
                        </table>
                        <br>
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DE LA EMPRESA O SERVICIO QUE PROPORCIONA</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->NombreEmpresaServicio,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->RFCEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">Razón social del representante</td>
                                <td style="width: 33%; text-align: center;">RFC</td>
                            </tr>
                        </table>
                        <br>
                        @if($cliente->TipoPersonaCliente == 'moral')
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL CLIENTE PRINCIPAL</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($cliente->RazonSocialCliente,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($cliente->ClienteRFC,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">Tipo de persona</td>
                                    <td style="width: 33%; text-align: center;">Nombre o razón social del representante</td>
                                    <td style="width: 33%; text-align: center;">RFC</td>
                                </tr>
                            </table>
                        @endif
                        <br>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Sector,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->MontoMensual,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Sector productivo al que pertenece</td>
                                <td style="width: 50%; text-align: center;">Monto aproximado del beneficio o ganancia mensual que obtiene del cliente principal</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($cliente->EsExtranjero == 1)
                                            EN EL EXTRANJERO
                                        @else
                                            EN MÉXICO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Lugar donde se ubica</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('BENEFICIOS PRIVADOS', $secciones_pdf))
        <!-- BENEFICIOS PRIVADOS -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BENEFICIOS PRIVADOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["BENEFICIOS PRIVADOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ BENEFICIO PRIVADO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["BENEFICIOS PRIVADOS"]->Info as $beneficio)
                @if($beneficio->Titular == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{mb_strtoupper($beneficio->TipoBeneficio,'UTF-8')}}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($beneficio->Titular,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($beneficio->RecepcionBeneficio,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Tipo de beneficio</td>
                                <td style="width: 33%; text-align: center;">Beneficiario</td>
                                <td style="width: 33%; text-align: center;">Recepción del beneficio</td>
                            </tr>
                        </table>
                        <br>
                        @if($beneficio->OtorganteTipoPersona == 'moral')
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->NombreRazonSocial,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->OtorganteTipoPersona,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->RFCOtorgante,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 33%; text-align: center;">Nombre o razón social del otorgante</td>
                                    <td style="width: 33%; text-align: center;">Tipo persona legal otorgante</td>
                                    <td style="width: 33%; text-align: center;">RFC del otorgante</td>
                                </tr>
                            </table>
                        @endif
                        @if($beneficio->RecepcionBeneficio == 'Epecie')
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->Beneficio,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 50%; text-align: center;">Especifique el beneficio recibido</td>
                                    <td style="width: 50%; text-align: center;">Sector al que pertenece</td>
                                </tr>
                            </table>
                        @else
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->MontoMensual,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->TipoMoneda,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 50%; text-align: center;">Monto mensual aproximado</td>
                                    <td style="width: 50%; text-align: center;">Tipo de moneda</td>
                                    <td style="width: 50%; text-align: center;">Sector al que pertenece</td>
                                </tr>
                            </table>
                        @endif
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    <footer style="font-size:10px;">
        SiDePat {{config('sidepat.version_actual')}}
    </footer>
</body>
</html>
