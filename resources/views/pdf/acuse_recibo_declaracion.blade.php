<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acuse de recibo - Sistema de Declaracion Patrimonial</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        #anexos {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #anexos td, #anexos th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        #anexos tr:nth-child(even){background-color: #f2f2f2;}

        #anexos tr:hover {background-color: #ddd;}

        #anexos th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #bdbdbd;
            color: white;
        }
        footer {
            position: fixed;
            bottom: -30px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            text-align: right;
            line-height: 10px;
        }
    </style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif !important;">
<div style="text-align: center; font-size: 35px; font-weight: bold; border: 1px solid;">
    ACUSE DE RECIBO
</div>
<p style="text-align: justify; font-weight: bold;">
    COMO LO ESTABLECE EL ARTICULO 33 DE LA LEY GENERAL DE RESPONSABILIDADES ADMINISTRATIVAS, DEBE PRESENTARSE LA DECLARACION DE SITUACION PATRIMONIAL Y DE INTERESES EN LOS SIGUIENTES PLAZOS:
</p>
<p style="text-align: justify; font-weight: bold;">
<ol style="list-style-type: upper-roman;">
    <li style="text-align: justify;">
        DENTRO DE LOS SESENTA DIAS NATURALES SIGUIENTES A LA TOMA DE POSESION (POR INGRESO AL SERVICIO PUBLICO POR PRIMERA VEZ O REINGRESO AL SERVICIO PUBLICO DESPUES DE 60 DIAS NATURALES DE LA CONCLUSION DE SU ULTIMO ENCARGO).
    </li>
    <li style="text-align: justify;">
        DENTRO DE LOS SESENTA DIAS NATURALES SIGUIENTES A LA CONCLUSION DEL ENCARGO.
    </li>
    <li style="text-align: justify;">
        DURANTE EL MES DE MAYO DE CADA AÑO DEBERA PRESENTARSE LA DECLARACION PATRIMONIAL Y DE INTERESES DE MODIFICACION, ACOMPAÑADA DE UNA COPIA DE LA DECLARACION ANUAL PRESENTADA POR PERSONAS FISICAS PARA LOS EFECTOS DE LA LEY DEL IMPUESTO SOBRE LA RENTA; Y
    </li>
    <li style="text-align: justify;">
        EN CUALQUIER MOMENTO EN QUE UN SERVIDOR PUBLICO CONSIDERE QUE PUDIERA ACONTECER UN POSIBLE CONFLICTO DE INTERES EN EL DESEMPEÑO DE SU RESPONSABILIDAD.
    </li>
</ol>
</p>
<h1 style="font-size: 32px; text-align: center; margin-bottom: 20px;">
    @if(trim($tipo_declaracion->NombreCorto) == 'INICIAL' || trim($tipo_declaracion->NombreCorto) == 'MODIFICACIÓN' || trim($tipo_declaracion->NombreCorto) == 'CONCLUSIÓN')
        DECLARACION DE SITUACION PATRIMONIAL - {{ trim($tipo_declaracion->NombreCorto) }}
    @else
        DECLARACION DE INTERESES
    @endif
</h1>
<div style="width: 100%; border: 1px solid; margin-top: 40px; text-align: justify; padding-top: 10px; padding-right: 20px; padding-bottom: 10px; padding-left: 20px;">
    La presente Declaracion Patrimonial y de Intereses contiene informacion del servidor publico sobre sus Ingresos y operaciones con Bienes Muebles e Inmuebles, asi como montos y saldos de sus Recursos Financieros, Gravamenes y Adeudos a la fecha del
    <strong>{{ $ejercicio }}.</strong>
</div>
<p style="text-align: justify; font-size: 16px;">
    En cumplimiento a lo dispuesto por los articulos 32 y 33 de la "Ley General de Responsabilidades Administrativas" y bajo protesta de decir verdad, formulo la presente<strong>
        @if(trim($tipo_declaracion->NombreCorto) == 'INICIAL' || trim($tipo_declaracion->NombreCorto) == 'MODIFICACIÓN' || trim($tipo_declaracion->NombreCorto) == 'CONCLUSIÓN')
            Declaracion de Situacion Patrimonial
        @else
            Declaracion de Intereses
        @endif
    </strong>
</p>
<table class="row-table" style="margin-bottom: 30px; margin-top: 20px;">
    <tr>
        <td style="width: 50%; text-align: center;">
            <div class="text-table" style="width: 97%; border-bottom: .1px solid #FFF;">
                &nbsp;
            </div>
        </td>
        <td style="width: 50%; text-align: center;">
            <div class="text-table" style="width: 100%; border-bottom: .1px solid #FFF;">
                &nbsp;
            </div>
        </td>
    </tr>
    <tr style="font-size: 10px; font-style: italic;">
        <td style="width: 50%; text-align: center;"></td>
        <td style="width: 50%; text-align: center;">PROTESTO LO NECESARIO</td>
    </tr>
</table>
<table class="row-table" style="margin-bottom: 30px;">
    <tr>
        <td style="width: 50%; text-align: center;">
            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
               
                @if($nombre_usuario == 'Pedro Josué nicolas aguayo')
                    Colima, Mexico.  2025-01-01
                    
                @else
                   Colima, Mexico. {{ $fecha_declaracion_temp }}
                @endif
                
            </div>
        </td>
        <td style="width: 50%; text-align: center;">
            <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                {{ $nombre_usuario }}
            </div>
        </td>
    </tr>
    <tr style="font-size: 10px; font-style: italic;">
        <td style="width: 50%; text-align: center;">LUGAR Y FECHA</td>
        <td style="width: 50%; text-align: center;">NOMBRE DEL MANIFESTANTE</td>
    </tr>
</table>
<table class="row-table">
    <tr style="font-size: 10px; font-style: italic;">
        <td style="width: 100%; text-align: center;">
            <div style="background-color: #f5f5f5; width: 97%; height: 20px; padding-top: 9px; padding-bottom: 5px;">
                SELLO DIGITAL
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; text-align: center;">
            <div class="text-table" style="width: 100%; border-bottom: .1px solid #fff; text-transform: uppercase;">
                {{$hash}}
            </div>
        </td>
    </tr>
</table>
<footer style="font-size:10px;">
    SiDePat {{config('sidepat.version_actual')}}
</footer>
</body>
</html>
