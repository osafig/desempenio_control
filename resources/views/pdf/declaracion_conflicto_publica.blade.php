<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Declaración Patrimonial completa - Sistema de Declaracion Patrimonial</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        footer {
                position: fixed;
                bottom: -30px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                text-align: right;
                line-height: 10px;
            }
    </style>
</head>
<body>
    <div style="text-align: center;">
        <img src="{{ url(config('sidepat.logo2')) }}" width="300" height="120">
    </div>
    <div style="text-align: center; margin-top: 90px; margin-bottom: 2px; font-size: 25px; border: 1px solid; font-weight:bold">
        DECLARACIÓN DE INTERESES
    </div>
    <br>
    <div>
        <table style="width: 100%">
            <tr>
                <td style="width: 75%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                            @case(1)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL INICIAL
                            @break
                            @case(2)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL MODIFICACIÓN
                            @break
                            @case(3)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL CONCLUSIÓN
                            @break
                            @case(4)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO INICIAL
                            @break
                            @case(5)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO MODIFICACIÓN
                            @break
                            @case(6)
                                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO CONCLUSIÓN
                            @break
                            @case(7)
                                DECLARACIÓN DE CONFLICTO DE INTERESES
                            @break
                        @endswitch
                    </div>
                </td>
                <td style="width: 25%; text-align: center;">
                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                        {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->DeclaracionFecha,'UTF-8') }}
                    </div>
                </td>
            </tr>
            <tr style="font-size: 10px; font-style: italic;">
                <td style="width: 75%; text-align: center;">Tipo de declaracion</td>
                <td style="width: 25%; text-align: center;">Fecha de presentación</td>
            </tr>
        </table>
    </div>
    <br>
    <div style="text-align: center;">
        @if($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->NombreCorto == 'MODIFICACIÓN')
            <div>
                Bajo protesta de decir verdad, presento a usted mi declaración de situación patrimonial del periodo {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->AnioDeclarado,'UTF-8') }}, conforme a lo dispuesto
                en la Ley General de Responsabilidades Administrativas, la Ley General del Sistema Nacional Anticorrupción  y la normatividad
                aplicable.
            </div>
        @else
            <div>
                Bajo protesta de decir verdad, presento a usted mi declaración de situación patrimonial a la fecha, conforme a lo dispuesto
                en la Ley General de Responsabilidades Administrativas, la Ley General del Sistema Nacional Anticorrupción  y la normatividad
                aplicable.
            </div>
        @endif
    </div>

    <!-- INICIAN DATOS GENERALES -->
    <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">DATOS GENERALES</span></h2>
    <table style="width: 100%">
        <tr>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->PrimerApellido, 'UTF-8') }}
                </div>
            </td>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->SegundoApellido,'UTF-8') }}
                </div>
            </td>
            <td style="width: 33%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->Nombres,'UTF-8') }}
                </div>
            </td>
        </tr>
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 33%; text-align: center;"> Primer apellido</td>
            <td style="width: 33%; text-align: center;"> Segundo apellido</td>
            <td style="width: 33%; text-align: center;"> Nombre(s) </td>
        </tr>
    </table>
    <br>
    <table class="row-table">
        <tr>
            <td style="width: 50%; text-align: center;">
                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                    {{ $secciones_pdf["DATOS GENERALES"]->InfoUsuario->Email }}
                </div>
            </td>
        </tr>
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 50%; text-align: center;"> Correo electrónico Institucional</td>
        </tr>
    </table>
    <br>
    <div style="page-break-after:always;"></div>
    <!-- PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES -->
    @if(array_key_exists('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES', $secciones_pdf))
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚN PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info as $participacion_empresa)
                @if($participacion_empresa->Participante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Participante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->TipoParticipacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Participante</td>
                                <td style="width: 50%; text-align: center;">Tipo de participación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->NombreEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->RFCEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->PorcentajeParticipacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Nombre de la empresa, sociedad o asociación</td>
                                <td style="width: 50%; text-align: center;">RFC</td>
                                <td style="width: 50%; text-align: center;">Porcentaje de participación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->MontoMensual != null)
                                            {{$participacion_empresa->MontoMensual}}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_empresa->EsExtranjero == 0)
                                            EN EL EXTRANJERO
                                        @else
                                            EN MÉXICO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Es extranjero</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_empresa->Sector,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Sector productivo</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN', $secciones_pdf))
        <!-- PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚNA PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info as $participacion_institucion)
                @if($participacion_institucion->Participante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->Participante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->TipoInstitucion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Participante</td>
                                <td style="width: 50%; text-align: center;">Tipo de institución</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->PuestoRol,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Puesto/Rol</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->FechaInicio,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->MontoMensual != null)
                                            {{$participacion_institucion->MontoMensual}}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Fecha de inicio de participación</td>
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($participacion_institucion->EsExtranjero == 0)
                                            EN MÉXICO
                                        @else
                                            EN EL EXTRANJERO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($participacion_institucion->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Es extranjero</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif

    @if(array_key_exists('APOYOS O BENEFICIOS PÚBLICOS', $secciones_pdf))
        <!-- APOYOS O BENEFICIOS PÚBLICOS -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">APOYOS O BENEFICIOS PÚBLICOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚN APOYO O BENEFICIO PÚBLICO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info as $apoyo)
                <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                    <table style="width: 100%">
                        @if($apoyo->Beneficiario == 'DECLARANTE')
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->Beneficiario,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->NombrePrograma,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Beneficiario</td>
                                <td style="width: 50%; text-align: center;">Nombre del programa</td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($apoyo->NombrePrograma,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Nombre del programa</td>
                            </tr>
                        @endif
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->NombreInstitucion,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->NombreAmbito,'UTF-8') }}
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Institucion que otorga el apoyo</td>
                            <td style="width: 50%; text-align: center;">Nivel u orden de gobierno</td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($apoyo->TipoApoyo,'UTF-8') }}
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($apoyo->MontoApoyo == null)
                                        NO APLICA
                                    @else
                                        {{ mb_strtoupper($apoyo->MontoApoyo,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                            <td style="width: 50%; text-align: center;">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($apoyo->Especificacion == null)
                                        NO APLICA
                                    @else
                                        {{ mb_strtoupper($apoyo->Especificacion,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr style="font-size: 10px; font-style: italic;">
                            <td style="width: 50%; text-align: center;">Tipo de apoyo</td>
                            <td style="width: 50%; text-align: center;">Monto aproximado</td>
                            <td style="width: 50%; text-align: center;">Especificacion del apoyo</td>
                        </tr>
                    </table>
                </div>
            @endforeach
        @endif
    @endif

    @if(array_key_exists('REPRESENTACIÓN', $secciones_pdf))
        <!-- REPRESENTACIÓN -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">REPRESENTACIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["REPRESENTACIÓN"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚNA REPRESENTACIÓN EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["REPRESENTACIÓN"]->Info as $representacion)
                @if($representacion->Representante == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Representante,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->TipoRepresentacion,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->FechaInicio,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Representante</td>
                                <td style="width: 50%; text-align: center;">Tipo de representacion</td>
                                <td style="width: 50%; text-align: center;">Fecha de inicio de la representación</td>
                            </tr>
                        </table>
                        @if($representacion->NombreRazonSocial != null && $representacion->RepresentateTipoPersona == 'moral')
                            <br>
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL REPRESENTANTE / REPRESENTADO</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->NombreRazonSocial,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->RFCRepresentante,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">Tipo de persona</td>
                                    <td style="width: 33%; text-align: center;">Razón social del representante</td>
                                    <td style="width: 33%; text-align: center;">RFC</td>
                                </tr>
                            </table>
                            <br>
                        @endif
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->RecibeRemuneracion == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->RecibeRemuneracion == 1)
                                            {{ mb_strtoupper($representacion->MontoMensual,'UTF-8') }}
                                        @else
                                            NO APLICA
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Sector,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Recibe remuneración</td>
                                <td style="width: 50%; text-align: center;">Monto mensual neto</td>
                                <td style="width: 50%; text-align: center;">Sector</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($representacion->EsExtranjero == 1)
                                            EN EL EXTRANJERO
                                        @else
                                            EN MÉXICO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($representacion->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Lugar donde se ubica</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('CLIENTES PRINCIPALES', $secciones_pdf))
        <!-- CLIENTES PRINCIPALES -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">CLIENTES PRINCIPALES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["CLIENTES PRINCIPALES"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚN CLIENTE PRINCIPAL EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            <div style="text-align: center;">
                SE MANIFESTARÁ EN BENEFICIO O GANANCIA DEL DECLARANTE SI SUPERA MENSUALMENTE 250 UNIDADES DE MEDIDA Y ACTUALIZACIÓN (UMA)
            </div>
            @foreach ($secciones_pdf["CLIENTES PRINCIPALES"]->Info as $cliente)
                @if($cliente->Titular == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($cliente->ActLucrativaDependiente == 1)
                                            SÍ
                                        @else
                                            NO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Titular,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Realiza alguna actividad independiente al empleo, cargo o comisión</td>
                                <td style="width: 50%; text-align: center;">Participante</td>
                            </tr>
                        </table>
                        <br>
                        <table class="row-table" style="border: 1px solid; width: 100%;">
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DE LA EMPRESA O SERVICIO QUE PROPORCIONA</span></h3>
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->NombreEmpresaServicio,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->RFCEmpresa,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%; text-align: center;">Razón social del representante</td>
                                <td style="width: 33%; text-align: center;">RFC</td>
                            </tr>
                        </table>
                        <br>
                        @if($cliente->TipoPersonaCliente == 'moral')
                            <table class="row-table" style="border: 1px solid; width: 100%;">
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL CLIENTE PRINCIPAL</span></h3>
                                <tr>
                                    <td style="width: 33%; text-align: center; border-bottom: .1px solid;">
                                        PERSONA MORAL
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($cliente->RazonSocialCliente,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="border-bottom: .1px solid;">
                                            {{ mb_strtoupper($cliente->ClienteRFC,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 33%; text-align: center;">Tipo de persona</td>
                                    <td style="width: 33%; text-align: center;">Nombre o razón social del representante</td>
                                    <td style="width: 33%; text-align: center;">RFC</td>
                                </tr>
                            </table>
                        @endif
                        <br>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Sector,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->MontoMensual,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Sector productivo al que pertenece</td>
                                <td style="width: 50%; text-align: center;">Monto aproximado del beneficio o ganancia mensual que obtiene del cliente principal</td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        @if($cliente->EsExtranjero == 1)
                                            EN EL EXTRANJERO
                                        @else
                                            EN MÉXICO
                                        @endif
                                    </div>
                                </td>
                                <td style="width: 50%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($cliente->Ubicacion,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 50%; text-align: center;">Lugar donde se ubica</td>
                                <td style="width: 50%; text-align: center;">Ubicación</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    @if(array_key_exists('BENEFICIOS PRIVADOS', $secciones_pdf))
        <!-- BENEFICIOS PRIVADOS -->
        <h2 style="font-size: 22px; text-align: center;"><span style="padding: 5px;">BENEFICIOS PRIVADOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
        @if($secciones_pdf["BENEFICIOS PRIVADOS"]->Info == [])
            <div style="text-align: center; border: 1px solid;">
                EL DECLARANTE NO PRESENTÓ NINGÚN BENEFICIO PRIVADO EN LA DECLARACIÓN DE ESTE AÑO
            </div>
            <br>
            <br>
        @else
            @foreach ($secciones_pdf["BENEFICIOS PRIVADOS"]->Info as $beneficio)
                @if($beneficio->Titular == 'Declarante')
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{mb_strtoupper($beneficio->TipoBeneficio,'UTF-8')}}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($beneficio->Titular,'UTF-8') }}
                                    </div>
                                </td>
                                <td style="width: 33%; text-align: center;">
                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                        {{ mb_strtoupper($beneficio->RecepcionBeneficio,'UTF-8') }}
                                    </div>
                                </td>
                            </tr>
                            <tr style="font-size: 10px; font-style: italic;">
                                <td style="width: 33%; text-align: center;">Tipo de beneficio</td>
                                <td style="width: 33%; text-align: center;">Beneficiario</td>
                                <td style="width: 33%; text-align: center;">Recepción del beneficio</td>
                            </tr>
                        </table>
                        <br>
                        @if($beneficio->OtorganteTipoPersona == 'moral')
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->NombreRazonSocial,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->OtorganteTipoPersona,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 33%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->RFCOtorgante,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 33%; text-align: center;">Nombre o razón social del otorgante</td>
                                    <td style="width: 33%; text-align: center;">Tipo persona legal otorgante</td>
                                    <td style="width: 33%; text-align: center;">RFC del otorgante</td>
                                </tr>
                            </table>
                        @endif
                        @if($beneficio->RecepcionBeneficio == 'Epecie')
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->Beneficio,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 50%; text-align: center;">Especifique el beneficio recibido</td>
                                    <td style="width: 50%; text-align: center;">Sector al que pertenece</td>
                                </tr>
                            </table>
                        @else
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{mb_strtoupper($beneficio->MontoMensual,'UTF-8')}}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->TipoMoneda,'UTF-8') }}
                                        </div>
                                    </td>
                                    <td style="width: 50%; text-align: center;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    <td style="width: 50%; text-align: center;">Monto mensual aproximado</td>
                                    <td style="width: 50%; text-align: center;">Tipo de moneda</td>
                                    <td style="width: 50%; text-align: center;">Sector al que pertenece</td>
                                </tr>
                            </table>
                        @endif
                    </div>
                @endif
            @endforeach
        @endif
    @endif
    <footer style="font-size:10px;">
        SiDePat {{config('sidepat.version_actual')}}
    </footer>
</body>
</html>
