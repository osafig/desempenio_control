<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Declaración Patrimonial completa - Sistema de Declaracion Patrimonial</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        footer {
                position: fixed;
                bottom: -30px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                text-align: right;
                line-height: 10px;
            }
    </style>
</head>
<body>
    <div style="text-align: center;">
        <img src="{{ url(config('sidepat.logo2')) }}" width="300" height="120">
    </div>
    <div style="text-align: center; margin-top: 45px; margin-bottom: 2px; font-size: 25px; font-weight:bold; background-color: #cccccc;">
        @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
            @case(1)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL INICIAL VERSIÓN PÚBLICA 
            @break
            @case(2)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL MODIFICACIÓN VERSIÓN PÚBLICA
            @break
            @case(3)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL CONCLUSIÓN VERSIÓN PÚBLICA
            @break
            @case(4)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO INICIAL VERSIÓN PÚBLICA
            @break
            @case(5)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO MODIFICACIÓN VERSIÓN PÚBLICA
            @break
            @case(6)
                DECLARACIÓN DE SITUACIÓN PATRIMONIAL FORMATO SIMPLIFICADO CONCLUSIÓN VERSIÓN PÚBLICA
            @break
            @case(7)
                DECLARACIÓN DE CONFLICTO DE INTERESES VERSIÓN PÚBLICA
            @break
        @endswitch
    </div>
    <br>
    <div>
        {{ $secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->Justificacion,'UTF-8' }}
    </div>

    <!-- INICIAN DATOS GENERALES -->
    @if($secciones_formato[0]->Aplica == 1)
        <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS GENERALES</span></h2>
        <table style="width: 100%">
            <tr style="font-size: 10px;">
                @if($secciones_formato[0]->Campos[0]->Visible == 1)
                    <td style="width: 20%;"> Nombre(s):</td>
                    <td style="width: 30%;">
                        <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->Nombres,'UTF-8') }}
                        </div>
                    </td>
                @else
                    <td></td>
                @endif
                @if($secciones_formato[0]->Campos[1]->Visible == 1)
                    <td style="width: 20%;"> Primer apellido:</td>
                    <td style="width: 30%;">
                        <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->PrimerApellido, 'UTF-8') }}
                        </div>
                    </td>
                @else
                    <td></td>
                @endif
            </tr>
            <tr style="font-size: 10px;">
                @if($secciones_formato[0]->Campos[2]->Visible == 1)
                    <td style="width: 20%;"> Segundo apellido:</td>
                    <td style="width: 30%;">
                        <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                            {{ mb_strtoupper($secciones_pdf["DATOS GENERALES"]->InfoUsuario->SegundoApellido,'UTF-8') }}
                        </div>
                    </td>
                @else
                    <td></td>
                @endif
                @if($secciones_formato[0]->Campos[3]->Visible == 1)
                    <td style="width: 20%;"> Correo electrónico Institucional:</td>
                    <td style="width: 30%;">
                        <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                            {{ $secciones_pdf["DATOS GENERALES"]->InfoUsuario->Email }}
                        </div>
                    </td>
                @else
                    <td></td>
                @endif
            </tr>
        </table>
    @endif


    <!-- DATOS CURRICULARES DEL DECLARANTE -->
    @if($secciones_formato[2]->Aplica == 1)
        @if (in_array('DATOS CURRICULARES DEL DECLARANTE',$indice) == true)
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS CURRICULARES DEL DECLARANTE</span></h2>
            <table style="width: 100%">
                <tr>
                    @if($secciones_formato[2]->Campos[0]->Visible == 1)
                        <td style="width: 20%;">Nivel:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Nombre,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[2]->Campos[1]->Visible == 1)
                        <td style="width: 20%;">Institución:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Institucion,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[2]->Campos[2]->Visible == 1)
                        <td style="width: 20%;">Carrera o área de conocimiento:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Carrera,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[2]->Campos[3]->Visible == 1)
                        <td style="width: 20%;">Estatus:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->Status,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[2]->Campos[4]->Visible == 1)
                        <td style="width: 20%;">Documento obtenido:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->DocumentoObtenido,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[2]->Campos[5]->Visible == 1)
                        <td style="width: 20%;">Fecha obtención del documento:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->FechaDocumento,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[2]->Campos[6]->Visible == 1)
                        <td style="width: 20%;">Lugar dónde se ubica la institución educativa:</td>
                        <td style="width: 30%;">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                @if ($secciones_pdf["DATOS CURRICULARES DEL DECLARANTE"]->Info->EsExtranjero == 0)
                                    EN MÉXICO
                                @else
                                    EN EL EXTRANJERO
                                @endif
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
            </table>
            <div style="page-break-after:always;"></div>
        @endif
    @endif



    <!-- DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA -->
    @if($secciones_formato[3]->Aplica == 1)
        @if (in_array('DATOS DEL EMPLEO, CARGO O COMISIÓN',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(1)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA</span></h2>
                @break
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN ACTUAL</span></h2>
                @break
                @case(3)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</span></h2>
                @break
                @case(4)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA</span></h2>
                @break
                @case(5)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN ACTUAL</span></h2>
                @break
                @case(6)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</span></h2>
                @break
            @endswitch

            <table style="width: 100%">
                <tr>
                    @if($secciones_formato[3]->Campos[0]->Visible == 1)
                        <td style="width: 20%; ">Nivel/ Orden de gobierno:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NombreOrden,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[3]->Campos[1]->Visible == 1)
                        <td style="width: 20%; ">Ámbito público:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NombreAmbito,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[3]->Campos[2]->Visible == 1)
                        <td style="width: 20%; ">Nombre del ente público:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Dependencia,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[3]->Campos[3]->Visible == 1)
                        <td style="width: 20%; ">Área de adscripción:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Nombre,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[3]->Campos[4]->Visible == 1)
                        <td style="width: 20%; ">Empleo, cargo o comisión:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Puesto,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[3]->Campos[5]->Visible == 1)
                        <td style="width: 20%; ">¿Está contratado por honorarios:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Honorarios == 1)
                                    SÍ
                                @else
                                    NO
                                @endif
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[3]->Campos[6]->Visible == 1)
                        <td style="width: 20%; ">Nivel del empleo, cargo o comisión:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Nivel,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[3]->Campos[7]->Visible == 1)
                        @if($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->NombreCorto == 'CONCLUSIÓN')
                            <td style="width: 20%; ">Fecha de conclución del empleo, cargo o comisión:</td>
                        @else
                            <td style="width: 20%; ">Fecha de toma de posesión del empleo, cargo o comisión:</td>
                        @endif
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->FechaIngreso,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    @if($secciones_formato[3]->Campos[8]->Visible == 1)
                        <td style="width: 20%; ">Especifique función principal:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->FuncionPrincipal,'UTF-8') }}
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                    @if($secciones_formato[3]->Campos[9]->Visible == 1)
                        <td style="width: 20%; ">Teléfono de oficina y extensión:</td>
                        <td style="width: 30%; ">
                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Telefono == null)
                                N/A
                                @else
                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Telefono,'UTF-8') }}
                                @endif
                            </div>
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
            </table>
            <h3 style="font-size: 11px; "><span style="padding: 5px;">DOMICILIO DEL EMPLEO, CARGO O COMISIÓN</span></h3>
            @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->EsExtranjero == 0)
                <!-- cargo en mexico -->
                <table style="width: 100%">
                    <tr>
                        @if($secciones_formato[3]->Campos[10]->Visible == 1)
                            <td style="width: 20%; ">Calle:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Calle,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[11]->Visible == 1)
                            <td style="width: 20%; ">Número exterior:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroExterior,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr style="width: 100%">
                        @if($secciones_formato[3]->Campos[12]->Visible == 1)
                            <td style="width: 20%; ">Número interior:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior == null)
                                        N/A
                                    @else
                                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[13]->Visible == 1)
                            <td style="width: 20%; ">Colonia/Localidad:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Colonia,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        @if($secciones_formato[3]->Campos[14]->Visible == 1)
                            <td style="width: 20%; ">Municipio/Alcaldía:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Municipio,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[15]->Visible == 1)
                            <td style="width: 20%; ">Entidad Federativa:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Estado,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        @if($secciones_formato[3]->Campos[17]->Visible == 1)
                            <td style="width: 20%; ">Código postal:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->CodigoPostal,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                </table>
                <br>
            @else
                <!-- cargo en el extranjero -->
                <table style="width: 100%">
                    <tr>
                        @if($secciones_formato[3]->Campos[10]->Visible == 1)
                            <td style="width: 20%; ">Calle:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Calle,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[11]->Visible == 1)
                            <td style="width: 20%; ">Número exterior:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroExterior,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        @if($secciones_formato[3]->Campos[12]->Visible == 1)
                            <td style="width: 20%; ">Número interior:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior == null)
                                        N/A
                                    @else
                                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->NumeroInterior,'UTF-8') }}
                                    @endif
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[13]->Visible == 1)
                            <td style="width: 20%; ">Ciudad/Localidad:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Ciudad,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr style="font-size: 10px; font-style: italic;">
                        @if($secciones_formato[3]->Campos[16]->Visible == 1)
                            <td style="width: 20%; ">Estado/Provincia:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->EstadoExtranjero,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                        @if($secciones_formato[3]->Campos[18]->Visible == 1)
                            <td style="width: 20%; ">País:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->Pais,'UTF-8')}}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        @if($secciones_formato[3]->Campos[17]->Visible == 1)
                            <td style="width: 20%; ">Código postal:</td>
                            <td style="width: 30%; ">
                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->Info->CodigoPostal,'UTF-8') }}
                                </div>
                            </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                </table>
            @endif
            @if(isset($secciones_formato[3]->Campos[19]))
                @if($secciones_formato[3]->Campos[19]->Visible == 1)
                    @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo != [])
                        <br>
                        <br>
                        <div style="">
                            El declarante manifestó que cuenta con otro empleo, cargo o comisión distinto al declarado.
                        </div>
                        <br>
                        <br>
                        <table style="width: 100%">
                            <tr>
                                @if($secciones_formato[3]->Campos[0]->Visible == 1 || $secciones_formato[3]->Campos[1]->Visible == 1)
                                    @if($secciones_formato[3]->Campos[0]->Visible == 1)
                                        <td style="width: 20%; ">Nivel/ Orden de gobierno:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NombreOrden,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[3]->Campos[1]->Visible == 1)
                                        <td style="width: 20%; ">Ámbito público:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NombreAmbito,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                @if($secciones_formato[3]->Campos[2]->Visible == 1 || $secciones_formato[3]->Campos[3]->Visible == 1)
                                    @if($secciones_formato[3]->Campos[2]->Visible == 1)
                                        <td style="width: 20%; ">Nombre del ente público:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Dependencia,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[3]->Campos[3]->Visible == 1)
                                        <td style="width: 20%; ">Área de adscripción:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->AreaEspecifica,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                @if($secciones_formato[3]->Campos[4]->Visible == 1 || $secciones_formato[3]->Campos[5]->Visible == 1)
                                    @if($secciones_formato[3]->Campos[4]->Visible == 1)
                                        <td style="width: 20%; ">Empleo, cargo o comisión:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->PuestoEspecifico,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[3]->Campos[5]->Visible == 1)
                                        <td style="width: 20%; ">¿Está contratado por honorarios:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Honorarios == 1)
                                                    SÍ
                                                @else
                                                    NO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                        </table>
                        <table style="width:100%">
                            <tr>
                                @if($secciones_formato[3]->Campos[6]->Visible == 1 || $secciones_formato[3]->Campos[8]->Visible == 1)
                                    @if($secciones_formato[3]->Campos[6]->Visible == 1)
                                        <td style="width: 20%; ">Nivel del empleo, cargo o comisión:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Nivel,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[3]->Campos[8]->Visible == 1)
                                        <td style="width: 20%; ">Fecha de toma de posesión del empleo, cargo o comisión:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->FechaIngreso,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                @if($secciones_formato[3]->Campos[7]->Visible == 1 || $secciones_formato[3]->Campos[8]->Visible == 1)
                                    @if($secciones_formato[3]->Campos[7]->Visible == 1)
                                        <td style="width: 20%; ">Especifique función principal:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->FuncionPrincipal,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[3]->Campos[8]->Visible == 1)
                                        <td style="width: 20%; ">Teléfono de oficina y extención:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ $secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono }}
                                                @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono == null)
                                                    N/A
                                                @else
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Telefono,'UTF-8') }}
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                        </table>
                        <h3 style="font-size: 11px; "><span style="padding: 5px;">DOMICILIO DEL EMPLEO, CARGO O COMISIÓN</span></h3>
                        @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->EsExtranjero == 0)
                            <!-- cargo en mexico -->
                            <table style="width: 100%">
                                <tr>
                                    @if($secciones_formato[3]->Campos[10]->Visible == 1 || $secciones_formato[3]->Campos[11]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[10]->Visible == 1)
                                            <td style="width: 20%; ">Calle:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Calle,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[11]->Visible == 1)
                                            <td style="width: 20%; ">Número exterior:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroExterior,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                <tr>
                                    @if($secciones_formato[3]->Campos[12]->Visible == 1 || $secciones_formato[3]->Campos[13]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[12]->Visible == 1)
                                            <td style="width: 20%; ">Número interior:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior == null)
                                                        N/A
                                                    @else
                                                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior,'UTF-8') }}
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[13]->Visible == 1)
                                            <td style="width: 20%; ">Colonia/Localidad:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Colonia,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                    @if($secciones_formato[3]->Campos[14]->Visible == 1 || $secciones_formato[3]->Campos[15]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[14]->Visible == 1)
                                            <td style="width: 20%; ">Municipio/Alcaldía:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Municipio,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[15]->Visible == 1)
                                            <td style="width: 20%; ">Entidad Federativa:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Estado,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                @if($secciones_formato[3]->Campos[17]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%; ">Código postal:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->CodigoPostal,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            <br>
                        @else
                            <!-- cargo en el extranjero -->
                            <table style="width: 100%">
                                <tr>
                                    @if($secciones_formato[3]->Campos[10]->Visible == 1 || $secciones_formato[3]->Campos[11]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[10]->Visible == 1)
                                            <td style="width: 20%; ">Calle:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Calle,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[11]->Visible == 1)
                                            <td style="width: 20%; ">Número exterior:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroExterior,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                <tr>
                                    @if($secciones_formato[3]->Campos[12]->Visible == 1 || $secciones_formato[3]->Campos[12]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[12]->Visible == 1)
                                            <td style="width: 20%; ">Número interior:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    @if($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior == null)
                                                        N/A
                                                    @else
                                                        {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->NumeroInterior,'UTF-8') }}
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[13]->Visible == 1)
                                            <td style="width: 20%; ">Ciudad/Localidad:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Ciudad,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                <tr>
                                    @if($secciones_formato[3]->Campos[16]->Visible == 1 || $secciones_formato[3]->Campos[16]->Visible == 1)
                                        @if($secciones_formato[3]->Campos[16]->Visible == 1)
                                            <td style="width: 20%; ">Estado/Provincia:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->EstadoExtranjero,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[3]->Campos[18]->Visible == 1)
                                            <td style="width: 20%; ">País:</td>
                                            <td style="width: 30%; ">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->Pais,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                @if($secciones_formato[3]->Campos[17]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%; ">Código postal:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($secciones_pdf["DATOS DEL EMPLEO, CARGO O COMISIÓN"]->InfoOtroCargo->CodigoPostal,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            <br>
                        @endif
                        <br>
                    @else
                        <br>
                        <br>
                        <div>
                            El declarante manifestó que no cuenta con otro empleo, cargo o comisión distinto al declarado
                        </div>
                    @endif
                @endif
            @endif
        @endif
    @endif


    <!-- EXPERIENCIA LABORAL (ÙLTIMOS CINCO EMPLEOS) -->
    @if($secciones_formato[4]->Aplica == 1)
        @if(in_array('EXPERIENCIA LABORAL',$indice) == true)
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">EXPERIENCIA LABORAL (ÙLTIMOS CINCO EMPLEOS)</span></h2>
            @if($secciones_pdf["EXPERIENCIA LABORAL"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ EXPERIENCIA LABORAL PREVIA EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["EXPERIENCIA LABORAL"]->Info as $experiencia)
                    @if($experiencia->AmbitoSector == 'Público')
                        <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                            <table style="width: 100%">
                                @if($secciones_formato[4]->Campos[0]->Visible == 1 || $secciones_formato[4]->Campos[1]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[0]->Visible == 1)
                                            <td style="width: 20%;">Ámbito/Sector en el que laboraste:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->AmbitoSector,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[1]->Visible == 1)
                                            <td style="width: 20%;">Nivel/Orden de gobierno:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->NombreOrden,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[2]->Visible == 1 || $secciones_formato[4]->Campos[3]->Visible == 1)
                                    <tr style="font-size: 10px; font-style: italic;">
                                        @if($secciones_formato[4]->Campos[2]->Visible == 1)
                                            <td style="width: 20%;">Ámbito público:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->NombreAmbito,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[3]->Visible == 1)
                                            <td style="width: 20%;">Nombre del ente público:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Institucion,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[4]->Visible == 1 || $secciones_formato[4]->Campos[10]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[4]->Visible == 1)
                                            <td style="width: 20%;">Área de adscripción:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Area,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[10]->Visible == 1)
                                            <td style="width: 20%;">Empleo, cargo o comisión:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Puesto,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[8]->Visible == 1 || $secciones_formato[4]->Campos[5]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[8]->Visible == 1)
                                            <td style="width: 20%;">Lugar donde se ubica:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    @if($experiencia->EnExtranjero == 0)
                                                        EN MÉXICO
                                                    @else
                                                        EN EL EXTRANJERO
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[5]->Visible == 1)
                                            <td style="width: 20%;">Función principal:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->FuncionPrincipal,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[6]->Visible == 1 || $secciones_formato[4]->Campos[7]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[6]->Visible == 1)
                                            <td style="width: 20%;">Fecha de ingreso:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ $experiencia->FechaIngreso }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[7]->Visible == 1)
                                            <td style="width: 20%;">Fecha de término:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ $experiencia->FechaTermino }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                            </table>
                        </div>
                    @elseif($experiencia->AmbitoSector == 'Privado')
                        <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                            <table style="width: 100%">
                                @if($secciones_formato[4]->Campos[0]->Visible == 1 || $secciones_formato[4]->Campos[3]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[0]->Visible == 1)
                                            <td style="width: 20%;">Ámbito/Sector en el que laboraste:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->AmbitoSector,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[3]->Visible == 1)
                                            <td style="width: 20%;">Nombre de la empresa, sociedad o asociación:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Institucion,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[4]->Visible == 1 || $secciones_formato[4]->Campos[11]->Visible == 1)
                                    <tr style="font-size: 10px; font-style: italic;">
                                        @if($secciones_formato[4]->Campos[4]->Visible == 1)
                                            <td style="width: 20%;">RFC:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->RFCInstitucion,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[11]->Visible == 1)
                                            <td style="width: 20%; text-align: center;">Área:</td>
                                            <td style="width: 30%; text-align: center;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Area,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[10]->Visible == 1 || $secciones_formato[4]->Campos[9]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[10]->Visible == 1)
                                            <td style="width: 20%; text-align: center;">Puesto:</td>
                                            <td style="width: 30%; text-align: center;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Puesto,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[9]->Visible == 1)
                                            <td style="width: 20%; text-align: center;">Sector al que pertenece:</td>
                                            <td style="width: 30%; text-align: center;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->Sector,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[8]->Visible == 1 || $secciones_formato[4]->Campos[6]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[4]->Campos[8]->Visible == 1)
                                            <td style="width: 20%; text-align: center;">Lugar donde se ubica:</td>
                                            <td style="width: 30%; text-align: center;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    @if($experiencia->EnExtranjero == 0)
                                                        EN MÉXICO
                                                    @else
                                                        EN EL EXTRANJERO
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[4]->Campos[6]->Visible == 1)
                                            <td style="width: 20%; text-align: center;">Fecha de ingreso:</td>
                                            <td style="width: 30%; text-align: center;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($experiencia->FechaIngreso,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[4]->Campos[7]->Visible == 1)
                                    <tr style="font-size: 10px; font-style: italic;">
                                        <td style="width: 20%; text-align: center;">Fecha de término:</td>
                                        <td style="width: 30%; text-align: center;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($experiencia->FechaTermino,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    @else
                        <!-- AQUÍ VA TODO LO DE EL REGRISTO DE EXPERIENCIA EN UN AMBITO SOCIAL QUE NO ES PÚBLICO O PRIVADO -->
                        otro
                    @endif
                    <br>
                @endforeach
            @endif
        @endif
    @endif


    <!-- INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS -->
    @if($secciones_formato[7]->Aplica == 1)
        @if(in_array('EXPERIENCIA LABORAL',$indice) == true)
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">INGRESOS NETOS DEL DECLARANTE</span></h2>
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(2)
                    <div style="text-align: left;">Ingresos netos del declarante (Entre el 1 de enero y 31 diciembre del año inmediato anterior)</div>
                @break
                @case(5)
                    <div style="text-align: left;">Ingresos netos del declarante (Entre el 1 de enero y 31 diciembre del año inmediato anterior)</div>
                @break
                @default
                    <div style="text-align: left;">Ingresos netos del declarante (Situación actual)</div>
                @break
            @endswitch
            <br>
            <div style="text-align: left;">
                @if(count($secciones_formato[7]->Campos))
                    @if(isset($secciones_formato[7]->Campos[0]))
                        @if($secciones_formato[7]->Campos[0]->Visible == 1)
                        <table class="row-table" style="border: 1px solid;">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;">
                                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                                        @case(1)
                                            I. Remuneración mensual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                                            prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                        @case(2)
                                            I. Remuneración anual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                                            prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                        @case(3)
                                            I. Remuneración neta del año en curso a la fecha de conclusión del empleo, cargo o comisión del declarante por su cargo público (por concepto
                                            de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                        @case(4)
                                            I. Remuneración mensual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                                            prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                        @case(5)
                                            I. Remuneración anual neta del declarante por su cargo público (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                                            prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                        @case(6)
                                            I. Remuneración neta del año en curso a la fecha de conclusión del empleo, cargo o comisión del declarante por su cargo público (por concepto
                                            de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras prestaciones) (Cantidades netas después de impuestos)
                                        @break
                                    @endswitch
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta}}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[1]))
                        @if($secciones_formato[7]->Campos[1]->Visible == 1)
                        <table class="row-table" style="border: 1px solid;">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;" >
                                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                                        @case(1)
                                            II. Otros ingresos mensuales del declarante (Suma del II.1 al II.5)
                                        @break
                                        @case(4)
                                            II. Otros ingresos mensuales del declarante (Suma del II.1 al II.5)
                                        @break
                                        @default
                                            II. Otros ingresos del declarante (Suma del II.1 al II.5)
                                        @break
                                    @endswitch
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos}}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[2]))
                        @if($secciones_formato[7]->Campos[2]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.1 Por actividad industrial, comercial y/o empresarial
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                        <thead>
                                            <td style="width: 40%; text-align: left;">Nombre o razón social</td>
                                            <td style="width: 40%; text-align: left;">Tipo de negocio</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_industriales'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_industriales'] as $actividad_industrial)
                                                    <tr>
                                                        <td style="width: 40%; text-align: left;">
                                                            {{ mb_strtoupper($actividad_industrial->NombreRazonSocial,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 40%; text-align: left;">
                                                            {{ mb_strtoupper($actividad_industrial->TipoNegocio,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$actividad_industrial->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[3]))
                        @if($secciones_formato[7]->Campos[3]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.2 Por actividad financiera (rendimientos o ganancias) (después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de instrumento que generó el rendimiento o ganancia</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_financieras'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['actividades_financieras'] as $actividad_financiera)
                                                    <tr>
                                                        <td style="width: 80%; text-align: left;">
                                                            @if($actividad_financiera->IntrumentoID == 99)
                                                                {{ mb_strtoupper($actividad_financiera->OtroInstrumento,'UTF-8')}}
                                                            @else
                                                                {{ mb_strtoupper($actividad_financiera->TipoInstrumento,'UTF-8')}}
                                                            @endif
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$actividad_financiera->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[4]))
                        @if($secciones_formato[7]->Campos[4]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.3 Por servicios profesionales, consejos, consultorías, y/o asesorías (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de servicio prestado</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['servicios_profesionales'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['servicios_profesionales'] as $servicio_profesional)
                                                    <tr>
                                                        <td style="width: 80%; text-align: left;">
                                                            {{ mb_strtoupper($servicio_profesional->TipoServicioPrestado,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$servicio_profesional->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[5]))
                        @if($secciones_formato[7]->Campos[5]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.4 Por enajenación de bienes (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de bien enajenado</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['bienes_enajenados'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['bienes_enajenados'] as $otro_ingreso)
                                                    <tr>
                                                        <td style="width: 80%; text-align: left;">
                                                            {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$otro_ingreso->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[6]))
                        @if($secciones_formato[7]->Campos[6]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.5 Otros ingresos no considerados anteriormente (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Especificar tipo de ingreso (Arrendamiento, regalía, sorteos, concursos, donaciones, seguro de vida, etc.)</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['otros_ingresos'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['otros_ingresos']['otros_ingresos'] as $otro_ingreso)
                                                    <tr>
                                                        <td style="width: 80%; text-align: left;">
                                                            {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$otro_ingreso->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[7]))
                        @if($secciones_formato[7]->Campos[7]->Visible == 1)
                        <table class="row-table" style="border: 1px solid;">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;" >
                                    @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                                        @case(1)
                                            A.- Ingreso mensual neto del declarante (Suma del numeral I y II)
                                        @break
                                        @case(2)
                                            A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                                        @break
                                        @case(3)
                                            A.- Ingresos del declarante del año en curso a la fecha de conclusión del empleo, cargo o comisión (Suma del numeral I y II)
                                        @break
                                        @case(4)
                                            A.- Ingreso mensual neto del declarante (Suma del numeral I y II)
                                        @break
                                        @case(5)
                                            A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                                        @break
                                        @case(6)
                                            A.- Ingresos del declarante del año en curso a la fecha de conclusión del empleo, cargo o comisión (Suma del numeral I y II)
                                        @break
                                    @endswitch
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta +
                                        $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos
                                    }}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @endif
                    @if(isset($secciones_formato[7]->Campos[8]))
                        @if($secciones_formato[7]->Campos[8]->Visible == 1)
                    <table class="row-table" style="border: 1px solid;">
                        <tr>
                            <td style="width: 75%; text-align: left; font-weight:bold;" >
                            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                                @case(1)
                                    C.- Total de ingresos mensuales netos percibidos por el declarante
                                @break
                                @case(2)
                                    C.- Total de ingresos anuales netos percibidos por el declarante
                                @break
                                @case(3)
                                    C.- Total de ingresos del año en curso a la fecha de conclusión del empleo, cargo o comisión percibidos por el declarante
                                @break
                                @case(4)
                                    C.- Total de ingresos mensuales netos percibidos por el declarante
                                @break
                                @case(5)
                                    C.- Total de ingresos anuales netos percibidos por el declarante
                                @break
                                @case(6)
                                    C.- Total de ingresos del año en curso a la fecha de conclusión del empleo, cargo o comisión percibidos por el declarante
                                @break
                            @endswitch
                            </td>
                            <td style="width: 25%; text-align: center; font-weight:bold;">
                                {{$secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->RemuneracionMensualNeta +
                                    $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->TotalOtrosIngresos +
                                    $secciones_pdf["INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS"]->Info['ingresos']->IngNetoParejaDependientes
                                }}
                            </td>
                        </tr>
                    </table>
                @endif
                    @endif
                @endif
            </div>
        @endif
    @endif


    <!-- DESEMPEÑO COMO SERVIDOR EL AÑO PASADO -->
    @if($secciones_formato[8]->Aplica == 1)
        @if (in_array('DESEMPEÑO COMO SERVIDOR EL AÑO PASADO',$indice) == true)
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">DESEMPEÑO COMO SERVIDOR EL AÑO PASADO</span></h2>
            @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info == 'No aplica')
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO SE DESEMPEÑÓ COMO SERVIDOR PÚBLICO EL AÑO PASADO
                </div>
                <br>
                <br>
            @else
                <div style="text-align: left;">
                    Ingresos netos, recibidos durante el tiempo en el que se desempeñó como servidor público el año inmediato anterior
                </div>
                <br>
                <div style="text-align: left;">
                    @if($secciones_formato[8]->Campos[0]->Visible == 1 || $secciones_formato[8]->Campos[1]->Visible == 1)
                        <table class="row-table" style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;">
                            <tr>
                                @if($secciones_formato[8]->Campos[0]->Visible == 1)
                                    <td style="width: 50%; text-align: left; font-weight:bold;">
                                        Fecha de inicio: {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->FechaInicio}}
                                    </td>
                                @endif
                                @if($secciones_formato[8]->Campos[1]->Visible == 1)
                                    <td style="width: 50%; text-align: left; font-weight:bold;">
                                        Fecha de conclusión: {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->FechaConclusion}}
                                    </td>
                                @endif
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[2]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; ">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;">
                                    I. Remuneración neta del declarante, recibida durante el tiempo en el que se desempeñó como servidor público en el año inmediato anterior (por concepto de sueldos, honorarios, compensaciones, bonos, aguinaldos y otras
                                    prestaciones) (Cantidades netas después de impuestos)
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta }}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[1]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; ">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;" >
                                    II. Otros ingresos del declarante, recibidos durante el tiempo en el que se desempeñó como servidor público en el año inmediato anterior (Suma del II.1 al II.5)
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos}}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[3]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.1 Por actividad industrial, comercial y/o empresarial
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid;">
                                        <thead>
                                            <td style="width: 40%; text-align: left;">Nombre o razón social</td>
                                            <td style="width: 40%; text-align: left;">Tipo de negocio</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border: 1px solid; ">
                                        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_industriales'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGUN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_industriales'] as $actividad_industrial)
                                                    <tr>
                                                        <td style="width: 40%; text-align: left;">
                                                            {{ mb_strtoupper($actividad_industrial->NombreRazonSocial,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 40%; text-align: left;">
                                                            {{ mb_strtoupper($actividad_industrial->TipoNegocio,'UTF-8')}}
                                                        </td>
                                                        <td style="width: 20%; text-align: left;">
                                                            {{$actividad_industrial->Monto}}
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[4]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.2 Por actividad financiera (rendimientos o ganancias) (después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de instrumento que generó el rendimiento o ganancia</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border: 1px solid; ">
                                        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_financieras'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['actividades_financieras'] as $actividad_financiera)
                                                <tr>
                                                    <td style="width: 80%; text-align: left;">
                                                        @if($actividad_financiera->IntrumentoID == 99)
                                                            {{ mb_strtoupper($actividad_financiera->OtroInstrumento,'UTF-8')}}
                                                        @else
                                                            {{ mb_strtoupper($actividad_financiera->TipoInstrumento,'UTF-8')}}
                                                        @endif
                                                    </td>
                                                    <td style="width: 20%; text-align: left;">
                                                        {{$actividad_financiera->Monto}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[5]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.3 Por servicios profesionales, consejos, consultorías, y/o asesorías (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de servicio prestado</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border: 1px solid; ">
                                        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['servicios_profesionales'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['servicios_profesionales'] as $servicio_profesional)
                                                <tr>
                                                    <td style="width: 80%; text-align: left;">
                                                        {{ mb_strtoupper($servicio_profesional->TipoServicioPrestado,'UTF-8')}}
                                                    </td>
                                                    <td style="width: 20%; text-align: left;">
                                                        {{$servicio_profesional->Monto}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[6]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.4 Por enajenación de bienes (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Tipo de bien enajenado</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border: 1px solid; ">
                                        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['bienes_enajenados'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['bienes_enajenados'] as $otro_ingreso)
                                                <tr>
                                                    <td style="width: 80%; text-align: left;">
                                                        {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                    </td>
                                                    <td style="width: 20%; text-align: left;">
                                                        {{$otro_ingreso->Monto}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[7]->Visible == 1)
                        <table class="row-table" style="border: 1px solid; padding-left: 20px;">
                            <tr>
                                <td style="width: 75%; text-align: left;">
                                    II.5 Otros ingresos no considerados anteriormente (Después de impuestos)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid;">
                                        <thead>
                                            <td style="width: 80%; text-align: left;">Especificar tipo de ingreso (Arrendamiento, regalía, sorteos, concursos, donaciones, seguro de vida, etc.)</td>
                                            <td style="width: 20%; text-align: left;">Monto</td>
                                        </thead>
                                    </table>
                                    <table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; ">
                                        @if($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['otros_ingresos'] == [])
                                            <tr>
                                                <td style="text-align: center;">
                                                    NO DECLARÓ NINGÚN INGRESO DE ESTE TIPO
                                                </td>
                                            </tr>
                                        @else
                                            @foreach($secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['otros_ingresos']['otros_ingresos'] as $otro_ingreso)
                                                <tr>
                                                    <td style="width: 80%; text-align: left;">
                                                        {{ mb_strtoupper($otro_ingreso->TipoBien,'UTF-8')}}
                                                    </td>
                                                    <td style="width: 20%; text-align: left;">
                                                        {{$otro_ingreso->Monto}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[8]->Visible == 1)
                        <table class="row-table" style="border: 1px solid;">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;" >
                                    A.- Ingreso anual neto del declarante (Suma del numeral I y II)
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta +
                                        $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos
                                    }}
                                </td>
                            </tr>
                        </table>
                    @endif
                    @if($secciones_formato[8]->Campos[9]->Visible == 1)
                        <table class="row-table" style="border: 1px solid;">
                            <tr>
                                <td style="width: 75%; text-align: left; font-weight:bold;" >
                                    C.- Total de ingresos anuales netos percibidos por el declarante
                                </td>
                                <td style="width: 25%; text-align: center; font-weight:bold;">
                                    {{$secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->RemuneracionNeta +
                                        $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->TotalOtrosIngresos +
                                        $secciones_pdf["DESEMPEÑO COMO SERVIDOR EL AÑO PASADO"]->Info['ingresos']->IngNetoParejaDependientes
                                    }}
                                </td>
                            </tr>
                        </table>
                    @endif
                </div>
            @endif
        @endif
    @endif


    <!-- BIENES INMUEBLES -->
    @if($secciones_formato[9]->Aplica == 1)
        @if (in_array('BIENES INMUEBLES',$indice) == true)
            <div style="page-break-after:always;"></div>
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(1)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES INMUEBLES (SITUACIÓN ACTUAL)</span></h2>
                @break
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES INMUEBLES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @case(3)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES INMUEBLES (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            <div>
                BIENES DEL DECLARANTE
            </div>
            @if($secciones_pdf["BIENES INMUEBLES"]->Info == [])
                <div style="border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ BIEN INMUEBLE ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["BIENES INMUEBLES"]->Info as $bien_inmueble)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%;">
                            @if($secciones_formato[9]->Campos[0]->Visible == 1 || $secciones_formato[9]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[9]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo inmueble:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->TipoInmueble,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[9]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Titular inmueble:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        <br>
                        @if($secciones_formato[9]->Campos[2]->Visible == 1 || $secciones_formato[9]->Campos[3]->Visible == 1)
                            <tr>
                                @if($secciones_formato[9]->Campos[2]->Visible == 1)
                                    <td style="width: 20%;">Porcentaje de propiedad del declarante conforme a escrituración o contrato:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            @if($bien_inmueble->PorcentajePropiedad == null)
                                                EL TITULAR TIENE LA TOTALIDAD DE LA PROPIEDAD
                                            @else
                                                {{ mb_strtoupper($bien_inmueble->PorcentajePropiedad,'UTF-8') }}
                                            @endif
                                        </div>
                                    </td>
                                @endif
                                @if($secciones_formato[9]->Campos[3]->Visible == 1)
                                    <td style="width: 20%;">Superficie del terreno (en metros cuadrados):</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->SuperficieTerreno,'UTF-8') }}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endif
                        @if($secciones_formato[9]->Campos[4]->Visible == 1 || $secciones_formato[9]->Campos[9]->Visible == 1)
                            <tr>
                                @if($secciones_formato[9]->Campos[4]->Visible == 1)
                                    <td style="width: 20%;">Superficie de construcción (en metros cuadrados):</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->SuperficieConstruida,'UTF-8') }}
                                        </div>
                                    </td>
                                @endif
                                @if($secciones_formato[9]->Campos[9]->Visible == 1)
                                    <td style="width: 20%;">Forma de adquisición:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->FormaAdquisicion,'UTF-8') }}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endif
                        @if($secciones_formato[9]->Campos[10]->Visible == 1 || $secciones_formato[9]->Campos[14]->Visible == 1)
                            <tr>
                                @if($secciones_formato[9]->Campos[10]->Visible == 1)
                                    <td style="width: 20%;">Forma de pago:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->FormaPago,'UTF-8') }}
                                        </div>
                                    </td>
                                @endif
                                @if($secciones_formato[9]->Campos[14]->Visible == 1)
                                    <td style="width: 20%;">Valor de adquisición:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->Valor,'UTF-8') }}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endif
                            @if($secciones_formato[9]->Campos[15]->Visible == 1 || $secciones_formato[9]->Campos[18]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[9]->Campos[15]->Visible == 1)
                                        <td style="width: 20%;">Tipo de moneda:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->TipoMoneda,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[9]->Campos[18]->Visible == 1)
                                        <td style="width: 20%;">El valor de adquisición del inmueble es conforme a:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->ValorConformeA,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[9]->Campos[16]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Fecha de adquisición:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($bien_inmueble->FechaOperacion,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </table>
                        <br>
                        @if($bien_inmueble->MotivoBaja != null && $bien_inmueble->MotivoBaja != ' ')
                            <table style="width: 100%;">
                                <tr>
                                    @if($secciones_formato[9]->Campos[20]->Visible == 1)
                                        <td style="width: 50%;">Motivo de baja del inmueble:</td>
                                        <td style="width: 50%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_inmueble->MotivoBaja,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            </table>
                        @endif
                        @if($bien_inmueble->NombreTercero != null and $bien_inmueble->Titular != 'DECLARANTE')
                            @if($bien_inmueble->TipoPersonaTercero != 1)
                                <br>
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                    @if($secciones_formato[9]->Campos[6]->Visible == 1 || $secciones_formato[9]->Campos[7]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[9]->Campos[6]->Visible == 1)
                                                <td style="width: 20%;">Tipo persona legal del tercero:</td>
                                                <td style="width: 30%; border-bottom: .1px solid;">
                                                    PERSONA MORAL
                                                </td>
                                            @endif
                                            @if($secciones_formato[9]->Campos[7]->Visible == 1)
                                                <td style="width: 20%;">Nombre del tercero:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($bien_inmueble->NombreTercero,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @if($secciones_formato[9]->Campos[8]->Visible == 1)
                                        <tr>
                                            <td style="width: 20%;">RFC del tercero:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($bien_inmueble->RFCTercero,'UTF-8') }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                        @if($bien_inmueble->NombreTransmisor != null && $bien_inmueble->TipoPersonaTransmisor == 1 || $secciones_formato[9]->Campos[13]->Visible == 1 || $secciones_formato[9]->Campos[5]->Visible == 1)
                            <br>
                            @if($secciones_formato[9]->Campos[11]->Visible == 1 || $secciones_formato[9]->Campos[12]->Visible == 1)
                                <div style="border: 1px solid;">
                                    <table class="row-table" style=" width: 100%;">
                                        <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DE LA PROPIEDAD</span></h3>
                                        @if($secciones_formato[9]->Campos[11]->Visible == 1 || $secciones_formato[9]->Campos[12]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[9]->Campos[11]->Visible == 1)
                                                    <td style="width: 20%;">Tipo persona legal del transmisor:</td>
                                                    <td style="width: 30%; border-bottom: .1px solid;">
                                                        PERSONA MORAL
                                                    </td>
                                                @endif
                                                @if($secciones_formato[9]->Campos[12]->Visible == 1)
                                                    <td style="width: 20%;">Nombre del transmisor de la propiedad:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($bien_inmueble->NombreTransmisor,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                        @if($secciones_formato[9]->Campos[13]->Visible == 1 || $secciones_formato[9]->Campos[5]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[9]->Campos[13]->Visible == 1)
                                                    <td style="width: 20%;">RFC del transmisor de propiedad:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($bien_inmueble->RFCTransmisor,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                                @php
                                                    $relaciones_no_mostrables = array(
                                                        "Abuelo (a)",
                                                        "Bisabuelo (a)",
                                                        "Bisnieto (a)",
                                                        "Concubino (a) / Concubinario / Unión libre",
                                                        "Concuño (a)",
                                                        "Cuñado (a)",
                                                        "Cónyuge",
                                                        "Hermano (a)",
                                                        "Hijo (a)",
                                                        "Madre",
                                                        "Padre",
                                                        "Primo (a)",
                                                        "Sobrino (a)",
                                                        "Suegro (a)",
                                                        "Tatarabuelo (a)",
                                                        "Tataranieto (a)",
                                                        "Tío (a)"
                                                    );
                                                @endphp
                                                @if(in_array($bien_inmueble->RelacionTransmisor,$relaciones_no_mostrables) == false)
                                                    @if($secciones_formato[9]->Campos[5]->Visible == 1)
                                                        <td style="width: 20%;">Tipo de relación con el titular:</td>
                                                        <td style="width: 30%;">
                                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                                {{ mb_strtoupper($bien_inmueble->RelacionTransmisor,'UTF-8') }}
                                                            </div>
                                                        </td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            @endif
                        @endif
                    </div>
                @endforeach
                <br>
                <br>
            @endif
        @endif
    @endif


    <!-- VEHÍCULOS -->
    @if($secciones_formato[10]->Aplica == 1)
        @if (in_array('VEHÍCULOS',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(1)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">VEHÍCULOS (SITUACIÓN ACTUAL)</span></h2>
                @break
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">VEHÍCULOS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @case(3)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">VEHÍCULOS (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            <div>
                VEHÍCULOS DEL DECLARANTE
            </div>
            @if($secciones_pdf["VEHÍCULOS"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ VEHÍCULO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["VEHÍCULOS"]->Info as $vehiculo)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%;">
                            @if($secciones_formato[10]->Campos[0]->Visible == 1 || $secciones_formato[10]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[10]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo Vehículo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->TipoVehiculo,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[10]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Titular:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        </table>
                        <table style="width: 100%;">
                            @if($secciones_formato[10]->Campos[6]->Visible == 1 || $secciones_formato[10]->Campos[7]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[10]->Campos[6]->Visible == 1)
                                        <td style="width: 20%;">Marca:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->VehiculoMarca,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[10]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Modelo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->VehiculoModelo,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[10]->Campos[8]->Visible == 1 || $secciones_formato[10]->Campos[12]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[10]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Año:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->VehiculoAnio,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[10]->Campos[12]->Visible == 1)
                                        <td style="width: 20%;">Forma de adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->FormaAdquisicion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[10]->Campos[13]->Visible == 1 || $secciones_formato[10]->Campos[17]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[10]->Campos[13]->Visible == 1)
                                        <td style="width: 20%;">Forma de pago:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->FormaPago,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[10]->Campos[17]->Visible == 1)
                                        <td style="width: 20%;">Valor de adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->Valor,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[10]->Campos[14]->Visible == 1 || $secciones_formato[10]->Campos[15]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[10]->Campos[14]->Visible == 1)
                                        <td style="width: 20%;">Tipo de moneda:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->TipoMoneda,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[10]->Campos[15]->Visible == 1)
                                        <td style="width: 20%;">Fecha de adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($vehiculo->FechaOperacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[10]->Campos[16]->Visible == 1)
                                <tr>
                                    @if(is_null($vehiculo->MotivoBaja) == false)
                                        <td style="width: 20%; text-align: left; font-weight:bold;" >Motivo de baja:</td>
                                        <td style="width: 30%;">
                                            {{ mb_strtoupper($vehiculo->MotivoBaja,'UTF-8') }}
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        </table>
                        @if($vehiculo->NombreRazonSocial != null && $vehiculo->TipoPersonaTransmisor != 0)
                            <br>
                            @if($secciones_formato[10]->Campos[2]->Visible == 1 || $secciones_formato[10]->Campos[3]->Visible == 1 || $secciones_formato[10]->Campos[4]->Visible == 1 || $secciones_formato[10]->Campos[5]->Visible == 1)
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DEL VEHÍCULO</span></h3>
                                    @if($secciones_formato[10]->Campos[2]->Visible == 1 || $secciones_formato[10]->Campos[3]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[10]->Campos[2]->Visible == 1)
                                                <td style="width: 20%;">Tipo persona legal del transmisor:</td>
                                                <td style="width: 30%; border-bottom: .1px solid;">
                                                    PERSONA MORAL
                                                </td>
                                            @endif
                                            @if($secciones_formato[10]->Campos[3]->Visible == 1)
                                                <td style="width: 20%;">Nombre del transmisor de la propiedad:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($vehiculo->NombreRazonSocial,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @if($secciones_formato[10]->Campos[4]->Visible == 1 || $secciones_formato[10]->Campos[5]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[10]->Campos[4]->Visible == 1)
                                                <td style="width: 20%;">RFC del transmisor de propiedad:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($vehiculo->RFCTRansmisor,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                            @php
                                                $relaciones_no_mostrables = array(
                                                    "Abuelo (a)",
                                                    "Bisabuelo (a)",
                                                    "Bisnieto (a)",
                                                    "Concubino (a) / Concubinario / Unión libre",
                                                    "Concuño (a)",
                                                    "Cuñado (a)",
                                                    "Cónyuge",
                                                    "Hermano (a)",
                                                    "Hijo (a)",
                                                    "Madre",
                                                    "Padre",
                                                    "Primo (a)",
                                                    "Sobrino (a)",
                                                    "Suegro (a)",
                                                    "Tatarabuelo (a)",
                                                    "Tataranieto (a)",
                                                    "Tío (a)"
                                                );
                                            @endphp
                                            @if($secciones_formato[10]->Campos[5]->Visible == 1)
                                                @if(in_array($vehiculo->RelacionTransmisor,$relaciones_no_mostrables) == false)
                                                    <td style="width: 20%;">Tipo de relación con el titular:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($vehiculo->RelacionTransmisor,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            @endif
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                        @if($vehiculo->NombreTercero != null && $vehiculo->TipoPersonaTercero != 0)
                            <br>
                            @if($secciones_formato[10]->Campos[9]->Visible == 1 || $secciones_formato[10]->Campos[9]->Visible == 1 || $secciones_formato[10]->Campos[11]->Visible == 1)
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                    <tr>
                                        @if($secciones_formato[10]->Campos[9]->Visible == 1)
                                            <td style="width: 20%;">Tipo persona legal del tercero:</td>
                                            <td style="width: 30%; border-bottom: .1px solid;">
                                                PERSONA MORAL
                                            </td>
                                        @endif
                                        @if($secciones_formato[10]->Campos[10]->Visible == 1)
                                            <td style="width: 20%;">Nombre del tercero:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($vehiculo->NombreTercero,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                    @if($secciones_formato[10]->Campos[11]->Visible == 1)
                                        <tr>
                                            <td style="width: 20%;">RFC del tercero:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($vehiculo->RFCTercero,'UTF-8') }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                    </div>
                @endforeach
                <br>
                <br>
            @endif
        @endif
    @endif


    <!-- BIENES MUEBLES -->
    @if($secciones_formato[11]->Aplica == 1)
        @if(in_array('BIENES MUEBLES',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(1)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES MUEBLES (SITUACIÓN ACTUAL)</span></h2>
                @break
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES MUEBLES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @case(3)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BIENES MUEBLES (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            <div>
                BIENES DEL DECLARANTE
            </div>
            @if($secciones_pdf["BIENES MUEBLES"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ BIEN MUEBLE ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["BIENES MUEBLES"]->Info as $bien_mueble)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%;">
                            @if($secciones_formato[11]->Campos[0]->Visible == 1 || $secciones_formato[11]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[11]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo del bien:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->TipoBien,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[11]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Titular:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[11]->Campos[7]->Visible == 1 || $secciones_formato[11]->Campos[8]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[11]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Descripción:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->DescripcionBien,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[11]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Forma de adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->TipoAdquisicion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[11]->Campos[9]->Visible == 1 || $secciones_formato[11]->Campos[10]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[11]->Campos[9]->Visible == 1)
                                        <td style="width: 20%;">Forma de pago::</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->DescripcionBien,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[11]->Campos[10]->Visible == 1)
                                        <td style="width: 20%;">Valor adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->Valor,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[11]->Campos[11]->Visible == 1 || $secciones_formato[11]->Campos[12]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[11]->Campos[11]->Visible == 1)
                                        <td style="width: 20%;">Tipo moneda:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->TipoMoneda,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[11]->Campos[12]->Visible == 1)
                                        <td style="width: 20%;">Fecha adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($bien_mueble->FechaOperacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($bien_mueble->MotivoBaja != null)
                            <table style="width: 100%;">
                                @if($secciones_formato[11]->Campos[13]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-weight:bold;" >Motivo de baja:</td>
                                        <td style="width: 30%;">
                                            {{ mb_strtoupper($bien_mueble->MotivoBaja,'UTF-8') }}
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        @endif
                        </table>
                        @if($bien_mueble->NombreRazonSocial != null && $bien_mueble->TipoPersonaTransmisor == 2)
                            @if($secciones_formato[11]->Campos[14]->Visible == 1 || $secciones_formato[11]->Campos[2]->Visible == 1 || $secciones_formato[11]->Campos[15]->Visible == 1 || $secciones_formato[11]->Campos[6]->Visible == 1)
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TRANSMISOR DEL BIEN MUEBLE</span></h3>
                                    @if($secciones_formato[11]->Campos[14]->Visible == 1 || $secciones_formato[11]->Campos[2]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[11]->Campos[14]->Visible == 1)
                                                <td style="width: 20%;">Tipo persona legal del transmisor:</td>
                                                <td style="width: 30%; border-bottom: .1px solid;">
                                                    PERSONA MORAL
                                                </td>
                                            @endif
                                            @if($secciones_formato[11]->Campos[2]->Visible == 1)
                                                <td style="width: 20%;">Nombre del transmisor de la propiedad:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($bien_mueble->NombreRazonSocial,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @if($secciones_formato[11]->Campos[15]->Visible == 1 || $secciones_formato[11]->Campos[6]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[11]->Campos[15]->Visible == 1)
                                                <td style="width: 20%;">RFC del transmisor de propiedad:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($bien_mueble->RFCTRansmisor,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                            @php
                                                $relaciones_no_mostrables = array(
                                                    "Abuelo (a)",
                                                    "Bisabuelo (a)",
                                                    "Bisnieto (a)",
                                                    "Concubino (a) / Concubinario / Unión libre",
                                                    "Concuño (a)",
                                                    "Cuñado (a)",
                                                    "Cónyuge",
                                                    "Hermano (a)",
                                                    "Hijo (a)",
                                                    "Madre",
                                                    "Padre",
                                                    "Primo (a)",
                                                    "Sobrino (a)",
                                                    "Suegro (a)",
                                                    "Tatarabuelo (a)",
                                                    "Tataranieto (a)",
                                                    "Tío (a)"
                                                );
                                            @endphp
                                            @if(in_array($bien_mueble->RelacionTransmisor,$relaciones_no_mostrables) == false)
                                                @if($secciones_formato[11]->Campos[6]->Visible == 1)
                                                    <td style="width: 20%;">Tipo de relación con el titular:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($bien_mueble->RelacionTransmisor,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            @endif
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                        @if($bien_mueble->NombreTercero != null && $bien_mueble->TipoPersonaTercero == 1)
                            @if($secciones_formato[11]->Campos[3]->Visible == 1 || $secciones_formato[11]->Campos[4]->Visible == 1 || $secciones_formato[11]->Campos[5]->Visible == 1)
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                    <tr>
                                        @if($secciones_formato[11]->Campos[3]->Visible == 1)
                                            <td style="width: 20%;">Tipo persona legal del tercero:</td>
                                            <td style="width: 30%; border-bottom: .1px solid;">
                                                PERSONA MORAL
                                            </td>
                                        @endif
                                        @if($secciones_formato[11]->Campos[4]->Visible == 1)
                                            <td style="width: 20%;">Nombre del tercero:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($bien_mueble->NombreTercero,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                    @if($secciones_formato[11]->Campos[5]->Visible == 1)
                                        <tr>
                                            <td style="width: 20%;">RFC del tercero:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($bien_mueble->RFCTercero,'UTF-8') }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                    </div>
                @endforeach
                <br>
                <br>
            @endif
        @endif
    @endif


    <!-- INVERSIONES -->
    @if($secciones_formato[12]->Aplica == 1)
        @if(in_array('INVERSIONES',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">INVERSIONES (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @default
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">INVERSIONES (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            <div>
                INVERSIONES, CUENTAS BANCARIAS Y OTRO TIPO DE VALORES DEL DECLARANTE
            </div>
            @if($secciones_pdf["INVERSIONES"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ INVERSIÓN ALGUNA EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["INVERSIONES"]->Info as $inversion)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%;">
                            @if($secciones_formato[12]->Campos[0]->Visible == 1 || $secciones_formato[12]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[12]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($inversion->TipoInversion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[12]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Titular de la inversión:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($inversion->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[12]->Campos[5]->Visible == 1 || $secciones_formato[9]->Campos[9]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[12]->Campos[5]->Visible == 1)
                                        <td style="width: 20%; ">Activo:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($inversion->TipoInstrumento,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[12]->Campos[9]->Visible == 1)
                                        <td style="width: 20%; ">Tipo de moneda:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($inversion->Moneda,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($inversion->NombreTercero != null && $inversion->TipoPersonaTercero == 1)
                                <br>
                                @if( $secciones_formato[12]->Campos[5]->Visible == 1 || $secciones_formato[3]->Campos[3]->Visible == 1 || $secciones_formato[12]->Campos[4]->Visible == 1)
                                    <table class="row-table" style="border: 1px solid; width: 100%;">
                                        <h3 style="font-size: 11px; "><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                        @if( $secciones_formato[12]->Campos[5]->Visible == 1 || $secciones_formato[3]->Campos[3]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[12]->Campos[5]->Visible == 1)
                                                    <td style="width: 20%; ">Tipo persona legal del tercero:</td>
                                                    <td style="width: 30%;  border-bottom: .1px solid;">
                                                        PERSONA MORAL
                                                    </td>
                                                @endif
                                                @if($secciones_formato[12]->Campos[3]->Visible == 1)
                                                    <td style="width: 20%; ">Nombre del tercero:</td>
                                                    <td style="width: 30%; ">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($inversion->NombreTercero,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                        @if($secciones_formato[12]->Campos[4]->Visible == 1)
                                            <tr>
                                                <td style="width: 20%; ">RFC del tercero:</td>
                                                <td style="width: 30%; ">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($inversion->RFCTercero,'UTF-8') }}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                @endif
                                <br>
                            @endif
                            @if($inversion->Institucion != null)
                                @if($secciones_formato[12]->Campos[8]->Visible == 1 || $secciones_formato[12]->Campos[6]->Visible == 1 || $secciones_formato[12]->Campos[7]->Visible == 1)
                                    <table class="row-table" style="border: 1px solid; width: 100%;">
                                        <h3 style="font-size: 11px; "><span style="padding: 5px;">DATOS DE LA INSTITUCIÓN CON DONDE SE REALIZA LA INVERSIÓN</span></h3>
                                        @if($secciones_formato[12]->Campos[8]->Visible == 1 || $secciones_formato[12]->Campos[6]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[12]->Campos[8]->Visible == 1)
                                                    <td style="width: 20%; ">Institución o razón social:</td>
                                                    <td style="width: 30%; ">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($inversion->Institucion,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                                @if($secciones_formato[12]->Campos[6]->Visible == 1)
                                                    <td style="width: 20%; ">RFC:</td>
                                                    <td style="width: 30%; ">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($inversion->RFCIntitucion,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                        @if($secciones_formato[12]->Campos[7]->Visible == 1)
                                            <tr>
                                                <td style="width: 20%; ">País de localización:</td>
                                                <td style="width: 30%; ">
                                                    @if($inversion->EsExtranjero == 0)
                                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                            MÉXICO
                                                        </div>
                                                    @else
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($inversion->Ubicacion,'UTF-8') }}
                                                    </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                @endif
                                <br>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- ADEUDOS/PASIVOS -->
    @if($secciones_formato[13]->Aplica == 1)
        @if(in_array('ADEUDOS/PASIVOS',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">ADEUDOS/PASIVOS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @default
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">ADEUDOS/PASIVOS (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            <div>
                ADEUDOS/PASIVOS DEL DECLARANTE
            </div>
            @if($secciones_pdf["ADEUDOS/PASIVOS"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ ADEUDO O PASIVO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["ADEUDOS/PASIVOS"]->Info as $adeudo)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[13]->Campos[1]->Visible == 1 || $secciones_formato[13]->Campos[0]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[13]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Tipo de adeudo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($adeudo->TipoAdeudo,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[13]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Titular del adeudo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($adeudo->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[13]->Campos[3]->Visible == 1 || $secciones_formato[13]->Campos[4]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[13]->Campos[3]->Visible == 1)
                                        <td style="width: 20%;">Fecha de adquisición:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($adeudo->FechaOtorgamiento,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[13]->Campos[4]->Visible == 1)
                                        <td style="width: 20%;">Monto original del adeudo / pasivo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($adeudo->MontoOriginal,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[13]->Campos[5]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Tipo de moneda:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($adeudo->Moneda,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </table>
                        @if($adeudo->NombreTercero != null)
                            @if($adeudo->TipoPersonaTercero == 0)
                                <br>
                                @if($secciones_formato[13]->Campos[12]->Visible == 1 || $secciones_formato[13]->Campos[6]->Visible == 1 || $secciones_formato[13]->Campos[8]->Visible == 1)
                                    <table class="row-table" style="border: 1px solid; width: 100%;">
                                        <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL TERCERO EN COPROPIEDAD</span></h3>
                                        @if($secciones_formato[13]->Campos[12]->Visible == 1 || $secciones_formato[13]->Campos[6]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[13]->Campos[12]->Visible == 1)
                                                    <td style="width: 20%;">Tipo persona legal del tercero:</td>
                                                    <td style="width: 30%; border-bottom: .1px solid;">
                                                        PERSONA FISÍCA
                                                    </td>
                                                @endif
                                                @if($secciones_formato[13]->Campos[6]->Visible == 1)
                                                    <td style="width: 20%;">Nombre del tercero:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($adeudo->NombreTercero,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                        @if($secciones_formato[13]->Campos[8]->Visible == 1)
                                            <tr>
                                                <td style="width: 20%;">RFC del tercero:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($adeudo->RFCTercero,'UTF-8') }}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                @endif
                                <br>
                            @endif
                        @endif
                        @if($adeudo->TipoPersonaOtorgante != 0)
                            @if($secciones_formato[13]->Campos[13]->Visible == 1 || $secciones_formato[13]->Campos[9]->Visible == 1 || $secciones_formato[13]->Campos[10]->Visible == 1)
                                <table class="row-table" style="border: 1px solid; width: 100%;">
                                    <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL OTORGANTE</span></h3>
                                    @if($secciones_formato[13]->Campos[13]->Visible == 1 || $secciones_formato[13]->Campos[9]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[13]->Campos[13]->Visible == 1)
                                                <td style="width: 20%;">Tipo persona legal:</td>
                                                <td style="width: 30%; border-bottom: .1px solid;">
                                                    PERSONA MORAL
                                                </td>
                                            @endif
                                            @if($secciones_formato[13]->Campos[9]->Visible == 1)
                                                <td style="width: 20%;">Nombre/ Institución o razón social:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($adeudo->NombreRazonSocial,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @if($secciones_formato[13]->Campos[10]->Visible == 1)
                                        <tr>
                                            <td style="width: 20%;">RFC:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($adeudo->RFCOtorgante,'UTF-8') }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            @endif
                        @endif
                    </div>
                @endforeach
            @endif
        @endif
    @endif

    <!-- PRESTAMO O COMODATO POR TERCEROS -->
    @if($secciones_formato[14]->Aplica == 1)
        @if(in_array('PRESTAMO O COMODATO POR TERCEROS',$indice) == true)
            @switch($secciones_pdf["DATOS GENERALES"]->InfoDeclaracion->TipoDeclaracionID)
                @case(2)
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">PRÉSTAMO O COMODATO POR TERCEROS (ENTRE EL 1 DE ENERO Y EL 31 DE DICIEMBRE DEL AÑO INMEDIATO ANTERIOR)</span></h2>
                @break
                @default
                    <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">PRÉSTAMO O COMODATO POR TERCEROS (SITUACIÓN ACTUAL)</span></h2>
                @break
            @endswitch
            @if($secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ PRESTAMO O COMODATO POR TERCEROS EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["PRESTAMO O COMODATO POR TERCEROS"]->Info as $prestamo)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        @if($prestamo->TipoDeBien == 1)
                            <table style="width: 100%">
                                <tr>
                                    @if($secciones_formato[14]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo de bien:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                INMUEBLE
                                            </div>
                                        </td>
                                    @endif
                                    <td style="width: 20%;">Especificación del bien:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->BienInmueble,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; font-style: italic;">
                                </tr>
                            </table>
                        @else
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 20%;">Tipo de bien:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            VEHÍCULO
                                        </div>
                                    </td>
                                    <td style="width: 20%;">Especificación del bien:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($prestamo->BienMueble,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            @if($secciones_formato[14]->Campos[1]->Visible == 1 || $secciones_formato[14]->Campos[2]->Visible == 1 || $secciones_formato[14]->Campos[3]->Visible == 1)
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">Datos del vehículo</span></h3>
                                <div style="border: 1px solid">
                                    <table style="width: 100%">
                                        @if($secciones_formato[14]->Campos[1]->Visible == 1 || $secciones_formato[14]->Campos[2]->Visible == 1)
                                            <tr>
                                                @if($secciones_formato[14]->Campos[1]->Visible == 1)
                                                    <td style="width: 20%;">Marca:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($prestamo->Marca,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                                @if($secciones_formato[14]->Campos[2]->Visible == 1)
                                                    <td style="width: 20%;">Modelo:</td>
                                                    <td style="width: 30%;">
                                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                            {{ mb_strtoupper($prestamo->Modelo,'UTF-8') }}
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                        @if($secciones_formato[14]->Campos[3]->Visible == 1)
                                            <tr>
                                                <td style="width: 20%;">Año:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($prestamo->Anio,'UTF-8') }}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                    <br>
                                </div>
                            @endif
                        @endif
                        @if($prestamo->TipoPersonaTitular == 2)
                            <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">Datos del dueño o titular</span></h3>
                            <div style="border: 1px solid">
                                <table style="width: 100%">
                                    @if($secciones_formato[14]->Campos[4]->Visible == 1 || $secciones_formato[14]->Campos[4]->Visible == 1 )
                                        <tr>
                                            @if($secciones_formato[14]->Campos[4]->Visible == 1)
                                                <td style="width: 20%;">Tipo persona legal:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        PERSONA MORAL
                                                    </div>
                                                </td>
                                            @endif
                                            @if($secciones_formato[14]->Campos[5]->Visible == 1)
                                                <td style="width: 20%;">Nombre:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($prestamo->Titular,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @if($secciones_formato[14]->Campos[6]->Visible == 1 || $secciones_formato[14]->Campos[5]->Visible == 1)
                                        <tr>
                                            @if($secciones_formato[14]->Campos[6]->Visible == 1)
                                                <td style="width: 20%;">RFC:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($prestamo->RFCTitular,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                            @if($secciones_formato[12]->Campos[5]->Visible == 1)
                                                <td style="width: 20%;">Relación con el dueño o titular:</td>
                                                <td style="width: 30%;">
                                                    <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                        {{ mb_strtoupper($prestamo->Vinculo,'UTF-8') }}
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        @endif
                    </div>
                    <br>
                @endforeach
            @endif
        @endif
    @endif

    <!-- SEGUNDA PARTE DE LA DECLARACION -->
    <!-- PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES -->
    @if($secciones_formato[15]->Aplica == 1)
        @if(array_key_exists('PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["PARTICIPACIÓN EN EMPRESAS, SOCIEDADES O ASOCIACIONES"]->Info as $participacion_empresa)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[15]->Campos[0]->Visible == 1 || $secciones_formato[15]->Campos[4]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[15]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Participante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->Participante,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[15]->Campos[4]->Visible == 1)
                                        <td style="width: 20%;">Tipo de participación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->TipoParticipacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[15]->Campos[1]->Visible == 1 || $secciones_formato[15]->Campos[2]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[15]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Nombre de la empresa, sociedad o asociación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->NombreEmpresa,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[15]->Campos[2]->Visible == 1)
                                        <td style="width: 20%;">RFC:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->RFCEmpresa,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[15]->Campos[3]->Visible == 1 || $secciones_formato[15]->Campos[5]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[15]->Campos[3]->Visible == 1)
                                        <td style="width: 20%;">Porcentaje de participación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->PorcentajeParticipacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[15]->Campos[5]->Visible == 1)
                                        <td style="width: 20%;">Recibe remuneración:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($participacion_empresa->RecibeRemuneracion == 1)
                                                    SÍ
                                                @else
                                                    NO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[15]->Campos[6]->Visible == 1 || $secciones_formato[15]->Campos[7]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[15]->Campos[6]->Visible == 1)
                                        <td style="width: 20%;">Monto mensual neto:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($participacion_empresa->MontoMensual != null)
                                                    {{$participacion_empresa->MontoMensual}}
                                                @else
                                                    NO APLICA
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[15]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Es extranjero:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($participacion_empresa->EsExtranjero == 0)
                                                    EN MÉXICO
                                                @else
                                                    EN EL EXTRANJERO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[15]->Campos[7]->Visible == 1 || $secciones_formato[15]->Campos[8]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[15]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Ubicación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->Ubicacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[15]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Sector productivo:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_empresa->Sector,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN -->
    @if($secciones_formato[16]->Aplica == 1)
        @if(array_key_exists('PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["PARTICIPACIÓN EN TOMA DE DECISIONES DE ALGUNA INSTITUCIÓN"]->Info as $participacion_institucion)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[16]->Campos[0]->Visible == 1 || $secciones_formato[16]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[16]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Participante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_institucion->Participante,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[16]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Tipo de institución:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_institucion->TipoInstitucion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[16]->Campos[5]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[16]->Campos[5]->Visible == 1)
                                        <td style="width: 20%;">Puesto/Rol:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_institucion->PuestoRol,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[16]->Campos[6]->Visible == 1)
                                        <td style="width: 20%;">Fecha de inicio de participación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($participacion_institucion->FechaInicio,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[16]->Campos[7]->Visible == 1 || $secciones_formato[16]->Campos[9]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[16]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Recibe remuneración:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($participacion_institucion->RecibeRemuneracion == 1)
                                                    SÍ
                                                @else
                                                    NO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[16]->Campos[9]->Visible == 1)
                                        <td style="width: 20%;">Monto mensual neto:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($participacion_institucion->MontoMensual != null)
                                                    {{$participacion_institucion->MontoMensual}}
                                                @else
                                                    NO APLICA
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[16]->Campos[8]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Es extranjero:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            @if($participacion_institucion->EsExtranjero == 0)
                                                EN MÉXICO
                                            @else
                                                EN EL EXTRANJERO
                                            @endif
                                        </div>
                                    </td>
                                    <td style="width: 20%;">Ubicación:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($participacion_institucion->Ubicacion,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- APOYOS O BENEFICIOS PÚBLICOS -->
    @if($secciones_formato[17]->Aplica == 1)
        @if(array_key_exists('APOYOS O BENEFICIOS PÚBLICOS', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">APOYOS O BENEFICIOS PÚBLICOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ APOYO O BENEFICIO PÚBLICO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["APOYOS O BENEFICIOS PÚBLICOS"]->Info as $apoyo)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            <tr>
                                @if($secciones_formato[17]->Campos[0]->Visible == 1 || $secciones_formato[17]->Campos[1]->Visible == 1)
                                    @if($secciones_formato[17]->Campos[0]->Visible == 1)
                                        <td style="width: 20%; ">Beneficiario:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($apoyo->Beneficiario,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[17]->Campos[1]->Visible == 1)
                                        <td style="width: 20%; ">Nombre del programa:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($apoyo->NombrePrograma,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                @if($secciones_formato[17]->Campos[2]->Visible == 1 || $secciones_formato[17]->Campos[3]->Visible == 1)
                                    @if($secciones_formato[17]->Campos[2]->Visible == 1)
                                        <td style="width: 20%; ">Institución que otorga el apoyo:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($apoyo->NombreInstitucion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[17]->Campos[3]->Visible == 1)
                                        <td style="width: 20%; ">Nivel u orden de gobierno:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($apoyo->NombreAmbito,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                @if($secciones_formato[17]->Campos[4]->Visible == 1 || $secciones_formato[17]->Campos[4]->Visible == 1)
                                    @if($secciones_formato[17]->Campos[4]->Visible == 1)
                                        <td style="width: 20%; ">Tipo de apoyo:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($apoyo->TipoApoyo,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[17]->Campos[6]->Visible == 1)
                                        <td style="width: 20%; ">Monto aproximado:</td>
                                        <td style="width: 30%; ">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($apoyo->MontoApoyo == null)
                                                    NO APLICA
                                                @else
                                                    {{ mb_strtoupper($apoyo->MontoApoyo,'UTF-8') }}
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                            @if($secciones_formato[17]->Campos[7]->Visible == 1)
                                <tr>
                                    <td style="width: 20%; ">Especificación del apoyo:</td>
                                    <td style="width: 30%; ">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            @if($apoyo->Especificacion == null)
                                                NO APLICA
                                            @else
                                                {{ mb_strtoupper($apoyo->Especificacion,'UTF-8') }}
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif

    <!-- REPRESENTACIÓN -->
    @if($secciones_formato[18]->Aplica == 1)
        @if(array_key_exists('REPRESENTACIÓN', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">REPRESENTACIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["REPRESENTACIÓN"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ REPRESENTACIÓN ALGUNA EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["REPRESENTACIÓN"]->Info as $representacion)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[18]->Campos[0]->Visible == 1 || $secciones_formato[18]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[18]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Representante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($representacion->Representante,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[18]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Tipo de representación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($representacion->TipoRepresentacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[18]->Campos[3]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Fecha de inicio de la representación</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->FechaInicio,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @if($representacion->NombreRazonSocial != null && $representacion->RepresentateTipoPersona == 'moral')
                                <h3 style="font-size: 11px; text-align: center;"><span style="padding: 5px;">DATOS DEL REPRESENTANTE / REPRESENTADO</span></h3>
                                @if($secciones_formato[18]->Campos[2]->Visible == 1 || $secciones_formato[18]->Campos[4]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[18]->Campos[2]->Visible == 1)
                                            <td style="width: 20%;">Tipo de persona:</td>
                                            <td style="width: 30%; border-bottom: .1px solid;">
                                                PERSONA MORAL
                                            </td>
                                        @endif
                                        @if($secciones_formato[18]->Campos[4]->Visible == 1)
                                            <td style="width: 20%;">Razón social del representante:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($representacion->NombreRazonSocial,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[18]->Campos[5]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">RFC:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                {{ mb_strtoupper($representacion->RFCRepresentante,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                            @if($secciones_formato[18]->Campos[6]->Visible == 1 || $secciones_formato[18]->Campos[7]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[18]->Campos[6]->Visible == 1)
                                        <td style="width: 20%;">Recibe remuneración:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($representacion->RecibeRemuneracion == 1)
                                                    SÍ
                                                @else
                                                    NO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[18]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Monto mensual neto:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($representacion->RecibeRemuneracion == 1)
                                                    {{ mb_strtoupper($representacion->MontoMensual,'UTF-8') }}
                                                @else
                                                    NO APLICA
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[18]->Campos[9]->Visible == 1 || $secciones_formato[18]->Campos[8]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[18]->Campos[9]->Visible == 1)
                                        <td style="width: 20%;">Sector:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($representacion->Sector,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[18]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Lugar dónde se ubica:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($representacion->EsExtranjero == 1)
                                                    EN EL EXTRANJERO
                                                @else
                                                    EN MÉXICO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[18]->Campos[8]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Ubicación:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($representacion->Ubicacion,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- CLIENTES PRINCIPALES -->
    @if($secciones_formato[19]->Aplica == 1)
        @if(array_key_exists('CLIENTES PRINCIPALES', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">CLIENTES PRINCIPALES (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["CLIENTES PRINCIPALES"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ CLIENTE PRINCIPAL ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                <div style="text-align: center;">
                    SE MANIFESTARÁ EN BENEFICIO O GANANCIA DEL DECLARANTE SI SUPERA MENSUALMENTE 250 UNIDADES DE MEDIDA Y ACTUALIZACIÓN (UMA)
                </div>
                @foreach ($secciones_pdf["CLIENTES PRINCIPALES"]->Info as $cliente)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[19]->Campos[0]->Visible == 1 || $secciones_formato[19]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[19]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Realiza alguna actividad independiente al empleo, cargo o comisión:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($cliente->ActLucrativaDependiente == 1)
                                                    SÍ
                                                @else
                                                    NO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[19]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Participante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DE LA EMPRESA O SERVICIO QUE PROPORCIONA</span></h3>
                            @if($secciones_formato[19]->Campos[2]->Visible == 1 || $secciones_formato[19]->Campos[3]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[19]->Campos[2]->Visible == 1)
                                        <td style="width: 20%;">Razón social del representante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->NombreEmpresaServicio,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[19]->Campos[3]->Visible == 1)
                                        <td style="width: 20%;">RFC:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->RFCEmpresa,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($cliente->TipoPersonaCliente == 'moral')
                                <h3 style="font-size: 11px;"><span style="padding: 5px;">DATOS DEL CLIENTE PRINCIPAL</span></h3>
                                @if($secciones_formato[19]->Campos[5]->Visible == 1 || $secciones_formato[19]->Campos[7]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[19]->Campos[5]->Visible == 1)
                                            <td style="width: 20%;">Tipo de persona</td>
                                            <td style="width: 30%; border-bottom: .1px solid;">
                                                PERSONA MORAL
                                            </td>
                                        @endif
                                        @if($secciones_formato[19]->Campos[6]->Visible == 1)
                                            <td style="width: 20%;">Nombre o razón social del representante</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($cliente->RazonSocialCliente,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[19]->Campos[6]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">RFC</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->ClienteRFC,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                            @if($secciones_formato[19]->Campos[7]->Visible == 1 || $secciones_formato[19]->Campos[8]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[19]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Sector productivo al que pertenece:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->Sector,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[19]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Monto aproximado del beneficio o ganancia mensual que obtiene del cliente principal:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->MontoMensual,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[19]->Campos[9]->Visible == 1 || $secciones_formato[19]->Campos[9]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[19]->Campos[9]->Visible == 1)
                                        <td style="width: 20%;">Lugar dónde se ubica:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($cliente->EsExtranjero == 1)
                                                    EN EL EXTRANJERO
                                                @else
                                                    EN MÉXICO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[19]->Campos[9]->Visible == 1)
                                        <td style="width: 20%;">Ubicación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($cliente->Ubicacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- BENEFICIOS PRIVADOS -->
    @if($secciones_formato[20]->Aplica == 1)
        @if(array_key_exists('BENEFICIOS PRIVADOS', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">BENEFICIOS PRIVADOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if($secciones_pdf["BENEFICIOS PRIVADOS"]->Info == [])
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ BENEFICIO PRIVADO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach ($secciones_pdf["BENEFICIOS PRIVADOS"]->Info as $beneficio)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[20]->Campos[0]->Visible == 1 || $secciones_formato[20]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[20]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Tipo de beneficio:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{mb_strtoupper($beneficio->TipoBeneficio,'UTF-8')}}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[20]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Beneficiario:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($beneficio->Titular,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[20]->Campos[5]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">Recepción del beneficio:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($beneficio->RecepcionBeneficio,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @if($beneficio->OtorganteTipoPersona == 'moral')
                                @if($secciones_formato[20]->Campos[3]->Visible == 1 || $secciones_formato[20]->Campos[2]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[20]->Campos[3]->Visible == 1)
                                            <td style="width: 20%;">Nombre o razón social del otorgante:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{mb_strtoupper($beneficio->NombreRazonSocial,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[20]->Campos[2]->Visible == 1)
                                            <td style="width: 20%;">Tipo persona legal otorgante:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($beneficio->OtorganteTipoPersona,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[20]->Campos[4]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">RFC del otorgante:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($beneficio->RFCOtorgante,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                            @if($beneficio->RecepcionBeneficio == 'Epecie')
                                @if($secciones_formato[20]->Campos[6]->Visible == 1 || $secciones_formato[20]->Campos[9]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[20]->Campos[6]->Visible == 1)
                                            <td style="width: 20%;">Especifique el beneficio recibido:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{mb_strtoupper($beneficio->Beneficio,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[20]->Campos[9]->Visible == 1)
                                            <td style="width: 20%;">Sector al que pertenece:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                            @else
                                @if($secciones_formato[20]->Campos[7]->Visible == 1 || $secciones_formato[20]->Campos[8]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[20]->Campos[7]->Visible == 1)
                                            <td style="width: 20%;">Monto mensual aproximado:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{mb_strtoupper($beneficio->MontoMensual,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[20]->Campos[8]->Visible == 1)
                                            <td style="width: 20%;">Tipo de moneda:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($beneficio->TipoMoneda,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[20]->Campos[9]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">Sector al que pertenece:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($beneficio->Sector,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif


    <!-- FIDEICOMISOS -->
    @if($secciones_formato[21]->Aplica == 1)
        @if(array_key_exists('FIDEICOMISOS', $secciones_pdf))
            <h2 style="font-size: 22px; border: 1px solid;"><span style="padding: 5px;">FIDEICOMISOS (HASTA LOS 2 ÚLTIMOS AÑOS)</span></h2>
            @if(count($secciones_pdf["FIDEICOMISOS"]->Info) == 0)
                <div style="text-align: center; border: 1px solid;">
                    EL DECLARANTE NO PRESENTÓ FIDEICOMISO ALGUNO EN LA DECLARACIÓN DE ESTE AÑO
                </div>
                <br>
                <br>
            @else
                @foreach($secciones_pdf["FIDEICOMISOS"]->Info as $fideicomiso)
                    <div style="margin-top: 10px; margin-bottom: 2px; border: 1px solid; padding: 10px;">
                        <table style="width: 100%">
                            @if($secciones_formato[21]->Campos[0]->Visible == 1 || $secciones_formato[21]->Campos[1]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[21]->Campos[0]->Visible == 1)
                                        <td style="width: 20%;">Participante en el fideicomiso:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{mb_strtoupper($fideicomiso->Participante,'UTF-8')}}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[21]->Campos[1]->Visible == 1)
                                        <td style="width: 20%;">Tipo de fideicomiso:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->TipoFideicomiso,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[21]->Campos[2]->Visible == 1 || $secciones_formato[21]->Campos[3]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[21]->Campos[2]->Visible == 1)
                                        <td style="width: 20%;">Tipo de participación:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->TipoParticipacion,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[21]->Campos[3]->Visible == 1)
                                        <td style="width: 20%;">RFC fideicomiso:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->RFCFideicomiso,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($fideicomiso->TipoParticipacion == 'Fideicomitente')
                                @if($secciones_formato[21]->Campos[5]->Visible == 1 || $secciones_formato[21]->Campos[4]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[21]->Campos[5]->Visible == 1)
                                            <td style="width: 20%;">Nombre o razón social del fideicomitente:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{mb_strtoupper($fideicomiso->RazonSocialFideicomitente,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[21]->Campos[4]->Visible == 1)
                                            <td style="width: 20%;">Tipo persona fideicomitente:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($fideicomiso->TipoPersonaFidecomitente,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[21]->Campos[6]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">RFC fideicomitente:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->RFCFideicomitente,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @elseif ($fideicomiso->TipoParticipacion == 'Fiduciario')
                            @if($secciones_formato[21]->Campos[7]->Visible == 1 || $secciones_formato[21]->Campos[8]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[21]->Campos[7]->Visible == 1)
                                        <td style="width: 20%;">Nombre o razón social del fiduciario:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{mb_strtoupper($fideicomiso->RazonSocialFiduciario,'UTF-8')}}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[21]->Campos[8]->Visible == 1)
                                        <td style="width: 20%;">Tipo persona fiduciario:</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->TipoPersonaFiduciario,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @if($secciones_formato[21]->Campos[9]->Visible == 1)
                                <tr>
                                    <td style="width: 20%;">RFC fiduciario:</td>
                                    <td style="width: 30%;">
                                        <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                            {{ mb_strtoupper($fideicomiso->RFCFiduciario,'UTF-8') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @elseif ($fideicomiso->TipoParticipacion == 'Fideicomisario')
                                @if($secciones_formato[21]->Campos[11]->Visible == 1 || $secciones_formato[21]->Campos[10]->Visible == 1)
                                    <tr>
                                        @if($secciones_formato[21]->Campos[11]->Visible == 1)
                                            <td style="width: 20%;">Nombre o razón social del fideicomisario:</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{mb_strtoupper($fideicomiso->RazonSocialFideicomisario,'UTF-8')}}
                                                </div>
                                            </td>
                                        @endif
                                        @if($secciones_formato[21]->Campos[10]->Visible == 1)
                                            <td style="width: 20%;">Tipo persona fideicomisario</td>
                                            <td style="width: 30%;">
                                                <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                    {{ mb_strtoupper($fideicomiso->TipoPersonaFideicomisario,'UTF-8') }}
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                @if($secciones_formato[21]->Campos[12]->Visible == 1)
                                    <tr>
                                        <td style="width: 20%;">RFC fideicomisario</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->RFCFideicomisario,'UTF-8') }}
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                            @if($secciones_formato[21]->Campos[13]->Visible == 1 || $secciones_formato[21]->Campos[14]->Visible == 1)
                                <tr>
                                    @if($secciones_formato[21]->Campos[13]->Visible == 1)
                                        <td style="width: 20%;">Sector</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                {{ mb_strtoupper($fideicomiso->Sector,'UTF-8') }}
                                            </div>
                                        </td>
                                    @endif
                                    @if($secciones_formato[21]->Campos[14]->Visible == 1)
                                        <td style="width: 20%;">Localización</td>
                                        <td style="width: 30%;">
                                            <div class="text-table" style="width: 97%; border-bottom: .1px solid;">
                                                @if($fideicomiso->EsExtranjero == 0)
                                                    EN MÉXICO
                                                @else
                                                    EN EL EXTRANJERO
                                                @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        </table>
                    </div>
                @endforeach
            @endif
        @endif
    @endif

    <footer style="font-size:10px;">
        SiDePat {{config('sidepat.version_actual')}}
    </footer>
</body>
</html>
