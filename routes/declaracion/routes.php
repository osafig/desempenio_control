<?php

//Rutas de declaracion
Route::namespace('Declaracion')->prefix('declaracion')->group(function(){
    Route::post('/get-data-declaracion','DeclaracionController@getDataDeclaracion');
    Route::get('/admin/get-users-declaracion', 'DeclaracionController@getUsersToDeclare');
    Route::get('/admin/get-tipos-declaracion', 'DeclaracionController@getTiposDeclaracion');
    Route::post('/admin/asignar-usuario-declaracion', 'DeclaracionController@asignarUsuariosDeclaracion');
    Route::post('/admin/remover-usuario-declaracion', 'DeclaracionController@removerUsuariosDeclaracion');
    Route::get('/get-catalogo-estado_civil','DeclaracionController@getCatalogoEstadoCivil');
    Route::get('/get-datos-declaracion','DeclaracionController@getDatosPersonales');
    Route::post('/saveDatosPersonales','DeclaracionController@saveInformacionPersonal');
    Route::get('/get-estados', 'DeclaracionController@getEstados');
    Route::post('/get-municipios', 'DeclaracionController@getMunicipios');
    Route::post('/save-direccion', 'DeclaracionController@saveDireccion');
    Route::post('/get-direccion', 'DeclaracionController@getDireccion');
    Route::get('/get-niveles-estudio','DeclaracionController@getNivelesEstudio');
    Route::post('/save-datos-curriculares','DeclaracionController@saveDatosCurriculares');
    Route::post('/get-data-curriculares', 'DeclaracionController@getDataCurriculares');
    Route::get('/get-data-cargo', 'DeclaracionController@getDataCargo');
    Route::post('/save-datos-cargo', 'DeclaracionController@saveDatosCargo');
    Route::post('/get-cargo-registrado', 'DeclaracionController@getCargoRegistrado');
    Route::get('/get-catalogo-orden-gobierno','DeclaracionController@getCatOrdenGobierno');
    Route::get('/get-catalogo-ambito-publico','DeclaracionController@getCatAmbitoPublico');
    Route::get('/get-catalogo-sector-laboral','DeclaracionController@getCatSectorLaboral');
    Route::post('/save-exp-laboral','DeclaracionController@saveExpLaboral');
    Route::get('/get-experiencias','DeclaracionController@getExperienciasLaborales');
    Route::post('/borrar-experiencia', 'DeclaracionController@borrarExperienciaLaboral');
    Route::post('/marcar-ninguno', 'DeclaracionController@marcarNungunoExperienciaLaboral');
    Route::post('/check-experiencia-terminada', 'DeclaracionController@marcarExperieinciaTerminada');
    Route::post('/marcar-terminado-como', 'DeclaracionController@marcarTerminadoComo');
    Route::get('/get-catalogo-instrumento','DeclaracionController@getInstrumentosFinancieros');
    Route::post('/save-ingresos', 'DeclaracionController@saveIngresos');
    Route::post('/get-ingresos-declaracion', 'DeclaracionController@getIngresosDeclaracion');
    Route::get('/get-catalogo-instrumento','DeclaracionController@getInstrumentosFinancieros');
    Route::post('/save-servidor-pasado', 'DeclaracionController@saveServidorPasado');
    Route::post('/save-servidor-pasado-no-aplica', 'DeclaracionController@saveServidorPasadoNoAplica');
    Route::post('/get-ingresos-declaracion-servidor-pasado', 'DeclaracionController@getIngresosDeclaracionServidorPasado');
    Route::get('/get-catalogo-bienes_inmuebles','DeclaracionController@getCatBienesInmuebles');
    Route::get('/get-catalogo-tipo-titulares','DeclaracionController@getCatTipoTitulares');
    Route::get('/get-catalogo-forma-adquisicion','DeclaracionController@getCatFormaAdquisicion');
    Route::get('/get-catalogo-vinculo-personal','DeclaracionController@getCatVinculoPersonal');
    Route::post('/save-bien-inmueble', 'DeclaracionController@saveBienInmueble');
    Route::post('/get-bienes_inmuebles','DeclaracionController@getBienesInmuebles');
    Route::post('/borrar-bien-inmueble', 'DeclaracionController@borrarBienInmueble');
    Route::post('/marcar-ninguna-bienes-inmuebles', 'DeclaracionController@marcarNingunaBienInmueble');
    Route::post('/marcar-terminada-bienes-inmuebles', 'DeclaracionController@marcarTerminadaBienInmueble');
    Route::get('/get-catalogo-vehiculos','DeclaracionController@getCatVehiculos');
    Route::post('/save-vehiculo','DeclaracionController@saveVehiculo');
    Route::post('/get-vehiculos','DeclaracionController@getVehiculos');
    Route::post('/borrar-bien-mueble', 'DeclaracionController@borrarBienMueble');
    Route::post('/marcar-terminada-vehiculos', 'DeclaracionController@marcarTerminadaVehiculos');
    Route::post('/marcar-ninguna-vehiculos', 'DeclaracionController@marcarNingunaVehiculos');
    Route::get('/get-catalogo-bienes_mueble','DeclaracionController@getCatBienesMueble');
    Route::post('/get-bienes-mueble','DeclaracionController@getBienesMueble');
    Route::post('/marcar-terminada-bienes-muebles', 'DeclaracionController@marcarTerminadaBienMueble');
    Route::post('/marcar-ninguna-bienes-muebles', 'DeclaracionController@marcarNingunaBienMueble');
    Route::get('/get-catalogo-inversion','DeclaracionController@getCatInversion');
    Route::post('/get-catalogo-activos','DeclaracionController@getCatActivos');
    Route::post('/save-inversion','DeclaracionController@saveInversion');
    Route::get('/get-inversiones/{declaracion_id}','DeclaracionController@getInversiones');
    Route::post('/borrar-inversion', 'DeclaracionController@borrarInversion');
    Route::post('/marcar-terminada-inversiones', 'DeclaracionController@marcarTerminadaInversiones');
    Route::post('/marcar-ninguna-inversiones', 'DeclaracionController@marcarNingunaInversion');
    Route::get('/get-catalogo-tipo-adeudo','DeclaracionController@getCatTipoAdeudo');
    Route::post('/save-adeudos','DeclaracionController@saveAdeudos');
    Route::post('/get-adeudos','DeclaracionController@getAdeudos');
    Route::post('/borrar-adeudo', 'DeclaracionController@borrarAdeudo');
    Route::post('/marcar-terminada-adeudos', 'DeclaracionController@marcarTerminadaAdeudos');
    Route::post('/marcar-ninguna-adeudos', 'DeclaracionController@marcarNingunaAdeudos');
    Route::post('/get-prestamos', 'DeclaracionController@getPrestamos');
    Route::post('/save-prestamo', 'DeclaracionController@savePrestamo');
    Route::post('/borrar-prestamo', 'DeclaracionController@borrarPrestamo');
    Route::post('/marcar-terminada-prestamos', 'DeclaracionController@marcarTerminadaPrestamos');
    Route::post('/marcar-ninguna-prestamos', 'DeclaracionController@marcarNingunaPrestamos');
    Route::post('/subir-declaracion-sat', 'DeclaracionController@subirDeclaracionSat');
    Route::get('/get-declaracion-sat', 'DeclaracionController@getDeclaracionSat');
    Route::post('/borrar-declaracion-sat', 'DeclaracionController@borrarDeclaracionSat');
    Route::get('/get-tipo-participante','DeclaracionController@getCatTipoParticipante');
    Route::post('/save-participacion-empresa', 'DeclaracionController@saveParticipacionEmpresa');
    Route::post('/get-participaciones-empresa','DeclaracionController@getParticipacionEmpresas');
    Route::post('/borrar-participacion-empresa','DeclaracionController@borrarParticipacionEmpresa');
    Route::post('/marcar-terminada-participacion-empresa', 'DeclaracionController@marcarTerminadaParticipacionEmpresa');
    Route::post('/marcar-ninguna-participacion-empresa','DeclaracionController@marcarNingunaParticipacionEmpresa');
    Route::get('/get-catalogo-tipo-institucion','DeclaracionController@getCatTipoInstitucion');
    Route::post('/save-participacion_institucion','DeclaracionController@saveParticipacionInstitucion');
    Route::post('/get-participacion-institucion','DeclaracionController@getParticipacionInstitucion');
    Route::post('/borrar-participacion-institucion','DeclaracionController@borrarParticipacionInstitucion');
    Route::post('/marcar-terminada-participacion-institucion', 'DeclaracionController@marcarTerminadaParticipacionInstitucion');
    Route::post('/marcar-ninguna-participacion-institucion','DeclaracionController@marcarNingunaParticipacionInstitucion');
    Route::get('/get-datos-pareja', 'DeclaracionController@getDatosPreja');
    Route::get('/get-datos-dependientes', 'DeclaracionController@getDatosDependientes');
    Route::post('/save-dependientes', 'DeclaracionController@saveDependientes');
    Route::get('/get-experiencias/dependiente/{ActividadLaboralID}', 'DeclaracionController@getExperienciasDependiente');
    Route::post('/save-exp-laboral-dependiente', 'DeclaracionController@saveExperienciaLaboralDependientes');
    Route::get('/get-dependiente/{dependiente_id}', 'DeclaracionController@getDependiente');
    Route::post('/borrar-experiencia-dependiente', 'DeclaracionController@borrarExperienciaDependiente');
    Route::post('/borrar-dependiente', 'DeclaracionController@borrarDependiente');
    Route::post('/marcar-terminada-datos-pareja', 'DeclaracionController@marcarTerminadaDatosPareja');
    Route::post('/marcar-ninguna-datos-pareja', 'DeclaracionController@marcarNingunaDatosPareja');
    Route::post('/marcar-terminada-datos-dependientes', 'DeclaracionController@marcarTerminadaDatosDependientes');
    Route::post('/marcar-ninguna-datos-dependientes', 'DeclaracionController@marcarNingunaDatosDependientes');
    Route::get('/get-catalogo-beneficiarios','DeclaracionController@getCatBeneficiarios');
    Route::post('/save-apoyo-beneficio','DeclaracionController@saveApoyoBeneficio');
    Route::post('/get-apoyos-beneficios','DeclaracionController@getApoyosBeneficios');
    Route::post('/borrar-apoyos-beneficios','DeclaracionController@borrarApoyosBeneficios');
    Route::post('/marcar-terminada-apoyos-beneficios', 'DeclaracionController@marcarTerminadaApoyosBeneficios');
    Route::post('/marcar-ninguna-apoyos-beneficios','DeclaracionController@marcarNingunaApoyosBeneficios');
    Route::post('/save-representacion','DeclaracionController@saveRepresentacion');
    Route::post('/get-representacion','DeclaracionController@getRepresentaciones');
    Route::post('/borrar-representacion','DeclaracionController@borrarRepresentacion');
    Route::post('/marcar-terminada-representacion', 'DeclaracionController@marcarTerminadaRepresentacion');
    Route::post('/marcar-ninguna-representacion','DeclaracionController@marcarNingunaRepresentacion');
    Route::post('/save-cliente', 'DeclaracionController@saveCliente');
    Route::post('/get-clientes','DeclaracionController@getClientes');
    Route::post('/borrar-cliente-principal','DeclaracionController@borrarCliente');
    Route::post('/marcar-terminada-cliente-principal', 'DeclaracionController@marcarTerminadaCliente');
    Route::post('/marcar-ninguna-cliente-principal','DeclaracionController@marcarNingunaCliente');
    Route::post('/save-beneficios-privados','DeclaracionController@saveBeneficiosPrivados');
    Route::post('/get-beneficios','DeclaracionController@getBeneficios');
    Route::post('/borrar-beneficio-apoyo','DeclaracionController@borrarBeneficioApoyo');
    Route::post('/marcar-terminada-beneficio-apoyo', 'DeclaracionController@marcarTerminadaBeneficioApoyo');
    Route::post('/marcar-ninguna-beneficio-apoyo','DeclaracionController@marcarNingunaBeneficioApoyo');
    Route::post('/admin/enviar-correos-notificacion', 'DeclaracionController@enviarCorreosNotificacion');
    Route::post('/admin/enviar-correo-notificacion', 'DeclaracionController@enviarCorreoNotificacion');
    Route::post('/save-fideicomiso','DeclaracionController@saveFideicomiso');
    Route::post('/get-fideicomisos','DeclaracionController@getFideicomisos');
    Route::post('/borrar-fideicomiso', 'DeclaracionController@borrarFideicomiso');
    Route::post('/marcar-terminada-fideicomisos', 'DeclaracionController@marcarTerminadaFideicomisos');
    Route::post('/marcar-ninguna-fideicomisos', 'DeclaracionController@marcarNingunaFideicomisos');
    Route::post('/finalizar-declaracion', 'DeclaracionController@finalizarDeclaracion');
    Route::post('/finalizar-declaracion-sinpdf', 'DeclaracionController@finalizarDeclaracionSinPdf');
    Route::get('/get-declaraciones-ejercicio','DeclaracionController@getDeclaraciones');
    Route::post('/get-declaraciones-terminadas','DeclaracionController@getDeclaracionesTerminadas');
    Route::post('/get-declaracion-estadistica','DeclaracionController@getEstadisticaDeclaracion');
    Route::post('/set-all-passwords', 'DeclaracionController@setAllPasswords');
    Route::post('/generar-declaracion','DeclaracionController@generarDeclaracion');
    Route::post('/generar-declaracion-publica','PdfDeclaracionController@generarDeclaracionPublica');
    Route::post('/habilitar-declaracion', 'DeclaracionController@habilitarDeclaracion');
    Route::post('/previo-declaracion','PdfDeclaracionController@previoDeclaracion');
    Route::post('/obtener-declaraciones', 'DeclaracionController@obtenerDeclaraciones');
    Route::post('/obtener-adscripciones', 'DeclaracionController@obtenerAdscripciones');
    Route::get('/get-formatos-publicos', 'DeclaracionController@getFormatosPublicos');
    Route::post('/registrar-formato-publico', 'DeclaracionController@crearFormatoPublico');
    Route::post('/borrar-formato', 'DeclaracionController@borrarFormato');
    Route::post('/marcar-principal-formato', 'DeclaracionController@marcarPrincipalFormato');
    Route::post('/editar-formato-publico', 'DeclaracionController@editarFormatoPublico');
    Route::post('/get-lista-pdf-generados', 'PdfDeclaracionController@listaDocumentosGenerados');
    Route::get('/get-secciones-declaracion', 'DeclaracionController@getSeccionesDeclaracion');
    Route::post('/get-secciones-formato', 'DeclaracionController@getSeccionesFormato');
    Route::post('/generar-multiples-declaraciones', 'PdfDeclaracionController@generarMultiplesDeclaraciones');
    Route::get('/get-campos-formato', 'DeclaracionController@getCamposFormato');
    Route::post('/get-campos-formato-seccion-seleccionados', 'DeclaracionController@getCamposFormatoSeccionSeleccionado');
    Route::post('/get-rutas-declaraciones', 'PdfDeclaracionController@getRutasDeclaracion');
    Route::post('/copiar-formato','DeclaracionController@copiarFormatoDeclaracion');
    Route::post('/borrar-declaracion-publica','PdfDeclaracionController@borrarDeclaracionPublica');
    Route::post('/borrar-declaraciones-multiples','PdfDeclaracionController@borrarDeclaracionesPublicas');
    Route::post('/descarga-docs-monitoreo','PdfDeclaracionController@getDescargaDocsMonitoreo');
    Route::post('/get-catalogos','DeclaracionController@getListaCatalogos');
    Route::post('/copiar-declaracion', 'DeclaracionController@copiarDeclaracion');
    Route::get('/get-ejercicios-publica', 'DeclaracionController@getEjerciciosDeclaracionesPublica');

    // Rutas de los catalogos
    Route::post('/get-catalogo-inversiones','CatalogosController@getInversiones');

    Route::post('/get-catalogo-puestos','CatalogosController@getPuestos');
    Route::post('/get-catalogo-bienes','CatalogosController@getTiposBienes');
    Route::post('/get-catalogo-instituciones','CatalogosController@getTiposInstituciones');
    Route::post('/get-catalogo-tipos-participantes','CatalogosController@getTiposParticipantes');
    Route::post('/get-catalogo-tipos-trabajo', 'CatalogosController@getTiposTrabajo');
    Route::post('/guardar-tipo-trabajo', 'CatalogosController@guardarTipoTrabajo');
    Route::post('/ocultar-tipo', 'CatalogosController@ocultarTipo');
    Route::post('/mostrar-tipo', 'CatalogosController@mostrarTipo');
    Route::post('/actualizar-tipo', 'CatalogosController@actualizarTipo');
    Route::post('/ocultar-inversion', 'CatalogosController@ocultarInversión');
    Route::post('/mostrar-inversion', 'CatalogosController@mostrarInversión');
    Route::post('/actualizar-tipo-inversion', 'CatalogosController@actualizarTipoInversion');
    Route::post('/ocultar-activo', 'CatalogosController@ocultarActivo');
    Route::post('/mostrar-activo', 'CatalogosController@mostrarActivo');
    Route::post('/guardar-activo', 'CatalogosController@guardarActivo');

    // catalogo de adscripciones
    Route::post('/get-catalogo-adscripciones','CatalogosController@getAdscripciones');
    Route::post('/visibilidad-adscripcion','CatalogosController@editVisibilidadAdscripciones');
    Route::post('/actualizar-adscripcion','CatalogosController@actualizarAdscripcion');
    Route::post('/nueva-adscripcion','CatalogosController@registrarAdscripcion');
    // catalogo de ambitos
    Route::post('/get-catalogo-ambitos','CatalogosController@getAmbitos');
    Route::post('/visibilidad-ambito','CatalogosController@editVisibilidadAmbitos');
    Route::post('/actualizar-ambito','CatalogosController@actualizarAmbito');
    Route::post('/nuevo-ambito','CatalogosController@registrarAmbito');
    // catalogo de beneficiarios
    Route::post('/get-catalogo-beneficiarios','CatalogosController@getBeneficiarios');
    Route::post('/visibilidad-beneficiario','CatalogosController@editVisibilidadBeneficiarios');
    Route::post('/actualizar-beneficiario','CatalogosController@actualizarBeneficiario');
    Route::post('/nuevo-beneficiario','CatalogosController@registrarBeneficiario');
    // catalogo de formas de operacion
    Route::post('/get-catalogo-formas-operacion','CatalogosController@getFormasOperacion');
    Route::post('/visibilidad-forma','CatalogosController@editVisibilidadFormaOperacion');
    Route::post('/actualizar-forma-operacion','CatalogosController@actualizarFormaOperacion');
    Route::post('/nueva-forma-operacion','CatalogosController@registrarFormaOperacion');
    // Catalogo de niveles de empleo
    Route::post('/get-catalogo-nivel-empleo','CatalogosController@getNivelesEmpleo');
    Route::post('/visibilidad-nivel','CatalogosController@editVisibilidadNivelEmpleo');
    Route::post('/actualizar-nivel','CatalogosController@actualizarNivelEmpleo');
    Route::post('/nuevo-nivel','CatalogosController@registrarNivelEmpleo');
    // Catalogo de niveles de estudio
    Route::post('/get-catalogo-nivel-estudio','CatalogosController@getNivelesEstudio');
    Route::post('/visibilidad-nivel-estudio','CatalogosController@editVisibilidadNivelEstudio');
    Route::post('/actualizar-nivel-estudio','CatalogosController@actualizarNivelEstudio');
    Route::post('/nuevo-nivel-estudio','CatalogosController@registrarNivelEstudio');
    // Catalogo de sectores productivos
    Route::post('/get-catalogo-sectores','CatalogosController@getSectores');
    Route::post('/visibilidad-sector','CatalogosController@editVisibilidadSectorProductivo');
    Route::post('/actualizar-sector','CatalogosController@actualizarSectorProductivo');
    Route::post('/nuevo-sector','CatalogosController@registrarSector');
    // Catalogo de tipo de adeudos
    Route::post('/get-catalogo-adeudos','CatalogosController@getTiposAdeudos');
    Route::post('/visibilidad-tipo-adeudo','CatalogosController@editVisibilidadTipoAdeudo');
    Route::post('/actualizar-adeudo','CatalogosController@actualizarTipoAdeudo');
    Route::post('/nueva-tipo-adeudo','CatalogosController@registrarTipoAdeudo');

    // Catalogo de tipo de bienes
    Route::post('/get-catalogo-bienes','CatalogosController@getTiposBienes');
    Route::post('/visibilidad-tipo-bien','CatalogosController@editVisibilidadTipoBien');
    Route::post('/nuevo-tipo-bien','CatalogosController@registrarTipoBien');

    // Catalogo de tipos de instituciones
    Route::post('/get-catalogo-instituciones','CatalogosController@getTiposInstituciones');
    Route::post('/visibilidad-tipo-institucion','CatalogosController@editVisibilidadTipoInstitucion');
    Route::post('/actualizar-tipo-institucion','CatalogosController@actualizarTipoInstitucion');
    Route::post('/nuevo-tipo-institucion','CatalogosController@registrarTipoInstitucion');

    // Catalogo de tipos de participantes
    Route::post('/get-catalogo-tipos-participantes','CatalogosController@getTiposParticipantes');
    Route::post('/visibilidad-tipo-participante','CatalogosController@editVisibilidadTipoParticipante');
    Route::post('/actualizar-tipo-participante','CatalogosController@actualizarTipoParticipante');
    Route::post('/nuevo-tipo-participante','CatalogosController@registrarTipoParticipante');

    // Catalogo de puestos
    Route::post('/get-catalogo-puestos','CatalogosController@getPuestos');
    Route::post('/visibilidad-puesto','CatalogosController@editVisibilidadPuesto');
    Route::post('/actualizar-puesto','CatalogosController@actualizarPuesto');
    Route::post('/nuevo-puesto','CatalogosController@registrarPuesto');
    Route::post('/cambiar-formato-puesto','CatalogosController@editFormatoPuesto');
    Route::get('/get-ejercicios-declarados', 'DeclaracionController@getEjerciciosDeclarados');
    Route::get('/get-ejercicios-fiscales', 'DeclaracionController@getEjerciciosFiscales');

    // Bacheo
    Route::get('/buscar-declaraciones-bacheo', 'DeclaracionController@getDeclaracionesBacheo');
    Route::post('/declaraciones-detalle', 'DeclaracionController@declaracionesDetalle');
    Route::post('/cambiar-declaraciones-ejercicio', 'DeclaracionController@cambiarDeclaracionEjercicio');
    Route::get('/usuarios-duplicados', 'DeclaracionController@usuariosDuplicados');
    Route::get('/obtener-usuario-nombre', 'DeclaracionController@obtenerUsuarioNombre');
});
