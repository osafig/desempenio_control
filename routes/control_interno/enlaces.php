<?php

Route::namespace('ControlInterno')->group(function () {
    Route::post('/enlace', 'EnlacesController@getDataEnlaceByToken');
    Route::post('/check-has-auditoria', 'EnlacesController@checkHasAuditoria');
    Route::post('/get-auditoria-by-funcionario-id', 'AuditoriasController@getAuditoriaByFuncionarioId');
    Route::post('/get-general-data-cuestionario', 'AuditoriasController@getGeneralDataCuestionario');
    Route::post('/get-temas-cuestionario', 'AuditoriasController@getTemasCuestionario');
    Route::post('/get-preguntas-tema', 'AuditoriasController@getPreguntasTema');
    Route::post('/get-opciones-pregunta', 'AuditoriasController@getOpcionesPregunta');
    Route::post('/save-respuesta', 'AuditoriasController@saveRespuesta');
    Route::post('/get-anexos', 'AuditoriasController@getAnexos');
    Route::post('/delete-anexo', 'AuditoriasController@deleteAnexos');
    Route::post('/enviar-cuestionario', 'AuditoriasController@enviarCuestionario');
    Route::post('/descargar-respuestas', function () {
        $enlace = DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = \DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de respuestas')
            ->first();
        DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $ejercicio_fiscal = DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', 1)
            ->first();
        $ejercicio = $ejercicio_fiscal->Year -1;
        $auditoria = request()->auditoria;
        $file = public_path() . "/control_interno/{$ejercicio}/{$auditoria}/acuses/respuestas_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_control_interno.pdf', $headers);
    });
    Route::post('/descargar-acuses', function () {
        $enlace = \DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de acuse')
            ->first();
        \DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $ejercicio_fiscal = DB::connection('main')
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', 1)
            ->first();
        $ejercicio = $ejercicio_fiscal->Year -1;
        $auditoria = request()->auditoria;
        $file = public_path() . "/control_interno/{$ejercicio}/{$auditoria}/acuses/acuse_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_control_interno.pdf', $headers);
    });
    Route::post('/descargar-respuestas/solventacion', function () {
        $enlace = DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = \DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de respuestas')
            ->first();
        DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        //TODO: tomar el ejercicio de la auditoria
        $ejercicio = \DB::connection('main')->table('osaf_ejercicios_cat')->where('Actual', '=', true)->first();
        $ejercicio_auditado = $ejercicio->Year - 1;
        $file = public_path() . "/control_interno/{$ejercicio_auditado}/{$auditoria}/acuses/solventacion_respuestas_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_control_interno.pdf', $headers);
    });
    Route::post('/descargar-acuses/solventacion', function () {
        $enlace = \DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de acuse')
            ->first();
        \DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        $ejercicio = \DB::connection('main')->table('osaf_ejercicios_cat')->where('Actual', '=', true)->first();
        $ejercicio_auditado = $ejercicio->Year - 1;
        $file = public_path() . "/control_interno/{$ejercicio_auditado}/{$auditoria}/acuses/acuse_solventacion_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_control_interno.pdf', $headers);
    });
    Route::post('/update-description-anexo', 'AuditoriasController@updateDescriptionAnexo');
});
