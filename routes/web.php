<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use League\Csv\Writer;
use App\Http\Repositories\Declaracion\DeclaracionRepository;

Route::get('/download-users/yzvJkB0OB3fkTva8XHK3ROTNvSkBVnU2u3b5bbtm', function() {
    $users = \DB::connection('main')->table('osaf_usuarios')->get(['Nombres', 'PrimerApellido', 'SegundoApellido', 'Email', 'PasswordEncrypt']);
    foreach($users as $user) {
        try{
            $user->PasswordEncrypt = \Crypt::decryptString($user->PasswordEncrypt);
        } catch(\Exception $e) {
            $user->PasswordEncrypt = 'Error | no se pudo obtener la contraseña';
        }
    }
    // these are the headers for the csv file.
    $headers = [
        'Content-Type' => 'application/vnd.ms-excel;',
        'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
        'Content-Disposition' => 'attachment; filename=download.csv',
        'Expires' => '0',
        'Pragma' => 'public',
    ];
    //I am storing the csv file in public >> files folder. So that why I am creating files folder
    if (!\File::exists(public_path()."/files")) {
        \File::makeDirectory(public_path() . "/files");
    }
    //creating the download file
    $filename =  public_path("files/download.csv");
    $handle = fopen($filename, 'w');
    //adding the first row
    fputcsv($handle, [
        "Nombre",
        "Correo",
        "Contraseña"
    ]);
    //adding the data from the array
    foreach ($users as $each_user) {
        fputcsv($handle, [
            $each_user->Nombres.' '.$each_user->PrimerApellido.' '.$each_user->SegundoApellido,
            $each_user->Email,
            $each_user->PasswordEncrypt
        ]);
    }
    fclose($handle);
    //download command
    return Response::download($filename, "download.csv", $headers);
});
Route::get('/declaraciones-publicas/yzvJkB0OB3fkTva8XHK3ROTNvSkBVnU2u3b5bbtm', function() {
    $file = public_path('declaraciones_publicas.zip');
    return Response::download($file);
});

Route::get('/descarga-doc/3247775E007FB72502E7C5912368290F87D8EA5C/{ejercicio}',function($ejercicio){
    $file = public_path('Declaraciones - '.$ejercicio.'.zip');
    return Response::download($file);
});

Route::get('/descarga-doc-csv/3247775E007FB72502E7C5912368290F87D8EA5C', function(DeclaracionRepository $dr) {
    $declaraciones = $dr->getDeclaracionesEjercicioCvs(request()->all());
    $csv = Writer::createFromPath('archivo.csv', 'w+');
    $csv->setOutputBOM(Writer::BOM_UTF8);
    $csv->insertOne([
        'Declarante',
        'Área de adscripción',
        'Tipo de declaración',
        'Formato',
        'Estatus',
        'Tipo de trabajador',
        'Fecha de ingreso',
        'Fecha de presentación de declaración'
    ]);
    foreach($declaraciones as $declaracion) {
        $csv->insertOne([
            $declaracion->Declarante,
            $declaracion->AreaAdscripcion,
            $declaracion->TipoDeclaracion,
            $declaracion->tipo,
            ($declaracion->Finalizado == 1) ? "Terminada" : "Sin terminar",
            $declaracion->TipoTrabajador,
            $declaracion->FechaIngreso,
            explode(' ', $declaracion->FechaDeclaracion)[0],
        ]);
    }
    $csv->output('reporte_declaraciones.csv');
});

Route::get('/control-gral', function () {
    $auditorias = DB::table('v_monitoreo_auditoria')
        ->where('Anio', '=', 2023)
        ->orderBy('Nombre', 'asc')
        ->get();
    foreach ($auditorias as $auditoria) {
        $auditoria->estatus = DB::table('v_respuestas_recomendaciones')
            ->where('AuditoriaID','=', $auditoria->AuditoriaID)
            ->get();
    }
    $csv = Writer::createFromPath('controlgral.csv', 'w+');
    $csv->setOutputBOM(Writer::BOM_UTF8);
    $csv->insertOne([
        'Nombre',
        'Control Interno',
        'Revisión control interno',
        'Etapa',
        'Etapa detalle'
    ]);
    /**
     * Enlace         Enlace contestando
     * Auditor        Revisión auditor
     * Responsable    Revision responsable
     * Responsable    Generando CRP
     * Responsable    Enviar CRP
     * Enlace         Solventación de CRP
     * Auditor        Revisión auditor
     * Responsable    Revision responsable
     * Responsable    Generando Informe
    */
    // Obtener etapa actual
    foreach($auditorias as $auditoria) {
        $nombre = $auditoria->Nombre;
        $etapa = $auditoria->etapa_actual;
        $detalle = $auditoria->etapa_actual_detalle;
        $usuarioActual = '';
        $pendientes = 0;
        if($etapa == 'Enlace contestando' || $etapa == 'Solventación de CRP') {
            // Enlace
            $usuarioActual = $auditoria->Enlace;
            if($etapa == 'Enlace contestando') {
                $pendientes = 51 - $auditoria->estatus[0]->RespuestasCRP;
            } else {
                $pendientes = 0; //$auditoria->estatus[0]->RespuestasPorSolventar - $auditoria->estatus[0]->RespuestaSolv;
            }
        } else if($etapa == 'Revisión auditor') {
            // Auditor
            $usuarioActual = $auditoria->Auditor;
            $pendientes = 51 - $auditoria->estatus[0]->RecomendacionesAuditorCrp;
        } else if ($etapa == 'Revision responsable') {
            // Responsable
            $usuarioActual = $auditoria->Responsable;
            $pendientes = 51 - $auditoria->estatus[0]->RecomendacionesCRP;
        } else {
            $usuarioActual = 'N/A';
            $pendientes = 'N/A';
        }
        $csv->insertOne([
            $nombre,
            $usuarioActual,
            $pendientes,
            $etapa,
            $detalle
        ]);
    }
    $csv->output('reporte_declaraciones.csv');
});

Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');

//Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');
Route::get('/', function(){
   return redirect()->away('http://auditoriaenlineaosaf.mx/');  
});

