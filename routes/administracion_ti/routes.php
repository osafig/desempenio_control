<?php

Route::namespace('AdministracionTi')->prefix('administracion-ti')->group(function() {
    Route::get('/get-users', 'UsersController@getUsers');
    Route::post('/provide-access-to-sdp', 'PrivilegiosController@provideAccessToSdp');
    Route::get('/get-user-roles', 'UsersController@getRoles');
    Route::post('/save-user-rol', 'UsersController@saveRoles');
    Route::post('/get-tipo-usuarios-ti', 'UsersController@getTipoUsuarios');
    Route::post('/get-platform', 'UsersController@getPlatform');
    Route::post('/get-users-privilegios', 'UsersController@getUsersPrivilegios');
    Route::post('/add-user-privilegio', 'UsersController@addUserPrivilegio');
    Route::post('/add-users-privilegio-sdp', 'UsersController@addUsersPrivilegio');
    Route::post('/remove-user-privilegio', 'UsersController@removeUserPrivilegio');
    Route::post('/update-password', 'UsersController@updatePassword');
    Route::post('/delete-user', 'UsersController@deleteUser');
    Route::post('/save-user-data','UsersController@saveUser');
    Route::get('/get-puestos','UsersController@getPuestos');
    Route::post('/get-datos-personales','UsersController@getDatosPersonales');
    Route::post('/get-configuracion', 'SidepatController@getConfiguracion');
    Route::post('/save-configuracion', 'SidepatController@saveConfiguracion');
    Route::post('/get-tipo-usuarios', 'SidepatController@getTipoUsuarios');
    Route::post('/actualizar-tipo-trabajador', 'SidepatController@actualizarTipoTrabajador');
    Route::post('/cambiar-ejericio-activo', 'SidepatController@cambiarEjercicioActivo');
    Route::post('/validar-password', 'SidepatController@validarPassword');
    Route::post('/apertura-ejericio', 'SidepatController@aperturaEjericio');
});
